(*
 * Part of Isamdae: a structural analysis method for multimode DAE system, inspired by J. Pryce's
 * Sigma method.
 *
 * (c) Benoit Caillaud, Inria, <benoit.caillaud@inria.fr>, 2007, 2016, 2019-2022.
 *
 * This software is licensed under the Free Software Licensing
 * Agreement CeCILL:
 *
 * http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.html
 *
 *)

(**
   Extendible arrays.
*)

type 'a t

val length : 'a t -> int64 (* highest defined index + 1, zero otherwise *)

val get_default : 'a t -> 'a (* returns the default value (as stated at creation) *)

val get : 'a t -> int64 -> 'a

val set : 'a t -> int64 -> 'a -> unit

val make : int (* base, > 1 *) -> 'a (* default value *) -> 'a t

val copy : 'a t -> 'a t

val to_array : 'a t -> int (* length *) -> 'a array

(* [ranged_iter f t fst len] *)

val ranged_iter : ('a -> unit) -> 'a t -> int64 -> int64 -> unit

(* iterates on all elements (including 'Blank' ones) within range [0 ... length-1] *)
val iter : ('a -> unit) -> 'a t -> unit

(* iterates on all non-'Blank' elements *)
val sparse_iteri : ('a -> bool) -> (int64 -> 'a -> unit) -> 'a t -> unit

val option_iter : ('a -> unit) -> 'a option t -> unit

(* [rec_fold_left red init t fst lst] *)
(* similar to [Array.fold_left] but with flexarray (in the range [fst ... lst-1]) *)
val rec_fold_left : ('b -> 'a -> 'b) -> 'b -> 'a t -> int64 -> int64 -> 'b

(* [ranged_fold_left red init t fst len] *)
(* similar to [Array.fold_left] but with flexarray (in the range [fst ... fst+len-1] *)
val ranged_fold_left : ('b -> 'a -> 'b) -> 'b -> 'a t -> int64 -> int64 -> 'b

(* similar to [Array.fold_left] but with flexarray *)
val fold_left : ('b -> 'a -> 'b) -> 'b -> 'a t -> 'b

val option_fold_left : ('b -> 'a -> 'b) -> 'b -> 'a option t -> 'b

val rec_fold_right : ('a -> 'b -> 'b) -> 'a t -> 'b -> int64 -> int64 -> 'b

val ranged_fold_right : ('a -> 'b -> 'b) -> 'a t -> 'b -> int64 -> int64 -> 'b

val fold_right : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b

val option_fold_right : ('a -> 'b -> 'b) -> 'a option t -> 'b -> 'b

(* (* [FIXME] missing documentation *)
   (* [NOTE?] unit test ? *)
val self_test :
  int (* p *) -> int (* q *) -> int (* u0 *) -> int (* n *) -> int (* m *) -> int (* b *) -> unit
 *)

