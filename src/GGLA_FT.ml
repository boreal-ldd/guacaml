(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraphGenLA : Generic Adjacency List Graph
 *
 *)

open Extra
module GGLA = GraphGenLA
open GGLA.Type

module Type =
struct
  type vlabel = {
    useful : bool;
    fixed  : bool;
  }

  (* [vlabel = false] iff the vertex is used *)
  type elabel = unit

  type hv = (vlabel, elabel) vertex
  type hg = (vlabel, elabel) graph

  type hgl = (vlabel list, elabel list) graph
end
open Type

let vertex_fixed  (v:hv) : bool =
  v.label.fixed
let vertex_useful (v:hv) : bool =
  v.label.useful

let check (g:hg) =
  GGLA.Check.graph g &&
  GGLA.Utils.graph_is_undirected g &&
  GGLA.Utils.graph_is_reflexive ~refl:false g

module ToS =
struct
  open STools
  open ToS
  open GGLA.ToS

  let vlabel {useful; fixed} =
    SUtils.record [("useful", bool useful); ("fixed", bool fixed)]

  (* [vlabel = false] iff the vertex is used *)
  let elabel = unit

  let hv = vertex vlabel elabel
  let hg = graph vlabel elabel

  let hgl = graph (list vlabel) (list elabel)
end

module ToShiftS =
struct
  open GGLA.ToShiftS
  open STools
  open ShiftS
  open ToShiftS

  let vlabel {useful; fixed} =
    ShiftS.inline (ShiftS.record [("useful", bool useful); ("fixed", bool fixed)])

  (* [vlabel = false] iff the vertex is used *)
  let elabel = unit

  let hv ?(inline=false) v = vertex ~inline vlabel elabel v
  let hg ?(inline=false) g = graph ~inline vlabel elabel g
end

let empty (nv:int) : hg =
  if nv < 0
  then invalid_arg "[GuaCaml.GGLA_FT.empty] negative number of vertices";
  Array.init nv (fun index ->
    {
      index;
      label = {useful = false; fixed = false};
      edges = []
    }
  )

(* [WIP] *)
let set_edges (a:hg) (vl:int list) (edges:(int * unit) list) : unit =
  let label = {useful = true; fixed = false} in
  List.iter (fun index -> a.(index) <- {index; label; edges}) vl

let set_biclique (g:hg) (vl:int list) (edges:int list) : unit =
  set_edges g vl (Tools.map (fun i -> (i, ())) edges)

(* [of_edge_list ~check:true ~symmetries:false nv:int edges:((int * int)list) : hg =
    ~symmetries:true enforces the graph to by undirected (i.e. symmetric)
 *)
let of_edge_list ?(extra_check=true) ?(symmetries=false) ?(fixed=[]) (nv:int) (edges:(int * int)list) : hg =
  let edges : (int * int)list =
    (if symmetries
      then (edges @> (List.rev_map Tools.swap edges))
      else edges
    )
    |> List.sort_uniq Stdlib.compare
  in
  if extra_check then
    List.iter (fun (x, y) ->
      assert(0 <= x && x < nv);
      assert(0 <= y && y < nv);
    ) edges;
  let a : hg = empty nv in
  (* ryl : reversed 'y' list *)
  let update (index:int) (ryl:int list) : unit =
      let edges = List.rev_map (fun y -> (y, ())) ryl in
      assert(Assoc.sorted edges);
      let label = {useful = true; fixed = false} in
      a.(index) <- {index; label; edges}
  in
  (* [TODO] replace by [Assoc.assoc_array_of_assoc_list] *)
  let rec to_graph_rec (carry:int list) (x0:int) : (int * int) list -> unit =
    function
    | [] -> update x0 carry
    | (x, y)::tail -> (
      if x = x0
      then to_graph_rec (y::carry) x0 tail
      else (
        update x0 carry;
        to_graph_rec [y] x tail
      )
    )
  in
  (match edges with [] -> () | (x, y)::tail -> to_graph_rec [y] x tail);
  (* properly sets vertex's [fixed] status *)
  List.iter (fun i ->
    let v = a.(i) in
    a.(i) <- {v with label = {v.label with fixed = true}}
  ) fixed;
  if extra_check then assert(check a);
  a

(* asserts that the KD-graph is tight *)
let of_GraphKD (kd:GraphKD.graph) : hg =
  List.iteri (fun i v -> assert(i = v)) kd.nodes;
  let n = List.length kd.nodes in
  let h = Hashtbl.create (List.length kd.fixed) in
  List.iter (fun i ->
    assert(0 <= i && i < n);
    Hashtbl.add h i ()
  ) kd.fixed;
  let edges = GraphKD.internal_to_GraphLA n kd.cliques in
  Array.mapi (fun index edges ->
    let useful = edges <> [] in
    let fixed = Hashtbl.mem h index in
    let label = {useful; fixed} in
    {index; label; edges = Tools.map (fun i -> (i, ())) edges}
  ) edges

type remove_useless = {
  ru_graph  : hg;
  ru_useful : bool array;
  ru_access : int option array;
  ru_revers : int array;
}

let remove_useless (g:hg) : remove_useless =
  let ru_useful = Array.map vertex_useful g in
  let ru_access, ru_revers = MyArray.true_rename ru_useful in
  let rma = Array.map (Tools.unop_default (-1)) ru_access in
  let ru_graph = GGLA.Utils.graph_rma_vertices rma g in
  {ru_graph; ru_useful; ru_access; ru_revers}

let union (g1:hg) (g2:hg) : hg =
  if Array.length g1 <> Array.length g2
  then invalid_arg "[GuaCaml.GGLA_FT.union] graph must have the same domain";
  let union_vertices (v1:hv) (v2:hv) : hv =
    if v1.index <> v2.index
    then failwith "[GuaCaml.GGLA_FT.union] likely inconsistency in vertex indexes";
    let index = v1.index in
    let label1 = v1.label in
    let label2 = v2.label in
    let label = {
      useful = label1.useful || label2.useful;
      fixed  = label1.fixed  || label2.fixed;
    } in
    let edges = Assoc.update_with v1.edges v2.edges in
    {index; label; edges}
  in
  Array.map2 union_vertices g1 g2

let clique (nv:int) (l:int list) : hg =
  let a : hg = empty nv in
  if l = [] then a else (
    let l = List.sort_uniq Stdlib.compare l in
    set_biclique a l l;
    a
  )

let biclique (nv:int) (l1:int list) (l2:int list) : hg =
  let a : hg = empty nv in
  if l1 = [] || l2 = [] then a else (
    let l1 = List.sort_uniq Stdlib.compare l1 in
    let l2 = List.sort_uniq Stdlib.compare l2 in
    let l'12 = SetList.inter l1 l2 in
    if l'12 = []
    then (
      List.iter
        (fun (vertices, edges) -> set_biclique a vertices edges)
        [(l1, l2); (l2, l1)]
    )
    else (
      let l12 = SetList.union l1 l2 in
      let l'1 = SetList.minus l1 l'12 in
      let l'2 = SetList.minus l2 l'12 in
      List.iter
        (fun (vertices, edges) -> set_biclique a vertices edges)
        [(l'1, l2); (l'2, l1); (l'12, l12)];
    );
    a
  )

let hyperedge (g1:hg) (g2:hg) : hg =
  if Array.length g1 <> Array.length g2
  then invalid_arg "[GuaCaml.GGLA_FT.hyperedge] graph must have the same domain";
  let l1 = MyArray.list_of_indexes (fun v -> v.label.useful) g1 in
  let l2 = MyArray.list_of_indexes (fun v -> v.label.useful) g2 in
  let g12 = union g1 g2 in
  (* merging of fixed vertices is non-trivial *)
  if l1 = [] || l2 = []
  then g12
  else (union (biclique (Array.length g1) l1 l2) g12)

(* [MOVEME] *)
(* [TODO] ToS *)
type alt =
  | Clique of int list
  | Union of alt list
  | HyperEdge of alt list

(* [TODO] *)
let rec flatten_alt (nv:int) : alt -> hg =
  function
  | Clique l ->
    clique nv l
  | Union gl ->
    List.fold_left union     (empty nv) Extra.(gl ||> flatten_alt nv)
  | HyperEdge gl ->
    List.fold_left hyperedge (empty nv) Extra.(gl ||> flatten_alt nv)

let color_of_vertex label : string =
  match label.fixed, label.useful with
  | true, _ -> "black"
  | false, true -> "grey"
  | false, false -> "white"

let to_graphviz_vertex index label : string =
  let open STools.ToS in
  let s_color  = string (color_of_vertex label) in
  String.concat "" ["[color="; s_color; "]"]

let to_graphviz_string ?(astree=true) ?(name="") (hg:hg) : string =
  let cv = to_graphviz_vertex in
  if astree
  then (GGLA_Graphviz.ToASTree.graph_to_string ~directed:false ~name ~cv hg)
  else (GGLA_Graphviz.ToS.graph_to_string ~directed:false ~name ~cv hg)

let to_graphviz_file ?(astree=true) ?(name="") (target:string) (hg:hg) : unit =
  if astree
  then (
    let cv = to_graphviz_vertex in
    GGLA_Graphviz.ToASTree.graph_to_file ~directed:false ~name ~cv target hg
  )
  else (STools.SUtils.output_string_to_file target (to_graphviz_string ~astree ~name hg))

(*
(* [FIXME] *)
(* returns an H-FT-Graph matching the KD-graph given as input
 * vertices apearing in the input KD-graph and labelged as fixed are
 * effectively labelged as fixed (i.e. fixed vertices which are not used
 * are not added)
 * ~remove_useless:false (by default) [kd.nodes] is used as graph's domain
 * ~remove_useless:true [support kd] is used as graph's domain
 *)
let of_GraphKD ?(verbose=0) ?(remove_useless=false) (kd:GraphKD.graph) : hg =
  assert(GraphKD.check kd);
  (* renaming vertices to induce a tight namespace *)
  let profile (text:string) =
    if verbose > 0
    then OProfile.(profile default) ("[GuaCaml.GGLA_HFT.of_GraphKD] "^text)
    else (fun f -> f)
  in
  let nodes = if remove_useless then GraphKD.support kd else kd.nodes in
  let n = List.length nodes in
  let h = Hashtbl.create n in
  let r = Hashtbl.create n in
  List.iteri (fun i v ->
    Hashtbl.add h v i;
    Hashtbl.add r i v;
  ) nodes;
  let nodes = MyList.init n (fun i -> i) in
  let fixed : int list = MyList.opmap (Hashtbl.find_opt h) kd.fixed in
  let cliques = kd.cliques ||> (fun k -> k ||> Hashtbl.find h) in
  let kd : GraphKD.graph = {nodes; fixed; cliques} in
  assert(GraphKD.check kd);
  kd
  |> profile "GGLA_FT.of_GraphKD" GGLA_FT.of_GraphKD
  |> profile "of_FT" of_FT
  |> GGLA.Utils.graph_map_vlabel
    (fun label ->
      {label with vlabel_names = Tools.map (Hashtbl.find r) label.vlabel_names}
    )
*)

