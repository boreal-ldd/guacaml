open UnionFind
open OUnit

let test_init () =
  let uf = init 10 in
  assert (uf = [|0; 1; 2; 3; 4; 5; 6; 7; 8; 9|])

let _ = push_test "test_init" test_init

let test_find_union () =
  let uf = init 10 in
  union uf 0 1;
  union uf 1 2;
  union uf 3 4;
  union uf 5 6;
  union uf 5 7;
  union uf 5 8;
  union uf 5 9;
  assert (find uf 0 = find uf 1);
  assert (find uf 0 = find uf 2);
  assert (find uf 3 = find uf 4);
  assert (find uf 5 = find uf 6);
  assert (find uf 5 = find uf 7);
  assert (find uf 5 = find uf 8);
  assert (find uf 5 = find uf 9);
  assert (find uf 0 <> find uf 3);
  assert (find uf 0 <> find uf 5);
  assert (find uf 3 <> find uf 5)

let _ = push_test "test_find_union" test_find_union

let test_normalized () =
  let uf = init 10 in
  union uf 0 1;
  union uf 1 2;
  union uf 3 4;
  union uf 5 6;
  union uf 5 7;
  union uf 5 8;
  union uf 5 9;
  (* assert (normalized uf = false); *)
  normalize uf;
  assert (normalized uf = true)

let _ = push_test "test_normalized" test_normalized

(* This test creates a union-find table with 6 elements and performs the following unions:
 * - 0 and 1
 * - 2 and 3
 * - 4 and 5
 * [count uf] should return :
 * - 3 as the number of components, and,
 * - the array [|0; 0; 1; 1; 2; 2|] as the remapped table, where each element represents
 *   the component it belongs to.
 *)
let test_count () =
  let uf = init 6 in
  union uf 0 1;
  union uf 2 3;
  union uf 4 5;
  assert (count uf = 3)

let _ = push_test "test_count" test_count

(* This test creates a union-find table with 6 elements and performs the following unions:
 * - 0 and 1
 * - 2 and 3
 * - 4 and 5
 * [remap uf] should return :
 * - 3 as the number of components, and,
 * - the array [|0; 0; 1; 1; 2; 2|] as the remapped table, where each element represents
 *   the component it belongs to.
 *)
let test_remap () =
  let uf = init 6 in
  union uf 0 1;
  union uf 2 3;
  union uf 4 5;
  let ncomp, remap = remap uf in
  assert (ncomp = 3);
  assert (remap = [|0; 0; 1; 1; 2; 2|])

let _ = push_test "test_remap" test_remap

let _ = push_test "remap empty"
    (fun () ->
       let uf = init 0 in
       error_assert
        "remap uf = (0, [||])"
        (remap uf = (0, [||]))
    )

let _ = push_test "remap empty"
    (fun () ->
       let uf = init 0 in
       let n, remap = remap uf in
       error_assert
        "remap uf = (0, [||])"
        (n = 0 && remap = [||]))

let _ = push_test "remap singleton"
    (fun () ->
       let uf = init 1 in
       let n, remap = remap uf in
       error_assert
        "n = 1 && remap = [|0|]"
        (n = 1 && remap = [|0|]))

let _ = push_test "remap singleton (un-classed)"
    (fun () ->
       let uf = init 1 in
       uf.(0) <- (-1);
       let n, remap = remap uf in
       error_assert
        "n = 0 && remap = [|-1|]"
        (n = 0 && remap = [|-1|]))

let _ = push_test "remap 2 nodes, singleton components"
    (fun () ->
       let uf = init 2 in
       let n, remap = remap uf in
       error_assert
        "n = 2 && remap = [|0; 1|]"
        (n = 2 && remap = [|0; 1|]))

let _ = push_test "remap 2 nodes, same component"
    (fun () ->
       let uf = init 2 in
       union uf 0 1;
       let n, remap = remap uf in
       error_assert
        "n = 1 && remap = [|0; 0|]"
        (n = 1 && remap = [|0; 0|]))

let _ = push_test "remap 2 nodes, same component (un-classed)"
    (fun () ->
       let uf = init 2 in
       union uf 0 1;
       uf.(1) <- (-1);
       let n, remap = remap uf in
       error_assert
        "n = 1 && remap = [|0; -1|]"
        (n = 1 && remap = [|0; -1|]))

let _ = push_test "remap 3 nodes, 3 singleton components"
    (fun () ->
       let uf = init 3 in
       let n, remap = remap uf in
       error_assert
        "n = 3 && remap = [|0; 1; 2|]"
        (n = 3 && remap = [|0; 1; 2|]))

let _ = push_test "remap empty"
    (fun () ->
       let uf = init 0 in
       let n, remap = remap uf in
       error_assert
        "remap uf = (0, [||])"
        (n = 0 && remap = [||]))

let _ = push_test "remap all unclassed"
    (fun () ->
       let uf = Array.init 20 (fun i -> -1) in
       let n, remap = remap uf in
       error_assert
        "remap uf = (0, [-1; -1; ...])"
        (n = 0 && Array.for_all (fun x -> x = -1) remap))

let _ = push_test "remap all classed"
    (fun () ->
       let uf = Array.init 20 (fun i -> i) in
       let n, remap = remap uf in
       error_assert
        "remap uf = (20, [0; 1; ...])"
        (n = 20 && MyArray.for_alli (fun i x -> x = i) remap))

let _ = push_test "remap unclassed and classed"
    (fun () ->
       let uf = Array.init 20 (fun i -> if i mod 2 = 0 then i else -1) in
       print_endline ("uf:"^(STools.ToS.(array int) uf));
       print_endline ("count:"^(STools.ToS.int (UnionFind.count uf)));
       let n, remap = remap uf in
       print_endline ("uf:"^(STools.ToS.(array int) uf));
       error_assert
        "remap uf = (10, [0; -1; ...])"
        (n = 10 && remap = [|0; -1; 1; -1; 2; -1; 3; -1; 4; -1; 5; -1; 6; -1; 7; -1; 8; -1; 9; -1|])
        (* (n = 10 && MyArray.for_alli (fun i x -> x = (if i mod 2 = 0 then (i/2) else -1)) remap) *)
    )

let _ = push_test "remap non-trivial"
    (fun () ->
       let uf = init 10 in
       (*
       for i = 0 to 9 do
         for j = i+1 to 9 do
           if i mod 3 = j mod 3
           then (
            print_endline STools.ToS.("union uf "^(int i)^" "^(int j));
            union uf i j
          )
         done
       done;
       *)
       union uf 0 6;
       union uf 0 9;
       union uf 1 4;
       union uf 1 7;
       union uf 2 5;
       union uf 2 8;
       union uf 3 6;
       union uf 3 9;
       union uf 4 7;
       union uf 5 8;
       union uf 6 9;
       print_endline ("uf:"^(STools.ToS.(array int) uf));
       print_endline ("count:"^(STools.ToS.int (UnionFind.count uf)));
       let n, remap = remap uf in
       print_endline ("remap:"^(STools.ToS.(array int) remap));
       error_assert
        "remap uf = (3, [|0; 1; 2; 0; 1; 2; 0; 1; 2; 0|])"
        (n = 3 && remap = [|0; 1; 2; 0; 1; 2; 0; 1; 2; 0|])
    )

let _ = push_test "remap 3 nodes, 3 components"
    (fun () ->
       let uf = init 3 in
       union uf 0 1;
       union uf 1 2;
       let n, remap = remap uf in
       error_assert
        "n = 1 && remap = [|0; 0; 0|]"
        (n = 1 && remap = [|0; 0; 0|])
    )

let _ = push_test "remap 3 nodes, 2 components"
    (fun () ->
       let uf = init 3 in
       union uf 0 1;
       let n, remap = remap uf in
       error_assert
          "n = 2 && remap = [|0; 0; 1|]"
          (n = 2 && remap = [|0; 0; 1|])
    )

let _ = push_test "remap 3 nodes, 2 components (un-classed)"
    (fun () ->
       let uf = init 3 in
       union uf 0 1;
       uf.(2) <- (-1);
       let n, remap = remap uf in
       error_assert
          "n = 1 && remap = [|0; 0; -1|]"
          (n = 1 && remap = [|0; 0; -1|])
    )

let _ = push_test "remap 3 nodes, 3 singleton components"
    (fun () ->
       let uf = init 3 in
       let n, remap = remap uf in
       assert(n = 3 && remap = [|0; 1; 2|])
    )

let _ = push_test "remap 4 nodes, 4 singleton components"
    (fun () ->
       let uf = init 4 in
       let n, remap = remap uf in
       assert(n = 4 && remap = [|0; 1; 2; 3|])
  )

let _ = push_test "remap 4 nodes, 3 components"
    (fun () ->
       let uf = init 4 in
       union uf 0 1;
       union uf 1 2;
       let n, remap = remap uf in
       assert(n = 2 && remap = [|0; 0; 0; 1|])
  )

let _ = push_test "remap 4 nodes, 3 components (un-classed)"
    (fun () ->
       let uf = init 4 in
       union uf 0 1;
       union uf 1 2;
       uf.(3) <- (-1);
       let n, remap = remap uf in
       assert(n = 1 && remap = [|0; 0; 0; -1|])
  )

let _ = push_test "error 2022/12/27"
    (fun () ->
      let uf = [|0; 1; 2; 1; 4; 1; 1; 1; 1; 1; 1; 1; 12; 13; 14; 15; 16; 17|] in
      assert(normalized uf);
      print_endline ("count:"^(STools.ToS.int (UnionFind.count uf)));
      let n, remap = remap uf in
      assert(n = 10 && remap = [|0; 1; 2; 1; 3; 1; 1; 1; 1; 1; 1; 1; 4; 5; 6; 7; 8; 9|]);
      let remaped, components, unclassed = ends uf in
      assert(remaped = [|0; 1; 2; 1; 3; 1; 1; 1; 1; 1; 1; 1; 4; 5; 6; 7; 8; 9|]);
      assert(components = [|[0]; [1; 3; 5; 6; 7; 8; 9; 10; 11]; [2]; [4]; [12]; [13]; [14]; [15]; [16]; [17]|]);
      assert(unclassed = []);
      ()
    )

let _ = run_tests()
