(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * OpalParaLex : Opal-based Parenthesis Lexer
 *
 * === NOTE ===
 *
 * In usual languages such as C, OCaml, python, ..., there exists several notions
 * of sub-component. Such sub-components may be delimited by a parenthesis like
 * pattern. OpalParaLex is specifically designed to parse such patterns.
 *)

open Opal

(* [FIXME] add documentation *)
module type MSig =
sig
  type token
  type bracket

  val ignore  : token -> bool
  val bracket : token -> (token -> bracket option) option
  val failure : token -> string option
end

(* [FIXME] add documentation *)
module Make(M0:MSig) =
struct
  module M = M0
  open M

  type token_tree = (M.bracket, M.token) Tree.atree

  let rec lexer_rec (carry:token_tree list) (stop:(token -> bracket option)) : (token, bracket * (token_tree list)) t =
    fun stream -> (
      let* t = any in
      if ignore t then lexer_rec carry stop else
      match stop t with
      | Some bracket -> return (bracket, List.rev carry)
      | None ->
      match bracket t with
      | Some stop' -> (
        let* anot, node = lexer_rec [] stop' in
        lexer_rec ((Tree.ANode (anot, node))::carry) stop
      )
      | None ->
      match failure t with
      | Some text -> failwith ("[GuaCaml.ParaLex] "^text)
      | None ->
        lexer_rec ((Tree.ALeaf t)::carry) stop
    ) stream

  let rec lexer : (token, token_tree) t =
    fun stream -> (
      let* t = any in
      if ignore t then lexer else
      match bracket t with
      | Some stop' -> (
        let* anot, node = lexer_rec [] stop' in
        return (Tree.ANode (anot, node))
      )
      | None ->
      match failure t with
      | Some text -> failwith ("[GuaCaml.ParaLex] "^text)
      | None ->
        return (Tree.ALeaf t)
    ) stream
end

(* (* Example of a ParaLex instanciation for the C language *)
module ParaLex_C =
struct
  module Model =
  struct
    type token = AtomicLexer.token
    type bracket = string

    open AtomicLexer
    let ignore =
      function
      | Comment _
      | White _ -> true
      | _ -> false

    let bracket =
      function
      | Symbol "(" -> Some(function Symbol ")" -> Some "( )" | _ -> None)
      | Symbol "{" -> Some(function Symbol "}" -> Some "{ }" | _ -> None)
      | Symbol "[" -> Some(function Symbol "]" -> Some "[ ]" | _ -> None)
      | _ -> None

    let failure =
      function
      | Symbol ")" -> Some "closed bracket ')' with prior opening with '('"
      | Symbol "}" -> Some "closed bracket '}' with prior opening with '{'"
      | Symbol "]" -> Some "closed bracket ')' with prior opening with '['"
      | _ -> None
  end
  include Model

  module ToPrettyS =
  struct
    open STools.ToS
    open AtomicLexer.ToS

    let token_tree = TreeUtils.ToPrettyS.atree string token
  end

  module Module = ParaLex_Make(Model)
  include Module
end
*)
