module GGLA = GraphGenLA
open GGLA.Type
open Examples_graphs
open LexBFS
open OUnit

let gen_test_undirected graph () : unit =
  error_assert "graph_is_undirected" (GGLA.Utils.graph_is_undirected graph)

let _ =
  Array.iter (fun (name, graph) ->
    push_test ("["^name^"] test_undirected") (gen_test_undirected graph)
  ) graph_unit_unit

let gen_test_lexbfs1 graph () : unit =
  let vl = LexBFS.lexbfs1 graph in
  print_endline ("vl:"^(STools.ToS.(list int) vl));
  let va = Array.of_list vl in
  error_assert "is_lexbfs lexbfs1" (GGLA_LexBFS.Check.is_lexbfs graph va)

let _ = gen_test_lexbfs1 graph_5_4 ()

let _ =
  Array.iter (fun (name, graph) ->
    push_test ("["^name^"] test_lexbfs1") (gen_test_lexbfs1 graph)
  ) graph_unit_unit

let gen_test_lexbfs2 graph () : unit =
  let vl =
    try LexBFS.lexbfs2 graph
    with Not_found -> failwith "[Not_found] in LexBFS.lexbfs2"
  in
  let va = Array.of_list vl in
  error_assert "is_lexbfs lexbfs2" (GGLA_LexBFS.Check.is_lexbfs graph va)

let _ =
  Array.iter (fun (name, graph) ->
    push_test ("["^name^"] test_lexbfs2") (gen_test_lexbfs2 graph)
  ) graph_unit_unit

let _ = run_tests ()

