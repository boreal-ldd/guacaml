(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_PACE2017_Core : Bare Syntax for PACE 2017 Challenge (written in Opal)
 *
 *)

module Types =
struct
  type primitive = string * int * int * int
    (* (mode, nbag, tw', nbv) where :
     *  mode = "td" but can be replaced ed by "td", "td-2e", "td-lex" or other defined variant
     *  tw' = tw+1
     *  nbv : number of vertices in the underlying graph
     *)
  type bag  = int list
  type edge = int * int

  type pace2017_output_line =
    | Comment   of string
    | Primitive of primitive
    | Bag       of bag
    | Edge      of edge

  type pace2017_output = pace2017_output_line list
end
open Types

module ToS =
struct
  open STools
  open ToS

  let primitive = quad string int int int
  let bag = list int
  let edge = int * int

  let pace2017_output_line = function
    | Comment   s -> constructor "Comment"   string    s
    | Primitive p -> constructor "Primitive" primitive p
    | Bag       b -> constructor "Bag"       bag       b
    | Edge      e -> constructor "Edge"      edge      e

  let pace2017_output = list pace2017_output_line
end

module ToPrettyS =
struct
  open STools
  open ToS

  let comment s = "c"^s

  let primitive ((mode, nb, tw, nv):primitive) : string =
    String.concat " " ["p"; mode; int nb; int tw; int nv]

  let bag (b:bag) =
    String.concat " " ("b"::(Tools.map int b))

  let edge (x, y) =
    SUtils.catmap " " ToS.int [x; y]

  let pace2017_output_line = function
    | Comment   c -> comment   c
    | Primitive p -> primitive p
    | Bag       b -> bag       b
    | Edge      e -> edge      e

  let pace2017_output d =
    (SUtils.catmap "\n" pace2017_output_line d)^"\n"
end
