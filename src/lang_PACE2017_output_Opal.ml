(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_PACE2017_output_Opal : Bare Syntax Parser for PACE 2017 Challenge (written in Opal)
 *
 *)

open Lang_PACE2017_output_Core
open Types
open Utils
open Opal
open CharStream

let bag : (char, Types.bag) Opal.t =
  exactly 'b' >> until_stop (spaces >> nat) (exactly '\n') => fst

let edge : (char, Types.edge) Opal.t =
  spaces >> nat $* (spaces >> nat << skip_until_newline)

let comment : (char, string) Opal.t =
  exactly 'c' >> (until_stop any (exactly '\n') => (fun (cl, _) -> implode cl))

let primitive : (char, string * int * int * int) Opal.t =
  let* _ = exactly 's' in
  let* mode = spaces >> word in
  let* nb = spaces >> nat in
  let* tw = spaces >> nat in
  let* nv = spaces >> nat in
  let* _ = skip_until_newline in
  return (mode, nb, tw, nv)

let pace2017_output_line : (char, pace2017_output_line) Opal.t =
  (comment => (fun s -> Comment s)) <|>
  (primitive => (fun (mode, nb, tw, nv) -> Primitive(mode, nb, tw, nv))) <|>
  (bag => (fun b -> Bag b)) <|>
  (edge => (fun e -> Edge e))

let pace2017_output : (char, pace2017_output) Opal.t =
  until pace2017_output_line

let from_file (target:string) : pace2017_output =
  match parse_from_file pace2017_output target with
  | Some some -> some
  | None -> failwith "[GuaCaml.Lang_PACE2017_output_Opal.from_file] uncaught parsing error"
