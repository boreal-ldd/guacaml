(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * DoubleLinkedList : Implement standard Doubly-Linked List
 *
 * === NOTE ===
 *
 * 1. [Circular Doubly Linked List (Wikipedia)](https://en.wikipedia.org/wiki/Doubly_linked_list#Circular_doubly_linked_lists)
 * 2. partially replicated from [GuaCaml/DoubleLinkedList]
 *
 *)

(* [WARNING] avoid direct access to the internal structure *)

module DLL = DoubleLinkedList

module CDLL :
sig
  type 'a t = 'a DoubleLinkedList.t = {
    mutable prev : 'a o;
    mutable succ : 'a o;
    mutable item : 'a;
  }
  and 'a o = 'a t option
  val internal_check_items_opt : ?lst:'a o option -> 'a list -> 'a o -> bool
  val internal_check_structure_forward_opt :
    ?verbose:bool -> 'a o -> ('a o * 'a o) list -> 'a o -> bool
  val internal_to_seq_opt : 'a o -> 'a o -> 'a t Seq.t
  val get_item : 'a t -> 'a
  val set_item : 'a t -> 'a -> unit
  val get_succ_opt : 'a t -> 'a o
  val get_prev_opt : 'a t -> 'a o
  val internal_set_oprev_osucc : 'a o -> 'a o -> unit
  val internal_set_oprev_succ : 'a o -> 'a t -> unit
  val internal_set_prev_osucc : 'a t -> 'a o -> unit
  val internal_set_prev_succ : 'a t -> 'a t -> unit
  val create_before : 'a t -> 'a -> 'a t
  val create_after : 'a t -> 'a -> 'a t
  val remove : 'a t -> unit
  val nth_opt : 'a o -> int -> 'a option
  val nth : 'a t -> int -> 'a option
  val check : 'a t -> bool
  val check_opt : 'a o -> bool
  val check_items_opt : 'a list -> 'a o -> bool
  val check_items : 'a list -> 'a t -> bool
  val check_structure_forward_opt :
    ?verbose:bool -> ('a o * 'a o) list -> 'a o -> bool
  val check_structure_forward :
    ?verbose:bool -> ('a o * 'a o) list -> 'a t -> bool
  val create : 'a -> 'a DLL.t
  val create_before_opt : 'a o -> 'a -> 'a t
  val create_after_opt : 'a o -> 'a -> 'a t
  val to_seq_opt : 'a o -> 'a t Seq.t
  val to_seq : 'a t -> 'a t Seq.t
  val to_item_seq : 'a t -> 'a Seq.t
  val to_item_seq_opt : 'a o -> 'a Seq.t
  val create_after_item_seq : 'a t -> 'a Seq.t -> 'a t
  val create_before_item_seq : 'a t -> 'a Seq.t -> 'a t
  val of_item_seq : 'a Seq.t -> 'a o
  val to_list : 'a t -> 'a list
  val to_list_opt : 'a o -> 'a list
  val of_list : 'a list -> 'a o
  val iter_opt : ('a -> unit) -> 'a o -> unit
  val iter : ('a -> unit) -> 'a t -> unit
end

(* Circular Double Linked List attached with a Set structure (in this case,
 * an HashSet), allows to make ordered set with fast insertion, deletion, and,
 * access
 *)
module HashSet :
sig
  type 'k ht = {
    access : ('k, 'k CDLL.t) Hashtbl.t;
    mutable first : 'k CDLL.o;
  }
  type 'k t = 'k ht

  val mem : 'k ht -> 'k -> bool

  val create : ?hsize:int -> unit -> _ ht

  val iter : ('k -> unit) -> 'k ht -> unit

  val to_list : 'k ht -> 'k list

  val nth : 'k ht -> int -> 'k

  val length : 'k ht -> int

  val to_string : ('k -> string) -> 'k ht -> string

  val get : 'k ht -> 'k -> (unit -> 'k CDLL.t) -> 'k CDLL.t

  (* [insert_after t x y] inserts [y] after [x] *)
  val insert_after : 'k ht -> 'k -> 'k -> unit

  (* [insert_before t x y] inserts [y] before [x] *)
  val insert_before : 'k ht -> 'k -> 'k -> unit

  val append : 'k ht -> 'k -> unit

  val of_list : ?hsize:int -> 'k list -> 'k ht

  (* Remove x from the sequence.
   * If [x == first]
   *   If [first.next == first]
   *   then [first := None]
   *   Else [first := first.next]
   *)
  val remove : 'k ht -> 'k -> unit

  (* Find the previous element in the sequence. *)
  val get_prev : 'k ht -> 'k -> 'k

  (* Find the next element in the sequence. *)
  val get_succ : 'k ht -> 'k -> 'k

  val get_first : 'k ht -> 'k

  val get_first_opt : 'k ht -> 'k option
end

(* Circular Double Linked List attached with a Map structure (in this case,
 * an HashMap), allows to make ordered set with fast insertion, deletion, and,
 * access, even on elements which are not easily hashable (e.g. mutable elements).
 *)
module HashKeySet :
sig
  type ('k, 'v) ht = {
    key : 'v -> 'k;
    access : ('k, 'v) Hashtbl.t;
    order : 'k HashSet.ht;
  }
  type ('k, 'v) t = ('k, 'v) ht

  val mem : ('k, 'v) ht -> 'v -> bool

  val create : ?hsize:int -> ('v -> 'k) -> ('k, 'v) ht

  val item_of_key : ('k, 'v) ht -> 'k -> 'v

  (* Iterate through the objects in the sequence.
   * May give unpredictable results if sequence changes mid-iteration.
   * Terminate iff the underlying linked-list is indeed a perfect loop
   *)
  val iter : ('k -> 'v -> unit) -> ('k, 'v) ht -> unit

  val to_list : ('k, 'v) ht -> 'v list

  val nth : ('k, 'v) ht -> int -> 'v

  val length : ('k, 'v) ht -> int

  val to_string : ('v -> string) -> ('k, 'v) ht -> string

  (* val get : ('k, 'v) ht -> 'k -> (unit -> 'k CDLL.t) -> 'k CDLL.t *)

  (* [insert_after t x y] inserts [y] after [x] *)
  val insert_after : ('k, 'v) ht -> 'v -> 'v -> unit

  (* [insert_before t x y] inserts [y] before [x] *)
  val insert_before : ('k, 'v) ht -> 'v -> 'v -> unit

  val append : ('k, 'v) ht -> 'v -> unit

  val of_list : ?hsize:int -> ('v -> 'k) -> 'v list -> ('k, 'v) ht

  (* Remove x from the sequence.
   * If [x == first]
   *   If [first.next == first]
   *   then [first := None]
   *   Else [first := first.next]
   *)
  val remove : ('k, 'v) ht -> 'v -> unit

  (* Find the previous element in the sequence. *)
  val get_prev : ('k, 'v) ht -> 'v -> 'v

  (* Find the next element in the sequence. *)
  val get_succ : ('k, 'v) ht -> 'v -> 'v

  val get_first : ('k, 'v) ht -> 'v

  val get_first_opt : ('k, 'v) ht -> 'v option
end
