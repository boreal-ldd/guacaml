(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Opal_Dimacs : Bare Syntax C Parser (written in Opal)
 *
 * === NOTE ===
 *
 * Look at the BNF of C : https://cs.wmich.edu/~gupta/teaching/cs4850/sumII06/The%20syntax%20of%20C%20in%20Backus-Naur%20form.htm
 *
 *)

open Extra
module Types = Lang_C_Types
open Types
(* module ToPrettyS = Lang_C_ToPrettyS *)
module ToS = Lang_C_ToS
open Opal

(* Section. Atomic Lexer *)

(* we split the [char] stream into [token] stream with the following rules :
 * - Word    => contiguous sequence of alpha-numerical characters + '_' are assembled
 *   + Keyword (* [TODO] *)
 *   + Ident   (* [TODO] *)
 * - Comment => both comments '/* ... */' and '// ... \n' are assembled
 * - String  => we assemble <string> atoms
 * - Symbol  => we assemble symbolic tokens
 * - White   => white characters are isolated
 * - Other   => every other character is left as is
 *)

module AtomicLexer =
struct
  (* Symbols *)

  let list_of_symbols = [
    "#";
    ":"; ";";
    "."; "...";
    "("; ")";
    "["; "]";
    "{"; "}";
    ",";
    "+"; "++"; "+=";
    "-"; "--"; "-=";
    "<"; "<="; "<<"; "<<=";
    ">"; ">="; ">>"; ">>=";
    "*"; "*=";
    "&"; "&&"; "&=";
    "|"; "||"; "|=";
    "^"; "^=";
    "%"; "%=";
    "!"; "!=";
    "~"; "~=";
    "="; "==";
  ]

  (* [TODO] generate automatically *)
  let symbol : (char, string) t =
    let* c = any in
    match c with
    | '#' -> return "#"
    | ':' -> return ":"
    | ';' -> return ";"
    | '.' -> (
      (exactly '.' >> exactly '.' >> return "...") <|>
       return "."
    )
    | '(' -> return "("
    | ')' -> return ")"
    | '[' -> return "["
    | ']' -> return "]"
    | '{' -> return "{"
    | '}' -> return "}"
    | ',' -> return ","
    | '+' -> (
      (exactly '+' >> return "++") <|>
      (exactly '=' >> return "+=") <|>
       return "+"
    )
    | '-' -> (
      (exactly '-' >> return "--") <|>
      (exactly '>' >> return "->") <|>
      (exactly '=' >> return "-=") <|>
       return "-"
    )
    | '<' -> (
      (exactly '<' >> exactly '<' >> exactly '=' >> return "<<=") <|>
      (exactly '<' >> return "<<") <|>
      (exactly '=' >> return "<=") <|>
       return "<"
    )
    | '>' -> (
      (exactly '>' >> exactly '>' >> exactly '=' >> return ">>=") <|>
      (exactly '>' >> return ">>") <|>
      (exactly '=' >> return ">=") <|>
       return ">"
    )
    | '*' -> (
      (exactly '=' >> return "*=") <|>
       return "*"
    )
    | '&' -> (
      (exactly '&' >> return "&&") <|>
      (exactly '=' >> return "&=") <|>
       return "&"
    )
    | '|' -> (
      (exactly '|' >> return "||") <|>
      (exactly '=' >> return "|=") <|>
       return "|"
    )
    | '^' -> (
      (exactly '=' >> return "^=") <|>
       return "^"
    )
    | '%' -> (
      (exactly '=' >> return "%=") <|>
       return "%"
    )
    | '!' -> (
      (exactly '=' >> return "!=") <|>
       return "!"
    )
    | '~' -> (
      (exactly '=' >> return "~=") <|>
       return "~"
    )
    | '=' -> (
      (exactly '=' >> return "==") <|>
       return "="
    )
    | _ -> mzero

  (* Comments *)
  let comment_EndOfLine : (char, string) t =
    let* _ = exactly '/' in
    let* _ = exactly '/' in
    must
      (fun () -> failwith "reached end of file without ending comment '//'")
      (
        let* cl = (until_stop any (exactly '\n' >> return ['n'] <|> eof [])) => (fun (a, b) -> a @> b) in
        return (implode ('/'::'/'::cl))
      )

  let comment_StarBracket : (char, string) t =
    let* _ = exactly '/' in
    let* _ = exactly '*' in
    must
      (fun () -> failwith "started comment with '/*', reached end of file without closure")
      (
        let* cl = ((until_stop any (exactly '*' $:: (exactly '/' >> return ['/']))) => (fun (a, b) -> a @> b)) in
        return (implode ('/'::'*'::cl))
      )

  let comment : (char, string) t =
    comment_EndOfLine <|> comment_StarBracket

  (* Keywords *)
  (* [TODO?] use an hashtable instead ? *)
  let list_of_keywords = [
    "_Pragma";
    "define";
    "auto";
    "register";
    "return";
    "static";
    "extern";
    "typedef";
    "white";
    "switch";
    "for";
    "do";
    "if";
    "else";
    "case";
    "break";
    "continue";
    "default";
    "void";
    "_Bool";
    "_Bool2";
    "char";
    "short";
    "int";
    "long";
    "float";
    "double";
    "signed";
    "unsigned";
  ]

  let keywords = one_of list_of_keywords

  (* Atomic Values *)

  type value =
    | String of string
    | Char   of char
    | Int    of int

  let value : (char, value) t =
    ( CharStream.string => (fun s -> String s)) <|>
    ( CharStream.char   => (fun c -> Char   c)) <|>
    ( CharStream.int    => (fun i -> Int    i))

  let is_keyword : string -> bool =
    fun s -> List.mem s list_of_keywords

  (* [is_ident : string -> bool] returns [true] iff
   * the input string starts with an 'alpha_' character followed by 'alpha_num_' character
   *)
  let is_ident : string -> bool =
    CharStream.(recognize_string (alpha_ >> skip_many alpha_num_ >> eof()))

  (* Atomic Lexer *)

  type token =
    | Comment of string (* comments *)
    | Symbol  of string (* registered <symbol>s *)
    | Value   of value  (* special values : integers, characters, strings, ... *)
    | Keyword of string (* registered <keyword>s *)
    | Ident   of string (* identifier *)
    | Word    of string (* any other words *)
    | White   of char   (* white character *)
    | Other   of char   (* any other character *)

  module ToS =
  struct
    open STools
    open ToS

    let value =
      function
      | String s -> "String "^(string s)
      | Char   c -> "Char "^(char c)
      | Int    i -> "Int "^(int i)

    let token =
      function
      | Comment c -> "Comment "^(string c)
      | Symbol  s -> "Symbol "^(string s)
      | Value   v -> "Value "^(value v)
      | Keyword k -> "Keyword "^(string k)
      | Ident   i -> "Ident "^(string i)
      | Word    w -> "Word "^(string w)
      | White   w -> "White "^(char w)
      | Other   o -> "Other "^(char o)
  end

  let lexer : (char, token) t =
    (comment => (fun s -> Comment s)) <|>
    (symbol  => (fun s -> Symbol  s)) <|>
    (value   => (fun v -> Value   v)) <|>
    (CharStream.word_ =>
      (fun w ->
             if is_keyword w
        then Keyword w
        else if is_ident w
        then Ident   w
        else (
          print_endline ("unexpected word:"^(STools.ToS.string w));
          Word    w
        )
      )
    ) <|>
    (CharStream.white => (fun c -> White c)) <|>
    (any     => (fun c -> print_endline ("unexpected character: "^(STools.ToS.char c)); Other c))
end

module ParaLex =
struct
  module Model : OpalParaLex.MSig
  with type token   = AtomicLexer.token
  and  type bracket = string
  =
  struct
    type token   = AtomicLexer.token
    type bracket = string

    open AtomicLexer
    let ignore =
      function
      | Comment _
      | White _ -> true
      | _ -> false

    let bracket =
      function
      | Symbol "(" -> Some(function Symbol ")" -> Some "( )" | _ -> None)
      | Symbol "{" -> Some(function Symbol "}" -> Some "{ }" | _ -> None)
      | Symbol "[" -> Some(function Symbol "]" -> Some "[ ]" | _ -> None)
      | _ -> None

    let failure =
      function
      | Symbol ")" -> Some "closed bracket ')' with prior opening with '('"
      | Symbol "}" -> Some "closed bracket '}' with prior opening with '{'"
      | Symbol "]" -> Some "closed bracket ')' with prior opening with '['"
      | _ -> None
  end
  include Model

  module ToPrettyS =
  struct
    open STools.ToS
    open AtomicLexer.ToS

    let token_tree = TreeUtils.ToPrettyS.atree string token
  end

  module Module = OpalParaLex.Make(Model)
  include Module
end

module Priority =
struct
  (* INFIX OPERATORS *)
  (* NB : all infix operators at the same level of priority have left to right associativity *)

  open Lang_C_Types

  let op_logic_infix : _ -> int =
    function
    | OLI_LazyAnd -> 110
    | OLI_BinAnd  -> 80
    | OLI_LazyOr  -> 120
    | OLI_BinOr   -> 100
    | OLI_Xor     -> 90

  let op_arith_infix : _ -> int =
    function
    | OAI_Plus       -> 40
    | OAI_Minus      -> 40
    | OAI_Mult       -> 30
    | OAI_Div        -> 30
    | OAI_Rem        -> 30
    | OAI_LeftShift  -> 50
    | OAI_RightShift -> 50

  let op_compare : _ -> int =
    function
    | OC_Eq  (* == *) -> 70
    | OC_Neq (* != *) -> 70
    | OC_Le  (* <  *) -> 60
    | OC_Leq (* <= *) -> 60
    | OC_Ge  (* >  *) -> 60
    | OC_Geq (* >= *) -> 60

  let op_infix : _ -> int =
    function
    | OI_logic x -> op_logic_infix x
    | OI_arith x -> op_arith_infix x
    | OI_compare x -> op_compare x
end

module Parser =
struct
  open AtomicLexer
  open ParaLex
  open OpalATree

  type 'r t = (string, token, 'r) OpalATree.t

  let comment : string t =
    let* leaf = any_leaf in
    match leaf with
    | Comment c -> return c
    | _ -> mzero

  let exactly_symbol (s:string) : token t =
    exactly_leaf (Symbol s)

  let symbol : string t =
    let* leaf = any_leaf in
    match leaf with
    | Symbol s -> return s
    | _ -> mzero

  let value : value t =
    let* leaf = any_leaf in
    match leaf with
    | Value v -> return v
    | _ -> mzero

  let string : string t =
    let* v = value in
    match v with
    | String s -> return s
    | _ -> mzero

  let exactly_keyword (k:string) : token t =
    exactly_leaf (Keyword k)

  let keyword : string t =
    let* leaf = any_leaf in
    match leaf with
    | Keyword s -> return s
    | _ -> mzero

  let ident : string t =
    let* leaf = any_leaf in
    match leaf with
    | Ident i -> return i
    | _ -> mzero

  let tuple_coma_bracket (elem:'r t) : 'r list t =
    sub_tuple "( )" (exactly_symbol ",") elem

  let tuple_coma_wavy ?(empty=true) (elem:'r t) : 'r list t =
    sub_tuple ~empty "{ }" (exactly_symbol ",") elem

  let tuple_wavy ?(empty=true) (elem:'t t) : 'r list t =
    sub "{ }" (if empty then (until elem) else (until1 elem))

  let tuple_coma (elem:'r t) : 'r list t =
    elem $:: (until (exactly_symbol "," >> elem))

  (* Section. Non-Recurisve Types *)

  open Lang_C_Types

  let storage_class_specifier : storage_class_specifier t =
    let* key = keyword in
    match key with
    | "auto"     -> return SCS_auto
    | "register" -> return SCS_register
    | "static"   -> return SCS_static
    | "extern"   -> return SCS_extern
    | "typedef"  -> return SCS_typedef
    | _          -> mzero

  let op_logic_infix : op_logic_infix t =
    let* op = symbol in
    match op with
    | "&&" -> return OLI_LazyAnd
    | "&"  -> return OLI_BinAnd
    | "||" -> return OLI_LazyOr
    | "|"  -> return OLI_BinOr
    | "^"  -> return OLI_Xor
    | _    -> mzero

  let op_arith_infix : op_arith_infix t =
    let* op = symbol in
    match op with
    | "+"  -> return OAI_Plus  (* addition *)
    | "-"  -> return OAI_Minus (* substraction *)
    | "*"  -> return OAI_Mult  (* multiplication *)
    | "/"  -> return OAI_Div   (* division *)
    | "%"  -> return OAI_Rem   (* remainder *)
    | "<<" -> return OAI_LeftShift  (* binary left shift *)
    | ">>" -> return OAI_RightShift (* binary right shift *)
    | _    -> mzero

  let op_compare : op_compare t =
    let* op = symbol in
    match op with
    | "==" -> return OC_Eq
    | "!=" -> return OC_Neq
    | "<"  -> return OC_Le
    | "<=" -> return OC_Leq
    | ">"  -> return OC_Ge
    | ">=" -> return OC_Geq
    | _    -> mzero

  let op_infix : op_infix t =
    (op_logic_infix => (fun op -> OI_logic op)) <|>
    (op_arith_infix => (fun op -> OI_arith op)) <|>
    (op_compare => (fun op -> OI_compare op))

  let op_prefix_symbol : op_prefix t =
    let* op = symbol in
    match op with
    | "!"  -> return OPre_LNot
    | "~"  -> return OPre_BinNot
    | "*"  -> return OPre_DeRef
    | "&"  -> return OPre_Addr
    | "++" -> return OPre_Incr
    | "--" -> return OPre_Decr
    | "+"  -> return OPre_Plus
    | "-"  -> return OPre_Minus
    | _    -> mzero

  let op_prefix : op_prefix t =
    op_prefix_symbol <|>
    (exactly_keyword "sizeof" >> return OPre_SizeOf)

  let op_postfix : op_postfix t =
    let* op = symbol in
    match op with
    | "++" -> return OPost_Incr
    | "--" -> return OPost_Decr
    | _ -> mzero

  let identifier = ident

  (* [FIXME] missing floating numbers *)
  let constant : constant t =
    (identifier => (fun i -> C_Enum i)) <|>
    (
      let* v = value in
      return
        (
          match v with
          | String s -> C_Str  s
          | Char   c -> C_Char c
          | Int    i -> C_Int  i
        )
    )

  let assignment_operator : assignment_operator t =
    let* op = symbol in
    match op with
    | "="   -> return AO_Set
    | "*="  -> return AO_Mult
    | "/="  -> return AO_Div
    | "%="  -> return AO_Rem
    | "+="  -> return AO_Plus
    | "-="  -> return AO_Minus
    | "<<=" -> return AO_LeftShift
    | ">>=" -> return AO_RightShift
    | "&="  -> return AO_And
    | "|="  -> return AO_Or
    | "^="  -> return AO_Xor
    | _ -> mzero

  let struct_or_union : struct_or_union t =
    (exactly_keyword "struct" >> return SOU_Struct) <|>
    (exactly_keyword "union"  >> return SOU_Union )

  let type_qualifier : type_qualifier t =
    (exactly_keyword "const" >> return TQ_const) <|>
    (exactly_keyword "volatile"  >> return TQ_volatile )

  let type_leaf : type_leaf t =
    let* key = keyword in
    match key with
    | "_Bool"    -> return TL_Bool
    | "_Bool2"   -> return TL_Bool2
    | "void"     -> return TL_void
    | "char"     -> return TL_char
    | "short"    -> return TL_short
    | "int"      -> return TL_int
    | "long"     -> return TL_long
    | "float"    -> return TL_float
    | "double"   -> return TL_double
    | "signed"   -> return TL_signed
    | "unsigned" -> return TL_unsigned
    | _ -> mzero

  (* Section. Declarations, Types and Expressions *)

  (* [FIXME] dynamic verification of previous typedef *)
  let typedef_name : typedef_name t = mzero (* identifier *)

  let rec declaration_specifier : declaration_specifier t =
    fun stream -> (
      (storage_class_specifier => (fun x -> DS_StoreClassSpec x)) <|>
      (type_specifier => (fun x -> DS_TypeSpec x)) <|>
      (type_qualifier => (fun x -> DS_TypeQual x))
    ) stream

(* <struct-declaration> ::= {<specifier-qualifier>}* <struct-declarator-list> *)
  and     struct_declaration : struct_declaration t =
    fun stream -> (
      let* l1 = until specifier_qualifier in
      let* l2 = tuple_coma struct_declarator in
      return (l1, l2)
    ) stream

(* <struct-or-union-specifier> ::=
      | <struct-or-union> <identifier> { {<struct-declaration>}+ }
      | <struct-or-union> { {<struct-declaration>}+ }
      | <struct-or-union> <identifier>
 *)
  and     struct_or_union_specifier : struct_or_union_specifier t =
    fun stream -> (
      let* sous_struct_or_union = struct_or_union in
      let* sous_identifier = option identifier in
      let* sous_declaration = option (tuple_wavy ~empty:false struct_declaration) in
      if Tools.(isNone sous_identifier && isNone sous_declaration)
      then mzero
      else return {sous_struct_or_union; sous_identifier; sous_declaration}
    ) stream

  and     type_specifier =
    fun stream -> (
      (type_leaf => (fun leaf -> TS_leaf leaf)) <|>
      (struct_or_union_specifier => (fun x -> TS_struct_or_union_specifier x)) <|>
      (enum_specifier => (fun x -> TS_enum_specifier x)) <|>
      (typedef_name => (fun x -> TS_typedef_name x))
    ) stream

  and     specifier_qualifier =
    fun stream -> (
      (type_specifier => (fun x -> SQ_type_specifier x)) <|>
      (type_qualifier => (fun x -> SQ_type_qualifier x))
    ) stream

  and     enum_specifier =
    fun stream -> (
      let* _ = exactly_keyword "enum" in
      let* ident : identifier option = option identifier in
      let* enum_list : enumerator list option = option (tuple_wavy ~empty:false enumerator) in
      return (ident, Tools.unop_default [] enum_list)
    ) stream

  and     enumerator : enumerator t =
    fun stream -> (
      identifier $* (option (exactly_symbol "=" >> constant_expression))
    ) stream

  and     struct_declarator : struct_declarator t =
    fun stream -> (
      let* sd_declarator = option declarator in
      let* sd_value      = option (exactly_symbol ":" >> constant_expression) in
      return {sd_declarator; sd_value}
    ) stream

  and     pointer_option : pointer_option t =
    fun stream -> (
        until (exactly_symbol "*" >> until type_qualifier)
    ) stream

  and     pointer : pointer_option t =
    fun stream -> (
        until1 (exactly_symbol "*" >> until type_qualifier)
    ) stream

  and     declarator : declarator t =
    fun stream -> (
      pointer_option $* direct_declarator
    ) stream

  and     direct_declarator_left : direct_declarator_left t =
    fun stream -> (
      (identifier => (fun x -> DDL_identifier x)) <|>
      (sub "( )" declarator => (fun x -> DDL_declarator x))
    ) stream

  and     direct_declarator_right : direct_declarator_right t =
    fun stream -> (
      (sub "[ ]" (option constant_expression) => (fun x -> DDR_array x)) <|>
      (sub "( )" parameter_type_list => (fun x -> DDR_params x)) <|>
      (sub "( )" (until identifier) => (fun x -> DDR_decls x))
    ) stream

  and     direct_declarator : direct_declarator t =
    fun stream -> (
      direct_declarator_left $* until direct_declarator_right
    ) stream

  and     primary_expression =
    fun stream -> (
      (identifier => (fun x -> PE_Identifier x)) <|>
      (constant   => (fun x -> PE_Constant   x)) <|>
      (sub "( )" expression => (fun x -> PE_subexpr x))
    ) stream

  and     constant_expression_postfix =
    fun stream -> (
      ( sub "[ ]" expression                    => (fun x -> PostE_Crochet  x)) <|>
      ( sub "( )" expression                    => (fun x -> PostE_Bracket  x)) <|>
      ( exactly_symbol "." >> identifier        => (fun x -> PostE_Field    x)) <|>
      ( exactly_symbol "->" >> identifier       => (fun x -> PostE_PtrField x)) <|>
      ( op_postfix                              => (fun x -> PostE_Operator x))
    ) stream

  and     atomic_expression =
    fun stream -> (
      let* ae_casts = until (sub "( )" type_name) in
      let* ae_prefixes = until op_prefix in
      let* ae_primary = primary_expression in
      let* ae_postfixes = until constant_expression_postfix in
      return {ae_casts; ae_prefixes; ae_primary; ae_postfixes}
    ) stream

  (* [LATER] extract the priority-based carry-based mechanism as a more generic object *)
  and     constant_expression_rec (carry:constant_expression list) (level:int) : constant_expression t =
    fun stream -> (
      (
        let* x = exactly_keyword "sizeof" >> type_name in
        constant_expression_rec ((E_SizeOf x)::carry) level
      ) <|>
      (
        let* x = atomic_expression in
        constant_expression_rec ((E_Atomic x)::carry) level
      ) <|>
      (
        let* op = op_infix in
        match carry with
        | [] -> mzero
        | left::carry -> (
          let op_level = Priority.op_infix op in
          if op_level >= level (* left to right associativity *)
          then mzero
          else (
            let* right = constant_expression_rec [] op_level in
            constant_expression_rec ((E_Infix (left, op, right))::carry) level
          )
        )
      ) <|>
      (
        let* _ = exactly_symbol "?" in
        match carry with
        | [] -> mzero
        | cond::carry -> (
          let op_level = 130 in
          if op_level > level (* right to left associativity *)
          then mzero
          else (
            let* left = constant_expression_rec [] op_level in
            let* _ = exactly_symbol ":" in
            let* right = constant_expression_rec [] op_level in
            constant_expression_rec ((E_Cond (cond, left, right))::carry) level
          )
        )
      ) <|>
      (
        fun stream ->
          match carry with
          | [expr] -> return expr stream
          | _ -> mzero stream
      )
    ) stream

  and     constant_expression : constant_expression t =
    fun stream -> constant_expression_rec [] 130 stream

  and     assignment_expression : assignment_expression t =
    fun stream -> (
      until (atomic_expression $* assignment_operator) $* constant_expression
    ) stream

  and     expression : expression t =
    fun stream -> (
      tuple (exactly_symbol ",") assignment_expression
    ) stream

  and     type_name =
    fun stream -> (
      until1 specifier_qualifier $* option abstract_declarator
    ) stream

  and     parameter_type_list =
    fun stream -> (
      parameter_list $* ( (exactly_symbol "..." >> return true) <|> return false )
    ) stream

  and     parameter_list =
    fun stream -> (
      tuple_coma parameter_declaration
    ) stream

  and     parameter_declaration_right =
    fun stream -> (
      (declarator => (fun x -> PDR_declarator x)) <|>
      (abstract_declarator => (fun x -> PDR_abstract_declarator x))
    ) stream

  and     parameter_declaration =
    fun stream -> (
      until1 declaration_specifier $* option parameter_declaration_right
    ) stream

  and     abstract_declarator =
    fun stream -> (
      let* ptr = pointer_option in
      let* dad = option direct_abstract_declarator in
      if Tools.isNone dad && ptr = []
      then mzero
      else return (AD (ptr, dad))
    ) stream

  and     direct_abstract_declarator_right =
    fun stream -> (
      (sub "[ ]" (option constant_expression) => (fun x -> DADR_crochet x)) <|>
      (sub "( )" (option parameter_type_list) => (fun x -> DADR_function x))
    ) stream

  and     direct_abstract_declarator =
    fun stream -> (
      (option (sub "( )" abstract_declarator)) $* (until direct_abstract_declarator_right)
    ) stream

(* {<declaration_specifier>}+ {<init_declarator>}* ';' *)
  and     declaration =
    fun stream -> (
      until1 declaration_specifier $* until init_declarator << exactly_symbol ";"
    ) stream

(* <declarator> | <declarator> = <t_initializer> *)
  and     init_declarator=
    fun stream -> (
      declarator $* (option (exactly_symbol "=" >> t_initializer))
    ) stream

  and t_initializer =
    fun stream -> (
      (assignment_expression => (fun leaf -> Tree.Leaf leaf)) <|>
      (sub "{ }" (until t_initializer) => (fun sons -> Tree.Node sons))
    ) stream

  (* Section. Pragma *)
  let pragma : pragma t =
    exactly_keyword "_Pragma" >> sub "( )" string

  (* Section. Statement *)

  let expression_statement =
    post_process
(*      ~parsed:(fun x -> print_endline ("[GuaCaml.Lang_C_Opal.expression_statement] parsed:"^(STools.ToS.option Lang_C_ToS.expression x))) *)
        ((option expression) << exactly_symbol ";")

  let jump_statement_left =
    (exactly_keyword "goto" >> identifier => (fun x -> JS_goto x)) <|>
    (exactly_keyword "continue" >> return JS_continue) <|>
    (exactly_keyword "break" >> return JS_break) <|>
    (exactly_keyword "return" >> (option expression) => (fun x -> JS_return x))

  let jump_statement = jump_statement_left << exactly_symbol ";"
(*
<statement> ::=
  | <labeled-statement>
  | <expression-statement>
  | <compound-statement>
  | <selection-statement>
  | <iteration-statement>
  | <jump-statement>
 *)
  let rec statement =
    fun stream -> (
      post_process
(*        ~parsed:(fun x -> print_endline ("[GuaCaml.Lanc_C_Opal.statement] parsed:"^(Lang_C_ToPrettyS.statement x (Some 0)))) *)
      (
        ( declaration          => (fun x -> S_declaration x )) <|>
        ( labeled_statement    => (fun x -> S_labeled     x )) <|>
        ( expression_statement => (fun x -> S_expression  x )) <|>
        ( compound_statement   => (fun x -> S_compound    x )) <|>
        ( selection_statement  => (fun x -> S_selection   x )) <|>
        ( iteration_statement  => (fun x -> S_iteration   x )) <|>
        ( jump_statement       => (fun x -> S_jump        x ))
      )
    ) stream

(* <labeled-statement> ::=
  | <identifier> : <statement>
  | case <constant-expression> : <statement>
  | default : <statement>
 *)
  and     labeled_statement =
    fun stream -> (
      (
        (identifier $* (exactly_symbol ":" >> statement)) =>
          (fun (i, s) -> LS_identified_statement (i, s))
      ) <|>
      (
        (exactly_keyword "case" >> constant_expression $* (exactly_symbol ":" >> statement)) =>
          (fun (e, s) -> LS_case (e, s))
      ) <|>
      (
        (exactly_keyword "default" >> exactly_symbol ":" >> statement =>
          (fun x -> LS_default x))
      )
    ) stream

(* '{' {<declaration>}* {<statement>}* '}' *)
  and     compound_statement : compound_statement t =
    fun stream -> (sub "{ }" (until statement)) stream

(* <selection-statement> ::=
  | 'if' ( <expression> ) <statement> { 'else' <statement> }?
  | switch ( <expression> ) <statement>
 *)
  and     selection_statement =
    fun stream -> (
      (
        let* _ = exactly_keyword "if" in
(*        print_endline ("[GuaCaml.Lang_C_Opal.selection_statement] if!"); *)
        let* cond = sub "( )" expression in
(*        print_endline ("[GuaCaml.Lang_C_Opal.selection_statement] cond:"^(Lang_C_ToS.expression cond)); *)
        let* then_block = statement in
(*        print_endline ("[GuaCaml.Lang_C_Opal.selection_statement] then_block:"^(Lang_C_ToS.statement then_block)); *)
        let* op_else_block = option (exactly_keyword "else" >> statement) in
        return (SS_ITE (cond, then_block, op_else_block))
      ) <|>
      (
        (exactly_keyword "switch" >> sub "( )" expression $* statement) =>
          (fun (cond, block) -> SS_Switch (cond, block))
      )
    ) stream

  and     for_statement =
    fun stream -> (
      let* fs_pragma = option pragma in
      let* _ = exactly_keyword "for" in
      let for_parameter : (expression option * expression option * expression option) t =
        let* fs_init = option expression in
        let* _ = exactly_symbol ";" in
        let* fs_cond = option expression in
        let* _ = exactly_symbol ";" in
        let* fs_post = option expression in
        return (fs_init, fs_cond, fs_post)
      in
      let* fs_init, fs_cond, fs_post = sub "( )" for_parameter in
      let* fs_statement = statement in
      return {fs_init; fs_cond; fs_post; fs_statement}
    ) stream

  and     iteration_statement =
    fun stream -> (
      (
        (exactly_keyword "while" >> sub "( )" expression $* statement) =>
          (fun (e, s) -> IS_while (e, s))
      ) <|>
      (
        (exactly_keyword "do" >> statement << exactly_keyword "while" $*
          sub "( )" expression) =>
            (fun (s, e) -> IS_dowhile (s, e))
      ) <|>
      (for_statement => (fun fs -> IS_for fs))
    ) stream

  (* Section. #define *)

  let ext_define : ext_define t =
    let* _ = exactly_symbol "#" in
    let* _ = exactly_keyword "define" in
    let* td_name = identifier in
    let* td_params = option (tuple_coma_bracket identifier) in
    let* td_expr = expression in
    return {td_name; td_params; td_expr}

  (* Section. External Declaration *)

(* <function-definition> ::=
    {<declaration-specifier>}* <declarator> {<declaration>}* <compound-statement>
 *)
  let function_definition =
    let* fd_dec_spec = until declaration_specifier in
    print_endline ("[function_definition] fd_dec_spec:"^(STools.ToS.list Lang_C_ToS.declaration_specifier fd_dec_spec));
    let* fd_pragma = option pragma in
    print_endline ("[function_definition] fd_pragma:"^(STools.ToS.option Lang_C_ToS.pragma fd_pragma));
    let* fd_declarator = declarator in
    print_endline ("[function_definition] fd_declarator:"^(Lang_C_ToS.declarator fd_declarator));
    let* fd_declaration = until declaration in
    print_endline ("[function_definition] fd_declaration:"^(STools.ToS.list Lang_C_ToS.declaration fd_declaration));
    let* fd_compound_statement = (exactly_symbol ";" >> return None) <|> (compound_statement => (fun x -> Some x)) in
    print_endline ("[function_definition] fd_compound_statement:"^(STools.ToS.option Lang_C_ToS.compound_statement fd_compound_statement));
    return {
      fd_dec_spec;
      fd_pragma;
      fd_declarator;
      fd_declaration;
      fd_compound_statement
    }

(* <external-declaration> ::= <function-definition> | <declaration> *)
  let external_declaration =
    (ext_define => (fun x -> ED_define x)) <|>
    (function_definition => (fun x -> ED_FunDec x))  <|>
    (declaration => (fun x -> ED_Dec x))

(* <translation-unit> ::= {<external-declaration>}* *)
  let translation_unit : translation_unit t = until external_declaration
end

let lex_file (target:string) : AtomicLexer.token list =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let lexed_list = LazyStream.to_list lexed_stream in
  lexed_list

let paralex_file (target:string) : ParaLex.token_tree list =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  paralexed_list

let parse_file (target:string) : Lang_C_Types.translation_unit =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let program = Parser.translation_unit paralexed_list |> Tools.unop |> fst in
  program

