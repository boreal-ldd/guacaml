(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * UnionFind : Simpl implementation of the Union-Find algorithm
 *
 * === NOTES ===
 *
 * a cell with a negative number is interpreted a un-classed cell
 *)

open Extra

type uf_table = int array

let init n = Array.init n (fun x -> x)

let rec find uf_table x =
  if uf_table.(x) = x
  then x
  else
  (
    let x' = find uf_table (uf_table.(x)) in
    uf_table.(x) <- x';
    x'
  )

let union uf_table x y =
  let x' = find uf_table x
  and y' = find uf_table y in
  if x' < y'
  then uf_table.(y') <- x'
  else if y' < x'
  then uf_table.(x') <- y'
  else ()

(* [normalized uf] returns [true] iff the input union-find table [uf] is normalized,
 *   that is, every element [x] at position [i] is either :
 *     - non-classified (i.e., [x < 0])
 *     - a root (i.e., [i = x])
 *     - points to a root (i.e., [0 <= x < i], i.e. [uf.(x) = x])
 *)
let normalized uf_table : bool =
  let n = Array.length uf_table in
  assert(Array.for_all (fun x -> x < n) uf_table);
  MyArray.for_alli (fun i x -> (x < 0) || (x = i) || (uf_table.(x) = x)) uf_table

(* [naive_normalize uf] works by side effect on [uf] a union-find table
 * for each index [i], it updates [uf.(i) <- find uf i] if [x >= 0].
 *    (negative elements are left unchanged)
 * complexity in O(n)
 *)
let naive_normalize uf_table : unit =
  Array.iteri (fun i x -> if x >= 0 then ignore(find uf_table i)) uf_table;
  assert(normalized uf_table)

(* [normalize uf] works by side effect on [uf] a union-find table
 * for each index [i], it updates [uf.(i) <- find uf i] if [x >= 0].
 *   (negative elements are left unchanged)
 * complexity in O(n) (similar to [naive_normalize uf] but with a lower
 *   constant).
 *)
let normalize uf_table : unit =
  let n = Array.length uf_table in
  for i = 0 to n-1
  do
    let x = uf_table.(i) in
    if x >= 0 && i <> x then (
        uf_table.(i) <- uf_table.(x)
    )
  done;
  assert(normalized uf_table)

(* return the number of component *)
let count uf_table : int =
  MyArray.counti (=) uf_table

(**
 * [remap uf = (ncomp, remaped)] where :
 *   - [uf] is a valid union-find table (not necesarily normalized)
 *   - [ncomp = count uf]
 *   - [remaped] is a normalized version of [uf] where each component has
 *     been renamed with an index between [0] and [ncomp - 1]
 *     In particular, [Array.length remaped = Array.length uf]
 *
 * @param uf the union-find table
 * @return (ncomp, remaped) the number of component and the remaped table
 *)
let remap uf_table : int * (int array) =
  let n = Array.length uf_table in
  let remaped = Array.copy uf_table in
  let cnt = ref 0 in
  for i = 0 to n-1
  do
    let x = uf_table.(i) in
    if x = i then (
      remaped.(i) <- !cnt;
      incr cnt
    ) else if x >= 0 then (
      remaped.(i) <- remaped.(x)
    )
  done;
  (!cnt, remaped)

(** [partition uf_table] returns a triple [(remap, components, unclassed)] where:
  * - [remap] is an array such that [remap.(i)] is the index of the component that
  *     contains i (unclassed elements are remapped to (-1)).
  *     In particular, [Array.length remap = Array.length uf_table].
  * - [components] is an array such that [components.(i)] is the list of element in the
  *     component i.
  *     In particular, [Array.length components = count uf_table].
  * - [unclassed] is the list of pairs (i, j) such that i is an unclassed element
  *     and j is its initial value (before the function call).
  *
  * Assumes that uf_table is normalized.
  * Time complexity: O(n).
  *)

let partition uf_table : (int array) * (int list array) * ((int * int) list) =
  let ncomp, remap = remap uf_table in
  (* transposes the table *)
  let comps = Array.make ncomp [] in
  let other = ref [] in
  MyArray.rev_iteri
    (fun i x ->
      if x < 0
      then stack_push other (i, x)
      else array_push comps x i
    )
    remap;
  (remap, comps, !other)

let ends uf_table =
  normalize uf_table;
  partition uf_table
