(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA_CFT : GGLA-based implementation of C-FT-Graph (C = Cograph-factored)
 *
 * === NOTES ===
 *
 * C-Graphs are undirected graphs where cograph modules are factorized, i.e.,
 * we iterative factorize (hyper-)vertices which are either true of false twins*, i.e.,
 * *twins : vertices with identical neighorbood
 *   - true-twins : adjacent twins
 *   - false-twins : non-adjacent twins
 * the factorization step is called C-reduction
 *
 * === NOTES ===
 *
 * H-FT-Graph
 * House - Fixed Terminals - Graphs
 * This kind of graph is defined for WAP (Weighted Adjacency Propagation) Problem.
 * House = true-twins are merged
 * Fixed Terminals = some vertices cannot be supressed,
 *   thus cannot merge with regular vertices
 *)

open Extra
open STools
open BTools
module GGLA = GraphGenLA
open GGLA.Type

module Type =
struct
  (* [TODO] improve the representation to use a canonical represation of
   * co-graphs instanciated one or more times with actual names *)
  (* [MOVEME?] *)
  type cograph = int CoGraph.Explicit.Type.t

  type cometric = unit CoGraph.Implicit.Type.t

  type vlabel = {
    vlabel_module : cograph;
    vlabel_metric : cometric;
    vlabel_fixed  : bool;
    (* true  => the vertex is fixed/grounded cannot be eliminated
     * false => the vertex can be eliminated
     *)
    vlabel_useful : bool;
    (* true  => the vertex is used
     * false => the vertex is not used
     *)
  }

  type elabel = unit

  type hv = (vlabel, unit) vertex
  type hg = (vlabel, unit) graph

  type hgl = (vlabel list, elabel list) graph
end
open Type

module ToS =
struct
  open ToS
  open GGLA.ToS

  let cograph : cograph t = CoGraph.Explicit.ToS.t int

  let cometric : cometric t = CoGraph.Implicit.ToS.t unit

  let vlabel (vt:vlabel) : string =
    SUtils.record [
      ("vlabel_module", cograph vt.vlabel_module);
      ("vlabel_metric", cometric vt.vlabel_metric);
      ("vlabel_fixed" , bool vt.vlabel_fixed);
      ("vlabel_useful", bool vt.vlabel_useful);
    ]

  let elabel = unit

  let hv hv = vertex vlabel elabel hv
  let hg hg = graph  vlabel elabel hg

  let pprint_hg hg =
    pprint_graph vlabel elabel hg

  let pprint_ophg hg =
    pprint_opgraph vlabel elabel hg
end

module ToShiftS =
struct
  open ToShiftS
  open GGLA.ToShiftS

  let cograph : cograph t = CoGraph.Explicit.ToShiftS.t int

  let cometric : cometric t = CoGraph.Implicit.ToShiftS.t unit

  let vlabel : vlabel t =
    fun vt ->
      ShiftS.record [
        ("vlabel_module", cograph vt.vlabel_module);
        ("vlabel_metric", cometric vt.vlabel_metric);
        ("vlabel_fixed" , bool vt.vlabel_fixed);
        ("vlabel_useful", bool vt.vlabel_useful);
      ]

  let elabel = unit

  let hv ?(inline=false) hv = vertex ~inline vlabel elabel hv
  let hg ?(inline=false) hg = graph  ~inline vlabel elabel hg
end

module ToPrettyS =
struct
  let rec sbt (b:bool) : unit CoGraph.SymBagTree.Type.t -> string =
    let open CoGraph.SymBagTree.Type in
    function
    | Leaf () -> "V"
    | Node f -> sbf b f
  and     sbf (b:bool) : unit CoGraph.SymBagTree.Type.f -> string =
    fun f -> (
      (if b then "{" else "(")^
      (SUtils.catmap
        (if b then " ** " else " ++ ")
        (fun (t, n) -> sbt (not b) t^(if n = 1 then "" else ((if b then "^" else "x")^(STools.ToS.int n))))
        f)^
      (if b then "}" else ")")
    )

  (* [FIXME] *)
  let cograph : cograph -> string = CoGraph.Explicit.ToS.t STools.ToS.int

  (* [FIXME] *)
  let cometric : cometric -> string =
    let open CoGraph.Explicit.Type in
    function
    | Empty -> "E"
    | Vertex () -> "V"
    | NonTrivial (b, f) -> sbf b f

  (* [FIXME] *)
  let vlabel (vt:vlabel) : string =
    let open STools.ToS in
    SUtils.record [
      ("vlabel_module", cograph vt.vlabel_module);
      ("vlabel_metric", cometric vt.vlabel_metric);
      ("vlabel_fixed" , bool vt.vlabel_fixed);
      ("vlabel_useful", bool vt.vlabel_useful);
    ]

  (* [FIXME] *)
  let elabel = STools.ToS.unit

(*
  (* [FIXME] *)
  let hv ?(inline=false) hv = GGLA.ToS.vertex ~inline vlabel elabel hv
  (* [FIXME] *)
  let hg ?(inline=false) hg = GGLA.ToS.graph  ~inline vlabel elabel hg
 *)
end

let print_vertex (v:hv) : unit =
  print_string (ToS.hv v)
let print_graph  (g:hg) : unit =
  ToS.pprint_hg g

let vertex_module  (v:hv) : cograph =
  v.label.vlabel_module
let vertex_metric (v:hv) : cometric =
  v.label.vlabel_metric
let vertex_fixed  (v:hv) : bool =
  v.label.vlabel_fixed
let vertex_useful (v:hv) : bool =
  v.label.vlabel_useful

let cograph_support : cograph -> int list = CoGraph.Explicit.support

(* [FIXME] *)
let check_cograph (cg:cograph) : bool =
  CoGraph.Explicit.Check.t cg

let check_cometric (cm:cometric) : bool =
  CoGraph.Implicit.Check.t cm

(* [TODO] Check for C-reduction *)
let check ?(tt_red=false) ?(ft_red=false) (g:hg) =
  GGLA.Check.graph g &&
  GGLA.Utils.graph_is_undirected g &&
  GGLA.Utils.graph_is_reflexive ~refl:false g &&
  Array.for_all (fun v -> check_cograph v.label.vlabel_module) g &&
  Array.for_all (fun v -> check_cometric v.label.vlabel_metric) g &&
  (if tt_red
   then GGLA.TrueTwinsFree.compute_naive ~twins:true  g vertex_fixed
   else true) &&
  (if ft_red
   then GGLA.TrueTwinsFree.compute_naive ~twins:false g vertex_fixed
   else true)

let cograph_parallel (g1:cograph) (g2:cograph) : cograph =
  CoGraph.Explicit.( // ) g1 g2

let cometric_parallel (m1:cometric) (m2:cometric) : cometric =
  CoGraph.Implicit.( // ) m1 m2

let cograph_series (g1:cograph) (g2:cograph) : cograph =
  CoGraph.Explicit.( ** ) g1 g2

let cometric_series (m1:cometric) (m2:cometric) : cometric =
  CoGraph.Implicit.( ** ) m1 m2

let vlabel_add ?(dual=true) (v1:vlabel) (v2:vlabel) : vlabel =
  if v1.vlabel_fixed <> v2.vlabel_fixed
  then failwith "[GuaCaml.GGLA_CFT.vlabel_add] [error] fields [vlabel_fixed] are not equal";
  if v1.vlabel_useful <> v1.vlabel_useful
  then print_endline "[GuaCaml.GGLA_CFT.vlabel_add] [warning] fields [vlabel_useful] are not equal";
  {
    vlabel_module = CoGraph.Explicit.append ~dual v1.vlabel_module v2.vlabel_module;
    vlabel_metric = CoGraph.Implicit.append ~dual v1.vlabel_metric v2.vlabel_metric;
    vlabel_fixed = v1.vlabel_fixed;
    vlabel_useful = v1.vlabel_useful || v1.vlabel_useful;
  }

let vlabel_parallel (v1:vlabel) (v2:vlabel) : vlabel =
  vlabel_add ~dual:false v1 v2

let vlabel_series (v1:vlabel) (v2:vlabel) : vlabel =
  vlabel_add ~dual:true v1 v2

(* Collapses a list-extend C-Graph to a regular C-Graph
 * using "//" if [dual=false] or "**" if [dual=true]
 *)
let hg_of_hgl ?(dual=true) (gl:hgl) : hg =
  let map_vlabel_list (vl:vlabel list) : vlabel =
    match vl with
    | [] -> (failwith "[GuaCaml.GGLA_CFT.hg_of_hgl] error")
    | v0 :: vl' ->
      List.fold_left (vlabel_add ~dual) v0 vl'
  in
  let map_elabel_list (el:elabel list) : elabel = ()
  in
  GGLA.Utils.graph_map_both
    map_vlabel_list
    map_elabel_list
    gl
  |> GGLA.Utils.graph_rm_reflexive
  |> Tools.check_print ~debug_only:true (check ~tt_red:(dual = true) ~ft_red:(dual = false))
    (fun hg ->
      let open STools.ToS in
      print_endline  "[hg_of_hgl] [error] \"[hg] is not reduced\"";
      print_endline ("[hg_of_hgl] [error] hg:"^(ToS.hg hg));
      print_endline ("[hg_of_hgl] [error] check_graph:"^(bool (GGLA.Check.graph hg)));
      print_endline ("[hg_of_hgl] [error] is_undirected:"^(bool (GGLA.Utils.graph_is_undirected hg)));
      print_endline ("[hg_of_hgl] [error] (is_reflexive ~refl:false):"^(bool (GGLA.Utils.graph_is_reflexive ~refl:false hg)));
      print_endline  "[hg_of_hgl] [error] end";
    )

let compute_twins ?(dual=true) (hg:hg) : hg =
  GGLA.TrueTwins.compute_pseudolinear ~twins:dual hg vertex_fixed
  |> (fun qt -> hg_of_hgl ~dual qt.GGLA.Component.qgraph)

let rec normalize_rec ?(verbose=false) (hg:hg) : hg =
  let len = Array.length hg in
  let hg' = hg |> compute_twins ~dual:true |> compute_twins ~dual:false in
  let len' = Array.length hg' in
  if verbose then print_endline STools.ToS.("[GuaCaml.GGLA_CFT.normalize] len:"^(int len)^" -> len':"^(int len'));
  assert(Array.length hg' <= Array.length hg);
  (* [NOTE] [TODO] checking the reduction in the number of meta-vertices should
   * be enough *)
  if hg = hg'
  then hg
  else normalize_rec ~verbose hg'

let normalize ?(verbose=false) (hg:hg) : hg =
  normalize_rec ~verbose hg
  |> Tools.check_print ~debug_only:true (check ~tt_red:false ~ft_red:true)
    (fun hg' ->
      print_string "[GGLA_CFT.normalize] [error] begin"; print_newline();
      print_string "[GGLA_CFT.normalize] [error] hg:"; print_newline();
      print_graph hg; print_newline();
      print_string "[GGLA_CFT.normalize] [error] hg':"; print_newline();
      print_graph hg'; print_newline();
      print_string "[GGLA_CFT.normalize] [error] end"; print_newline();
    )

(*
let vertex_contraction (hg:hg) (iv:int) : hg =
  let v = hg.(iv) in
  (* adding a clique on the adjacency of the vertex [v] *)
  (* we may chose to not use the "~reflexive_false:true"
     option of graph_add_clique, as reflexives edges are
     removed again later (and may appear during merging) *)
  let hgK = GGLA.Utils.graph_add_clique ~replace:true ~reflexive_false:true hg v.edges in
  (* vertex [v] is removed *)
  let hg' = GGLA.Utils.graph_rm_vertex v.index hgK in
  (* the result is normalized *)
  (* [TODO] perform local normalization *)
  normalize hg'
 *)

let of_HFT (cg:GGLA_HFT.Type.hg) : hg =
  let hft =
    GGLA.Utils.graph_map_vlabel
      (fun v ->
        assert(SetList.sorted v.GGLA_HFT.Type.vlabel_names);
        let vlabel_module =
          CoGraph.Explicit.make_empty
          ~dual:true
          ~sorted:true
          v.GGLA_HFT.Type.vlabel_names
        in
        let vlabel_metric =
          CoGraph.Implicit.make_empty
          ~dual:true
          v.GGLA_HFT.Type.vlabel_weight
          ()
        in
        let vlabel_fixed  = v.GGLA_HFT.Type.vlabel_fixed in
        let vlabel_useful = v.GGLA_HFT.Type.vlabel_useful in
        { vlabel_module; vlabel_metric; vlabel_fixed; vlabel_useful }
      )
      cg
  in
  normalize hft

(*
  ft : fixed terminals (default = [], no ft-vertex)
  [default_useful = true] all vertices are labelged as useful
  [default_useful = false] only vertices with at least one edge (including reflexive ones) are labelged as useful
 *)
let of_GraphLA ?(ft=([]:int list)) ?(default_useful=false) (g:GraphLA.graph) : hg =
  GGLA_HFT.of_GraphLA ~ft ~default_useful g |> of_HFT

(*
let neighbors_weight (g:hg) (iv:int) : int =
  let v = g.(iv) in
  List.fold_left
    (fun card (iu,()) -> card + vertex_weight g.(iu))
    (vertex_weight v)
     v.edges

let total_weight (hg:hg) : int =
  Array.fold_left (fun w hv -> w + vertex_weight hv) 0 hg

let wap_Sc (g:hg) (iv:int) : BNat.nat =
  BNat.pow2 (neighbors_weight g iv)

let call_to_wap_S = ref 0
let reset_call_to_wap_S () : int  =
  let value = !call_to_wap_S in
  call_to_wap_S:=0;
  value

let wap_S (g:hg) (iv:int) : BNat.nat * hg =
  incr call_to_wap_S;
  let sc = wap_Sc g iv in
  let sg = vertex_contraction g iv in
  (sc, sg)

let wap_S' (g:hg) (iv:int) : int * BNat.nat * hg =
  incr call_to_wap_S;
  let k = neighbors_weight g iv in
  let sc = BNat.pow2 k in
  let sg = vertex_contraction g iv in
  (k, sc, sg)

let wap_SS (g:hg) (ivl:int list) : BNat.nat * hg =
  List.fold_left (fun (sc, sg) iv ->
    let sc', sg' = wap_S sg iv in
    (BNat.(sc +/ sc'), sg')
  ) (BNat.zero(), g) ivl

let wap_SS_get_names (g:hg) (ivl:int list) : BNat.nat * hg * int list list =
  let sc, sg, vll =
    List.fold_left (fun (sc, sg, vll) iv ->
      let vl' = vertex_names sg.(iv) in
      let sc', sg' = wap_S sg iv in
      (BNat.(sc +/ sc'), sg', vl' :: vll)
    ) (BNat.zero(), g, []) ivl
  in (sc, sg, List.rev vll)

let graph_set_fixed (vl:int list) ?(fixed=false) (hg:hg) : hg =
  if vl = []
  then hg
  else (
    let hg = Array.copy hg in
    let vlabel_fixed = fixed in
    List.iter (fun index ->
      let label = hg.(index).label in
      hg.(index) <- {hg.(index) with label = {label with vlabel_fixed}}) vl;
    hg
  )

let call_to_wap_R = ref 0
let reset_call_to_wap_R () : int  =
  let value = !call_to_wap_R in
  call_to_wap_R:=0;
  value

let wap_R (hg:hg) (vl:int list) : hg list =
  incr call_to_wap_R;
  let len = Array.length hg in
  assert(List.for_all (fun iv -> 0 <= iv && iv < len) vl);
  (* we may chose to not use the "~reflexive_false:true"
     option of graph_add_clique, as reflexives edges are
     removed again later (and may appear during merging) *)
  let hgK = GGLA.Utils.graph_add_clique ~replace:true ~reflexive_false:true hg (vl ||> (fun iv -> (iv,()))) in
  (* change status to [fixed = true] on [vl] vertices *)
  let hgK = graph_set_fixed vl hgK in
  (* decomposition into connected components *)
  let hgKi, _, _ = GGLA.Connected.compute vertex_fixed hgK in
  hgKi |> Array.to_list ||> normalize

let find_pendant_vertex (hg:hg) : int option =
  let rec fpv_rec hg i =
         if i >= Array.length hg
      then None
    else if List.length hg.(i).edges = 1
      then Some i
      else fpv_rec hg (succ i)
  in fpv_rec hg 0
 *)

let color_of_vertex label : string =
  match label.vlabel_fixed, label.vlabel_useful with
  | true, _ -> "black"
  | false, true -> "grey"
  | false, false -> "white"

let to_graphviz_vertex index label : string =
  let open STools.ToS in
  (* [FIXME] use a prettier representation of cometric *)
  let s_label = string (ToPrettyS.cometric label.vlabel_metric) in
  let s_color = string (color_of_vertex label) in
  String.concat "" ["[label = "; s_label; "; color="; s_color; "]"]

let to_graphviz_string ?(astree=true) ?(name="") (hg:hg) : string =
  let cv = to_graphviz_vertex in
  if astree
  then (GGLA_Graphviz.ToASTree.graph_to_string ~directed:false ~name ~cv hg)
  else (GGLA_Graphviz.ToS.graph_to_string ~directed:false ~name ~cv hg)

let to_graphviz_file ?(astree=true) ?(name="") (target:string) (hg:hg) : unit =
  if astree
  then (
    let cv = to_graphviz_vertex in
    GGLA_Graphviz.ToASTree.graph_to_file ~directed:false ~name ~cv target hg
  )
  else (SUtils.output_string_to_file target (to_graphviz_string ~astree ~name hg))

let of_FT ?(remove_useless=false) (g:GGLA_FT.Type.hg) : hg =
  g |> GGLA_HFT.of_FT ~remove_useless |> of_HFT

(* returns an H-FT-Graph matching the KD-graph given as input
 * vertices apearing in the input KD-graph and labelged as fixed are
 * effectively labelged as fixed (i.e. fixed vertices which are not used
 * are not added)
 * ~remove_useless:false (by default) [kd.nodes] is used as graph's domain
 * ~remove_useless:true [support kd] is used as graph's domain
 *)
let of_GraphKD ?(verbose=0) ?(remove_useless=false) (kd:GraphKD.graph) : hg =
  kd |> GGLA_HFT.of_GraphKD ~verbose ~remove_useless |> of_HFT
