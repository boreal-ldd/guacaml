(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_OCaml_types_ToS : the let of OCaml's types => export to string
 *
 *)

open Lang_OCaml_type_Types
open STools
open ToS

(* comments are a hierarchical structure :
 *   every sub-comment starting with '(*' and ending with '*)'
 *)
let comment = TreeUtils.ToS.tree char

(* [upper_identifier] identifiers starting with an upper case letter *)
let upper_identifier x = x

(* [lower_identifier] identifiers starting with a lower case letter (or '_') *)
let lower_identifier x = x

(* [FIXME] rely on non-conflicting name generation *)
(* [abstract_identifier] identifiers starting with the '\'' character *)
let abstract_identifier x = "a"^x

let module_name = upper_identifier

(* [FIXME] rely on path resolution within context *)
(* { <module_name> '.' }* *)
let module_path p =
  SUtils.catmap "" (fun x -> (upper_identifier x)^".") p

(* identifier used for type definition *)
let type_name_def = lower_identifier

(* identifier used for type reference *)
(* <type-name-ref> ::= <module-path> <type-name-def> *)
let type_name_ref (p, d) =
  (module_path p)^(type_name_def d)

(* identifier used for constructor definition *)
let constr_name_def = upper_identifier
(* identifier used for constructor reference *)

(* type identifier regular identifier with lower case first letter or '_' *)
let field_name_def = lower_identifier

(* <atomic-type> ::=
    | <type-identifier>
    | { tuple '(' ',' ')' <product-type> } <type-identifier
    | '(' <product-type> ')'
 *)
let rec atomic_type : atomic_type t =
  function
  | AT_identifier  x     -> type_name_ref x
  | AT_abstract    x     -> abstract_identifier x
  | AT_multi      (x, y) ->
    ("("^(type_name_ref y)^" "^(SUtils.catmap " " product_type x)^")")
  | AT_subtype     x     -> "("^(product_type x)^")"

(* <single-intanciation-type> ::= <atomic-type> { <type-name-ref> }* *)
and single_instanciation_type : single_instanciation_type t =
  fun (x, l) -> (
    List.fold_left
      (fun s t -> "("^(type_name_ref t)^" "^s^")")
      (atomic_type x)
       l
  )

(* tuple '*' <single-instanctiation-type> *)
and product_type : product_type t =
  fun p -> (
    let sit = single_instanciation_type in
    match p with
    | [] -> (failwith "[GuaCaml.Guacaml_meta_ocaml_gen_ToShiftS] error")
    | [t] -> sit t
    | [t1; t2] ->
      ("pair "^(sit t1)^" "^(sit t2))
    | [t1; t2; t3] ->
      ("trio "^(sit t1)^" "^(sit t2)^" "^(sit t3))
    | [t1; t2; t3; t4] ->
      ("quad "^(sit t1)^" "^(sit t2)^" "^(sit t3)^" "^(sit t4))
    | _ -> (
      let n = List.length p in
      (* [FIXME] use non-conflicting name generation *)
      let vars = MyList.init n (fun i -> "x"^(string_of_int i)) in
      let param = SUtils.tuple vars in
      let value_list = Tools.map2 (fun t v -> (sit t)^" "^v) p vars in
      let value = SUtils.tuple_squarebracket value_list in
      ("(fun "^param^" -> ShiftS.tuple "^value^")")
    )
  )

let internal_product_type (p:product_type) : string * string =
  let sit = single_instanciation_type in
  match p with
  | [] -> (failwith "[GuaCaml.Guacaml_meta_ocaml_gen_ToShiftS] error")
  | [t] ->
    (* [FIXME] use non-conflicting name generation *)
    ("x", (sit t)^" x")
  | [t1; t2] ->
    (* [FIXME] use non-conflicting name generation *)
    ("(x1, x2)", "pair "^(sit t1)^" "^(sit t2)^" (x1, x2)")
  | [t1; t2; t3] ->
    (* [FIXME] use non-conflicting name generation *)
    ("(x1, x2, x3)", "trio "^(sit t1)^" "^(sit t2)^" "^(sit t3)^" (x1, x2, x3)")
  | [t1; t2; t3; t4] ->
    (* [FIXME] use non-conflicting name generation *)
    ("(x1, x2, x3, x4)", "quad "^(sit t1)^" "^(sit t2)^" "^(sit t3)^" "^(sit t4)^" (x1, x2, x3, x4)")
  | _ -> (
    let n = List.length p in
    (* [FIXME] use non-conflicting name generation *)
    let vars = MyList.init n (fun i -> "x"^(string_of_int i)) in
    let param = SUtils.tuple vars in
    let value_list = Tools.map2 (fun t v -> (sit t)^" "^v) p vars in
    let value = SUtils.tuple_squarebracket value_list in
    (param, "ShiftS.tuple "^value)
  )

(* <record-type> ::=
    tuple+ '{' ';' '}' { <name-identifier> ':' <product-type> }
      (* there is an optional ';' at the end *)
 *)
let internal_record_type record : string * string =
  let param = SUtils.tuple_wavybracket Extra.(record ||> fst) in
  let value = SUtils.tuple_squarebracket
    Extra.(record ||> (fun (d, t) ->
      let stype = product_type t in
      let sname = field_name_def d in
      "\n\t\t\t( \""^sname^"\", "^stype^" "^sname^" )"
    ))
  in
  (param, "ShiftS.record "^value)

let record_type : record_type t =
  fun record -> (
    let param, value = internal_record_type record in
    "(fun "^param^" -> "^value^" )"
  )

let product_or_record_type (wrap:bool) : product_or_record_type t =
  function
  | PORT_product x -> if wrap
    then ("(fun x -> "^(product_type x)^" x)")
    else (product_type x)
  | PORT_record  x -> record_type       x

let sum_type x =
  let value_list = Tools.map
    (fun (d, t) ->
      let sd = field_name_def d in
      match t with
      | Some t -> (
        let param, value = match t with
          | PORT_product [t] -> (
            let st = single_instanciation_type t in
            (* [FIXME] use non-conflicting name generation *)
            ("x", "constructor \""^sd^"\" ("^st^") x")
          )
          | PORT_product  p  -> (
            (* [FIXME] use ToS.constructor whenever possible *)
            let param, value = internal_product_type p in
            (param, "(ShiftS.of_string \""^sd^" \")^!("^value^")")
          )
          | PORT_record   r  -> (
            let param, value = internal_record_type r in
            (param, "(ShiftS.of_string \""^sd^" \")^!("^value^")")
          )
        in
        (sd^" "^param^" -> "^value)
      )
      | None -> (sd^" -> ShiftS.of_string \""^sd^"\"")
    ) x
  in
  ("(function "^(SUtils.catmap "" (fun s -> "\n\t\t| "^s) value_list)^"\n\t)")

(* <named-type-definition> ::= <product-or-record-type> | <sum-type> *)
(* NB: [wrap = true] iff the output needs to be encapsulated *)
let named_type_definition (wrap:bool) : named_type_definition t =
  function
  | NTR_Alias   x -> product_or_record_type wrap x
  | NTR_Sum     x -> sum_type                    x

(* single type declaration
 * NB: 'type' or 'and' has already been parsed
 * NB: [wrap = true] iff the output needs to be encapsulated
 *)
let named_type (wrap:bool) : named_type t =
  fun {nt_params; nt_name; nt_def} ->
  (* [FIXME] deal with non-conflicting naming *)
  let s_params = SUtils.catmap " " abstract_identifier nt_params in
  let s_name   = type_name_def nt_name in
  let s_type   =
    if nt_params = []
    then s_name^" t"
    else (
      let s_type_list : string list =
        MyList.init (List.length nt_params) (fun _ -> "_")
      in
      ((SUtils.tuple s_type_list)^" "^s_name^" t")
    )
  in
  let s_def    = named_type_definition wrap nt_def in
  (s_name^" "^s_params^" : "^s_type^" =\n\t"^s_def )

module PrimaryTypeIn =
struct
  type 't t = string -> 't -> bool

  (* identifier used for type definition *)
  let type_name_def s x = s = x

  (* identifier used for type reference *)
  (* <type-name-ref> ::= <module-path> <type-name-def> *)
  let type_name_ref s (p, x) = p = [] && type_name_def s x

  (* identifier used for constructor definition *)
  let constr_name_def s _ = false

  (* type identifier regular identifier with lower case first letter or '_' *)
  let field_name_def s _ = false

  (* <atomic-type> ::=
      | <type-name-ref>
      | { tuple '(' ',' ')' <product-type> } <type-name-ref>
      | '(' <product-type> ')'
   *)
  let rec atomic_type s =
    function
    | AT_identifier x -> type_name_ref s x
    | AT_abstract   x -> false
    | AT_multi      (x, y) -> List.exists (product_type s) x || type_name_ref s y
    | AT_subtype    x -> product_type s x

  (* <single-intanciation-type> ::= <atomic-type> { <type-name-ref> }* *)
  and     single_instanciation_type s (x, y) =
    (atomic_type s x) || (List.exists (type_name_ref s) y)

  (* csv ',' <single-instanctiation-type> *)
  and     product_type s x =
    List.exists (single_instanciation_type s) x

  (* <record-type> ::=
      tuple+ '{' ';' '}' { <name-identifier> ':' <product-type> }
        (* there is an optional ';' at the end *)
   *)
  let record_type s x =
    List.exists (fun (_, t) -> product_type s t) x

  let product_or_record_type s =
    function
    | PORT_product x -> product_type s x
    | PORT_record  x -> record_type  s x

  let sum_type s x =
    let option p =
      function
      | Some x -> p x
      | None   -> false
    in
    List.exists (fun (c, t) -> option (product_or_record_type s) t) x

  (* <named-type-right> ::=
    | <product-or-record-type>
    | <sum-type>
   *)
  let named_type_definition s =
    function
    | NTR_Alias x -> product_or_record_type s x
    | NTR_Sum   x -> sum_type               s x

end

let named_type_is_rec {nt_params; nt_name; nt_def} : bool =
  PrimaryTypeIn.named_type_definition nt_name nt_def

(* list of mutually-recursive types (SCC in the dependency graph) *)
let named_type_rec : named_type_rec t =
  function
  | [] -> (failwith "[GuaCaml.Guacaml_meta_ocaml_gen_ToShiftS] error")
  | [nt] when named_type_is_rec nt = false -> "let "^(named_type false nt)
  | ntl -> "let rec "^(SUtils.catmap "\n\nand     " (named_type true) ntl)

let header =
"(* file generated by GuaCaml's meta-generation ocaml code *)
open STools
open ShiftS
open ToShiftS

"

(* list of list of mutually-recursive types *)
let system_type : system_type t =
  fun s ->
    (header^(SUtils.catmap "\n\n\n" named_type_rec s)^"\n")
