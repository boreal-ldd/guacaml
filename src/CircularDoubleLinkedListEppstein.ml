(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * CircularDoubleLinkedList : Hashed version of Circular Double Linked List
 *
 * === NOTE ===
 *
 * Freely translated into OCaml from Eppstein's implementation in Python :
 * [Sequence](https://www.ics.uci.edu/~eppstein/PADS/Sequence.py)
 *
 * Doubly-linked circular list for maintaining a sequence of items
 * subject to insertions and deletions.
 *
 * D. Eppstein, November 2003.
 *
 * === NOTE ===
 *
 * this is a strange way of implement circular double linked list.
 * pointer management is done using global hash-table for the whole
 * list.
 * [LATER] compare performances with a more usual implementation of
 * circular double linked list.
 *
 *)

(* [REMOVE?] class Sequence: *)
(* Maintain a sequence of items subject to insertions and removals.
 * All sequence operations take constant time except indexing, which
 * takes time proportional to the index.
 *)

(* [TODO] break into a ring of keys on one side (can be implemented
 * with a regular circular-double-linked-list) and an key-to-value
 * hash-table on top of it.
 *)

module WithKey =
struct

  type ('k, 'v) t = {
    key : 'v -> 'k;
    items : ('k, 'v) Hashtbl.t;
    next : ('k, 'k) Hashtbl.t;
    prev : ('k, 'k) Hashtbl.t;
    mutable first : 'k option;
  }

  let create ?(hsize=100) (key:'v -> 'k) : ('k, 'v) t =
  (* [FIXME] fix comment to better suit the OCaml code *)
  (* We represent the sequence as a doubly-linked circular linked list,
   * stored in two dictionaries, self._next and self._prev.  We also store
   * a pointer self._first to the first item in the sequence.  If key is
   * supplied, key(x) is used in place of x to look up item positions;
   * e.g. using key=id allows sequences of lists or sets.
   *)
    {
      key;
      items = Hashtbl.create hsize;
      next  = Hashtbl.create hsize;
      prev  = Hashtbl.create hsize;
      first = None;
    }

  let _items_get (self:('k, 'v) t) (key:'k) : 'v =
    Hashtbl.find self.items key

  let iter (f:'v -> unit) (self:('k, 'v) t) : unit =
  (* Iterate through the objects in the sequence.
   * May give unpredictable results if sequence changes mid-iteration.
   * Terminate iff the underlying linked-list is indeed a perfect loop
   *)
    match self.first with
    | None -> ()
    | Some first -> (
      let rec cdlle_loop item =
        item |> _items_get self |> f;
        let item = Hashtbl.find self.next item in
        if item == first then () else cdlle_loop item
      in
      cdlle_loop first
    )

  let to_list (self:('k, 'v) t) : 'v list =
    Tools.to_list_of_iter iter self

  let nth (self:('k, 'v) t) (i:int) : 'v =
  (* Return the ith item in the sequence. *)
    assert(i >= 0);
    match self.first with
    | None -> invalid_arg "[GuaCaml/CircularDoubleLinkedListEppsteing.th] empty list"
    | Some first -> (
      let rec cdlle_nth item i =
        if i = 0
        then _items_get self item
        else (
          let item = Hashtbl.find self.next item in
          if item == first
          then failwith "[GuaCaml/CircularDoubleLinkedListEppstein.nth] out of range"
          else cdlle_nth item (pred i)
        )
      in
      cdlle_nth first i
    )

  (* Number of items in the sequence. *)
  let length (self:('a, 'b)t) : int =
    Hashtbl.length self.next

  let to_string (to_string:'v -> string) (self:('k, 'v)t) : string =
    "CDLL.of_list "^(STools.ToS.list to_string (to_list self))

  (* Apply supplied key function. *)
  let key (self:('k, 'v) t) (x:'v) : 'k =
    let k = self.key x in
    Hashtbl.replace self.items k x; (* self.items[k] = v *)
    k

  (* Unkeyed version of insert_after. *)
  let insert_after_internal (self:('k, 'v) t) (x:'k) (y:'k) : unit =
    if Hashtbl.mem self.next y
    then failwith "[GuaCaml/CircularDoubleLinkedListEppstein.insert_after_internal] item already in sequence";
    let z = Hashtbl.find self.next x in
    Hashtbl.replace self.next y z;
    Hashtbl.replace self.prev z y;
    Hashtbl.replace self.next x y;
    Hashtbl.replace self.prev y x;
    ()

  (* Unkeyed version of insert_before *)
  let insert_before_internal (self:('k, 'v) t) (x:'k) (y:'k) : unit =
    assert(self.first <> None);
    let prev_x = Hashtbl.find self.prev x in
    insert_after_internal self prev_x y;
    if x = Tools.unop self.first
    then self.first <- Some y;
    ()

  (* Add x to the end of the sequence. *)
  let append_internal (self:('k, 'v) t) (x:'k) : unit =
    match self.first with
    | None -> (
      self.first <- Some x;
      Hashtbl.replace self.next x x; (* self.next[x] = x *)
      Hashtbl.replace self.prev x x; (* self.pref[x] = x *)
    )
    | Some first ->
      (insert_before_internal self first x)

  (* Add x to the end of the sequence. *)
  let append (self:('k, 'v) t) (x:'v) : unit =
    append_internal self (key self x)

  let of_list ?(hsize=100) (key:'v -> 'k) (l:'v list) : ('k, 'v) t =
    let t : ('k, 'v) t = create ~hsize key in
    List.iter (fun x -> append t x) l;
    t

  (* Remove x from the sequence.
   * If [x == first]
   *   If [first.next == first]
   *   then [first := None]
   *   Else [first := first.next]
   *)
  let remove_internal (self:('k, 'v) t) (x:'k) : unit =
    let prev = Hashtbl.find self.prev x in
    let next = Hashtbl.find self.next x in
    Hashtbl.replace self.next prev next;
    Hashtbl.replace self.prev next prev;
    let first = Tools.unop self.first in
    if x == first
    then (
      let first_next = Hashtbl.find self.next first in
      if first == first_next
      then self.first <- None
      else self.first <- Some first_next
    );
    Hashtbl.remove self.next x;
    Hashtbl.remove self.prev x;
    ()

  (* Remove x from the sequence.
   * If [x == first]
   *   If [first.next == first]
   *   then [first := None]
   *   Else [first := first.next]
   *)
  let remove (self:('k, 'v) t) (x:'v) : unit =
    remove_internal self (key self x)

  (* Add y after x in the sequence. *)
  let insert_after (self:('k, 'v) t) (x:'v) (y:'v) : unit =
    insert_after_internal self (key self x) (key self y)

  (* Add y before x in the sequence. *)
  let insert_before (self:('k, 'v) t) (x:'v) (y:'v) : unit =
    insert_before_internal self (key self x) (key self y)

  (* Find the previous element in the sequence. *)
  let get_prev (self:('k, 'v) t) (x:'v) : 'v =
    let kx = key self x in
    let kp = Hashtbl.find self.prev kx in
    Hashtbl.find self.items kp

  (* Find the next element in the sequence. *)
  let get_succ (self:('k, 'v) t) (x:'v) : 'v =
    let kx = key self x in
    let ks = Hashtbl.find self.next kx in
    Hashtbl.find self.items ks

  let get_first (self:('k, 'v) t) : 'v =
    match self.first with
    | None -> failwith "[GuaCaml/CircularDoubleLinkedListEppstein.get_first] empty list"
    | Some first -> Hashtbl.find self.items first

  let get_first_opt (self:('k, 'v) t) : 'v option =
    Tools.opmap (Hashtbl.find self.items) self.first

  let pull_first_opt (self:('k, 'v) t) : 'v option =
    match self.first with
    | None -> None
    | Some first -> (
      let v = Hashtbl.find self.items first in
      remove_internal self first;
      Some v
    )
end

module NoKey =
struct
end
