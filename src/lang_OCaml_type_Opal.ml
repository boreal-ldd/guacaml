(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_OCaml_types_Opal : OCaml's type => Opal-based parser
 *
 *)

open Lang_OCaml_type_Types
open Opal

(* [MOVEME?] make a separate module for comment-lexing ? *)
module CommentLexer =
struct
  open CharStream

  let rec comment_rec (carry:comment list) : (char, comment) t =
    fun stream -> (
      (exactly '(' >> exactly '*' >>
        (
          let* sub = comment_rec [] in
          comment_rec (sub :: carry)
        )
      ) <|>
      (exactly '*' >> exactly ')' >> (fun s -> return (Tree.Node (List.rev carry)) s)) <|>
      (
        let* char = any in
        comment_rec ((Tree.Leaf char)::carry)
      )
    ) stream

  let comment : (char, comment) t =
    (exactly '(' >> exactly '*' >> comment_rec []) <|>
    (exactly '*' >> exactly ')' =>
      (fun _ -> failwith "closed comment token '*)' without beginning")
    )
end

(* Section. Atomic Lexer *)

(* we split the 'char' stream into 'string' stream with the following rules :
 * - contiguous sequence of alpha-numerical characters + '_' are assembled
 * - we detect '(*' '*)' comments are assembled
 * - we assemble symbolic tokens
 * - space-ish [' '; '\t', '\r'] characters are converted to ' '
 * - every other character is left as is
 *)

module AtomicLexer =
struct
  open CharStream
  (* Symbols *)

  let list_of_symbols = [
    ".";
    ":"; ";";
    "("; ")";
    "{"; "}";
    ",";
    "*"; "|";
    "="
  ]

  (* [TODO] generate automatically *)
  let symbol : (char, string) t =
    let* c = any in
    match c with
    | '.' -> return "."
    | ':' -> return ":"
    | ';' -> return ";"
    | '(' -> return "("
    | ')' -> return ")"
    | '{' -> return "{"
    | '}' -> return "}"
    | ',' -> return ","
    | '*' -> return "*"
    | '|' -> return "|"
    | '=' -> return "="
    | _ -> mzero

  let comment : (char, comment) t = CommentLexer.comment

  (* Keywords *)
  (* [TODO?] use an hashtable instead ? *)
  let list_of_keywords = ["type"; "rec"; "and"; "of"]

  let keywords = one_of list_of_keywords

  let is_keyword : string -> bool =
    fun s -> List.mem s list_of_keywords

  (* [MOVEME] *)
  let lower_ = lower <|> exactly ' '

(* (* [MOVEME] *)
  let upper_identifier = (upper $:: until alpha_num_) => implode
  let lower_identifier = (lower_ $:: until alpha_num) => implode
 *)
  (* [MOVEME] *)
  let is_abstract_identifier : string -> bool =
    CharStream.(recognize_string (exactly '\'' >> skip_many ocaml_ident_char >> eof()))

  (* [MOVEME] *)
  let is_upper_identifier : string -> bool =
    CharStream.(recognize_string (upper >> skip_many ocaml_ident_char >> eof()))

  (* [MOVEME] *)
  let is_lower_identifier : string -> bool =
    CharStream.(recognize_string (lower_ >> skip_many ocaml_ident_char >> eof()))

  (* Atomic Lexer *)

   type token =
    | Comment of comment (* comments *)
    | Symbol  of string  (* registered <symbol>s *)
    | Keyword of string  (* registered <keyword>s *)
    | IdentA  of string  (* <abstract-identifier> *)
    | IdentU  of string  (* <upper-identifier> *)
    | IdentL  of string  (* <lower-identifier> *)
    | Word    of string  (* any other words *)
    | White   of char    (* white character *)
    | Other   of char    (* any other character *)

  module ToS =
  struct
    open STools
    open ToS

    let c = STools.ToS.constructor

    let comment = Lang_OCaml_type_ToS.comment

    let token : token t =
      function
      | Comment x -> c "Comment" comment x
      | Symbol  x -> c "Symbol"  string  x
      | Keyword x -> c "Keyword" string  x
      | IdentA  x -> c "IdentA"  string  x
      | IdentU  x -> c "IdentU"  string  x
      | IdentL  x -> c "IdentL"  string  x
      | Word    x -> c "Word"    string  x
      | White   x -> c "White"   char    x
      | Other   x -> c "Other"   char    x
  end

  let lexer : (char, token) t =
    (comment => (fun s -> Comment s)) <|>
    (symbol  => (fun s -> Symbol  s)) <|>
    (CharStream.ocaml_ident =>
      (fun w ->
             if is_keyword w
        then Keyword w
        else if is_abstract_identifier w
        then IdentA w
        else if is_upper_identifier w
        then IdentU w
        else if is_lower_identifier w
        then IdentL   w
        else (
          print_endline ("unexpected word:"^(STools.ToS.string w));
          Word    w
        )
      )
    ) <|>
    (CharStream.white => (fun c -> White c)) <|>
    (any     => (fun c -> print_endline ("unexpected character: "^(STools.ToS.char c)); Other c))
end

module ParaLex =
struct
  module Model : OpalParaLex.MSig
  with type token   = AtomicLexer.token
  and  type bracket = string
  =
  struct
    type token   = AtomicLexer.token
    type bracket = string

    open AtomicLexer
    let ignore =
      function
      | White _ -> true
      | _ -> false

    let bracket =
      function
      | Symbol "(" -> Some(function Symbol ")" -> Some "( )" | _ -> None)
      | Symbol "{" -> Some(function Symbol "}" -> Some "{ }" | _ -> None)
      | _ -> None

    let failure =
      function
      | Symbol ")" -> Some "closed bracket ')' with prior opening with '('"
      | Symbol "}" -> Some "closed bracket '}' with prior opening with '{'"
      | _ -> None
  end
  include Model

  module ToPrettyS =
  struct
    open STools.ToS
    open AtomicLexer.ToS

    let token_tree = TreeUtils.ToPrettyS.atree string token
  end

  module Module = OpalParaLex.Make(Model)
  include Module
end

module ParaLex_NoCom =
struct
  module Model : OpalParaLex.MSig
  with type token   = AtomicLexer.token
  and  type bracket = string
  =
  struct
    type token   = AtomicLexer.token
    type bracket = string

    open AtomicLexer
    let ignore =
      function
      | Comment _
      | White _ -> true
      | _ -> false

    let bracket =
      function
      | Symbol "(" -> Some(function Symbol ")" -> Some "( )" | _ -> None)
      | Symbol "{" -> Some(function Symbol "}" -> Some "{ }" | _ -> None)
      | _ -> None

    let failure =
      function
      | Symbol ")" -> Some "closed bracket ')' with prior opening with '('"
      | Symbol "}" -> Some "closed bracket '}' with prior opening with '{'"
      | _ -> None
  end
  include Model

  module ToPrettyS =
  struct
    open STools.ToS
    open AtomicLexer.ToS

    let token_tree = TreeUtils.ToPrettyS.atree string token
  end

  module Module = OpalParaLex.Make(Model)
  include Module
end

module Parser =
struct
  open AtomicLexer
  open ParaLex
  open OpalATree

  type 'r t = (string, token, 'r) OpalATree.t

  let comment : comment t =
    let* leaf = any_leaf in
    match leaf with
    | Comment c -> return c
    | _ -> mzero

  let exactly_symbol (s:string) : token t =
    exactly_leaf (Symbol s)

  let symbol : string t =
    let* leaf = any_leaf in
    match leaf with
    | Symbol s -> return s
    | _ -> mzero

  let exactly_keyword (k:string) : token t =
    exactly_leaf (Keyword k)

  let keyword : string t =
    let* leaf = any_leaf in
    match leaf with
    | Keyword s -> return s
    | _ -> mzero

  let abstract_identifier : abstract_identifier t =
    let* leaf = any_leaf in
    match leaf with
    | IdentA i -> return i
    | _ -> mzero

  let upper_identifier : upper_identifier t =
    let* leaf = any_leaf in
    match leaf with
    | IdentU i -> return i
    | _ -> mzero

  let lower_identifier : lower_identifier t =
    let* leaf = any_leaf in
    match leaf with
    | IdentL i -> return i
    | _ -> mzero

  let sub (node0:string) (p:'r t) : 'r t =
    exactly_tree
      (parser_tree (fun node -> if node = node0 then p else mzero))

  let tuple_coma_bracket (elem:'r t) : 'r list t =
    sub_tuple "( )" (exactly_symbol ",") elem

  let tuple_wavy (elem:'t t) : 'r list t =
    sub "{ }" (until elem)

  let tuple_coma_wavy ?(empty=true) (elem:'r t) : 'r list t =
    sub_tuple ~empty "{ }" (exactly_symbol ",") elem

  let tuple_wavy ?(empty=true) (elem:'t t) : 'r list t =
    sub "{ }" (if empty then (until elem) else (until1 elem))

  let tuple_coma ?(coma=",") (elem:'r t) : 'r list t =
    elem $:: (until (exactly_symbol coma >> elem))

  let option (p:'r t) : 'r option t =
    fun stream ->
      match p stream with
      | Some(r, stream) -> Some(Some r, stream)
      | None            -> Some(None  , stream)

  (* Section. Non-Recurisve Types *)

  let module_name : module_name t = upper_identifier

  let module_path : module_path t = until (module_name << exactly_symbol ".")

  let type_name_def : type_name_def t = lower_identifier

  let type_name_ref : type_name_ref t =  module_path $* type_name_def

  let constr_name_def : constr_name_def t = upper_identifier

  let field_name_def : field_name_def t = lower_identifier

  (* <atomic-type> ::=
      | <type-name-ref>
      | { tuple '(' ',' ')' <product-type> } <type-name-ref>
      | '(' <product-type> ')'
   *)
  let rec atomic_type : atomic_type t =
    fun stream -> (
      (type_name_ref => (fun x -> AT_identifier x)) <|>
      (abstract_identifier => (fun x -> AT_abstract x)) <|>
      (sub "( )" product_type => (fun x -> AT_subtype x)) <|>
      (
        ((tuple_coma_bracket product_type) $* type_name_ref) =>
          (fun (x, y) -> AT_multi (x, y))
      )
    ) stream

  (* <single-intanciation-type> ::= <atomic-type> { <type-name-ref> }* *)
  and     single_instanciation_type : single_instanciation_type t =
    fun stream -> (
      atomic_type $* until type_name_ref
    ) stream

  (* csv '*' <single-instanctiation-type> *)
  and     product_type : product_type t =
    fun stream -> (
      tuple ~empty:false (exactly_symbol "*") single_instanciation_type
    ) stream

  (* <record-type> ::=
      tuple '{' ';' '}' { <name-identifier> ':' <product-type> }
        (* there is an optional ';' at the end *)
   *)
  let record_type  : record_type t =
    sub_tuple "{ }"
      ~empty:false
      ~post_sep:true
        (exactly_symbol ";")
        (field_name_def << exactly_symbol ":" $* product_type)

  let product_or_record_type : product_or_record_type t =
      (product_type => (fun x -> PORT_product x)) <|>
      (record_type  => (fun x -> PORT_record  x))

  let sum_type : sum_type t =
      tuple
        ~empty:false
        ~ante_sep:true
        ~post_sep:false
          (exactly_symbol "|")
          (constr_name_def $*
            (
              ((exactly_keyword "of" >> post_process ~failed:(fun () -> failwith "[sum_type] $1") product_or_record_type) => (fun x -> Some x)) <|>
              (return None)
            )
          )

  (* <named-type-definition> ::=
    | <product-or-record-type>
    | <sum-type>
   *)
  let named_type_definition =
    (product_or_record_type => (fun x -> NTR_Alias x)) <|>
    (sum_type               => (fun x -> NTR_Sum   x))

  (* single type declaration
   * NB: 'type' or 'and' has already been parsed
   *)
  let named_type : named_type t =
    let* nt_params =
      (sub_tuple "( )" ~empty:false ~ante_sep:false ~post_sep:false (exactly_symbol ",") abstract_identifier) <|>
      (return [])
    in
    let* nt_name = type_name_def in
    (
      post_process
        ~failed:(fun () ->
          failwith ("[named_type] {nt_name:"^(Lang_OCaml_type_ToS.type_name_def nt_name)^"}")
        )
          (exactly_symbol "=" >> named_type_definition)
    ) => (fun nt_def -> {nt_params; nt_name; nt_def})

  (* list of mutually-recursive types (SCC in the dependency graph) *)
  let named_type_rec : named_type_rec t =
      exactly_keyword "type" >>
        post_process
(*          ~parsed:(fun (nt:named_type_rec) -> print_endline("[named_type_rec] :"^(Lang_OCaml_type_ToS.named_type_rec nt))) *)
          ~failed:(fun () -> failwith "[named_type_rec] $1")
          (
            tuple
              ~empty:false
              ~ante_sep:false
              ~post_sep:false
                (exactly_keyword "and")
                 named_type
          )

  (* list of list of mutually-recursive types *)
  let system_type : system_type t = until named_type_rec
end

let lex_file (target:string) : AtomicLexer.token list =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let lexed_list = LazyStream.to_list lexed_stream in
  lexed_list

let paralex_file (target:string) : ParaLex.token_tree list =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  paralexed_list

let parse_file (target:string) : system_type =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let program = Parser.system_type paralexed_list |> Tools.unop |> fst in
  program

(*
module ThisToS = Lang_OCaml_type_ToS
module ThisToPrettyS = Lang_OCaml_type_ToPrettyS

let _ =
  if Array.length Sys.argv <= 1
  then print_endline "[Land_OCaml_type_Opal] ..."
  else (
    print_endline "[GuaCaml.Lang_C_Opal._] {step:0}";
    let file_name = Sys.argv.(1) in
    print_endline "[GuaCaml.Lang_C_Opal._] {step:1}";
    List.iter (fun t -> print_endline (AtomicLexer.ToS.token t)) (lex_file file_name);
    print_endline "[GuaCaml.Lang_C_Opal._] {step:2}";
    List.iter (fun t -> print_endline (ParaLex.ToPrettyS.token_tree t)) (paralex_file file_name);
    print_endline "[GuaCaml.Lang_C_Opal._] {step:3}";
    print_endline (ThisToS.system_type (parse_file file_name));
    print_endline "[GuaCaml.Lang_C_Opal._] {step:4}";
    print_endline (ThisToPrettyS.system_type (parse_file file_name));
    ()
  )
 *)

