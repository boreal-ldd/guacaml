(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * OpalATree : Bare Syntax C Parser (written in Opal)
 *
 * === NOTE ===
 *
 * Look at the BNF of C : https://cs.wmich.edu/~gupta/teaching/cs4850/sumII06/The%20syntax%20of%20C%20in%20Backus-Naur%20form.htm
 *
 *)

open Tree

type ('a, 'b, 'r) parse_tree = ('a, 'b) atree -> 'r option

type ('a, 'b, 'r) parse_forest = ('a, 'b) atrees -> ('r * ('a, 'b) atrees) option
type ('a, 'b, 'r) t = ('a, 'b, 'r) parse_forest

let tree_mzero _ = None
let mzero _ = None

let tree_return x _ = Some x
let return x s = Some(x, s)

let eof (r:'r) : ('a, 'b, 'r) t =
  function
  | [] -> Some(r, [])
  | _ -> None

let ( >>= ) (x:('a, 'b, 'r1) t) (f:'r1 -> ('a, 'b, 'r2)t) : ('a, 'b, 'r2) t =
  fun forest -> match x forest with
    | Some(result, forest) -> f result forest
    | None -> None

let (let*) = (>>=)

let (<|>) (x:('a, 'b, 'r)t) (y:('a, 'b, 'r)t) : ('a, 'b, 'r) t =
  fun input ->
  match x input with
  | Some _ as ret -> ret
  | None -> y input

let any_tree : ('a, 'b, ('a, 'b) atree) t =
  function
  | [] -> None
  | head :: tail -> Some(head, tail)

let any_leaf : ('a, 'b, 'b) t =
  function
  | (Tree.ALeaf leaf)::tail -> Some(leaf, tail)
  | _ -> None

let any_node : ('a, 'b, 'a * ('a, 'b) atrees) t =
  function
  | (Tree.ANode(node, forest))::tail -> Some((node, forest), tail)
  | _ -> None

let parser_tree : ('a -> ('a, 'b, 'r) t) -> ('a, 'b, 'r) parse_tree =
  fun f tree ->
    match tree with
    | Tree.ALeaf _ -> None
    | Tree.ANode (node, forest) -> (
      match f node forest with
      | Some(token, []) -> Some token
      | _ -> None
    )

let exactly_tree : ('a, 'b, 'r) parse_tree -> ('a, 'b, 'r) t =
  fun ptree forest ->
    match forest with
    | [] -> None
    | head :: tail -> (
      match ptree head with
      | Some result -> Some(result, tail)
      | None -> None
    )

let satisfy_leaf : ('b -> bool) -> ('a, 'b, 'b) t =
  fun p ->
    let* leaf = any_leaf in
    if p leaf then return leaf else mzero

let exactly_leaf (leaf0:'b) : ('a, 'b, 'b) t = satisfy_leaf ((=)leaf0)

let (=>) x f = x >>= fun r -> return (f r)
let (>>) x y = x >>= fun _ -> y
let (<<) x y = x >>= fun r -> y >>= fun _ -> return r

let ($::) head tail =
  let* h = head in
  let* t = tail in
  return (h::t)

let pair (pa:('a, 'b, 'r1)t) (pb:('a, 'b, 'r2)t) : ('a, 'b, 'r1 * 'r2)t =
  let* a = pa in
  let* b = pb in
  return (a, b)

let ( $* ) = pair

let trio (pa:('a, 'b, 'r1)t) (pb:('a, 'b, 'r2)t) (pc:('a, 'b, 'r3)t): ('a, 'b, 'r1 * 'r2 * 'r3)t =
  let* a = pa in
  let* b = pb in
  let* c = pc in
  return (a, b, c)

let until (elem:('a, 'b, 'r) t) : ('a, 'b, 'r list) t =
  let rec until_rec carry stream =
    match elem stream with
    | Some(head, stream) -> until_rec (head::carry) stream
    | None -> return (List.rev carry) stream
  in
  until_rec []

let until1 e = e $:: until e

let until_stop (elem:('a, 'b, 'r1) t) (stop:('a, 'b, 'r2) t) : ('a, 'b, ('r1 list) * 'r2) t =
  let rec until_rec carry =
    (let* b = stop in return (List.rev carry, b)) <|>
    (let* a = elem in until_rec (a::carry))
  in
  until_rec []

let option (p:('a, 'b, 'r)t) : ('a, 'b, 'r option) t =
  fun stream ->
    match p stream with
    | Some(r, stream) -> Some(Some r, stream)
    | None            -> Some(None  , stream)

let post_process
    ?(parsed=(fun _ -> ()))
    ?(failed=(fun () -> ())) (p:('a, 'b, 'r)t) : ('a, 'b, 'r) t =
  (fun stream -> (
    match p stream with
    | Some some -> parsed (fst some); Some some
    | None -> failed (); None
  ))

let tuple
    ?(empty=true)
(* allows an optional separator [sep] at the very beginning *)
    ?(ante_sep=false)
(* allows an optional separator [sep] at the very end *)
    ?(post_sep=false)
     (sep:('a, 'b, _)t) (elem:('a, 'b, 'r)t) : ('a, 'b, 'r list) t =
  (if ante_sep then option sep else return None) >>
  ((elem $:: until (sep >> elem))
    <|> (if empty then return [] else mzero))
    << (if post_sep then option sep else return None)

let sub (node0:string) (p:('a, 'b, 'r) t) : ('a, 'b, 'r) t =
  exactly_tree
    (parser_tree (fun node -> if node = node0 then p else mzero))

let sub_tuple
     (node0:'a)
    ?(empty=true)
    ?(ante_sep=false)
    ?(post_sep=false)
     (sep:('a, 'b, _)t) (elem:('a, 'b, 'r)t) : ('a, 'b, 'r list) t =
  sub node0 (tuple ~ante_sep ~empty ~post_sep sep elem)
