(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_DIMACS_Opal : Bare Syntax DIMACS Parser (written in Opal)
 *
 *)

open Lang_DIMACS_Core
open Types
open Utils
open Opal
open CharStream

let clause : (char, clause) Opal.t =
  let rec clause_rec carry =
    let* x = spaces >> int in
    match x with
    | 0 -> skip_until_newline >> return (List.rev carry)
    | _ -> clause_rec ((term_of_int x)::carry)
  in
  clause_rec []

let comment : (char, string) Opal.t =
  exactly 'c' >> (until_stop any (exactly '\n') => (fun (cl, _) -> implode cl))

let primitive : (char, string * int * int) Opal.t =
  let* _ = exactly 'p' in
  let* mode = spaces >> word in
  let* nv = spaces >> nat in
  let* nc = spaces >> nat in
  let* _ = skip_until_newline in
  return (mode, nv, nc)

let dimacs_line : (char, dimacs_line) Opal.t =
  (comment => (fun s -> Comment s)) <|>
  (primitive => (fun (mode, nv, nc) -> Primitive(mode, nv, nc))) <|>
  (clause => (fun c -> Clause c))

let dimacs : (char, dimacs) Opal.t =
  until dimacs_line

let from_file (target:string) : dimacs =
  match parse_from_file dimacs target with
  | Some some -> some
  | None -> failwith "[GuaCaml.Lang_DIMACS_Opal.from_file] uncaught parsing error"
