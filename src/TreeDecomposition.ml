(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * TreeDecomposition : tree decomposition
 *
 *)

open Extra
module GGLA = GraphGenLA
open GGLA.Type
open GGLA_HFT
open GGLA_HFT.Type

(* type of tree decomposition *)
type 'v t = {
  (* each component should appear at most once *)
  components : 'v SetList.t array;
  (* [(i, j); ...] with [ i < j < length components] *)
  interconnections : (int * int) array;
}

module ToS =
struct
  open STools
  open ToS

  let t (v:'v t) td : string =
    SUtils.record [
      ("components", array(list v) td.components);
      ("interconnections", array(int * int) td.interconnections)
    ]
end

let components_of_sequence (hg:hg) (s:int list) : int list array =
  let k = List.length s in
  let comp = Array.make k [] in
  let rec convert (h:hg) (i:int) =
    function
    | [] -> (
      assert(i = k);
      assert(Array.length h = 0);
      ()
    )
    | x :: tail -> (
      let nx = SetList.union [x] (h.(x).edges ||> fst) in
      comp.(i) <- vertices_names h nx;
      assert(SetList.sorted comp.(i));
      let h' = vertex_contraction h x in
      convert h' (succ i) tail
    )
  in
  convert hg 0 s;
  comp

let parent_of_sequence (hg:hg) (comp:int list array) : int option array =
  (* print_endline ("[GuaCaml.TreeDecomposition.parent_of_sequence] comp:"^(STools.ToS.(array(list int)) comp)); *)
  let n = Array.length hg in
  let k = Array.length comp in
  (* 1. Compute for each vertex the highest index in which it appears *)
  let highest : int array = Array.make n k in
  MyArray.rev_iteri (fun i c ->
    List.iter (fun j -> if highest.(j) = k then highest.(j) <- i) c
  ) comp;
  (* print_endline ("[GuaCaml.TreeDecomposition.parent_of_sequence] highest:"^(STools.ToS.(array int) highest)); *)
  (* 1.b check that every vertex appear in at least one component *)
  Array.iter (fun h -> assert(0 <= h && h < k)) highest;
  (* 2. Compute for each component its parent (if any) in the tree *)
  Array.mapi
    (fun i c ->
      c
      ||> (fun j -> highest.(j))
      |> List.filter (fun j -> j > i)
      |> MyList.opmin
      |> Tools.opmap snd
    )
    comp

(* converting sequence to tree decomposition *)
let of_sequence (hg:hg) (s:int list) : int t =
  let components = components_of_sequence hg s in
  let tree_pred = parent_of_sequence hg components in
  let interconnections = ref [] in
  MyArray.rev_iteri (fun i opj ->
    match opj with
    | None -> ()
    | Some j -> (
      assert(i < j);
      stack_push interconnections (i, j)
    )
  ) tree_pred;
  {
    components;
    interconnections = Array.of_list !interconnections
  }

(* we compute intefaces *)
let compute_interfaces (t:'v t) : 'v list array =
  let comp = t.components in
  Array.map (fun (i, j) -> SetList.inter comp.(i) comp.(j)) t.interconnections

(* result = [Some (pred, index); ...] *)
let predecessors_of_interconnections
    (n:int)
    (edges:(int * int) array) : (int * int) option array =
  let pred = Array.make n None in
  Array.iteri (fun i (x, y) ->
    assert(x < y);
    assert(pred.(x) = None);
    pred.(x) <- Some (y, i)
  ) edges;
  pred

(* [merge_at_interfaces t keep = t'] returns a new tree decomposition such that :
 *   t.interfaces.(i) has been kept/preserved iff [keep.(i) = true]
 * in other words if [keep(i) = false] and [t.interfaces.(i) = (x, y)], then,
 *   components [x] and [y] have been merged
 *)
let merge_at_interfaces (t:'v t) (keep:bool array) : 'v t =
  let n = Array.length t.components in
  let uf = UnionFind.init n in
  (* we merge adjacent components [x] and [y] such that [keep.[(x, y)] = false] *)
  Array.iter2 (fun b (x, y) -> if not b then UnionFind.union uf x y) keep t.interconnections;
  (* print_endline ("uf:"^(STools.ToS.(array int) uf)); *)
  let remap, comps, _ =
    try UnionFind.ends uf
    with err ->
      print_endline "[GuaCaml.TreeDecomposition.merge_at_interface:UnionFind.ends] [error]";
      raise err
  in
  (* print_endline ("uf:"^(STools.ToS.(array int) uf)); *)
  let components =
    Array.map
      (fun p -> p ||> (fun x -> t.components.(x)) |> SetList.union_list)
      comps
  in
  let interconnections =
    MyArray.opmapi
      (fun i (x, y) ->
        if keep.(i)
        then Some (remap.(x), remap.(y))
        else None
      )
      t.interconnections
  in
  {components; interconnections}
