(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_DOT : Basic Type for the DOT language from Graphviz
 *)

(* [Graphviz](https://graphviz.org/doc/info/lang.html) *)

type id = string

(* compass_point ::= "n" | "ne" | "e" | "se" | "s" | "sw" | "w" | "nw" | "c" | "_" *)
(* case independent *)
type compass_point =
  | CP_N
  | CP_NE
  | CP_E
  | CP_SE
  | CP_S
  | CP_SW
  | CP_W
  | CP_NW
  | CP_C
  | CP_Any (* '_' *)

(* port ::= { ':' ID }? { ':' compass_point }? *)

type port = (id option) * (compass_point option)

(* node_id ::= ID port *)

type node_id = id * port

(* edgeop ::= "->" | "--" *)
type edgeop =
  | EO_Toward (* -> *)
  | EO_Undirected (* -- *)

(* "subgraph" { ID }? *)
type subgraph_left = id option

type colon =
  | SemiColon (* ; *)
  | Coma      (* , *)

type attr_left =
  | AL_Graph (* graph *)
  | AL_Node  (* node  *)
  | AL_Edge  (* edge  *)

(* attr_set ::= ID '=' ID { colon }? *)
type attr_set = id * id

(* attr_list ::= '[' { attr_set }* ']' *)
type attr_list = attr_set list

(* attr_statement ::= attr_left { attr_list }+ *)
type attr_statement = attr_left * (attr_list list)

(* node_statement ::= node_id { attr_list }* *)
type node_statement = node_id * (attr_list list)

(* edge_right ::= edgeop node_id_or_subgraph *)
type edge_right = edgeop * node_id_or_subgraph

(* edge_statement ::= node_id_or_subgraph { edge_right }+ { attr_list }* *)
and  edge_statement = node_id_or_subgraph * (edge_right list) * (attr_list list)

(* node_id_or_subgraph ::= node_id | subgraph *)
and node_id_or_subgraph =
  | NIOS_Node of node_id
  | NIOS_SubGraph of subgraph

(* subgraph ::= { subgraph_left }? '{' statement_list '}' *)
and subgraph = subgraph_left option * statement_list

(*
statement ::=
| node_statement
| edge_statement
| attr_statement
| ID '=' ID
| subgraph
 *)
and statement =
  | Statement_Node of node_statement
  | Statement_Edge of edge_statement
  | Statement_Attr of attr_statement
  | Statement_Equal of id * id
  | Statement_SubGraph of subgraph

(* statement_list ::= '{'  { statement { ';' }? }* '}' *)
and statement_list = statement list

(* graph ::= { strict }? { graph | digraph } { ID }? statement_list *)
type graph = {
  graph_strict : bool;
  graph_directed : bool;
  graph_id : id option;
  graph_statement : statement_list;
}
