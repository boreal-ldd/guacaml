(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Opal_Dimacs : Bare Syntax DIMACS Parser (written in Opal)
 *
 *)

open Extra

module Types =
struct
  type term = int * bool
  type clause = term list

  type dimacs_line =
    | Comment of string
    | Primitive of string * int * int (* ("cnf", nbvariables, nbclauses) *)
    | Clause of clause

  type dimacs = dimacs_line list
end
open Types

module Utils =
struct
  let term_of_int (x:int) : term =
    if x < 0 then (-x-1, true) else (x-1, false)

  let int_of_term ((x, b):term) : int =
    if b then -(x+1) else (x+1)
end
open Utils

module ToS =
struct
  open STools
  open ToS

  let term = int * bool
  let clause = list term

  let dimacs_line = function
    | Comment s -> "Comment "^(string s)
    | Primitive (s, nv, nc) -> "Primitive "^(trio string int int (s, nv, nc))
    | Clause c -> "Clause "^(clause c)

  let dimacs = list dimacs_line
end

module ToPrettyS =
struct
  open STools

  let term t = string_of_int (int_of_term t)
  let clause c =
    SUtils.catmap " " string_of_int (Extra.(c ||> int_of_term) @> [0])

  let dimacs_line = function
    | Comment s -> "c"^s
    | Primitive (mode, nv, nc) -> String.concat " " ["p"; mode; string_of_int nv; string_of_int nc]
    | Clause c -> clause c

  let dimacs d =
    (SUtils.catmap "\n" dimacs_line d)^"\n"
end
