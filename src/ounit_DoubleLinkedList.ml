open Extra
open OUnit
open DoubleLinkedList

let test_create () =
  let dll = create 1 in
  error_assert
    "check dll"
    (check dll);
  error_assert
    "dll = [1]"
    (check_items [1] dll);
  error_assert
    "check_structure_forward dll"
    (check_structure_forward [(None, None)] dll);
  ()

let _ = push_test
  "test_create"
   test_create

let test_set_prev_succ () =
  let t1 = create 1 in
  let t2 = create 2 in
  internal_set_prev_succ t1 t2;
  error_assert
    "t1.prev = None"
    (t1.prev = None);
  error_assert
    "t1.succ = Some t2"
    (t1.succ %== t2);
  error_assert
    "t2.prev = Some t1"
    (t2.prev %== t1);
  error_assert
    "t2.succ = None"
    (t2.succ = None);
  ()

let _ = push_test
  "test_set_prev_succ"
   test_set_prev_succ

let test_create_before_000 () =
  let dll1 = create 1 in
  let dll2 = create_before dll1 2 in
  error_assert
    "dll1.item = 1"
    (dll1.item = 1);
  error_assert
    "dll2.item = 2"
    (dll2.item = 2);
  error_assert
    "dll1.prev = dll2"
    (dll1.prev %== dll2);
  error_assert
    "dll2.succ = dll1"
    (dll2.succ %== dll1);
  error_assert
    "dll1.succ = None"
    (dll1.succ = None);
  error_assert
    "dll2.prev = None"
    (dll2.prev = None);
(*
  let open ToS in
  print_endline ("dll1.item : "^(int dll1.item));
  print_endline ("dll2.item : "^(int dll2.item));
  print_endline ("dll1.prev.item : "^(int (Tools.unop dll1.prev).item));
  assert(dll1.succ = None);
  if not (dll2.prev = None)
  then print_endline ("dll2.prev.item : "^(int (Tools.unop dll2.prev).item));
  print_endline ("dll2.succ.item : "^(int (Tools.unop dll2.succ).item));
 *)
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "check dll2"
    (check dll2);
  error_assert
    "dll2 = [2; 1]"
    (check_items [2; 1] dll2);
  error_assert
    "check_structure_forward dll2"
    (check_structure_forward [(None, Some dll1); (Some dll2, None)] dll2);
  ()

let _ = push_test
  "test_create_before_000"
   test_create_before_000

let test_create_before_001 () =
  let dll1 = create 1 in (* [1] *)
  let dll2 = create_after dll1 2 in (* [1; 2] *)
  let dll3 = create_after dll2 3 in (* [1; 2; 3] *)
  let dll4 = create_before dll2 4 in (* [1; 4; 2; 3] *)
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "dll1 = [1; 4; 2; 3]"
    (check_items [1; 4; 2; 3] dll1);
  error_assert
    "check_structure_forward dll1"
    (check_structure_forward [(None, Some dll4); (Some dll1, Some dll2); (Some dll4, Some dll3); (Some dll2, None)] dll1);
  let dll0 = create_before dll2 0 in (* [1; 4; 0; 2; 3] *)
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "dll1 = [1; 4; 0; 2; 3]"
    (check_items [1; 4; 0; 2; 3] dll1);
  error_assert
    "check_structure_forward dll1"
    (check_structure_forward [(None, Some dll4); (Some dll1, Some dll0); (Some dll4, Some dll2); (Some dll0, Some dll3); (Some dll2, None)] dll1);
  ()

let _ = push_test
  "test_create_before_001"
   test_create_before_001

let test_create_after () =
  let dll1 = create 1 in
  let dll2 = create_after dll1 2 in
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "dll1 = [1; 2]"
    (check_items [1; 2] dll1);
  error_assert
    "check_structure_forward dll1"
    (check_structure_forward [(None, Some dll2); (Some dll1, None)] dll1);
  ()

let _ = push_test
  "test_create_after"
   test_create_after

let test_remove_000 () =
  let dll = create 1 in
  remove dll;
  error_assert
    "check dll"
    (check dll);
  error_assert
    "dll = []"
    (check_items [1] dll);
  error_assert
    "check_structure_forward dll"
    (check_structure_forward [(None, None)] dll);
  ()

let _ = push_test
  "test_remove_000"
   test_remove_000

let test_remove_001 () =
  let dll1 = create 1 in
  let dll2 = create_before dll1 2 in
  let dll3 = create_before dll2 3 in (* [3; 2; 1] *)
  remove dll2; (* [3; 1] *)
  error_assert "check dll1" (check dll1);
  error_assert "check dll2" (check dll2);
  error_assert "check dll3" (check dll3);
  error_assert
    "dll3 = [3; 1]"
    (check_items [3; 1] dll3);
  error_assert
    "check_structure_forward dll3"
    (check_structure_forward [(None, Some dll1); (Some dll3, None)] dll3);
  ()

let _ = push_test
  "test_remove_001"
   test_remove_001

let test_remove_fst () =
  let dll1 = create 1 in
  let dll2 = create_before dll1 2 in
  let item, dll' = remove_fst dll2 in
  error_assert
    "item = 2"
    (item = 2);
  error_assert
    "dll' %== dll1"
    (dll' %== dll1);
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "check dll2"
    (check dll2);
  error_assert
    "dll' = [1]"
    (check_items_opt [1] dll');
  error_assert
    "check_structure_forward dll'"
    (check_structure_forward_opt [(None, None)] dll');
  ()

let _ = push_test
  "test_remove_fst"
   test_remove_fst

let test_remove_lst () =
  let dll1 = create 1 in
  let dll2 = create_before dll1 2 in
  let item, dll' = remove_lst dll1 in
  error_assert
    "item = 1"
    (item = 1);
  error_assert
    "dll' %== dll2"
    (dll' %== dll2);
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "check dll2"
    (check dll2);
  error_assert
    "dll2 = [2]"
    (check_items [2] dll2);
  error_assert
    "check_structure_forward dll2"
    (check_structure_forward [(None, None)] dll2);
  ()

let _ = push_test
  "test_remove_lst"
   test_remove_lst

let test_create_before_opt_000 () =
  let dll1 = create 1 in
  let dll2 = create_before_opt (Some dll1) 2 in
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "check dll2"
    (check dll2);
  error_assert
    "dll2 = [2; 1]"
    (check_items [2; 1] dll2);
  error_assert
    "check_structure_forward dll2"
    (check_structure_forward [(None, Some dll1); (Some dll2, None)] dll2);
  ()

let _ = push_test
  "test_create_before_opt_000"
   test_create_before_opt_000

let test_create_before_opt_001 () =
  let dll3 = create_before_opt None 3 in
  error_assert
    "check dll3"
    (check dll3);
  error_assert
    "dll3 = [3]"
    (check_items [3] dll3);
  error_assert
    "check_structure_forward dll3"
    (check_structure_forward [(None, None)] dll3);
  ()

let _ = push_test
  "test_create_before_opt_001"
   test_create_before_opt_001

let test_create_after_opt_000 () =
  let dll1 = create 1 in
  let dll2 = create_after_opt (Some dll1) 2 in
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "dll1 = [1; 2]"
    (check_items [1; 2] dll1);
  error_assert
    "check_structure_forward dll1"
    (check_structure_forward [(None, Some dll2); (Some dll1, None)] dll1);
  ()

let _ = push_test
  "test_create_after_opt_000"
   test_create_after_opt_000

let test_create_after_opt_001 () =
  let dll3 = create_after_opt None 3 in
  error_assert
    "check dll3"
    (check dll3);
  error_assert
    "dll3 = [3]"
    (check_items [3] dll3);
  error_assert
    "check_structure_forward dll3"
    (check_structure_forward [(None, None)] dll3);
  ()

let _ = push_test
  "test_create_after_opt_001"
   test_create_after_opt_001

let test_set_item () =
  let dll1 = create 1 in
  set_item dll1 2;
  error_assert
    "check dll1"
    (check dll1);
  error_assert
    "dll1 = [2]"
    (check_items [2] dll1);
  error_assert
    "check_structure_forward dll1"
    (check_structure_forward [(None, None)] dll1);
  ()

let _ = push_test
  "test_set_item"
   test_set_item

let test_get_item () =
  let dll1 = create 1 in
  let item = get_item dll1 in
  error_assert
    "item = 1"
    (item = 1);
  ()

let _ = push_test
  "test_get_item"
   test_get_item

let test_create_after_item_seq_000 () =
  let dll1 = create 1 in
  let dll2 = create_before dll1 2 in
  let dll3 = create_before dll2 3 in
  let dll4 = create_after_item_seq dll1 (List.to_seq [4; 5; 6]) in
  error_assert "check dll1" (check dll1);
  error_assert "check dll2" (check dll2);
  error_assert "check dll3" (check dll3);
  error_assert "check dll4" (check dll4);
  error_assert
    "dll3 = [3; 4; 5; 6; 2; 1]"
    (check_items [3; 2; 1; 4; 5; 6] dll3);
  ()

let _ = push_test
  "test_create_after_item_seq_000"
   test_create_after_item_seq_000

let test_create_after_item_seq_001 () =
  let dll1 = create 1 in
  let dll2 = create_after dll1 2 in
  let dll3 = create_after dll2 3 in
  let dll4 = create_after_item_seq dll1 (List.to_seq [4; 5; 6]) in
  error_assert "check dll1" (check dll1);
  error_assert "check dll2" (check dll2);
  error_assert "check dll3" (check dll3);
  error_assert "check dll4" (check dll4);
  error_assert
    "dll1 = [1; 4; 5; 6; 2; 3]"
    (check_items [1; 4; 5; 6; 2; 3] dll1);
  ()

let _ = push_test
  "test_create_after_item_seq_001"
   test_create_after_item_seq_001

let test_create_after_item_seq_002 () =
  let dll1 = create 1 in
  let dll2 = create_after dll1 2 in
  let dll3 = create_after dll2 3 in
  let dll4 = create_after_item_seq dll3 (List.to_seq [4; 5; 6]) in
  error_assert "check dll1" (check dll1);
  error_assert "check dll2" (check dll2);
  error_assert "check dll3" (check dll3);
  error_assert "check dll4" (check dll4);
  error_assert
    "dll1 = [1; 2; 3; 4; 5; 6]"
    (check_items [1; 2; 3; 4; 5; 6] dll1);
  ()

let _ = push_test
  "test_create_after_item_seq_002"
   test_create_after_item_seq_002

let test_of_item_seq () =
  let fst, lst = of_item_seq (List.to_seq [1; 2; 3]) in
  error_assert
    "check_opt fst"
    (check_opt fst);
  error_assert
    "fst = [1; 2; 3]"
    (check_items_opt [1; 2; 3] fst);
  ()

let _ = push_test
  "test_of_item_seq"
   test_of_item_seq

let test_to_list_opt () =
  let fst, lst = of_item_seq (List.to_seq [1; 2; 3]) in
  let l = to_list_opt fst in
  let open ToS in
  print_endline ("\n[print] l = "^STools.ToS.(list int l));
  error_assert
    "l = [1; 2; 3]"
    (l = [1; 2; 3]);
  ()

let _ = push_test
  "test_to_list_opt"
   test_to_list_opt

let test_of_list () =
  let lst = [1; 2; 3] in
  let fst, lst = of_list lst in
  error_assert
    "check_opt fst"
    (check_opt fst);
  error_assert
    "fst = [1; 2; 3]"
    (check_items_opt [1; 2; 3] fst);
  ()

let _ = run_tests()
