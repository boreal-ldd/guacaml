(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_PACE2017_Core : Bare Syntax for PACE 2017 Challenge (written in Opal)
 *
 *)

module Types =
struct
  type edge = int * int

  type pace2017_line =
    | Comment   of string
    | Primitive of string * int * int
    (* ("tw", nbvertices, nbedges) , "tw" can be replaced by "td", "td-2e", "td-lex" or other defined variant *)
    | Edge      of edge

  type pace2017 = pace2017_line list
end
open Types

module ToS =
struct
  open STools
  open ToS

  let edge = int * int

  let pace2017_line = function
    | Comment s -> constructor "Comment " string s
    | Primitive (s, nv, nc) -> constructor "Primitive " (trio string int int) (s, nv, nc)
    | Edge e -> constructor "Edge " edge e

  let pace2017 = list pace2017_line
end

module ToPrettyS =
struct
  open STools

  let edge (x, y) =
    SUtils.catmap " " ToS.int [x; y]

  let pace2017_line = function
    | Comment s -> "c"^s
    | Primitive (mode, nv, ne) -> String.concat " " ["p"; mode; string_of_int nv; string_of_int ne]
    | Edge e -> edge e

  let pace2017 d =
    (SUtils.catmap "\n" pace2017_line d)^"\n"
end
