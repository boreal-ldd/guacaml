(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraphKD : Graph representation based upon Clique Decomposition
 *   !!this representation is not canonical!!
 *)

open Extra
open STools
open BTools

(* Data Structure *)

type graph = {
  nodes   : int list;
  fixed  : int list;
  cliques : int list list;
(* each [int list] is [SetList.sorted] *)
}

module ToS =
struct
  include ToS
  let graph graph =
    "{nodes="^(list int graph.nodes)^
    "; fixed="^(list int graph.fixed)^
    "; cliques="^(list(list int)graph.cliques)^"}"
end

module ToShiftS =
struct
  open ShiftS
  open ToShiftS
  let graph (graph:graph) : ShiftS.t =
    record [
      ("nodes", of_ToS ToS.(list int) graph.nodes);
      ("fixed", of_ToS ToS.(list int) graph.fixed);
      ("cliques", list (of_ToS ToS.(list int)) graph.cliques);
    ]
end

let print_graph g =
  print_endline (ToS.graph g)

(* Tools *)

let check (graph:graph) =
(* returns true iff the graph is properly formed *)
  let check_clique clique =
    SetList.sorted clique &&
    SetList.subset_of clique graph.nodes
  in
  (SetList.sorted graph.nodes) &&
  (SetList.sorted graph.fixed) &&
  (SetList.subset_of graph.fixed graph.nodes) &&
  (List.for_all check_clique graph.cliques)

let reduce (graph:graph) =
  {
    nodes = SetList.sort graph.nodes;
    fixed = SetList.sort graph.fixed;
    cliques = Tools.map SetList.sort graph.cliques;
  }

(* returns the set of vertices appearing in at least one clique *)
(*  SetList.sort (List.flatten graph.cliques) *)
(* time-complexity O(|KV|) *)
let support (graph:graph) : int list =
  let h = Hashtbl.create (List.length graph.nodes) in
  List.iter (List.iter (fun i -> Hashtbl.replace h i ())) graph.cliques;
  let stack = ref [] in
  Hashtbl.iter (fun i () -> stack_push stack i) h;
  SetList.sort !stack

(* neighbors related tools *)

let voisins (graph:graph) (set:int list) : int list =
  List.fold_left (fun voisins clique ->
    if SetList.nointer set clique
    then voisins
    else SetList.union voisins clique
  ) set graph.cliques

let voisins_strict graph set =
  SetList.minus (voisins graph set) set

(* Conversion *)

let internal_to_GraphLA (len:int) (graph:int list list) : int list array =
  match graph with
  | [] -> [| |]
  | _ -> (
    let edges = Array.make len [] in
    List.iter (fun clique ->
      List.iter (fun node ->
        assert(0 <= node && node < len);
        edges.(node) <- SetList.union edges.(node) clique
      ) clique
    ) graph;
    edges
  )

let to_GraphLA (graph:graph) : GraphLA.graph =
  if graph.nodes = []
  then GraphLA.{nodes = []; edges = [||]}
  else (
    let nodes = graph.nodes in
    let maxi = MyList.last nodes |> snd in
    let edges = internal_to_GraphLA (succ maxi) graph.cliques in
    GraphLA.{nodes; edges}
  )
