(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Sparsematrix: sparse matrix structure based on Flexarray
 *
 * === AUTHOR ===
 *
 * B. Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes
 *)

module type MSig = SparseVector.MSig
(*
module type MSig = SparseVector.MSig =
sig
  type t
  type s
  type sub
  val index : t -> int
  val card : s -> int
  val iter : (t -> unit) -> s -> unit
  val iter_subset : (t -> unit) -> sub -> unit
  val fold : (t -> 'a -> 'a) -> s -> 'a -> 'a
  val to_string : s -> t -> string
end
 *)

module type Sig =
sig
  module I : MSig
  module J : MSig
  type 'a t
  val make : ?base:int -> I.s -> J.s -> 'a -> 'a t

  val get_set_i : 'a t -> I.s
  val get_set_j : 'a t -> J.s
  val get_card_i : 'a t -> int
  val get_card_j : 'a t -> int

  val get : 'a t -> I.t -> J.t -> 'a
  val set : 'a t -> I.t -> J.t -> 'a -> unit
  val iter : ('a -> bool) -> (I.t -> J.t -> 'a -> unit) -> 'a t -> unit
  val iter_submatrix : I.sub -> J.sub -> ('a -> bool) -> (I.t -> J.t -> 'a -> unit) -> 'a t -> unit
  val fold : ('a -> bool) -> (I.t -> J.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val iter_row : ('a -> bool) -> (J.t -> 'a -> unit) -> 'a t -> I.t -> unit
  val iter_subrow : J.sub -> ('a -> bool) -> (J.t -> 'a -> unit) -> 'a t -> I.t -> unit
  val iter_col : ('a -> bool) -> (I.t -> 'a -> unit) -> 'a t -> J.t -> unit
end

module Make(MI : MSig)(MJ : MSig) : Sig
  with type I.t   = MI.t
  and  type I.s   = MI.s
  and  type I.sub = MI.sub
  and  type J.t   = MJ.t
  and  type J.s   = MJ.s
  and  type J.sub = MJ.sub
