module GGLA = GraphGenLA
open GGLA.Type
open Examples_graphs
open GGLA_KIC
open OUnit

let to_hft (g:(unit, unit)graph) : hg =
  Array.map
    (fun {index; edges} ->
      {
        index;
        label = {vlabel_names = [index]; vlabel_weight = 1; vlabel_useful=true}
         edges
      }
    )
    g

let examples = Array.map (fun (name, example) -> (name, to_hft example)) graph_unit_unit

