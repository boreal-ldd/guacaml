(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraGenLA : Generic Adjacency List Graph
 *
 * clique detection : ...
 *
 * === CONTRIBUTORS ===
 *
 * Joan Thibault
 *   - joan.thibault@irisa.fr
 *)

open Extra
module GGLA = GraphGenLA
open GGLA.Type
open GGLA_HFT
open GGLA_HFT.Type

module EnumClique =
struct
  (* match selected.(i) with
   * | None => unspecified
   * | Some true => selected
   * | Some false => not selected
   *)

  (* match result with
   * | None => hg(selected) is a kvm but not ks => keep going
   * | Some true => hg(selected) is kvm and ks, hence PMC => stop and record
   * | Some false => hg(selected) is not a kvm => stop and skip
   *)

  let rec explore (hg:hg) (selected:int list) (choices:int list) : bool array Tree.tree =
    assert(SetList.sorted(List.rev selected));
    assert(GGLA.Utils.graph_is_clique hg (List.rev selected));
    match choices with
    | [] -> Tree.Leaf (MyArray.of_indexes (Array.length hg) selected)
    | head :: choices -> (
      let choices1 = SetList.inter choices (hg.(head).edges ||> fst) in
      let option1 = explore hg (head::selected) choices1 in
      let option2 = explore hg        selected  choices  in
      Tree.Node[option1; option2]
    )

  let enum_maximal_clique (hg:hg) : bool array list =
    explore hg [] (List.init (Array.length hg) (fun i -> i))
    |> TreeUtils.tree_flatten
end
