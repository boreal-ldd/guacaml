(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Assoc : Sorted association list
 *)

open Extra

(* [update_with ~noconflict:false l1 l2 = l3] where :
 * - l1, l2, l3 are sorted association list
 * - l3 is l1 updated with the value of l2
 * - if noconflict = true raises an exception if l1 and l2 have keys in common
 *)
let rec update_with ?(noconflict=false) ?(carry=[]) (l1:('a * 'b) list) (l2:('a * 'b) list) : ('a * 'b) list =
  match l1, l2 with
  | [], l | l, [] -> List.rev_append carry l
  | (((k1, _) as e1)::l1'), (((k2, _) as e2)::l2') -> (
    if k1 = k2
    then if noconflict && (snd e1 <> snd e2)
      then failwith "[GuaCaml.Assoc.update_with ~noconflict:true] conflict detected"
      else update_with ~noconflict ~carry:(e2::carry) l1' l2'
    else if k1 < k2
      then update_with ~noconflict ~carry:(e1::carry) l1' l2
      else update_with ~noconflict ~carry:(e2::carry) l1  l2'
  )

(* [restr l1 r = l1'] where :
 * - l1, l1' are sorted association list
 * - r is a sorted list
 * - l1' is l1 with its domain restricted to r
 *)
let rec restr_rec carry (l1:('a * 'b)list) (r:'a list) : ('a * 'b)list =
  match l1, r with
  | [], _ | _, [] -> List.rev carry
  | (((k1, _) as e1)::l1'), kr::r' -> (
         if k1 = kr
    then restr_rec (e1::carry) l1' r'
    else if k1 < kr
    then restr_rec      carry  l1' r
    else restr_rec      carry  l1  r'
  )

let restr (l1:('a * 'b)list) (r:'a list) : ('a * 'b)list =
  restr_rec [] l1 r

let rec sorted_rec (k0:'a) : ('a* 'b)list -> bool =
  function
  | [] -> true
  | (k1, _)::l -> (k0 < k1) && sorted_rec k1 l

(* checks that keys are sorted by increasing order *)
let sorted : ('a * 'b) list -> bool =
  function
  | [] -> true
  | (k0, _)::l -> sorted_rec k0 l

(* sort keys by increasing order *)
let sort (assoc:('a * 'b) list) : ('a * 'b) list =
  List.sort (fun x y -> Stdlib.compare (fst x) (fst y)) assoc

let merge_sorted_keys (assoc:('a * 'b) list) : ('a * 'b list)list =
  let rec msk_rec (carry:('a * 'b list)list) (x0:'a) (y0l:'b list) =
    function
    | [] -> List.rev((x0, y0l)::carry)
    | (x, y)::tail -> (
      if x = x0
      then msk_rec carry x0 (y::y0l) tail
      else msk_rec ((x0, List.rev y0l)::carry) x [y] tail
    )
  in
  match assoc with
  | [] -> []
  | (x, y)::tail -> msk_rec [] x [y] tail

(* sort keys by increasing order *)
(* elements with the same keys are put together in a list *)
let sort_merge_keys (assoc:('a * 'b) list) : ('a * 'b list) list =
  List.stable_sort (fun x y -> Stdlib.compare (fst x) (fst y)) assoc
  |> merge_sorted_keys

let count_sorted_keys (assoc:'a list) : ('a * int) list =
  let rec csk_rec (carry:('a * int)list) (x0:'a) (y0l:int) =
    function
    | [] -> List.rev((x0, y0l)::carry)
    | x::tail -> (
      if x = x0
      then csk_rec carry x0 (succ y0l) tail
      else csk_rec ((x0, y0l)::carry) x 1 tail
    )
  in
  match assoc with
  | [] -> []
  | x::tail -> csk_rec [] x 1 tail

let sort_count_keys (l:'a list) : ('a * int) list =
  List.sort Stdlib.compare l
  |> count_sorted_keys

(* [filling_partial_assoc support default assoc = list]
 *
 * assumes that :
 *   - support is sorted
 *   - assoc is sorted
 *
 * ```ocaml
 * let list = List.map
 *    (fun key -> Tools.unop_default default (List.assoc_opt key assoc))
 *     support
 * ```
 *
 * returns as [list] where each element of [support] is either mapped to
 * its corresponding value in [assoc] or [default] otherwise
 *
 * === use case ===
 *
 * filling a partial valuation into a total one, while getting rid of variables'
 * names.
 *)

let filling_partial_assoc (support:'a list) (default:'b) (assoc:('a * 'b) list) : 'b list =
  if not (SetList.sorted support)
  then invalid_arg "[GuaCaml.Assoc.filling_partial_assoc] 'support' is not 'SetList.sorted'";
  if not (sorted assoc)
  then invalid_arg "[GuaCaml.Assoc.filling_partial_assoc] 'assoc' is not 'sorted'";
  let rec fpa (carry:'b list) (s:'a list) (d:'b) (a:('a * 'b) list) : 'b list =
    match s, a with
    | [], [] -> List.rev carry
    | _ , [] -> List.rev(MyList.ntimes ~carry default (List.length s))
    | [], _  -> failwith "[GuaCaml.Assoc.filling_partial_assoc] 'assoc' is not a subset of 'support'"
    | hs::ts, (ka, va)::ta -> (
      assert(hs <= ka);
      if ka = hs
      then (fpa (va::carry) ts d ta)
      else (fpa (default::carry) ts d a)
    )
  in
  fpa [] support default assoc

(* **NOTE** the input association list does not have to be sorted *)
let assoc_array_of_assoc_list (n:int) (assoc:(int * 'v) list) : 'v list array =
  assert(List.for_all (fun (k, _) -> 0 <= k && k < n) assoc);
  let a = Array.make n [] in
  List.iter (fun (k, v) -> array_push a k v) assoc;
  Array.iteri (fun i l -> a.(i) <- List.rev l) a;
  a
