module GGLA = GraphGenLA
open GGLA.Type

let graph_0 =
  [| { index = 0; label = (); edges = [] } |]

let graph_2_0 : (unit, unit) graph =
  [| {index = 0; label = (); edges = []};
     {index = 1; label = (); edges = []} |]

let graph_2_1 : (unit, unit) graph =
  [| {index = 0; label = (); edges = [(1, ())]};
     {index = 1; label = (); edges = [(0, ())]} |]

(* 3-path *)
let graph_3_2 : (unit, unit) graph =
  [| {index = 0; label = (); edges = [(1, ())]};
     {index = 1; label = (); edges = [(0, ()); (2, ())]};
     {index = 2; label = (); edges = [(1, ())]} |]

(* 3-clique *)
let graph_3_0 =
  [| { index = 0; label = (); edges = [(1, ()); (2, ())] };
     { index = 1; label = (); edges = [(0, ()); (2, ())] };
     { index = 2; label = (); edges = [(0, ()); (1, ())] } |]

(* 4-path *)
let graph_4_3 : (unit, unit) graph =
  [| {index = 0; label = (); edges = [(1, ())]};
     {index = 1; label = (); edges = [(0, ()); (2, ())]};
     {index = 2; label = (); edges = [(1, ()); (3, ())]};
     {index = 3; label = (); edges = [(2, ())]} |]

let graph_4_6 =
  [| { index = 0; label = (); edges = [(1, ()); (2, ()); (3, ())] };
     { index = 1; label = (); edges = [(0, ()); (2, ()); (3, ())] };
     { index = 2; label = (); edges = [(0, ()); (1, ()); (3, ())] };
     { index = 3; label = (); edges = [(0, ()); (1, ()); (2, ())] } |]

let graph_5_4 : (unit, unit) graph =
  [| {index = 0; label = (); edges = [(1, ())]};
     {index = 1; label = (); edges = [(0, ()); (2, ())]};
     {index = 2; label = (); edges = [(1, ()); (3, ())]};
     {index = 3; label = (); edges = [(2, ()); (4, ())]};
     {index = 4; label = (); edges = [(3, ())]} |]

let graph_5_10 =
  [| { index = 0; label = (); edges = [(1, ()); (2, ()); (3, ()); (4, ())] };
     { index = 1; label = (); edges = [(0, ()); (2, ()); (3, ()); (4, ())] };
     { index = 2; label = (); edges = [(0, ()); (1, ()); (3, ()); (4, ())] };
     { index = 3; label = (); edges = [(0, ()); (1, ()); (2, ()); (4, ())] };
     { index = 4; label = (); edges = [(0, ()); (1, ()); (2, ()); (3, ())] } |]

let graph_6_4 : (unit, unit) graph =
  [| { index = 0; label = (); edges = [(1, ())] };
     { index = 1; label = (); edges = [(0, ()); (2, ())] };
     { index = 2; label = (); edges = [(1, ()); (3, ())] };
     { index = 3; label = (); edges = [(2, ())] };
     { index = 4; label = (); edges = [(5, ())] };
     { index = 5; label = (); edges = [(4, ())] }
  |]

let graph_6_5_path : (unit, unit) graph =
  [| { index = 0; label = (); edges = [(1, ())] };
     { index = 1; label = (); edges = [(0, ()); (2, ())] };
     { index = 2; label = (); edges = [(1, ()); (3, ())] };
     { index = 3; label = (); edges = [(2, ()); (4, ())] };
     { index = 4; label = (); edges = [(3, ()); (5, ())] };
     { index = 5; label = (); edges = [(4, ())] }
  |]

let graph_unit_unit = [|
  ("graph_0",       graph_0);
  ("graph_2_0",     graph_2_0);
  ("graph_2_1",     graph_2_1);
  ("graph_3_2",     graph_3_2);
  ("graph_3_0",     graph_3_0);
  ("graph_4_3",     graph_4_3);
  ("graph_4_6",     graph_4_6);
  ("graph_5_4",     graph_5_4);
  ("graph_5_10",    graph_5_10);
  ("graph_6_4",     graph_6_4);
  ("graph_6_5_path",graph_6_5_path);
|]
