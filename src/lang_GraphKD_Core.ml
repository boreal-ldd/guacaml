(*
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_GraphKD : Bare Syntax GraphKD Parser (written in Opal)
 *
 *)

open Extra

module Types =
struct
  type graphkd_line =
    | Comment of string
    | Primitive of string * int * int (* ("kd", nbvertices, nbcliques) *)
    | Fixed     of int list
    | Clique    of int list

  type graphkd = graphkd_line list
end
open Types

module ToS =
struct
  open STools
  open ToS

  let graphkd_line = function
    | Comment s -> "Comment "^(string s)
    | Primitive (s, nv, nc) -> "Primitive "^(trio string int int (s, nv, nc))
    | Fixed  il -> "Fixed "^(list int il)
    | Clique il -> "Clique "^(list int il)

  let graphkd = list graphkd_line
end

module ToPrettyS =
struct
  open STools
  open ToS

  let fixed il =
    String.concat " " ("f"::(il ||> int))
  let clique il =
    String.concat " " (il ||> int)

  let graphkd_line = function
    | Comment s -> "c"^s
    | Primitive (mode, nv, nc) -> String.concat " " ["p"; mode; string_of_int nv; string_of_int nc]
    | Fixed  il -> fixed il
    | Clique il -> clique il

  let graphkd d =
    (SUtils.catmap "\n" graphkd_line d)^"\n"
end
