(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * MemoTable : generic memoization facility
 *)

module type Sig =
sig
  type ('a, 'b) t

  val clear : (_, _) t -> unit
  val reset : (_, _) t -> unit

  val default_size : int
  val create : int -> (_, _) t

  val test : ('a, 'b) t -> 'a -> bool
  val push : ('a, 'b) t -> 'a -> 'b -> unit
  val memo : ('a, 'b) t -> 'a -> 'b -> 'b
  val pull : ('a, 'b) t -> 'a -> 'b

  val apply : ('a, 'b) t -> ('a -> 'b) -> 'a -> 'b
  val make : int -> ('a, 'b) t * (('a -> 'b) -> 'a -> 'b)

  val print_stats : (_, _) t -> unit
  val dump_stats : (_, _) t -> Tree.stree
end

module Module : Sig =
struct
  let default_size = 10000

  type ('a, 'b) t = {
    table : ('a, 'b) Hashtbl.t;
    mutable hitCnt : int;
    mutable clcCnt : int;
  }

  let clear (t:('a, 'b) t) : unit =
    Hashtbl.clear t.table
  let reset (t:('a, 'b) t) : unit =
    Hashtbl.reset t.table

  let create n = {
    table = Hashtbl.create n;
    hitCnt = 0;
    clcCnt = 0;
  }

  let test mem a = Hashtbl.mem (mem.table) a
  let push mem a b =
    if test mem a
    then failwith "[GuaCaml.MemoTable:push] - already stored"
    else Hashtbl.add (mem.table) a b

  let memo mem a b = push mem a b; b
  let pull mem a = Hashtbl.find (mem.table) a

  let apply mem fonc a =
    try
      let r = Hashtbl.find mem.table a in
      mem.hitCnt <- mem.hitCnt + 1;
      r
    with Not_found -> (
      mem.clcCnt <- mem.clcCnt + 1;
      memo mem a (fonc a)
    )

  let make n = let mem = create n in ( mem, apply mem )

  let print_stats mem =
    print_string   "MemoTable's length:\t";
    print_int (Hashtbl.length mem.table);
    print_string  "\nMemoTable's HitCnt:\t";
    print_int mem.hitCnt;
    print_string  ".\nMemoTable's ClcCnt:\t";
    print_int mem.clcCnt;
    print_string  ".\n"

  let dump_stats mem =
    let open STools in
    Tree.Node [
      Tree.Node [Tree.Leaf "length:"; ToSTree.int (Hashtbl.length (mem.table))];
      Tree.Node [Tree.Leaf "hit count:"; ToSTree.int mem.hitCnt];
      Tree.Node [Tree.Leaf "clc count:"; ToSTree.int mem.clcCnt]
    ]
end

include Module
