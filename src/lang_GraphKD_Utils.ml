(*
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_GraphKD : Bare Syntax GraphKD Parser (written in Opal)
 *
 *)

open Extra
open Lang_GraphKD_Core
open Types
module ThisOpal = Lang_GraphKD_Opal

let to_GraphKD (g:graphkd) : string * GraphKD.graph =
  let fixed =
    g
    |> MyList.opmap (function Fixed l -> Some l | _ -> None)
    |> List.flatten
    |> SetList.sort
    ||> pred
  in
  let cliques =
    g
    |> MyList.opmap
      (function
        | Clique l -> Some(SetList.sort l ||> pred) | _ -> None)
  in
  let (mode, nv, nc) =
    g
    |> MyList.ifind
        (function
          | Primitive(mode, nv, nc) -> Some(mode, nv, nc)
          | _ -> None
        )
    |> Tools.unop
    |> snd
  in
  let nodes = List.init nv (fun i -> i) in
  (mode, GraphKD.{nodes; fixed; cliques})

let of_GraphKD (mode:string) (kd:GraphKD.graph) : graphkd =
  let com = Comment " file generated by GuaCaml" in
  let prim = Primitive(mode, List.length kd.nodes, List.length kd.cliques) in
  let fixed = Fixed (kd.fixed ||> succ) in
  let cliques = kd.cliques ||> (fun k -> Clique (k ||> succ)) in
  com :: prim :: fixed :: cliques

let of_file (file:string) : string * GraphKD.graph =
  file |> ThisOpal.from_file |> to_GraphKD

let to_file (file:string) ?(mode="kd") (kd:GraphKD.graph) : unit =
  kd
  |> of_GraphKD mode
  |> ToPrettyS.graphkd
  |> STools.SUtils.output_string_to_file file
