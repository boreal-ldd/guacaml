(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * STools : toolbox for [string] type, includes a conversion facility [ToS]
 *)

module SUtils :
sig
  val char_of_bool : bool -> char
  val bool_of_char : char -> bool
  val string_of_bool : bool -> string
  val bool_of_string : string -> bool
  val pretty_of_bool : bool -> string
  val print_bool : bool -> unit

  val char_0 : int
  val char_9 : int
  val char_A : int
  val char_Z : int
  val char_a : int
  val char_z : int

  val explode: string -> char list
  val implode: char list -> string
  val catmap:  string -> ('a -> string) -> 'a list -> string
  val catmap_list :
    string -> ('a -> string) -> 'a list  -> string
  val catmap_array :
    string -> ('a -> string) -> 'a array -> string

  (* [catmap_matrix sep_row sep_col map mat = s] *)
  val catmap_matrix :
    string -> string -> ('a -> string) -> 'a array array -> string

  val index: string -> char -> int option
  val index_from: string -> int -> char -> int option
  val split: char -> string -> string list

  val ntimes : string -> int -> string
  val print_stream : bool list -> unit

  val bool_array_of_string : string -> bool array
  val string_of_bool_array : bool array -> string

  val bool_list_of_string : string -> bool list
  val string_of_bool_list : bool list -> string

  val naive_hexa_of_int  : int -> char
  val hexa_of_int        : int -> char
  val unsafe_hexa_of_int : int -> char

  val char_of_hexa : char -> char
  val int_of_hexa  : char -> int

  val naive_widehexa_of_int  : int -> char
  val widehexa_of_int        : int -> char
  val unsafe_widehexa_of_int : int -> char

  val naive_int_of_widehexa   : char -> int
  val char_of_widehexa        : char -> char
  val int_of_widehexa         : char -> int
  val unsafe_char_of_widehexa : char -> char
  val unsafe_int_of_widehexa  : char -> int

  (* (unsafe_)widehexa_of_sized_int x s
   * with 0 < s <= 4 and 0 <= x < 2^s
   *)
  val widehexa_of_sized_int        : int -> int -> char
  val unsafe_widehexa_of_sized_int : int -> int -> char

  val sized_char_of_widehexa        : char -> int * char
  val sized_int_of_widehexa         : char -> int * int

  (* [left_padding len' pad s = s']
      assumes [String.length s <= len']
      ensures [String.length s' = len']
      padding characters are added on the left side (low indices)
      if [String.length s = len'] the [s == s']
   *)
  val left_padding : int -> char -> string -> string
  (* [right_padding len' pad s = s']
      assumes [String.length s <= len']
      ensures [String.length s' = len']
      padding characters are added on the right side (high indices)
      if [String.length s = len'] the [s == s']
   *)
  val right_padding : int -> char -> string -> string

  val char_height_list  : ?above:char -> ?under:char -> int -> (char * int)list -> string list
  val char_height_array : ?above:char -> ?under:char -> int -> (char * int)array -> string list

  (* [roman_of_int n = s] where :
   * - [n] is a positive integer
   * - [s] is the roman representation of [n]
   *)
  val roman_of_int : int -> string

  (* [output_string_to_file file_name string] *)
  val output_string_to_file : string -> string -> unit

  (* [input_string_of_file file_name = content] *)
  val input_string_of_file : string -> string

  (* takes a character between '0' and '9' and cast it into an integer between 0 and 9 *)
  val int_of_char : char -> int

  (* takes an integer between 0 and 9 and cast it into a character between '0' and '9' *)
  val char_of_int : int -> char

  (* [tuple ["a"; "b"; ...]] = "( a, b, [...] )" *)
  val tuple :
    ?nil:string -> ?fst:string -> ?sep:string -> ?lst:string ->
      string list -> string

  (* [tuple ["a"; "b"; ...]] = "[ a; b; [...] ]" *)
  val tuple_squarebracket : string list -> string

  (* [tuple ["a"; "b"; ...]] = "{ a; b; [...] }" *)
  val tuple_wavybracket : string list -> string

  (* [record ["name1", "val1"; "name2", "val2"; ...] =
        "{ name = val1; name2 = val2; [...] }"
   *)
  val record : (string * string) list -> string
end

module ToS :
sig
  type 'a t = 'a -> string
  val ref : 'a t -> 'a ref t
  val string : string t
  val char : char t
  val option : 'a t -> 'a option t
  val option_default : string -> 'a t -> 'a option t
  val list : ?sep:string -> 'a t -> 'a list t
  val array : ?sep:string -> 'a t -> 'a array t
  val unit : unit t
  val bool : bool t
  val bool' : ?t:string -> ?f:string -> bool t
  val int : int t
  (* display non-negative integers using '_' as thousand separator *)
  val pretty_nat : int t
  (* display integers using '_' as thousand separator *)
  val pretty_int : int t
  val float : float t
  val uno : 'a t -> 'a t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t
  val quad : 'a t -> 'b t -> 'c t -> 'd t -> ('a*'b*'c*'d) t
  val ignore : 'a t
    (* let ignore _ = " _ " *)

  val constructor : ?sub:bool -> string -> 'a t -> 'a t
  val hashtbl : 'k t -> 'v t -> ('k, 'v) Hashtbl.t t

  val ( >> ) : string -> 'a t -> 'a t
  val ( << ) : 'a t -> string -> 'a t
end

module ShiftS :
sig
  (* when [int option = None] then display regular string, if [int option = Some tab]
   * shift the element by [tab] tabulation *)
  type t = int option -> string

  val tab_elem : string ref
  val tab : t -> t

  val break_elem : string ref
  val break_elem_None : string ref
  val break : t

  val of_string : string -> t
  val ( ^! ) : t -> t -> t
  val merge : t -> t list -> t
  val if_None : string -> string -> t
  val tuple :
    ?nil:string ->
    ?fst:string ->
    ?sep:string ->
    ?lst:string ->
        t list -> t
  val list : t list -> t
  val array : t array -> t
  val record : (string * t) list -> t
  val uno : t -> t
  val pair : t -> t -> t
  val ( * ) : t -> t -> t
  val trio : t -> t -> t -> t
  val quad : t -> t -> t -> t -> t

  val constructor : string -> t -> t
  val option : t option -> t

  val inline : t -> t
  val conditional_inline : bool -> t -> t
end

module ToShiftS :
sig
  type 'a t = 'a -> ShiftS.t

  val of_ToS : 'a ToS.t -> 'a t

  val ref : 'a t -> 'a ref t
  val string : string t
  val char : char t
  val option : 'a t -> 'a option t
  val option_default : string -> 'a t -> 'a option t
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val unit : unit t
  val bool : bool t
  val int : int t
  (* display non-negative integers using '_' as thousand separator *)
  val pretty_nat : int t
  (* display integers using '_' as thousand separator *)
  val pretty_int : int t
  val float : float t
  val uno : 'a t -> 'a t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t
  val quad : 'a t -> 'b t -> 'c t -> 'd t -> ('a*'b*'c*'d) t

  val constructor : string -> 'a t -> 'a t

  val inline : 'a t -> 'a t
  val conditional_inline : bool -> 'a t -> 'a t

	val hashtbl : 'k t -> 'v t -> ('k, 'v) Hashtbl.t t
end

module OfS :
sig
  type 'a t = string -> 'a
  val string : string t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
end

module O3S :
sig
  type 'a t = ('a ToS.t) * ('a OfS.t)
  val string : string t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
end

module ToSTree :
sig
  type 'a t = 'a -> Tree.stree
  val leaf : 'a ToS.t -> 'a t
  val map : ('a -> 'b) -> 'b t -> 'a t

  val string : string t
  val option : 'a t -> 'a option t
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t
  val quad : 'a t -> 'b t -> 'c t -> 'd t -> ('a*'b*'c*'d) t

  val file : string -> Tree.stree list
(** [file target]
    opens the [target] file and parses it as a list of s-tree
 **)
end

module OfSTree :
sig
  type 'a t = Tree.stree -> 'a
  val leaf : 'a OfS.t -> 'a t
  val map  : ('a -> 'b) -> 'a t -> 'b t

  val string : string t
  val option : 'a t -> 'a option t
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t
  val quad : 'a t -> 'b t -> 'c t -> 'd t -> ('a*'b*'c*'d) t

  val file : Tree.stree list -> string -> unit
(** [file streelist target]
    opens the [target] file and writes down the list of s-tree in [streelist]
 **)
  val print : Tree.stree list -> unit
  val pprint : Tree.stree list -> unit

  val to_bytes  : Tree.stree -> bytes
  val to_string : Tree.stree -> string
end

(* Annotated String Tree : Utils *)
(* Dumping (or pretty-printing) an object as a string often leads to perform
 * many string concatenation, while being quite efficient for small object it
 * does not scale well with an overall asymptotic complexity of 'O(n.k)'
 * (where 'n' is the size of the resulting string sequence and 'k' the number
 *  of element to be concatenated)
 *
 * Using 'ASTree.t' concatenation are performed lazily by replacing the bare
 * string manipulation with string-tree. Hence, concatenation becomes 'ANode'
 * creation, hence with an overall O(k) complexity.
 * Once all string have been organized, they can be concatenated once, using
 * an O(n + k) algorithm : 'to_bytes' or 'to_string'.
 *
 * The output can be directly dumped into a file using either 'output_t' or
 * 'to_file' in order to minimize memory overhead.
 *)

module ASTreeUtils :
sig
  open Tree

  type t = (string, string) atree

  val output_t : (string -> unit) -> t -> unit
  val output_t_list : (string -> unit) -> string -> t list -> unit

  val size_t : t -> int
  val size_t_list : t list -> int

  val to_bytes : t -> bytes
  val to_string : t -> string

  val to_file : string -> t -> unit
end

module SOut :
sig
  type 'a t = (string -> unit) -> 'a -> unit

  val string : string t
  val pretty_option : ?some:string -> ?none:string ->
    'a t -> 'a option t
  val option : 'a t -> 'a option t
  val pretty_list :
    ?nil:string -> ?head:string ->
    ?tail:string -> ?sep:string ->
      'a t -> 'a list t
  val list : 'a t -> 'a list t
  val pretty_array :
    ?nil:string -> ?head:string ->
    ?tail:string -> ?sep:string ->
      'a t -> 'a array t
  val array : 'a t -> 'a array t
  val unit : unit t
  val pretty_bool : ?t:string -> ?f:string -> bool t
  val bool : bool t
  val int : int t
  val float : float t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t

  val file : 'a t -> string -> 'a -> unit
  val print : 'a t -> 'a -> unit
end

val short_stats : float -> string
