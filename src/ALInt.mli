(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * ALInt : Arbitrary Length Integers
 *
 * === NOTES ===
 *
 * This module is implemented with bool array on purpose. In
 * order to be used as a simpler to check reference for other
 * modules. In particular it is used as ground-truth by
 * Snowflake parametric equivalent of this module
 *)

module Unsigned :
sig
  val fulladd2 : bool -> bool -> bool * bool
  val fulladd3 : bool -> bool -> bool -> bool * bool
  type t

  val get : ?default:bool ->  t -> int -> bool
  val of_bool : bool -> t
  val of_nat  : int -> t
  val to_nat  : t -> int
  val one     : unit -> t
  val ( +/ )   : t -> t -> t
  val shift_left_nat  : t -> int -> t
  val shift_right_nat : t -> int -> t
  val shift_left  : t -> int -> t
  val shift_right : t -> int -> t
  val ( <! ) : bool -> bool -> bool
  val (  </ ) : t -> t -> bool
  val ( <=/ ) : t -> t -> bool
  val (  >/ ) : t -> t -> bool
  val ( >=/ ) : t -> t -> bool
end

module U = Unsigned

module Signed :
sig
  val signed_fulladd3 : bool -> bool -> bool -> bool * bool

  type t

  val length : t -> int
  val get : t -> int -> bool
  val of_unsigned : U.t -> t
  val of_bool : bool -> t
  val of_nat : int -> t
  val to_nat : t -> int
  val of_int : int -> t
  val to_int : t -> int
  val zero : unit -> t
  val one : unit -> t
  val negative_one : unit -> t
  val ( +/ ) : t -> t -> t
  val ( ~-/ ) : t -> t
  val ( -/ ) : t -> t -> t
end
