(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * STools : toolbox for [string] type, includes a conversion facility [ToS]
 *)

open Extra

module SUtils =
struct
  let char_of_bool = function
    | true  -> '1'
    | false  -> '0'
  let bool_of_char = function
    | '1'  -> true
    | '0'  -> false
    | _    -> failwith "[GuaCaml/strUtils:bool_of_char] bool parsing failure"
  let string_of_bool = function
    | true   -> "1"
    | false  -> "0"
  let bool_of_string = function
    | "1"  -> true
    | "0"  -> false
    | _    -> failwith "[GuaCaml/strUtils:bool_of_string] bool parsing failure"
  let pretty_of_bool = function
    | true  -> "1"
    | false  -> "."
  let print_bool = function
    | true  -> print_string "1"
    | false  -> print_string "0"

  let char_0 = Char.code '0'
  let char_9 = Char.code '9'
  let char_A = Char.code 'A'
  let char_Z = Char.code 'Z'
  let char_a = Char.code 'a'
  let char_z = Char.code 'z'

  let explode (s : string) : char list =
    let rec exp i l =
      if i < 0 then l else exp (i - 1) (s.[i] :: l)
    in exp (String.length s - 1) []

  let array_implode (l:char array) : string =
    String.init (Array.length l) (fun i -> l.(i))

  let list_implode (l:char list) : string =
    array_implode (Array.of_list l)

  let implode = list_implode

  let catmap s f l = String.concat s (Extra.(l ||> f))
  let catmap_list = catmap
  let catmap_array s f l =
    catmap s f (Array.to_list l)

  let catmap_matrix sep_row sep_col map mat : string =
    catmap_array sep_col (fun line -> catmap_array sep_row map line) mat

  let index s c =
    try               Some (String.index s c)
    with Not_found -> None

  let index_from s i c =
    try               Some(String.index_from s i c)
    with Not_found -> None

  let split = String.split_on_char

  let char_matrix_of_string ?(sep_row='\n') (s:string) : char array array =
    s
    |> split sep_row
    ||> (fun line -> line |> explode |> Array.of_list)
    |> Array.of_list

  let string_matrix_of_string ?(sep_row='\n') ?(sep_col=' ') (s:string) : string array array =
    s
    |> split sep_row
    ||> (fun line -> line |> split sep_col |> Array.of_list)
    |> Array.of_list

  let ntimes s n =
    let len = String.length s in
    let bytes = Bytes.create (n * len) in
    for i = 0 to n-1
    do
      Bytes.blit_string s 0 bytes (len * i) len
    done;
    Bytes.unsafe_to_string bytes

  let print_stream stream = print_string (Tools.map char_of_bool stream |> implode)

  let bool_array_of_string s =
    Array.init (String.length s) (fun i -> bool_of_char s.[i])

  let string_of_bool_array a =
    String.init (Array.length a) (fun i -> char_of_bool a.(i))

  let bool_list_of_string s =
    let rec aux carry = function
      | 0 -> carry
      | n -> (
        aux ((bool_of_char s.[n-1])::carry) (n-1)
      )
    in
    aux [] (String.length s)

  let string_of_bool_list l =
    let rl = ref l in
    String.init (List.length l) (fun _ -> match !rl with
      | [] -> (failwith "[GuaCaml.STools] error")
      | b::l -> rl:=l; char_of_bool b)

  let naive_hexa_of_int x : char =
    assert(0 <= x && x <= 0b1111);
    Char.chr(if x < 10 then (char_0+x) else (char_A+(x-10)))

  let hexa_of_int_string = String.init 16 (fun i -> naive_hexa_of_int i)
  (* char array -> string *)
  let hexa_of_int x = hexa_of_int_string.[x]
  let unsafe_hexa_of_int x = String.unsafe_get hexa_of_int_string x

  (* 0 <= x < 52 *)
  let naive_widehexa_of_int x =
    assert(0 <= x && x < 62);
    Char.chr (
           if x < 10 then (char_0+x)
      else if x < 36 then (char_A+(x-10)) (* 10 + 26 *)
      else                (char_a+(x-36))
    )
  let widehexa_of_int_string =
    String.init 62 (fun i -> naive_widehexa_of_int i)

  (* char array -> string *)
  let unsafe_widehexa_of_int x =
    String.unsafe_get widehexa_of_int_string x
  let widehexa_of_int x =
    if 0 <= x && x < 62
    then (unsafe_widehexa_of_int x)
    else (
      invalid_arg (
        "[GuaCaml:SUtils:widehexa_of_int] 'x:int = "^
        (string_of_int x)^"' outside the range [0; 62["
      )
    )

  (* 0 < s <= 4, 0 <= x < 2^s *)
  let unsafe_widehexa_of_sized_int x s =
    let t = [|0; 28; 24; 16; 0|] in
    unsafe_widehexa_of_int (x+t.(s))

  let widehexa_of_sized_int x s =
    assert (0 < s && s <= 4 && 0 <= x && x < (1 lsl s));
    unsafe_widehexa_of_sized_int x s

  let proto_int_of_widehexa x =
    if      (x >= char_0)&&(x <= char_9)
    then (x - char_0)
    else if (x >= char_A)&&(x <= char_Z)
    then (x - char_A + 10)
    else if (x >= char_a)&&(x <= char_z)
    then (x - char_a + 36)
    else 255

  let naive_int_of_widehexa c = proto_int_of_widehexa (Char.code c)

  let char_of_widehexa_string =
    String.init 256 (fun i -> Char.chr (proto_int_of_widehexa i))
  let unsafe_char_of_widehexa c =
    (* remarq : 0 <= Char.code c <= 255, thus its not unsafe *)
    String.unsafe_get char_of_widehexa_string (Char.code c)
  let char_of_widehexa c =
    let x = unsafe_char_of_widehexa c in
    assert(Char.code x < 255);
    x
  let unsafe_int_of_widehexa c =
    Char.code (unsafe_char_of_widehexa c)
  let int_of_widehexa c =
    let x = unsafe_int_of_widehexa c in
    assert(x<255);
    x

  let unsafe_int_of_hexa = unsafe_int_of_widehexa
  let int_of_hexa c =
    let x = unsafe_int_of_hexa c in
    assert(x<16);
    x

  let unsafe_char_of_hexa = unsafe_char_of_widehexa
  let char_of_hexa c =
    let x = unsafe_char_of_hexa c in
    assert(Char.code x < 16);
    x

  let sized_int_of_widehexa c =
    let x = proto_int_of_widehexa (Char.code c) in
    assert(0<= x && x < 30);
         if x < 16 then (4, x   )
    else if x < 24 then (3, x-16)
    else if x < 28 then (2, x-24)
    else                (1, x-28)

  let sized_char_of_widehexa c =
    let s, x = sized_int_of_widehexa c in
    assert(s>0);
    (s, Char.unsafe_chr x)

  let left_padding (len':int) (pad:char) (s:string) : string =
    let len = String.length s in
    assert(len <= len');
    if len = len' then s else (
      let b = Bytes.make len' pad in
      Bytes.blit_string s 0 b (len'-len) len;
      Bytes.unsafe_to_string b
    )

  let right_padding (len':int) (pad:char) (s:string) : string =
    let len = String.length s in
    assert(len <= len');
    if len = len' then s else (
      let b = Bytes.make len' pad in
      Bytes.blit_string s 0 b 0          len;
      Bytes.unsafe_to_string b
    )

  let char_height_list ?(above=' ') ?(under='.') (len:int) (mat:(char * int)list) : string list =
    let funmap (i:int) (mat:(char * int) list) : string =
      mat
      ||> (fun (c, j) ->
             if j = i
          then c
        else if j < i
          then under
          else above)
      |> list_implode
    in
    let rec loop (carry:string list) (i:int) (len:int) (mat:(char * int) list) : string list =
      if i >= len
      then (List.rev carry)
      else (loop ((funmap i mat)::carry) (succ i) len mat)
    in
    loop [] 0 len mat

  let char_height_array ?(above=' ') ?(under='.') (len:int) (mat:(char * int)array) : string list =
    let funmap (i:int) (mat:(char * int) array) : string =
      mat
      |> Array.map (fun (c, j) ->
             if j = i
          then c
        else if j < i
          then under
          else above)
      |> array_implode
    in
    let rec loop (carry:string list) (i:int) (len:int) (mat:(char * int) array) : string list =
      if i >= len
      then (List.rev carry)
      else (loop ((funmap i mat)::carry) (succ i) len mat)
    in
    loop [] 0 len mat

  (* [roman_of_int n = s] where :
   * - [n] is a positive integer
   * - [s] is the roman representation of [n]
   *)
  let roman_of_int (n:int) : string =
    (* splits the number into (units, tens, hundreds, thousands) *)
    let split n =
      assert ( n >= 0 );
      let u = n mod 10 in
      let n = n / 10 in
      let d = n mod 10 in
      let n = n / 10 in
      let c = n mod 10 in
      let m = n / 10 in
      (u, d, c, m)
    in
    (* parametrized version of the conversion from cifers to string : [0...9] -> string *)
    let string_of_u ?(i='I') ?(v='V') ?(x='X') : int -> string =
      function
      | 0 -> ""
      | n when 1 <= n && n <= 3 -> String.make n i
      | 4 -> (String.make 1 i)^(String.make 1 v)
      | n when 5 <= n && n <= 8 -> (String.make 1 v)^(String.make (n-5) i)
      | 9 -> (String.make 1 i)^(String.make 1 x)
      | _ -> (failwith "[GuaCaml.STools] error")
    in
    (* idems for tens and hundreds *)
    let string_of_d n = string_of_u ~i:'X' ~v:'L' ~x:'C' n in
    let string_of_c n = string_of_u ~i:'C' ~v:'D' ~x:'M' n in
    (* unary encofing of millenia *)
    let string_of_m n = String.make n 'M' in
    (* final call : split + map + combine *)
    let roman_of_int (n:int) : string =
      assert(n>=0);
      let u, d, c, m = split n in
      (string_of_m m)^(string_of_c c)^(string_of_d d)^(string_of_u u)
    in
    roman_of_int n

  let output_string_to_file (target:string) (s:string) : unit =
    let channel = open_out target in
    output_string channel s;
    close_out channel;
    ()

  let input_string_of_file (target:string) : string =
    let channel = open_in target in
    let length = in_channel_length channel in
    let text = really_input_string channel length in
    close_in channel;
    text

  let int_of_char (c:char) : int =
    let n = Char.code c - Char.code '0' in
    if 0 <= n && n <= 9
    then n
    else failwith "[GuaCaml.STools.SUtils.int_of_char] out of range character : cannot be casted to integer"

  let char_of_int (n:int) : char =
    if 0 <= n && n <= 9
    then Char.unsafe_chr (Char.code '0' + n)
    else invalid_arg "[GuaCaml.STools.SUtils.char_of_int] out of range integer : cannot be casted to char"

  let tuple ?(nil="()") ?(fst="( ") ?(sep=", ") ?(lst=" )") (l:string list) : string =
    if l = [] then nil else (fst^(String.concat sep l)^lst)

  let tuple_squarebracket = tuple ~nil:"[ ]" ~fst:"[ " ~sep:"; " ~lst:" ]"

  let tuple_wavybracket = tuple ~nil:"{ }" ~fst:"{ " ~sep:"; " ~lst:" }"

  let record (pairs:(string * string) list) : string =
    tuple ~nil:"{ }" ~fst:"{ " ~sep:"; " ~lst:" }"
      (pairs ||> (fun (name, value) -> name^" = "^value))
end

module ToS =
struct
  type 'a t = 'a -> string
  let ref dump x = "ref("^(dump !x)^")"
  let string item : string = "\""^(String.escaped item)^"\""
  let char item : string = "'"^(String.make 1 item |> String.escaped)^"'"
  let option dump = function
    | None -> "None"
    | Some x -> "Some ("^(dump x)^")"
  let option_default (default:string) dump =
    function
    | None -> default
    | Some x -> dump x
  let list ?(sep="; ") dump list =
    "["^(SUtils.catmap sep dump list)^"]"
  let array ?(sep="; ") dump array =
    "[|"^(SUtils.catmap sep dump (Array.to_list array))^"|]"
  let unit () = "()"
  let bool =
    function true -> "true" | false -> "false"
  let bool' ?(t="true") ?(f="false") =
    function true -> t | false -> f
  let int = string_of_int

  let rec pretty_nat n : string =
    assert(n>=0);
    if n < 1000 then int n
    else (
      let head = n mod 1000 in
      let tail = n / 1000 in
      (pretty_nat tail)^"_"^(SUtils.left_padding 3 '0' (int head))
    )

  let pretty_int n : string =
    if n < 0 then ("-"^(pretty_nat(-n))) else (pretty_nat n)

  let float = string_of_float
  let uno dumpA a = "("^(dumpA a)^")"
  let pair dumpA dumpB (a, b) =
    "("^(dumpA a)^", "^(dumpB b)^")"
  let ( * ) = pair
  let trio dumpA dumpB dumpC (a, b, c) =
    String.concat ""
      ["("; dumpA a; ", "; dumpB b; ", "; dumpC c; ")"]
  let quad dumpA dumpB dumpC dumpD (a, b, c, d) =
    String.concat ""
      ["("; dumpA a; ", "; dumpB b; ", "; dumpC c; ", "; dumpD d; ")"]
  let ignore _ = " _ "

  let constructor ?(sub=true) (name:string) (dump:'a t) (a:'a) : string =
    name^" "^(if sub then ("("^(dump a)^")") else (dump a))

  let hashtbl
       (dumpK:'k t)
       (dumpV:'v t)
       (h:('k, 'v) Hashtbl.t) : string =
    let l =
      h
      |> Hashtbl.to_seq
      |> List.of_seq
    in
    ("Hashtbl.of_seq (List.to_seq "^(list (pair dumpK dumpV) l)^")")

  let ( >> ) s (dump:'a t) : 'a t = fun a -> s^(dump a)
  let ( << ) (dump:'a t) s : 'a t = fun a -> (dump a)^s
end

module ShiftS =
struct
  (* when [int option = None] then display regular string, if [int option = Some tab]
   * shift the element by [tab] tabulation *)
  type t = int option -> string

  let tab_elem = ref "\t"
  let break_elem = ref "\n"
  let break_elem_None = ref " "

  let break : t =
    function
    | None -> !break_elem_None
    | Some _ -> !break_elem

  let of_string (s:string) : t =
    function
    | None -> s
    | Some tab -> (SUtils.ntimes !tab_elem tab)^s

  let tab (f:t) : t =
    fun lvl -> f (Tools.opmap succ lvl)

  let ( ^! ) t1 t2 =
    fun lvl -> (t1 lvl)^(break lvl)^(t2 lvl)

  let merge (sep:t) (lt:t list) : t =
    fun lvl -> SUtils.catmap (sep lvl) (fun f -> f lvl) lt

  let if_None (if_None:string) (else_None:string) =
    function
    | None -> if_None
    | Some _ -> else_None

  let tuple ?(nil="()") ?(fst="(") ?(sep=",") ?(lst=")") : t list -> t =
    function
    | [] -> (of_string nil)
    | lt ->
      let sep : t = fun lvl -> sep^(break lvl) in
      (of_string fst ^! tab (merge sep lt) ^! of_string lst)

  let list (lt:t list) : t =
    tuple ~nil:"[]" ~fst:"[" ~sep:";" ~lst:"]" lt
  let array (at:t array) : t =
    tuple ~nil:"[| |]" ~fst:"[|" ~sep:";" ~lst:"|]" (Array.to_list at)

  let record (pairs:(string * t) list) : t =
    tuple ~nil:"{ }" ~fst:"{" ~sep:"; " ~lst:"}"
      (pairs ||> (fun (name, value) -> (of_string (name^" =")) ^! (tab value)))

  let inline : t -> t =
    fun f -> of_string (f None)

  let conditional_inline cond (f:t) : t =
    if cond then inline f else f

  let uno (t:t) : t    = tuple[t]
  let pair t1 t2       = tuple [t1; t2]
  let ( * ) = pair
  let trio t1 t2 t3    = tuple [t1; t2; t3]
  let quad t1 t2 t3 t4 = tuple [t1; t2; t3; t4]

  let constructor (name:string) (t:t) : t =
    of_string name ^! tab t

  let option : t option -> t =
    function
    | None -> of_string "None"
    | Some t -> constructor "Some" t
end

module ToShiftS =
struct
  type 'a t = 'a -> ShiftS.t

  let of_string = ShiftS.of_string
  let of_ToS dump = fun elem -> of_string (dump elem)
  let ( ^! ) = ShiftS.( ^! )

  let ref dump x = of_string "ref" ^! ShiftS.tab (dump !x)
  let string item = of_string (ToS.string item)
  let char item = of_string (ToS.char item)
  let option dump = fun opx -> ShiftS.option (Tools.opmap dump opx)
  let option_default (default:string) dump =
    function
    | None -> of_string default
    | Some x -> dump x
  let list dump list = ShiftS.list (list ||> dump)
  let array dump array = ShiftS.array (Array.map dump array)

  let unit () = of_string (ToS.unit ())
  let bool b = of_string (ToS.bool b)
  let int i = of_string (ToS.int i)
  let pretty_nat nat = of_string (ToS.pretty_nat nat)
  let pretty_int int = of_string (ToS.pretty_int int)

  let float f = of_string(ToS.float f)
  let uno dumpA a =
    ShiftS.uno (dumpA a)
  let pair dumpA dumpB (a, b) =
    ShiftS.pair (dumpA a) (dumpB b)
  let ( * ) = pair
  let trio dumpA dumpB dumpC (a, b, c) =
    ShiftS.trio (dumpA a) (dumpB b) (dumpC c)
  let quad dumpA dumpB dumpC dumpD (a, b, c, d) =
    ShiftS.quad (dumpA a) (dumpB b) (dumpC c) (dumpD d)

  let constructor (name:string) (dump:'a t) (a:'a) =
    ShiftS.constructor name (dump a)

  let inline (d:'a t) : 'a t =
    fun a -> ShiftS.inline (d a)

  let conditional_inline (cond:bool) (d:'a t) : 'a t =
    if cond then inline d else d

  let hashtbl
       (dumpK:'k t)
       (dumpV:'v t)
       (h:('k, 'v) Hashtbl.t) =
    let l =
      h
      |> Hashtbl.to_seq
      |> List.of_seq
    in
    list (pair dumpK dumpV) l
end

module OfS =
struct
  type 'a t = string -> 'a
  let string item : string = item
  let unit = function
    | "()" -> ()
    | _ -> (failwith "[GuaCaml.STools] error")
  let bool = function
    | "true"  -> true
    | "false" -> false
    | _ -> (failwith "[GuaCaml.STools] error")
  let int = int_of_string
  let float = float_of_string
end

module O3S =
struct
  type 'a t = ('a ToS.t) * ('a OfS.t)
  let string = (ToS.string, OfS.string)
  let unit = (ToS.unit, OfS.unit)
  let bool = ((fun b -> ToS.bool b), OfS.bool)
  let int = (ToS.int, OfS.int)
  let float = (ToS.float, OfS.float)
end

(* [TODO] replace with Opal-based parser *)
module OfFile =
struct
  open Extra
  open Tree

  let unpack_text text =
    let x = String.get text 0 in
    let y = String.sub text 1 (String.length text - 2) in
    let z = String.get text (String.length text - 1) in
    assert(x = '"');
    assert(z = '"');
    Scanf.unescaped y

  let load_leaf strlist =
    let rec aux carry = function
      | []  -> (failwith "[GuaCaml.STools] error")
      | head::tail -> match head with
        | "]"  -> Leaf (String.concat " " (List.rev carry)), tail
        | text  -> aux ((unpack_text text)::carry) tail
    in
    aux [] strlist

  let rec load_tree strlist =
    let rec aux carry = function
      | []  -> (failwith "[GuaCaml.STools] error")
      | head::tail -> match head with
        | ")"  -> Node (List.rev carry), tail
        | "("  -> let tree, tail' = load_tree tail in aux (tree::carry) tail'
        | "["  -> let leaf, tail' = load_leaf tail in aux (leaf::carry) tail'
        | text  -> aux ((Leaf (unpack_text text))::carry) tail
    in aux [] strlist

  (* [TODO] SUtils.split_on_predicate : (char -> bool) -> string -> string list *)
  let load text =
    let strlist : string list =
      text
      |> SUtils.explode
      ||> (function ' ' | '\n' | '\t' -> ' ' | x -> x)
      |> SUtils.implode
      |> SUtils.split ' '
      |> List.filter (function "" -> false | _ -> true)
    in
    let rec aux carry = function
      | [] -> List.rev carry
      | head::tail -> match head with
        | "("  -> let tree, tail' = load_tree tail in aux (tree::carry) tail'
        | "["  -> let leaf, tail' = load_leaf tail in aux (leaf::carry) tail'
        | text  -> aux ((Leaf (unpack_text text))::carry) tail
    in aux [] strlist

  let stream_to_stree stream =
    let pull () =
      try        Some(Iter.next stream)
      with _ -> None
    in
    let blank = function ' ' | '\t' | '\n' -> true | _ -> false in
    let parse_word () =
      let rec aux carry = match pull () with
        | None -> failwith "[GuaCaml/STools/OfFile:stream_to_stree:parse_word] parsing error"
        | Some head -> match head with
          | '\\' -> let c = pull () |> Tools.unop in aux ('\\'::c::carry)
          | '"'  -> Scanf.unescaped (SUtils.implode (List.rev carry))
          |  c   -> aux (c::carry)
      in aux []
    in
    let parse_leaf () =
      let rec aux carry = match pull () with
        | None -> failwith "[GuaCaml/STools/OfFile:stream_to_stree:parse_leaf] parsing error : 0"
        | Some head -> match head with
          | '"' -> let word = parse_word () in aux (word::carry)
          | ']' -> String.concat " " (List.rev carry)
          |  c when blank c -> aux carry
          | _ -> failwith "[GuaCaml/STools/OfFile:stream_to_stree:parse_leaf] parsing error : 1"
      in aux []
    in
    let rec parse_node () =
      let rec aux carry = match pull () with
        | None -> List.rev carry
        | Some head ->
        ( match head with
          | '(' -> let anode = parse_node () in aux ((Node anode)::carry)
          | '[' -> let aleaf = parse_leaf () in aux ((Leaf aleaf)::carry)
          | '"' -> let aword = parse_word () in aux ((Leaf aword)::carry)
          | ')' -> List.rev carry
          |  c when blank c -> aux carry
          | _ -> failwith "[GuaCaml/STools/OfFile:stream_to_stree:parse_node] parsing error"
        )
      in aux []
    in
    parse_node ()

  let of_file target =
    let file = open_in target in
    let stree = file |> Iter.of_channel_as_char |> stream_to_stree in
    close_in file;
    stree
end

module ToSTree =
struct
  type 'a t = 'a -> Tree.stree
  let leaf map item = Tree.Leaf(map item)

  let map f dump item = dump(f item)

  let string = leaf ToS.string
  let bool   = leaf ToS.bool
  let int    = leaf ToS.int
  let float  = leaf ToS.float
  let option dump = function
    | None      -> Tree.Node []
    | Some some -> Tree.Node [dump some]
  let list dump list = Tree.Node (Tools.map dump list)
  let array dump array = list dump (Array.to_list array)
  let unit () = Tree.Node []
  let pair dumpA dumpB (a, b) = Tree.Node [dumpA a; dumpB b]
  let ( * ) = pair
  let trio dumpA dumpB dumpC (a, b, c) = Tree.Node [dumpA a; dumpB b; dumpC c]
  let quad dumpA dumpB dumpC dumpD (a, b, c, d) = Tree.Node [dumpA a; dumpB b; dumpC c; dumpD d]

  let file target = OfFile.of_file target
end

(* Annotated String Tree : Utils *)
module ASTreeUtils =
struct
  open Extra
  open Tree

  type t = (string, string) atree

  let rec output_t output_string : t -> unit =
    function
    | ALeaf s ->
      output_string s
    | ANode (sep, tl) ->
      output_t_list output_string sep tl
  and     output_t_list output_string sep : t list -> unit =
    function
    | [] -> ()
    | [h] -> output_t output_string h
    | h::t -> (
      output_t output_string h;
      output_string sep;
      output_t_list output_string sep t;
    )

  let rec size_t : t -> int =
    function
    | ALeaf s -> String.length s
    | ANode (sep, tl) -> (
      let k = List.length tl in
      match k with
      | 0 -> 0
      | 1 -> size_t_list tl
      | k -> (
        let sep_length = String.length sep in
        (sep_length * (k-1)) + size_t_list tl
      )
    )
  and     size_t_list (tl:t list) : int =
    List.fold_left (fun c t -> c + size_t t) 0 tl

  let to_bytes (t:t) : bytes =
    let len = size_t t in
    let dst = Bytes.make len '\000' in
    let pos = ref 0 in
    let output_string (src:string) : unit =
      let len = String.length src in
      let p = !pos in
      Bytes.blit_string src 0 dst p len;
      pos := p + len
    in
    output_t output_string t;
    dst

  let to_string (t:t) : string =
    t |> to_bytes |> Bytes.unsafe_to_string

  let to_file (target:string) (t:t) : unit =
    let file = open_out target in
    output_t (output_string file) t;
    close_out file;
    ()
end

module ToFile =
struct
  open Extra
  open Tree

  let dump_leaf text =
    match SUtils.split ' ' (String.escaped text) with
      | []  -> "\"\""
      | [x]  -> "\""^x^"\""
      | lx  -> "[ \""^(String.concat "\" \"" lx)^"\" ]"

  let rec dump_tree = function
    | Leaf text -> dump_leaf text
    | Node treelist -> String.concat " " ("("::(treelist ||> dump_tree) @> [")"])

  let output_tree output_string =
    let rec aux = function
      | Leaf text -> output_string(dump_leaf text)
      | Node treelist -> output_string "( "; List.iter (fun tree -> aux tree; output_string " ") treelist; output_string " )";
    in aux

  let to_bytes_size (t:stree) : int =
    let rec tnode = function
      | Leaf text -> 2 + String.length text (* '"text"' *)
      | Node treelist -> 1 + tlist treelist
    and     tlist = function
      | [] -> 2 (* ' )' *)
      | x::tl -> 1 + tnode x + tlist tl (* ' x.tl' *)
    in tnode t

  let to_bytes_put_char (s:bytes) (src:char) (pos:int) : int =
      Bytes.set s pos src;
      (pos + 1)

  let to_bytes_put_string (s:bytes) (src:string) (pos:int) : int =
      let len = String.length src in
      Bytes.blit_string src 0 s pos len;
      (pos + len)

  let rec to_bytes_put_stree (s:bytes) (t:stree) (pos:int) : int =
    let rec to_bytes_put_tnode (s:bytes) (node:stree) (pos:int) =
      match node with
      | Leaf text -> (
        pos
        |> to_bytes_put_char s '"'
        |> to_bytes_put_string s text
        |> to_bytes_put_char s '"'
      )
      | Node treelist -> (
        pos
        |> to_bytes_put_char s '('
        |> to_bytes_put_tlist s treelist
      )
    and     to_bytes_put_tlist (s:bytes) (tl:stree list) (pos:int) =
      match tl with
      | [] -> (
        pos
        |> to_bytes_put_string s " )"
      )
      | hd::tl -> (
        pos
        |> to_bytes_put_char s ' '
        |> to_bytes_put_tnode s hd
        |> to_bytes_put_tlist s tl
      )
    in to_bytes_put_tnode s t pos

  let to_bytes (t:stree) : bytes =
    let len = to_bytes_size t in
    let s = Bytes.make len '\000' in
    let pos = to_bytes_put_stree s t 0 in
    assert(pos = len);
    s

  let to_string (t:stree) : string =
    to_bytes t |> Bytes.unsafe_to_string

  let output_treelist output_string treelist =
    List.iter (fun tree -> output_tree output_string tree; output_string "\n") treelist

  let dump treelist = SUtils.catmap "\n" dump_tree treelist

  let to_file treelist target =
    let file = open_out target in
    output_treelist (output_string file) treelist;
    close_out file;
    ()

  let print_tree = output_tree print_string
  let print_treelist = output_treelist print_string

  let pretty_output_tree output_string =
    let lvlstr lvl text = output_string ((SUtils.ntimes " " lvl)^text^"\n") in
    let rec aux lvl = function
      | Leaf text -> lvlstr lvl text
      | Node treelist -> lvlstr lvl "("; List.iter (aux(lvl+1)) treelist; lvlstr lvl ")"
    in aux 0

  let pprint_v1 treelist =
    List.iter (fun tree -> pretty_output_tree print_string tree; print_newline()) treelist

  (* get colored version of str *)
  let colorize color str =
    if color > 0
    then "\027[" ^ (string_of_int color) ^ "m" ^ str ^ "\027[0m"
    else str

  type enum =
    | T000
    | T001
    | T010
    | T011
    | T100
    | T101
    | T110
    | T111

  (* print colored tree *)
  let pretty_output_treelist_v2 output_string =
    (* draw UTF-8 tree line *)
    let conv = function
      | T000 -> "  "
      | T001 -> "┌ "
      | T010 -> "──"
      | T011 -> "┌─"
      | T100 -> "└ "
      | T101 -> "│ "
      | T110 -> "└─"
      | T111 -> "├─"
    in
    let output_row row =
      output_string (SUtils.catmap""conv(List.rev row));
    in
    let rec tree row0 rows = function
      | Leaf leaf  -> output_row row0; output_string " "; output_string leaf; output_string "\n";
      | Node liste  -> match liste with
        | []         -> output_row row0; output_string "|\n";
        | [head]     -> tree (T010::row0) (T000::rows) head
        | head::tail -> tree (T011::row0) (T101::rows) head; treelist rows tail
    and treelist row = function
      | head::[]     -> tree (T110::row) (T000::row) head
      | head::tail   -> tree (T111::row) (T101::row) head; treelist row tail
      | []           -> ()
    in List.iter (tree [] [])

  let pprint_v2 treelist = pretty_output_treelist_v2 print_string treelist; print_newline()
end

module OfSTree =
struct
  open Tree
  type 'a t = Tree.stree -> 'a

  let map f load item = f(load item)

  let leaf map = function
    | Leaf leaf -> map leaf
    | _ -> failwith "[GuaCaml.STools.OfSTree:leaf] - parsing error"

  let string = leaf OfS.string
  let bool   = leaf OfS.bool
  let int    = leaf OfS.int
  let float  = leaf OfS.float
  let option load = function
    | Node [] -> None
    | Node [some] -> Some(load some)
    | _ -> failwith "[GuaCaml.STools.OfSTree:option] - parsing error"
  let list load = function
    | Node list -> Tools.map load list
    | Leaf _ -> failwith "[GuaCaml.STools.OfSTree:list] - parsing error"
  let array load stree = list load stree |> Array.of_list
  let unit = function
    | Node [] -> ()
    | _ -> failwith "[GuaCaml.STools.OfSTree.unit] - parsing error"
  let pair loadA loadB = function
    | Node [a; b] -> (loadA a, loadB b)
    | _ -> failwith "[GuaCaml.STools.OfSTree.pair] - parsing error"
  let ( * ) = pair
  let trio loadA loadB loadC = function
    | Node [a; b; c] -> (loadA a, loadB b, loadC c)
    | _ -> failwith "[GuaCaml.STools.OfSTree.trio] - parsing error"
  let quad loadA loadB loadC loadD = function
    | Node [a; b; c; d] -> (loadA a, loadB b, loadC c, loadD d)
    | _ -> failwith "[GuaCaml.STools.OfSTree.quad] - parsing error"

  let file treelist target = ToFile.to_file treelist target
  let print treelist = ToFile.print_treelist treelist
  let pprint treelist =  ToFile.pprint_v2 treelist

  let to_bytes t = ToFile.to_bytes t
  let to_string t = ToFile.to_string t
end

module SOut =
struct
  type 'a t = (string -> unit) -> 'a -> unit

  let string p s = p s
  let pretty_option ?(some="Some ") ?(none="None") pa p = function
    | Some a -> p some; pa p a
    | None   -> p none

  let option p s = pretty_option p s
  let pretty_list ?(nil="[]") ?(head="[ ") ?(tail=" ]") ?(sep="; ") (pa:'a t) p l =
    match l with
    | [] -> p nil
    | h::t -> p head; pa p h; List.iter (fun x -> p sep; pa p x) t; p tail
  let list pa p la = pretty_list pa p la
  let pretty_array ?(nil="[| |]") ?(head="[| ") ?(tail=" |]") ?(sep="; ") (a:'a t) p l =
    pretty_list ~nil ~head ~tail ~sep a p (Array.to_list l)
  let array pa p aa = pretty_array pa p aa
  let unit p l = p "()"
  let pretty_bool ?(t="true") ?(f="false") p =
    function true -> p t | false -> p f
  let bool p b = pretty_bool p b
  let int p x = p (string_of_int x)
  let float p x = p (string_of_float x)
  let pair a b p (xa, xb) =
    a p xa; b p xb
  let ( * ) = pair
  let trio pa pb pc p (a, b, c) =
    pa p a; pb p b; pc p c

  let file (pa:'a t) (target:string) (a:'a) : unit =
    let file = open_out target in
    pa (output_string file) a;
    close_out file;
    ()

  let print (pa:'a t) (a:'a) : unit =
    pa print_string a;
    ()
end

let short_stats time0 : string =
  let stat = Gc.quick_stat () in
  let heap = stat.Gc.heap_words * 8 in
  let stack = stat.Gc.stack_size * 8 in
  let top_heap = stat.Gc.top_heap_words * 8 in
     ("{ time="^(ToS.float(Sys.time() -. time0))^
       " heap="^(ToS.pretty_int heap)^
      " stack="^(ToS.pretty_int stack)^
   " top_heap="^(ToS.pretty_int top_heap)^" }")
