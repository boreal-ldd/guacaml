(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Opal_C_Types : Bare Syntax C Parser (written in Opal)
 *
 * === NOTE ===
 *
 * https://cs.wmich.edu/~gupta/teaching/cs4850/sumII06/The%20syntax%20of%20C%20in%20Backus-Naur%20form.htm
 *
 *)

(* Section. Non-Recursive Types *)

type storage_class_specifier =
  | SCS_auto
  | SCS_register
  | SCS_static
  | SCS_extern
  | SCS_typedef

type op_logic_infix =
  | OLI_LazyAnd
  | OLI_BinAnd
  | OLI_LazyOr
  | OLI_BinOr
  | OLI_Xor

type op_arith_infix =
  | OAI_Plus  (* addition *)
  | OAI_Minus (* substraction *)
  | OAI_Mult  (* multiplication *)
  | OAI_Div   (* division *)
  | OAI_Rem   (* remainder *)
  | OAI_LeftShift  (* binary left shift *)
  | OAI_RightShift (* binary right shift *)

type op_compare =
  | OC_Eq  (* == *)
  | OC_Neq (* != *)
  | OC_Le  (* <  *)
  | OC_Leq (* <= *)
  | OC_Ge  (* >  *)
  | OC_Geq (* >= *)

type op_infix =
  | OI_logic of op_logic_infix
  | OI_arith of op_arith_infix
  | OI_compare of op_compare

type op_prefix =
  | OPre_LNot   (* logical not *)
  | OPre_BinNot (* bit-wise negation *)
  | OPre_DeRef  (* return the object located at a given address *)
  | OPre_Addr   (* return the address *)
  | OPre_Incr   (* incrementation *)
  | OPre_Decr   (* decrementation *)
  | OPre_Plus   (* positive *)
  | OPre_Minus  (* negative *)
  | OPre_SizeOf (* size of type *)

type op_postfix =
  | OPost_Incr (* incrementation *)
  | OPost_Decr (* decrementation *)

type identifier = string (* alpha-numerical characters + '_' *)

type string_constant = string
type char_constant = char
type integer_constant = int
type floating_constant = float
type enumeration_constant = identifier

type constant =
  | C_Str   of string_constant
  | C_Char  of char_constant
  | C_Int   of integer_constant
  | C_Float of floating_constant
  | C_Enum  of enumeration_constant

type assignment_operator =
  | AO_Set
  | AO_Mult
  | AO_Div
  | AO_Rem
  | AO_Plus
  | AO_Minus
  | AO_LeftShift
  | AO_RightShift
  | AO_And
  | AO_Or
  | AO_Xor

type struct_or_union = SOU_Struct | SOU_Union

type type_qualifier = TQ_const | TQ_volatile

(* [FIXME] generelize _Bool'n' with 'n' an abitrary integer or nothing ( = 1 ) *)
type type_leaf =
  | TL_Bool
  | TL_Bool2
  | TL_void
  | TL_char
  | TL_short
  | TL_int
  | TL_long
  | TL_float
  | TL_double
  | TL_signed
  | TL_unsigned

(* Section. Pragma *)

type pragma = string

(* Section. Declarator *)

type typedef_name = identifier

type declaration_specifier =
  | DS_StoreClassSpec of storage_class_specifier
  | DS_TypeSpec       of type_specifier
  | DS_TypeQual       of type_qualifier

and struct_declaration = specifier_qualifier list * struct_declarator list
(* [MUST] the [struct_declarator list] should contain at least one element *)

and struct_or_union_specifier = {
  sous_struct_or_union : struct_or_union;
  sous_identifier  : identifier option;
  sous_declaration : struct_declaration list option;
}

and type_specifier =
  | TS_leaf of type_leaf
  | TS_struct_or_union_specifier of struct_or_union_specifier
  | TS_enum_specifier of enum_specifier
  | TS_typedef_name of typedef_name

and specifier_qualifier =
  | SQ_type_specifier of type_specifier
  | SQ_type_qualifier of type_qualifier

(* enum {
    | <identifier> '{' <enumerator_list> '}'
    | '{' <enumerator_list> '}'
    | <identifier>
  }
 *)
and enum_specifier = identifier option * enumerator list

and enumerator = identifier * (constant_expression option)

(* [MUST] at least one of both field must be non-None *)
and struct_declarator = {
  sd_declarator : declarator option;
  sd_value      : constant_expression option;
}

and declarator = pointer_option * direct_declarator

and pointer_option = type_qualifier list list

and direct_declarator_left =
  (* <identifier> *)
  | DDL_identifier of identifier
  (* ( <declarator> ) *)
  | DDL_declarator of declarator

and direct_declarator_right =
  (* '[' {<expression>}? ']' *)
  | DDR_array  of constant_expression option
  (* '(' <parameter_type_list> ')' *)
  | DDR_params of parameter_type_list
  (* '(' {<identifier>}* ')' *)
  | DDR_decls  of identifier list

and direct_declarator =
  direct_declarator_left * direct_declarator_right list

and primary_expression =
  | PE_Identifier of identifier
  | PE_Constant   of constant
  | PE_subexpr    of expression

(*
<postfix-expression> ::=
  | [ <coma-expression> ]
  | ( {<assignment-expression>}* )
  | . <identifier>
  | -> <identifier>
  | ++
  | --
 *)
and expression_postfix =
  | PostE_Crochet  of expression
  | PostE_Bracket  of assignment_expression list
  | PostE_Field    of identifier
  | PostE_PtrField of identifier
  | PostE_Operator of op_postfix

(* { '(' <type_name> ')' }* { <op_prefix> }* <primary_expression> {<expression_postfix>}* *)
and atomic_expression = {
  ae_casts    : type_name list;
  ae_prefixes : op_prefix list;
  ae_primary  : primary_expression;
  ae_postfixes: expression_postfix list;
}

and constant_expression =
  | E_Atomic of atomic_expression
  | E_SizeOf of type_name
  | E_Infix of constant_expression * op_infix * constant_expression (* e1 [op] e2 *)
  | E_Cond  of constant_expression * constant_expression * constant_expression (* c '?' if1 ':' if0 *)

(* <assignment-expression> ::=
    | <constant-expression>
    | <unary-expression> <assignment-operator> <assignment-expression>
 *)
and assignment_expression = (atomic_expression * assignment_operator) list * constant_expression

(* [MUST] at least one element, ',' separated <assigment_expression> *)
and expression = assignment_expression list

(* <type-name> ::= {<specifier-qualifier>}+ {<abstract-declarator>}? *)
and type_name = specifier_qualifier list * (abstract_declarator option)

and parameter_type_list = parameter_list * bool (* [bool] = true iff '...' *)

(* [MUST] at least one element, ',' coma separated <parameter_declaration> *)
and parameter_list = parameter_declaration list

and parameter_declaration_right =
  | PDR_declarator of declarator
  | PDR_abstract_declarator of abstract_declarator

(* [MUST] the left-side term (i.e. the list) must contain at least one element *)
and parameter_declaration =
  declaration_specifier list * (parameter_declaration_right option)

(* [MUST] at least one of both field is non-None *)
and abstract_declarator = AD of pointer_option * (direct_abstract_declarator option)

(* <direct_abstract_declarator_right> ::=
 *    | '[' {<constant-expression>}? ']'
 *    | '(' {<parameter-type-list>}? ')'
 *)

and direct_abstract_declarator_right =
  | DADR_crochet  of constant_expression option
  | DADR_function of parameter_type_list option

and direct_abstract_declarator =
  abstract_declarator option * direct_abstract_declarator_right list

(* <declarator> | <declarator> = <t_initializer> *)
and init_declarator = declarator * (t_initializer option)

(* {<declaration_specifier>}+ {<init_declarator>}* ';' *)
and declaration = declaration_specifier list * init_declarator list

(* coma-separated bracket-tree ( '{' '}' with <assignement_expression> as leaves *)
and t_initializer = assignment_expression Tree.tree

(* Section. Statement *)

(* { <expression> }? ';' *)
type expression_statement = expression option

type jump_statement =
  | JS_goto of identifier
  | JS_continue
  | JS_break
  | JS_return of expression option

type statement =
  | S_declaration of declaration
  | S_labeled of labeled_statement
  | S_expression of expression_statement
  | S_compound of compound_statement
  | S_selection of selection_statement
  | S_iteration of iteration_statement
  | S_jump of jump_statement

and labeled_statement =
  | LS_identified_statement of identifier * statement
  | LS_case of constant_expression * statement
  | LS_default of statement

and compound_statement = statement list

(* <selection-statement> ::=
  | 'if' ( <expression> ) <statement> { 'else' <statement> }?
  | switch ( <expression> ) <statement>
 *)
and selection_statement =
  | SS_ITE of expression * statement * (statement option)
  | SS_Switch of expression * statement

(* <for-statement> ::=
    'for' '(' {<expression>}? ; {<expression>}? ; {<expression>}? ')' <statement>
 *)
and for_statement = {
  fs_init : expression option;
  fs_cond : expression option;
  fs_post : expression option;
  fs_statement : statement;
}

(* <iteration-statement> ::=
      while ( <expression> ) <statement>
    | do <statement> while ( <expression> ) ;
    | <for-statement>
 *)

and iteration_statement =
  | IS_while of expression * statement
  | IS_dowhile of statement * expression
  | IS_for of for_statement

(* Section. #define *)

type ext_define = {
  td_name : identifier;
  td_params : identifier list option;
  td_expr : expression;
}

(* External Declaration *)

(* <function-definition> ::=
    {<declaration-specifier>}* <declarator> {<declaration>}* <compound-statement>
 *)
type function_definition = {
  fd_dec_spec    : declaration_specifier list;
  fd_pragma      : pragma option;
  fd_declarator  : declarator;
  fd_declaration : declaration list;
  fd_compound_statement : compound_statement option;
}

type external_declaration =
  | ED_define of ext_define
  | ED_FunDec of function_definition
  | ED_Dec    of declaration

type translation_unit = external_declaration list
