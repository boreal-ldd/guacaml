(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_OCaml_types_Types : the type of OCaml's types
 *
 * === LATER ===
 *
 * extend to objects
 *
 *)

(* comments are a hierarchical structure :
 *   every sub-comment starting with '(*' and ending with '*)'
 *)
type comment = char Tree.tree

(* [upper_identifier] identifiers starting with an upper case letter *)
type upper_identifier = string

(* [lower_identifier] identifiers starting with a lower case letter (or '_') *)
type lower_identifier = string

(* [abstract_identifier] identifiers starting with the '\'' character *)
type abstract_identifier = string

type module_name = upper_identifier

(* { <module_name> '.' }* *)
type module_path = module_name list

(* identifier used for type definition *)
type type_name_def = lower_identifier

(* identifier used for type reference *)
(* <type-name-ref> ::= <module-path> <type-name-def> *)
type type_name_ref = module_path * type_name_def

(* identifier used for constructor definition *)
type constr_name_def = upper_identifier
(* identifier used for constructor reference *)

(* type identifier regular identifier with lower case first letter or '_' *)
type field_name_def = lower_identifier

(* <atomic-type> ::=
    | <type-name-ref>
    | { tuple '(' ',' ')' <product-type> } <type-name-ref>
    | '(' <product-type> ')'
 *)
type atomic_type =
  | AT_identifier of type_name_ref
  | AT_abstract   of abstract_identifier
  | AT_multi      of product_type list * type_name_ref
  | AT_subtype    of product_type

(* <single-intanciation-type> ::= <atomic-type> { <type-name-ref> }* *)
and single_instanciation_type =  atomic_type * type_name_ref list

(* csv ',' <single-instanctiation-type> *)
and product_type = single_instanciation_type list

(* <record-type> ::=
    tuple+ '{' ';' '}' { <name-identifier> ':' <product-type> }
      (* there is an optional ';' at the end *)
 *)
type record_type  = (field_name_def * product_type) list

type product_or_record_type =
  | PORT_product of product_type
  | PORT_record  of record_type

type sum_type = (constr_name_def * (product_or_record_type option)) list

(* <named-type-right> ::=
  | <product-or-record-type>
  | <sum-type>
 *)
type named_type_definition =
  | NTR_Alias of product_or_record_type
  | NTR_Sum   of sum_type

(* single type declaration
 * NB: 'type' or 'and' has already been parsed
 *)
type named_type = {
  nt_params : abstract_identifier list;
  nt_name   : type_name_def;
  nt_def    : named_type_definition
}

(* list of mutually-recursive types (SCC in the dependency graph) *)
type named_type_rec = named_type list

(* list of list of mutually-recursive types *)
type system_type = named_type_rec list
