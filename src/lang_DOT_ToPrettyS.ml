open Lang_DOT_Types
open STools
open ToS

let id : id t = string

let compass_point  : compass_point t =
  (function
    | CP_N  -> "N"
    | CP_NE -> "NE"
    | CP_E  -> "E"
    | CP_SE -> "SE"
    | CP_S  -> "S"
    | CP_SW -> "SW"
    | CP_W  -> "W"
    | CP_NW -> "NW"
    | CP_C  -> "C"
    | CP_Any -> "_"
  )

let op (f:'a -> string) : 'a option -> string =
  function
  | Some x -> f x
  | None   -> ""

let ( >> ) (x:string) (f:'a -> string) : 'a -> string =
  (fun y -> x^(f y))

let ( << ) (f:'a -> string) (x:string) : 'a -> string =
  (fun y -> (f y)^y)

let pair (f:'a -> string) (g:'b -> string) : ('a * 'b) -> string =
  (fun (x, y) -> (f x)^(g y))

let ( $* ) f g = pair f g

let trio (f:'a -> string) (g:'b -> string) (h:'c -> string) : ('a * 'b * 'c) -> string =
  (fun (x, y, z) -> (f x)^(g y)^(h z))

let port  : port t =
  (op (" : " >> id)) $* (op (" : " >> compass_point))

let node_id  : node_id t =
  id $* (" " >> port)

let edgeop  : edgeop t =
  (function
    | EO_Toward     -> "->"
    | EO_Undirected -> "--"
  )

let subgraph_left  : subgraph_left t =
  "subgraph " >> op id

let colon  : colon t =
  (function
    | SemiColon -> ";"
    | Coma      -> ","
  )

let attr_left  : attr_left t =
  (function
    | AL_Graph -> "graph"
    | AL_Node  -> "node"
    | AL_Edge  -> "edge"
  )

let attr_set  : attr_set t =
  (id << " = ") $* (id << ";")

let until (fst:string) (sep:string) (lst:string) (f:'a -> string) (l:'a list) : string =
  (fst^(SUtils.catmap sep f l)^lst)

let attr_list  : attr_list t =
  until "[ " " " " ]" attr_set

let attr_list_list : attr_list list t =
  until "" " " "" attr_list

let attr_statement  : attr_statement t =
  attr_left $* attr_list_list

let node_statement  : node_statement t =
  node_id $* attr_list_list

let rec edge_right n : edge_right t =
  (fun x -> pair edgeop (" " >> (node_id_or_subgraph n)) x)

and     edge_list_list n : edge_right list t =
  (fun l -> until "" " " "" (edge_right n) l)

and     edge_statement n : edge_statement t =
  (fun x -> trio (node_id_or_subgraph n) (edge_list_list n) attr_list_list x)

and     node_id_or_subgraph n : node_id_or_subgraph t =
  (function
    | NIOS_Node x     -> node_id x
    | NIOS_SubGraph x -> subgraph n x
  )

and     subgraph  : int -> subgraph t =
  (fun n x -> pair (op subgraph_left) (statement_list n) x)

and     statement n : statement t =
  (function
    | Statement_Node x -> node_statement x
    | Statement_Edge x -> edge_statement n x
    | Statement_Attr x -> attr_statement x
    | Statement_Equal (x1, x2) -> (id $* (" = " >> id)) (x1, x2)
    | Statement_SubGraph x -> subgraph n x
  )

(* the level incrementation is done here (and should not be done anywhere else *)
and     statement_list : int -> statement_list t =
  fun n l -> (
    assert(n > 0);
    let space = String.make n '\t' in
    until ("{\n") (";\n"^space^"\t") (space^"}\n") (statement (succ n)) l
  )

let graph  : graph t =
  (fun { graph_strict; graph_directed; graph_id; graph_statement } ->
    (if graph_strict then "strict " else "")^
    (if graph_directed then "digraph " else "graph ")^
    (op id graph_id)^
    (statement_list 0 graph_statement)
  )
