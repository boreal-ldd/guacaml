(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * LexBFS : Implement of LexBFS (Lexicographic Breath First Search)
 *
 * === NOTE ===
 *
 * Freely translated into OCaml from Eppstein's implementation in Python :
 * [LexBFS](https://www.ics.uci.edu/~eppstein/PADS/LexBFS.py)
 *
 * Lexicographic breadth-first-search traversal of a graph, as described
 * in Habib, McConnell, Paul, and Viennot, "Lex-BFS and Partition Refinement,
 * with Applications to Transitive Orientation, Interval Graph Recognition,
 * and Consecutive Ones Testing", Theor. Comput. Sci. 234:59-84 (2000),
 * http://www.cs.colostate.edu/~rmm/lexbfs.ps
 *
 * D. Eppstein, November 2003.
 *)

open Extra

module Make1(MSig:PartitionRefinementEppstein.MSig) =
struct

  module PR = PartitionRefinementEppstein.Make(*Trace*)(MSig)
  module Sequence = CircularDoubleLinkedListEppstein.WithKey

  (* Find lexicographic breadth-first-search traversal order of a graph.
   * G should be represented in such a way that "for v in G" loops through
   * the vertices, and "G[v]" produces a sequence of the neighbors of v; for
   * instance, G may be a dictionary mapping each vertex to its neighbor set.
   * Running time is O(n+m) and additional space usage over G is O(n).
   *)
  type v = MSig.t

  let elt_list_to_string el =
    STools.ToS.list MSig.to_string el

  let set_to_string s =
    s
    |> PR.SET.to_list
    |> elt_list_to_string

  let lexbfs_internal (vertices : v list) (neighbors : v -> v list) : v list =
    let p = PR.make vertices in
    let s : (int, PR.set) Sequence.t = Sequence.of_list PR.SET.id (PR.to_list p) in
    let stack = ref [] in
    let rec lexbfs_loop () =
      (* print_newline(); *)
      (* print_endline ("p:"^(STools.ToS.(list elt_list_to_string) (PR.to_list_list p))); *)
      (* print_endline ("s:"^(STools.ToS.list set_to_string (Sequence.to_list s))); *)
      (* [NOTE] use [Sequence.get_first] and not [Sequence.pull_first]
       * we do not want to systematically remove the first element after
       * retrieving it *)
      match Sequence.get_first_opt s with
      | None -> ()
      | Some set -> (
        assert(PR.SET.is_empty set = false);
        let v = PR.SET.get_any set in
        stack_push stack v;
        PR.remove p v;
        if PR.SET.is_empty set
        then (Sequence.remove s set)
        else (
          let nv = neighbors v in
          (* print_endline ("nv:"^(elt_list_to_string nv)); *)
          let inter_minus_list = PR.refine p nv in
          List.iter (fun (inter, minus) ->
            (* insert [inter] before [minus] in [s] *)
            (* print_endline ("inter:"^(set_to_string inter)); *)
            (* print_endline ("minus:"^(set_to_string minus)); *)
            Sequence.insert_before s minus inter;
          ) inter_minus_list;
        );
        lexbfs_loop ()
      )
    in
    lexbfs_loop ();
    List.rev !stack
end

module Make2(MSig:PartitionRefinementEppstein.MSig) =
struct

  module PR = PartitionRefinementEppstein.Make(*Trace*)(MSig)
  module Sequence = CircularDoubleLinkedList.HashKeySet

  (* Find lexicographic breadth-first-search traversal order of a graph.
   * G should be represented in such a way that "for v in G" loops through
   * the vertices, and "G[v]" produces a sequence of the neighbors of v; for
   * instance, G may be a dictionary mapping each vertex to its neighbor set.
   * Running time is O(n+m) and additional space usage over G is O(n).
   *)
  type v = MSig.t

  let lexbfs_internal (vertices : v list) (neighbors : v -> v list) : v list =
    let p = PR.make vertices in
    let s : (int, PR.set) Sequence.t = Sequence.of_list PR.SET.id (PR.to_list p) in
    let stack = ref [] in
    let rec lexbfs_loop () =
      match Sequence.get_first_opt s with
      | None -> ()
      | Some set -> (
        let v = PR.SET.get_any set in
        stack_push stack v;
        PR.remove p v;
        if PR.SET.is_empty set
        then (Sequence.remove s set)
        else (
          let inter_minus_list = PR.refine p (neighbors v) in
          List.iter (fun (inter, minus) ->
            (* insert [inter] before [minus] in [s] *)
            Sequence.insert_before s minus inter;
          ) inter_minus_list;
        );
        lexbfs_loop ()
      )
    in
    lexbfs_loop ();
    List.rev !stack
end

module IntM =
struct
  type t = int
  let compare = Stdlib.compare
  let to_string = STools.ToS.int
end
module Int1 = Make1(IntM)
module Int2 = Make2(IntM)

module GGLA = GraphGenLA
open GGLA.Type

let lexbfs1 (graph:('v, 'e) graph) : int list =
  let vertices : int list = List.init (Array.length graph) (fun i -> i) in
  let neighbors v = graph.(v).edges ||> fst in
  Int1.lexbfs_internal vertices neighbors

let lexbfs2 (graph:('v, 'e) graph) : int list =
  let vertices : int list = List.init (Array.length graph) (fun i -> i) in
  let neighbors v = graph.(v).edges ||> fst in
  Int2.lexbfs_internal vertices neighbors

module Check =
struct
  exception Break

  (* Returns [true] iff the given enumeration of the vertices of a graph is a LexBFS ordering *)
  let is_lexbfs (g:('v, 'e) graph) (v : int array) : bool =
    let n = Array.length v in
    let neighbors : _ list array = Array.init n (fun i -> g.(v.(i)).edges ||> fst) in
    assert(Array.for_all SetList.sorted_nat neighbors);
    try
      for i = 0 to n - 3 do
        for j = i + 1 to n - 2 do
          for k = j + 1 to n - 1 do
            (* Check the condition for i, j, and k. *)
            if List.mem v.(i) neighbors.(k) && not (List.mem v.(i) neighbors.(j))
            then (
              let found = ref false in
              for m = 0 to i - 1 do
                if List.mem v.(m) neighbors.(j) && not (List.mem v.(m) neighbors.(k))
                then found := true
              done;
              if !found then raise Break
            );
          done;
        done;
      done;
      true
    with Break -> false

end
