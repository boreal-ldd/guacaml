# 2020 / 11 / 12 : 01h20 : Exhaustive Analysis of the carry vs sign bits with Two's Complement representation of arbitrary length integers

Description of the columns :
- 0. $`C`$ : the carry bit arriving at the n-th position
- 1. $`S_0`$ : the sign bit of the first input integer, virtually at the n-th position
- 2. $`S_1`$ : the sign bit of the second input integer, virtually at the n-th position
- 3. $`\Sigma`$ : the cumulate value of the three input values : $`C2^n-S_02^n-S_12^n`$.
- 4. $`R_n`$ : the value of the n-th bit of the result
- 5. $`R_{n+1}`$ : the value of the sign bit the result

| C | $`S_0`$ | $`S_1`$ | $`\Sigma`$ | $`R_n`$ | $`R_{n+1}`$ |
|:-:|:-------:|:-------:|:---------:|:-------:|:-----------:|
|$`2^n`$|$`-2^n`$|$`-2^n`$| 1 | $`2^n`$ | $`-2^{n+1}`$ |
| 0 | 0       | 0       | $`0`$           | 0 | 0 | 
| 0 | 0       | 1       | $`-2^n`$        | 1 | 1 |
| 0 | 1       | 0       | $`-2^n`$        | 1 | 1 |
| 0 | 1       | 1       | $`-2^{n+1}`$    | 0 | 1 |
| 1 | 0       | 0       | $`2^n`$         | 1 | 0 |
| 1 | 0       | 1       | $`0`$           | 0 | 0 |
| 1 | 1       | 0       | $`0`$           | 0 | 0 |
| 1 | 1       | 1       | $`+2^n-2^{n+1}`$| 1 | 1 |

remark: when $`R_n = R_{n+1}`$ we actually do not need to introduce an additional bit to the result. However, this step can be dealt with in a single pass during post-normalization.

One may deduce the following logical formulas from the above enumeration of cases

| name | formula |
|:----:|:--------|
| $`R_n`$ | $`C \oplus S_0 \oplus S_1`$ |
| $`R_{n+1}`$ | $`\texttt{ITE}(C, S_0 \land S_1, S_0 \lor S_1)`$ |
| $`R_{n+1}`$ | $`\lnot C \oplus \left((\lnot C \oplus S_0) \land (\lnot C \oplus S_1)\right)`$ |

We propose two alternative formulation of $`R_n`$ as both have
their pros and cons.

For Example :

On the one hand, the first formulation uses either :
- 1 ITE + 2 AND gates
- 5 AND gates
- 1 XOR gate + 4 AND gate (depending on the translation of the ITE)


On the other hand, the second representation only uses 3 XOR gates and 1 AND gate.
Hence depending on the relative interest of AND vs XOR one formulation might be prefered to the other.

