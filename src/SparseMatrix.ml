(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * SparseMatrix: sparse matrix structure based on FlexArray
 *
 * === AUTHOR ===
 *
 * B. Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes
 *)

(* open PPrint *)

let base = 64

module type MSig = SparseVector.MSig

module type Sig =
sig
  module I : MSig
  module J : MSig
  type 'a t
  val make : ?base:int -> I.s -> J.s -> 'a -> 'a t

  val get_set_i : 'a t -> I.s
  val get_set_j : 'a t -> J.s
  val get_card_i : 'a t -> int
  val get_card_j : 'a t -> int

  val get : 'a t -> I.t -> J.t -> 'a
  val set : 'a t -> I.t -> J.t -> 'a -> unit
  val iter : ('a -> bool) -> (I.t -> J.t -> 'a -> unit) -> 'a t -> unit
  val iter_submatrix : I.sub -> J.sub -> ('a -> bool) -> (I.t -> J.t -> 'a -> unit) -> 'a t -> unit
  val fold : ('a -> bool) -> (I.t -> J.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val iter_row : ('a -> bool) -> (J.t -> 'a -> unit) -> 'a t -> I.t -> unit
  val iter_subrow : J.sub -> ('a -> bool) -> (J.t -> 'a -> unit) -> 'a t -> I.t -> unit
  val iter_col : ('a -> bool) -> (I.t -> 'a -> unit) -> 'a t -> J.t -> unit
end

module Make(MI : MSig)(MJ : MSig) : Sig
  with type I.t   = MI.t
  and  type I.s   = MI.s
  and  type I.sub = MI.sub
  and  type J.t   = MJ.t
  and  type J.s   = MJ.s
  and  type J.sub = MJ.sub
=
struct
  module I = MI
  module J = MJ

  type 'a t = {
    set_i : I.s;
    set_j : J.s;
    card_i : int;
    card_j : int;
    mat : 'a FlexArray.t;
  }

  let get_set_i t = t.set_i
  let get_set_j t = t.set_j
  let get_card_i t = t.card_i
  let get_card_j t = t.card_j

  let index {card_i} i j : int64 =
    let i = Int64.of_int (I.index i)
    and j = Int64.of_int (J.index j)
    and card_i = Int64.of_int card_i in
    Int64.(add (mul card_i i) j)

  let proj {card_i} (x:int64) : int * int =
    let card_i = Int64.of_int card_i in
    let i = Int64.to_int (Int64.rem x card_i)
    and j = Int64.to_int (Int64.div x card_i) in
    (i, j)

  let make ?(base=64) set_i set_j default =
    {
      set_i;
      set_j;
      card_i = I.card set_i;
      card_j = J.card set_j;
      mat = FlexArray.make base default;
    }

  let get m i j =
    let z = index m i j in
    FlexArray.get m.mat z

  let set m i j x =
    let z = index m i j in
    FlexArray.set m.mat z x

  let iter_row test foo m i =
    J.iter
      (fun j ->
         let z = index m i j in
         let x = FlexArray.get m.mat z in
         if test x then foo j x)
      m.set_j

  let iter_subrow subi test foo m i =
    J.iter_subset
      (fun j ->
         let z = index m i j in
         let x = FlexArray.get m.mat z in
         if test x then foo j x)
      subi

  let iter_col test foo m j =
    I.iter
      (fun i ->
         let z = index m i j in
         let x = FlexArray.get m.mat z in
         if test x then foo i x)
      m.set_i

  let iter test foo m =
    I.iter
      (fun i ->
         J.iter
           (fun j ->
              let z = index m i j in
              let x = FlexArray.get m.mat z in
              if test x then foo i j x)
           m.set_j)
      m.set_i

  let iter_submatrix subi subj test foo m =
    I.iter_subset
      (fun i ->
         J.iter_subset
           (fun j ->
              let z = index m i j in
              let x = FlexArray.get m.mat z in
              if test x then foo i j x)
           subj)
      subi

  let fold test foo m y0 =
    let y = ref y0 in
    iter test (fun i j x -> y := foo i j x !y) m;
    !y
end
