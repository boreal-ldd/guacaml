open Extra
open OUnit
open CircularDoubleLinkedList
module DLL = DoubleLinkedList

module CDLL =
struct
  open CDLL
  let test_create () =
    let dll = create 1 in
    error_assert
      "check dll"
      (check dll);
    error_assert
      "dll = [1]"
      (check_items [1] dll);
    error_assert
      "check_structure_forward dll"
      (check_structure_forward [(Some dll, Some dll)] dll);
    ()

  let _ = push_test
    "test_create"
     test_create

  let test_set_prev_succ () =
    let t1 = create 1 in
    let t2 = create 2 in
    internal_set_prev_succ t1 t2;
    error_assert
      "t1.prev = Some t1"
      (t1.prev %== t1);
    error_assert
      "t1.succ = Some t2"
      (t1.succ %== t2);
    error_assert
      "t2.prev = Some t1"
      (t2.prev %== t1);
    error_assert
      "t2.succ = Some t2"
      (t2.succ %== t2);
    ()

  let _ = push_test
    "test_set_prev_succ"
     test_set_prev_succ

  let test_create_before_000 () =
    let dll1 = create 1 in
    let dll2 = create_before dll1 2 in
    error_assert
      "dll1.item = 1"
      (dll1.item = 1);
    error_assert
      "dll2.item = 2"
      (dll2.item = 2);
    error_assert
      "dll1.prev = dll2"
      (dll1.prev %== dll2);
    error_assert
      "dll1.succ = dll2"
      (dll1.succ %== dll2);
    error_assert
      "dll2.succ = dll1"
      (dll2.succ %== dll1);
    error_assert
      "dll2.prev = dll1"
      (dll2.prev %== dll1);
  (*
    let open ToS in
    print_endline ("dll1.item : "^(int dll1.item));
    print_endline ("dll2.item : "^(int dll2.item));
    print_endline ("dll1.prev.item : "^(int (Tools.unop dll1.prev).item));
    assert(dll1.succ = None);
    if not (dll2.prev = None)
    then print_endline ("dll2.prev.item : "^(int (Tools.unop dll2.prev).item));
    print_endline ("dll2.succ.item : "^(int (Tools.unop dll2.succ).item));
   *)
    error_assert
      "check dll1"
      (check dll1);
    error_assert
      "check dll2"
      (check dll2);
    error_assert
      "dll2 = [2; 1]"
      (check_items [2; 1] dll2);
    error_assert
      "check_structure_forward dll2"
      (check_structure_forward [(Some dll1, Some dll1); (Some dll2, Some dll2)] dll2);
    ()

  let _ = push_test
    "test_create_before_000"
     test_create_before_000

  let test_create_before_001 () =
    let dll1 = create 1 in (* [1] *)
    let dll2 = create_after dll1 2 in (* [1; 2] *)
    let dll3 = create_after dll2 3 in (* [1; 2; 3] *)
    let dll4 = create_before dll2 4 in (* [1; 4; 2; 3] *)
    error_assert
      "check dll1"
      (check dll1);
    error_assert
      "dll1 = [1; 4; 2; 3]"
      (check_items [1; 4; 2; 3] dll1);
    error_assert
      "check_structure_forward dll1"
      (check_structure_forward [(Some dll3, Some dll4); (Some dll1, Some dll2); (Some dll4, Some dll3); (Some dll2, Some dll1)] dll1);
    let dll0 = create_before dll2 0 in (* [1; 4; 0; 2; 3] *)
    error_assert
      "check dll1"
      (check dll1);
    error_assert
      "dll1 = [1; 4; 0; 2; 3]"
      (check_items [1; 4; 0; 2; 3] dll1);
    error_assert
      "check_structure_forward dll1"
      (check_structure_forward [(Some dll3, Some dll4); (Some dll1, Some dll0); (Some dll4, Some dll2); (Some dll0, Some dll3); (Some dll2, Some dll1)] dll1);
    ()

  let _ = push_test
    "test_create_before_001"
     test_create_before_001

  let test_create_after () =
    let dll1 = create 1 in
    let dll2 = create_after dll1 2 in
    error_assert
      "check dll1"
      (check dll1);
    error_assert
      "dll1 = [1; 2]"
      (check_items [1; 2] dll1);
    error_assert
      "check_structure_forward dll1"
      (check_structure_forward [(Some dll2, Some dll2); (Some dll1, Some dll1)] dll1);
    ()

  let _ = push_test
    "test_create_after"
     test_create_after

  let test_remove_000 () =
    let dll = create 1 in
    remove dll;
    error_assert
      "check dll"
      (check dll);
    error_assert
      "dll = [1] {DLL.check_items}"
      (DLL.check_items [1] dll);
    error_assert
      "DLL.check_structure_forward dll"
      (DLL.check_structure_forward [(None, None)] dll);
    ()

  let _ = push_test
    "test_remove_000"
     test_remove_000

  let test_remove_001 () =
    let dll1 = create 1 in
    let dll2 = create_before dll1 2 in
    let dll3 = create_before dll2 3 in (* [3; 2; 1] *)
    remove dll2; (* [3; 1] *)
    error_assert "check dll1" (check dll1);
    error_assert "check dll2" (check dll2);
    error_assert "check dll3" (check dll3);
    error_assert
      "dll3 = [3; 1]"
      (check_items [3; 1] dll3);
    error_assert
      "check_structure_forward dll3"
      (check_structure_forward [(Some dll1, Some dll1); (Some dll3, Some dll3)] dll3);
    ()

  let _ = push_test
    "test_remove_001"
     test_remove_001

  let test_create_before_opt_000 () =
    let dll1 = create 1 in
    let dll2 = create_before_opt (Some dll1) 2 in
    error_assert
      "check dll1"
      (check dll1);
    error_assert
      "check dll2"
      (check dll2);
    error_assert
      "dll2 = [2; 1]"
      (check_items [2; 1] dll2);
    error_assert
      "check_structure_forward dll2"
      (check_structure_forward [(Some dll1, Some dll1); (Some dll2, Some dll2)] dll2);
    ()

  let _ = push_test
    "test_create_before_opt_000"
     test_create_before_opt_000

  let test_create_before_opt_001 () =
    let dll3 = create_before_opt None 3 in
    error_assert
      "check dll3"
      (check dll3);
    error_assert
      "dll3 = [3]"
      (check_items [3] dll3);
    error_assert
      "check_structure_forward dll3"
      (check_structure_forward [(Some dll3, Some dll3)] dll3);
    ()

  let _ = push_test
    "test_create_before_opt_001"
     test_create_before_opt_001

  let test_create_after_opt_000 () =
    let dll1 = create 1 in
    let dll2 = create_after_opt (Some dll1) 2 in
    error_assert
      "check dll1"
      (check dll1);
    error_assert
      "dll1 = [1; 2]"
      (check_items [1; 2] dll1);
    error_assert
      "check_structure_forward dll1"
      (check_structure_forward [(Some dll2, Some dll2); (Some dll1, Some dll1)] dll1);
    ()

  let _ = push_test
    "test_create_after_opt_000"
     test_create_after_opt_000

  let test_create_after_opt_001 () =
    let dll3 = create_after_opt None 3 in
    error_assert
      "check dll3"
      (check dll3);
    error_assert
      "dll3 = [3]"
      (check_items [3] dll3);
    error_assert
      "check_structure_forward dll3"
      (check_structure_forward [(Some dll3, Some dll3)] dll3);
    ()

  let _ = push_test
    "test_create_after_opt_001"
     test_create_after_opt_001

  let test_set_item () =
    let dll1 = create 1 in
    set_item dll1 2;
    error_assert
      "check dll1"
      (check dll1);
    error_assert
      "dll1 = [2]"
      (check_items [2] dll1);
    error_assert
      "check_structure_forward dll1"
      (check_structure_forward [(Some dll1, Some dll1)] dll1);
    ()

  let _ = push_test
    "test_set_item"
     test_set_item

  let test_get_item () =
    let dll1 = create 1 in
    let item = get_item dll1 in
    error_assert
      "item = 1"
      (item = 1);
    ()

  let _ = push_test
    "test_get_item"
     test_get_item

  let test_create_after_item_seq_000 () =
    let dll1 = create 1 in
    let dll2 = create_before dll1 2 in
    let dll3 = create_before dll2 3 in
    let dll4 = create_after_item_seq dll1 (List.to_seq [4; 5; 6]) in
    error_assert "check dll1" (check dll1);
    error_assert "check dll2" (check dll2);
    error_assert "check dll3" (check dll3);
    error_assert "check dll4" (check dll4);
    error_assert
      "dll3 = [3; 4; 5; 6; 2; 1]"
      (check_items [3; 2; 1; 4; 5; 6] dll3);
    ()

  let _ = push_test
    "test_create_after_item_seq_000"
     test_create_after_item_seq_000

  let test_create_after_item_seq_001 () =
    let dll1 = create 1 in
    let dll2 = create_after dll1 2 in
    let dll3 = create_after dll2 3 in
    let dll4 = create_after_item_seq dll1 (List.to_seq [4; 5; 6]) in
    error_assert "check dll1" (check dll1);
    error_assert "check dll2" (check dll2);
    error_assert "check dll3" (check dll3);
    error_assert "check dll4" (check dll4);
    error_assert
      "dll1 = [1; 4; 5; 6; 2; 3]"
      (check_items [1; 4; 5; 6; 2; 3] dll1);
    ()

  let _ = push_test
    "test_create_after_item_seq_001"
     test_create_after_item_seq_001

  let test_create_after_item_seq_002 () =
    let dll1 = create 1 in
    let dll2 = create_after dll1 2 in
    let dll3 = create_after dll2 3 in
    let dll4 = create_after_item_seq dll3 (List.to_seq [4; 5; 6]) in
    error_assert "check dll1" (check dll1);
    error_assert "check dll2" (check dll2);
    error_assert "check dll3" (check dll3);
    error_assert "check dll4" (check dll4);
    error_assert
      "dll1 = [1; 2; 3; 4; 5; 6]"
      (check_items [1; 2; 3; 4; 5; 6] dll1);
    ()

  let _ = push_test
    "test_create_after_item_seq_002"
     test_create_after_item_seq_002

  let test_of_item_seq () =
    let fst = of_item_seq (List.to_seq [1; 2; 3]) in
    error_assert
      "check_opt fst"
      (check_opt fst);
    error_assert
      "fst = [1; 2; 3]"
      (check_items_opt [1; 2; 3] fst);
    ()

  let _ = push_test
    "test_of_item_seq"
     test_of_item_seq

  let test_to_list_opt () =
    let fst = of_item_seq (List.to_seq [1; 2; 3]) in
    let l = to_list_opt fst in
    let open ToS in
    print_endline ("\n[print] l = "^STools.ToS.(list int l));
    error_assert
      "l = [1; 2; 3]"
      (l = [1; 2; 3]);
    ()

  let _ = push_test
    "test_to_list_opt"
     test_to_list_opt

  let test_of_list () =
    let l = [1; 2; 3] in
    let fst = of_list l in
    error_assert
      "check_opt fst"
      (check_opt fst);
    error_assert
      "fst = [1; 2; 3]"
      (check_items_opt [1; 2; 3] fst);
    ()

  let _ = run_tests()
end

module HashSet =
struct
  open HashSet

  let test_HashSet_create () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 0);
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = [])

  let _ = push_test "test_HashSet_create" test_HashSet_create

  let test_HashSet_append () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 3);
    (* Check that the elements are in the correct order *)
    assert (HashSet.nth h 0 = "foo");
    assert (HashSet.nth h 1 = "bar");
    assert (HashSet.nth h 2 = "baz");
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = ["foo"; "bar"; "baz"])

  let _ = push_test "test_HashSet_append" test_HashSet_append

  let test_HashSet_mem () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Check that the elements can be found in the HashSet *)
    assert (HashSet.mem h "foo");
    assert (HashSet.mem h "bar");
    assert (HashSet.mem h "baz");
    (* Check that non-existent elements are not found in the HashSet *)
    assert (HashSet.mem h "qux" = false)

  let _ = push_test "test_HashSet_mem" test_HashSet_mem

  let test_HashSet_insert_after () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Insert an element after another element in the HashSet *)
    HashSet.insert_after h "bar" "qux";
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 4);
    (* Check that the elements are in the correct order *)
    assert (HashSet.nth h 0 = "foo");
    assert (HashSet.nth h 1 = "bar");
    assert (HashSet.nth h 2 = "qux");
    assert (HashSet.nth h 3 = "baz");
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = ["foo"; "bar"; "qux"; "baz"])

  let _ = push_test "test_HashSet_insert_after" test_HashSet_insert_after

  let test_HashSet_insert_before_000 () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Insert an element before another element in the HashSet *)
    HashSet.insert_before h "foo" "qux";
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 4);
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = ["qux"; "foo"; "bar"; "baz"])

  let _ = push_test "test_HashSet_insert_before_000" test_HashSet_insert_before_000

  let test_HashSet_insert_before_001 () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Insert an element before another element in the HashSet *)
    HashSet.insert_before h "bar" "qux";
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 4);
    (* Check that the elements are in the correct order *)
    assert (HashSet.nth h 0 = "foo");
    assert (HashSet.nth h 1 = "qux");
    assert (HashSet.nth h 2 = "bar");
    assert (HashSet.nth h 3 = "baz");
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = ["foo"; "qux"; "bar"; "baz"])

  let _ = push_test "test_HashSet_insert_before_001" test_HashSet_insert_before_001

  let test_HashSet_remove () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Remove an element from the HashSet *)
    HashSet.remove h "bar";
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 2);
    (* Check that the elements are in the correct order *)
    assert (HashSet.nth h 0 = "foo");
    assert (HashSet.nth h 1 = "baz");
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = ["foo"; "baz"])

  let _ = push_test "test_HashSet_remove" test_HashSet_remove

  let test_HashSet_of_list () =
    (* Create a new HashSet from a list of elements *)
    let h = HashSet.of_list ["foo"; "bar"; "baz"] in
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 3);
    (* Check that the elements are in the correct order *)
    assert (HashSet.nth h 0 = "foo");
    assert (HashSet.nth h 1 = "bar");
    assert (HashSet.nth h 2 = "baz");
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = ["foo"; "bar"; "baz"])

  let _ = push_test "test_HashSet_of_list" test_HashSet_of_list

  let test_HashSet_iter () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Check that iterating over the hash set returns the elements in the correct order *)
    let lst = ref [] in
    HashSet.iter (fun x -> lst := x :: !lst) h;
    assert (!lst = ["baz"; "bar"; "foo"]);
    ()

  let _ = push_test "test_HashSet_iter" test_HashSet_iter

  let test_HashSet_to_string () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Check the string representation of the hash set *)
    let str_h = HashSet.to_string (fun x -> x) h in
    print_endline "str_h:";
    print_endline str_h;
    assert (str_h = "CDLL.HashSet.of_list [foo; bar; baz]");
    (* Check another string representation of the hash set *)
    let str_h = HashSet.to_string STools.ToS.string h in
    print_endline "str_h:";
    print_endline str_h;
    assert (str_h = "CDLL.HashSet.of_list [\"foo\"; \"bar\"; \"baz\"]");
    ()

  let _ = push_test "test_HashSet_to_string" test_HashSet_to_string

  let test_HashSet_of_list_empty () =
    (* Create a new empty HashSet from a list of elements *)
    let h = HashSet.of_list [] in
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 0);
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = [])

  let _ = push_test "test_HashSet_of_list_empty" test_HashSet_of_list_empty

  let test_HashSet_to_list_empty () =
    (* Create a new empty HashSet *)
    let h = HashSet.create () in
    (* Check the length of the HashSet *)
    assert (HashSet.length h = 0);
    (* Check the list representation of the HashSet *)
    assert (HashSet.to_list h = [])

  let _ = push_test "test_HashSet_to_list_empty" test_HashSet_to_list_empty

  let test_HashSet_get_succ () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Check the successor of an element in the HashSet *)
    assert (HashSet.get_succ h "bar" = "baz");
    (* Check the successor of the last element in the HashSet *)
    assert (HashSet.get_succ h "baz" = "foo")

  let _ = push_test "test_HashSet_get_succ" test_HashSet_get_succ

  let test_HashSet_get_prev () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Check the predecessor of an element in the HashSet *)
    assert (HashSet.get_prev h "bar" = "foo");
    (* Check the predecessor of the first element in the HashSet *)
    assert (HashSet.get_prev h "foo" = "baz")

  let _ = push_test "test_HashSet_get_prev" test_HashSet_get_prev

  let test_HashSet_get_first () =
    (* Create a new HashSet *)
    let h = HashSet.create () in
    (* Add some elements to the HashSet *)
    HashSet.append h "foo";
    HashSet.append h "bar";
    HashSet.append h "baz";
    (* Check the first element in the HashSet *)
    assert (HashSet.get_first h = "foo");
    (* Remove the first element from the HashSet *)
    HashSet.remove h "foo";
    (* Check the new first element in the HashSet *)
    assert (HashSet.get_first h = "bar")

  let _ = push_test "test_HashSet_get_first" test_HashSet_get_first

  let test_HashSet_get_first_opt () =
    (* Create a new empty HashSet *)
    let h = HashSet.create () in
    (* Check the first element in the empty HashSet *)
    assert (HashSet.get_first_opt h = None);
    (* Add an element to the HashSet *)
    HashSet.append h "foo";
    (* Check the first element in the HashSet *)
    assert (HashSet.get_first_opt h = Some "foo")

  let _ = push_test "test_HashSet_get_first_opt" test_HashSet_get_first_opt

  (* Run all the unit tests *)
  let _ = run_tests()
end

module HashKeySet =
struct
  open HashKeySet

  let test_HashKeySet_create () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 0);
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = [])

  let _ = push_test "test_HashKeySet_create" test_HashKeySet_create

  let test_HashKeySet_append () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 3);
    (* Check that the elements are in the correct order *)
    assert (HashKeySet.nth h 0 = "foo");
    assert (HashKeySet.nth h 1 = "bar");
    assert (HashKeySet.nth h 2 = "baz");
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = ["foo"; "bar"; "baz"])

  let _ = push_test "test_HashKeySet_append" test_HashKeySet_append

  let test_HashKeySet_mem () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Check that the elements can be found in the HashKeySet *)
    assert (HashKeySet.mem h "foo");
    assert (HashKeySet.mem h "bar");
    assert (HashKeySet.mem h "baz");
    (* Check that non-existent elements are not found in the HashKeySet *)
    assert (HashKeySet.mem h "qux" = false)

  let _ = push_test "test_HashKeySet_mem" test_HashKeySet_mem

  let test_HashKeySet_insert_after () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Insert an element after another element in the HashKeySet *)
    HashKeySet.insert_after h "bar" "qux";
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 4);
    (* Check that the elements are in the correct order *)
    assert (HashKeySet.nth h 0 = "foo");
    assert (HashKeySet.nth h 1 = "bar");
    assert (HashKeySet.nth h 2 = "qux");
    assert (HashKeySet.nth h 3 = "baz");
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = ["foo"; "bar"; "qux"; "baz"])

  let _ = push_test "test_HashKeySet_insert_after" test_HashKeySet_insert_after

  let test_HashKeySet_insert_before () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Insert an element before another element in the HashKeySet *)
    HashKeySet.insert_before h "bar" "qux";
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 4);
    (* Check that the elements are in the correct order *)
    assert (HashKeySet.nth h 0 = "foo");
    assert (HashKeySet.nth h 1 = "qux");
    assert (HashKeySet.nth h 2 = "bar");
    assert (HashKeySet.nth h 3 = "baz");
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = ["foo"; "qux"; "bar"; "baz"])

  let _ = push_test "test_HashKeySet_insert_before" test_HashKeySet_insert_before

  let test_HashKeySet_remove () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Remove an element from the HashKeySet *)
    HashKeySet.remove h "bar";
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 2);
    (* Check that the elements are in the correct order *)
    assert (HashKeySet.nth h 0 = "foo");
    assert (HashKeySet.nth h 1 = "baz");
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = ["foo"; "baz"])

  let _ = push_test "test_HashKeySet_remove" test_HashKeySet_remove

  let test_HashKeySet_of_list () =
    (* Create a new HashKeySet from a list of elements *)
    let h = HashKeySet.of_list STools.SUtils.explode ["foo"; "bar"; "baz"] in
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 3);
    (* Check that the elements are in the correct order *)
    assert (HashKeySet.nth h 0 = "foo");
    assert (HashKeySet.nth h 1 = "bar");
    assert (HashKeySet.nth h 2 = "baz");
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = ["foo"; "bar"; "baz"])

  let _ = push_test "test_HashKeySet_of_list" test_HashKeySet_of_list

  let test_HashKeySet_iter () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Check that iterating over the hash set returns the elements in the correct order *)
    let lst = ref [] in
    HashKeySet.iter (fun _ x -> lst := x :: !lst) h;
    assert (!lst = ["baz"; "bar"; "foo"]);
    ()

  let _ = push_test "test_HashKeySet_iter" test_HashKeySet_iter

  let test_HashKeySet_to_string () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Check the string representation of the hash set *)
    let str_h = HashKeySet.to_string (fun x -> x) h in
    print_endline "str_h:";
    print_endline str_h;
    assert (str_h = "CDLL.HashKeySet.of_list [foo; bar; baz]");
    (* Check another string representation of the hash set *)
    let str_h = HashKeySet.to_string STools.ToS.string h in
    print_endline "str_h:";
    print_endline str_h;
    assert (str_h = "CDLL.HashKeySet.of_list [\"foo\"; \"bar\"; \"baz\"]");
    ()

  let _ = push_test "test_HashKeySet_to_string" test_HashKeySet_to_string

  let test_HashKeySet_of_list_empty () =
    (* Create a new empty HashKeySet from a list of elements *)
    let h = HashKeySet.of_list STools.SUtils.explode [] in
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 0);
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = [])

  let _ = push_test "test_HashKeySet_of_list_empty" test_HashKeySet_of_list_empty

  let test_HashKeySet_to_list_empty () =
    (* Create a new empty HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Check the length of the HashKeySet *)
    assert (HashKeySet.length h = 0);
    (* Check the list representation of the HashKeySet *)
    assert (HashKeySet.to_list h = [])

  let _ = push_test "test_HashKeySet_to_list_empty" test_HashKeySet_to_list_empty

  let test_HashKeySet_get_succ () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Check the successor of an element in the HashKeySet *)
    assert (HashKeySet.get_succ h "bar" = "baz");
    (* Check the successor of the last element in the HashKeySet *)
    assert (HashKeySet.get_succ h "baz" = "foo")

  let _ = push_test "test_HashKeySet_get_succ" test_HashKeySet_get_succ

  let test_HashKeySet_get_prev () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Check the predecessor of an element in the HashKeySet *)
    assert (HashKeySet.get_prev h "bar" = "foo");
    (* Check the predecessor of the first element in the HashKeySet *)
    assert (HashKeySet.get_prev h "foo" = "baz")

  let _ = push_test "test_HashKeySet_get_prev" test_HashKeySet_get_prev

  let test_HashKeySet_get_first () =
    (* Create a new HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Add some elements to the HashKeySet *)
    HashKeySet.append h "foo";
    HashKeySet.append h "bar";
    HashKeySet.append h "baz";
    (* Check the first element in the HashKeySet *)
    assert (HashKeySet.get_first h = "foo");
    (* Remove the first element from the HashKeySet *)
    HashKeySet.remove h "foo";
    (* Check the new first element in the HashKeySet *)
    assert (HashKeySet.get_first h = "bar")

  let _ = push_test "test_HashKeySet_get_first" test_HashKeySet_get_first

  let test_HashKeySet_get_first_opt () =
    (* Create a new empty HashKeySet *)
    let h = HashKeySet.create STools.SUtils.explode in
    (* Check the first element in the empty HashKeySet *)
    assert (HashKeySet.get_first_opt h = None);
    (* Add an element to the HashKeySet *)
    HashKeySet.append h "foo";
    (* Check the first element in the HashKeySet *)
    assert (HashKeySet.get_first_opt h = Some "foo")

  let _ = push_test "test_HashKeySet_get_first_opt" test_HashKeySet_get_first_opt

  (* Run all the unit tests *)
  let _ = run_tests()
end
