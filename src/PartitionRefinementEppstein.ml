(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * PartitionRefinement : A Partition Refinement Data-Structure
 *
 * === NOTE ===
 *
 * [Partition Refinement (Wikipedia)](https://en.wikipedia.org/wiki/Partition_refinement)
 *
 * === NOTE ===
 *
 * Freely translated into OCaml from Eppstein's implementation in Python :
 * [PartitionRefinement](https://www.ics.uci.edu/~eppstein/PADS/PartitionRefinement.py)
 *
 * Maintain and refine a partition of a set of items into subsets,
 * as used e.g. in Hopcroft's DFA minimization algorithm,
 * modular decomposition of graphs, etc.
 *
 * D. Eppstein, November 2003.
 *
 *)

open Extra

module Make(Ord:Set.OrderedType) =
struct
  (* [LATER] extract as mutable set ? *)
  module SET =
  struct
    module SET = Set.Make(Ord)
    type elt = SET.elt

    (* having a global reference is not very safe
     * we could make it safer by creating a manager
     * to mitigate the side effects *)
    let global_id = ref 0

    type t = {
      id : int;
      mutable set : SET.t;
    }

    let empty () : t =
      let id = !++ global_id in
      { id; set = SET.empty }

    let of_list (items:elt list) : t =
      let t = empty () in
      t.set <- SET.of_list items;
      t

    let id (t:t) : int = t.id

    let add (t:t) (e:elt) : unit =
      t.set <- SET.add e t.set

    let remove (t:t) (e:elt) : unit =
      t.set <- SET.remove e t.set

    let iter (f:elt -> unit) (t:t) : unit =
      SET.iter f t.set

    let minus (t:t) (t':t) : unit =
      t.set <- SET.diff t.set t'.set

    let get_any (t:t) : elt =
      try
        SET.min_elt t.set
      with Not_found ->
        failwith "[PartitionRefinementEppstein.get_any] [t] is empty"

    let is_empty (t:t) : bool =
      SET.is_empty t.set

    let to_seq (t:t) : elt Seq.t =
      SET.to_seq t.set

    let to_list (t:t) : elt list =
      List.of_seq (to_seq t)

    let equal t1 t2 : bool = t1.id = t2.id

    let equiv t1 t2 : bool = SET.equal t1.set t2.set

  end
  type elt = SET.elt
  type set = SET.t

  (* Maintain and refine a partition of a set of items into subsets.
   * Space usage for a partition of n items is O(n), and each refine
   * operation takes time proportional to the size of its argument.
   *)
  type t = {
    sets : (int, set) Hashtbl.t;
    partition : (elt, set) Hashtbl.t;
  }

  let make (items:elt list) : t =
    (* Create a new partition refinement data structure for the given
     * items.  Initially, all items belong to the same subset.
     *)
    let s = SET.of_list items in
    let sets = Hashtbl.create 128 in
    let partition = Hashtbl.create 128 in
    Hashtbl.replace sets (SET.id s) s;
    List.iter (fun item -> Hashtbl.replace partition item s) items;
    {sets; partition}

  let set_of_elt (self:t) (e:elt) : set =
    (* Return the set that contains the given element. *)
    Hashtbl.find self.partition e

  let iter (f:set -> 'a) (self:t) : unit =
    Hashtbl.iter (fun _ s -> f s) self.sets

  let to_list (self:t) : set list =
    Tools.to_list_of_iter iter self

  let to_list_list ?(sort=false) (self:t) : elt list list =
    let out = to_list self ||> SET.to_list in
    if sort then List.sort Stdlib.compare out else out

  let length (self:t) : int =
    (* Return the number of sets in the partition. *)
    Hashtbl.length self.sets

  let add (self:t) (element:elt) (theset:set) : unit =
    (* Add a new element to the given partition subset. *)
    if not (Hashtbl.mem self.sets (SET.id theset))
    then (failwith "[GuaCaml/PartitionRefinementEppstein.add] Set does not belong to the partition");
    if Hashtbl.mem self.partition element
    then (failwith "[GuaCaml/PartitionRefinementEppstein.add] Element already belongs to the partition");
    SET.add theset element;
    Hashtbl.replace self.partition element theset;
    ()

  let remove (self:t) (element:elt) : unit =
    (* Remove the given element from its partition subset. *)
    let theset = Hashtbl.find self.partition element in
    SET.remove theset element;
    Hashtbl.remove self.partition element;
    if SET.is_empty theset
    then Hashtbl.remove self.sets (SET.id theset);
    ()

  (* Refine each set A in the partition to the two sets :
   * (A \inter S), (A \minus S).
   * Return a list of pairs (A \inter S, A \minus S) for each changed set.
   * Within each pair, A \inter S will be a newly created set,
   * while (A \minus S) will be a modified version of an existing set in
   * the partition.
   *)
  let refine (self:t) (s:elt list) : (set * set) list =
    (* initialize [hit] an empty association map, which relates
     * some components of the decomposition to the its subset within
     * [s] *)
    let hit = Hashtbl.create (Hashtbl.length self.sets) in
    (* for x in S:
     *   if x in self._partition:
     *     Ax = self._partition[x]
     *     hit.setdefault(id(Ax),set()).add(x)
     *)
    (* For each element [x] in [s] such that [x] is an element of
     * the partition.
     * retrieve [ax] the set associated with [x] in the [partition] hashtbl
     * if [ax] is already register in [hit] add [x] to the related set
     * otherwise associate the singletong [x] to [ax] in [hit]
     *)
    List.iter (fun x ->
      match Hashtbl.find_opt self.partition x with
      | None -> () (* just ignore *)
      | Some ax -> (
        (* [LATER]
         * could be replaced by a generic lazy :
         *   [Hashtbl.set_default hit (SET.id ax) (fun () -> SEP.empty ())]
         * or just
         *   [Hashtbl.set_default hit (SET.id ax) SET.empty]
         *)
        let sx =
          match Hashtbl.find_opt hit (SET.id ax) with
          | Some set -> set
          | None -> (
            let set = SET.empty () in
            Hashtbl.add hit (SET.id ax) set;
            set
          )
        in
        SET.add sx x
      )
    ) s;
    let output = ref [] in
    Hashtbl.iter (fun a aset ->
      let a = Hashtbl.find self.sets a in
      if SET.equiv a aset = false
      then  (
        Hashtbl.replace self.sets (SET.id aset) aset;
        SET.iter (fun x ->
          Hashtbl.replace self.partition x aset
        ) aset;
        SET.minus a aset; (* a -= aset *)
        stack_push output (aset, a)
      )
    ) hit;
    let output = List.rev !output in
    assert(List.for_all (fun (inter, minus) ->
      (SET.is_empty inter = false) &&
      (SET.is_empty minus = false)
    ) output);
    output
end

module type MSig =
sig
  include Set.OrderedType

  val to_string : t -> string
end

module MakeTrace(M:MSig) =
struct
  module REF = Make(M)
  (* [LATER] extract as mutable set ? *)
  module SET =
  struct
    open REF.SET

    type elt = REF.SET.elt
    type t = REF.SET.t

    module ToS =
    struct
      open STools
      open ToS

      let elt = M.to_string

      let set (set:SET.t) : string =
        set
        (* Functorize as Set.to_string ? *)
        |> REF.SET.SET.to_seq
        |> List.of_seq
        |> list elt

      let t (t:REF.SET.t) : string =
        SUtils.record
        [ ("id", int t.id);
          ("set", set t.set) ]
    end

    let empty () : REF.SET.t =
      print_endline "SET.empty ()";
      REF.SET.empty ()

    let of_list (items:elt list) : REF.SET.t =
      print_endline ("SET.of_list "^(STools.ToS.list ToS.elt items));
      REF.SET.of_list items

    let id (t:REF.SET.t) : int =
      REF.SET.id t

    let add (t:REF.SET.t) (e:elt) : unit =
      print_endline ("SET.add "^(ToS.t t)^" "^(ToS.elt e));
      REF.SET.add t e

    let remove (t:REF.SET.t) (e:elt) : unit =
      print_endline ("SET.remove "^(ToS.t t)^" "^(ToS.elt e));
      REF.SET.remove t e

    let iter (f:elt -> unit) (t:REF.SET.t) : unit =
      print_endline ("SET.iter _ "^(ToS.t t));
      REF.SET.iter f t

    let minus (t:REF.SET.t) (t':REF.SET.t) : unit =
      print_endline ("SET.minus "^(ToS.t t)^" "^(ToS.t t'));
      REF.SET.minus t t'

    let get_any (t:REF.SET.t) : elt =
      print_endline ("SET.get_any "^(ToS.t t));
      REF.SET.get_any t

    let is_empty (t:REF.SET.t) : bool =
      print_endline ("SET.is_empty "^(ToS.t t));
      REF.SET.is_empty t

    let to_seq (t:REF.SET.t) : elt Seq.t =
      print_endline ("SET.to_seq "^(ToS.t t));
      REF.SET.to_seq t

    let to_list (t:REF.SET.t) : elt list =
      print_endline ("SET.to_list "^(ToS.t t));
      REF.SET.to_list t
  end

  type elt = REF.elt
  type set = REF.set

  (* Maintain and refine a partition of a set of items into subsets.
   * Space usage for a partition of n items is O(n), and each refine
   * operation takes time proportional to the size of its argument.
   *)
  type t = REF.t

  module ToS =
  struct
    open STools
    open ToS

    let elt = SET.ToS.elt
    let set = SET.ToS.t

    let t (t:REF.t) : string =
      SUtils.record [
        ("sets", hashtbl int set t.REF.sets);
        ("partition", hashtbl elt set t.REF.partition);
      ]
  end

  (* Create a new partition refinement data structure for the given
   * items. Initially, all items belong to the same component.
   *)
  let make (items:elt list) : t =
    print_endline ("PR.make "^(STools.ToS.list ToS.elt items));
    REF.make items

  (* Return the set that contains the given element. *)
  let set_of_elt (self:t) (e:elt) : set =
    print_endline ("PR.set_of_elt "^(ToS.t self)^" "^(ToS.elt e));
    REF.set_of_elt self e

  let iter (f:set -> 'a) (self:t) : unit =
    print_endline ("PR.iter _ "^(ToS.t self));
    REF.iter f self

  let to_list (self:t) : set list =
    print_endline ("PR.to_list "^(ToS.t self));
    REF.to_list self

  let to_list_list ?(sort=false) (self:t) : elt list list =
    print_endline ("PR.to_list_list "^(ToS.t self));
    REF.to_list_list ~sort self

  (* Return the number of sets in the partition. *)
  let length (self:t) : int =
    print_endline ("PR.length "^(ToS.t self));
    REF.length self

  (* Add a new element to the given partition subset. *)
  let add (self:t) (element:elt) (theset:set) : unit =
    print_endline ("PR.add "^(ToS.t self)^" "^(ToS.elt element)^" "^(ToS.set theset));
    REF.add self element theset

  (* Remove the given element from its partition subset. *)
  let remove (self:t) (element:elt) : unit =
    print_endline ("PR.remove "^(ToS.t self)^" "^(ToS.elt element));
    REF.remove self element

  (* Refine each set A in the partition to the two sets :
   * (A \inter S), (A \minus S).
   * Return a list of pairs (A \inter S, A \minus S) for each changed set.
   * Within each pair, A \inter S will be a newly created set,
   * while (A \minus S) will be a modified version of an existing set in
   * the partition.
   *)
  let refine (self:t) (s:elt list) : (set * set) list =
    print_endline ("PR.refine "^(ToS.t self)^" "^(STools.ToS.list ToS.elt s));
    REF.refine self s
end
