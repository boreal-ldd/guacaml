module GGLA = GraphGenLA
open GGLA.Type
open GGLA_HFT.Type
open Examples_graphs
open GGLA_SeparativeClique
open OUnit

let to_hft (g:(unit, unit)graph) : hg =
  Array.map
    (fun {index; edges} ->
      {
        index;
        label = {vlabel_names = [index]; vlabel_weight = 1; vlabel_fixed = false; vlabel_useful = true};
         edges
      }
    )
    g
  |> GGLA_HFT.normalize

let examples = Array.map (fun (name, example) -> (name, to_hft example)) graph_unit_unit

let _ = Array.iter (fun (name, example) -> print_endline name; ignore(GGLA_KIC.FindSeq.naive example)) examples

let _ = Array.iter (fun (name, example) -> print_endline name; ignore(compute example)) examples
