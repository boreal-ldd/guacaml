(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA_SeparativeClique : Computing Separative Clique on GGLA
 *
 * === NOTES ===
 *
 * We found several similar methods to compute minimal separative cliques
 * 1. compute a K-Decomposition
 * 2. compute the interface between its components
 * 3. check for each interface if the induced subgraph is a clique in
 *      the input graph.
 * 4. compute a clique-based tree-decomposition
 *      (i.e. a tree-decomposition where interfaces are cliques)
 *)

open Extra
module GGLA = GraphGenLA
open GGLA.Type
open GGLA_HFT
open GGLA_HFT.Type

(* Section 1.A compute a K-Decomposition in O(N^2(N+M) + N(N+M)(log D)) *)
(* the worst case of O(N^2(N+M) + N(N+M)(log D)) is O(N^4) with M := N^2 *)
(* 0. apply auto-rename to the graph (each vertex is renamed with its index)
 * 1. we do so by finding some K-vertex in the graph in O(N(N+M))
 *   testing if an H-vertex is a K-vertex is a linear-time operation,
 *   there is at most, N vertices to test
 * 2. once a K-vertex has been found, we vertex-contract it, and go back to
 *    step 1 in O((N+M)(log D))
 * 3. once a suppressing K-sequence has been computed, we retrieve the
 *    corresponding K-decomposition.
 *)

(* Section 1.B alternate implementation of arbitrary K-Decomposition in
 * O(NM(log D)) *)
(* the worst case of O(NM(log D)) is O(N^3(log D)) *)
(* variation of Section 1.A, by modifying the step (1) to use LexBFS, to
 * identify a K-vertex in linear time O(N+M)
 *)

module TD = TreeDecomposition

let find_kd (hg:hg) : int TD.t =
  hg
  |> GGLA_KIC.FindSeq.using_lexbfs
  |> TD.of_sequence hg

(* Section 2. compute the interfaces between each component *)

let compute_interfaces (t:int TD.t) : int list array = TD.compute_interfaces t

(* Section 3. check for each interface if the induced subgraph is a
 *   clique in the input graph.
 *)

let test_clique (hg:hg) (interfaces:int list array) : bool array =
  Array.map (GGLA.Utils.graph_is_clique hg) interfaces

(* Section 4. compute a clique-based tree-decomposition *)

let merge_at_interfaces (t:int TD.t) (keep:bool array) : int TD.t =
  TD.merge_at_interfaces t keep

(* Section. Conclusion *)

let compute (hg:hg) : int TD.t option =
  (* print_endline ("[GGLA_SeparativeClique.compute] hg:"^(GGLA_HFT.ToS.hg ~short:false hg)); *)
  let hg = GGLA_HFT.autorename_vertex hg in
  (* print_endline ("[GGLA_SeparativeClique.compute] autorename(hg):"^(GGLA_HFT.ToS.hg ~short:false hg)); *)
  let kd = find_kd hg in
  (* print_endline ("[GGLA_SeparativeClique.compute] kd:"^(TreeDecomposition.ToS.t STools.ToS.int kd)); *)
  let is = compute_interfaces kd in
  let keep = test_clique hg is in
  if Array.for_all (fun x -> x) keep (* every interface is a clique separator *)
  then Some kd
  else if Array.exists (fun x -> x) keep (* there exists a clique separator *)
  then Some(merge_at_interfaces kd keep)
  else None (* there is no clique separator *)
