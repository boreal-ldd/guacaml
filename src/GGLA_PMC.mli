(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraGenLA : Generic Adjacency List Graph
 *
 * PMC : potential maximal clique
 *
 * === CONTRIBUTORS ===
 *
 * Joan Thibault
 *  - joan.thibault@irisa.fr
 * Maxime Bridoux
 *  - maxime.bridoux@ens-rennes.fr
 *)

(* Component : (Component, Separator) *)
type component = int list * int list

  (* indirection to [GGLA.connected_components_and_separators]
   * [connected_components_and_separators g idents = [(component, separator)]]
   *  (we assume [SetList.sorted idents])
   *  returns a list of triplet, each triplet correspond to one connected component of G\K :
   *  1. component : list of vertices C
   *  2. excluded : set S of vertices of K which are adjacent to C
   *)
val connected_components_and_separators : GGLA_HFT.Type.hg -> int list -> component list

(* [vm_is_ks cif_list] returns [true] iff there is not full component *)
val vm_is_ks : int list -> component list -> bool

(* [vm_is_kvm G K cif_list] returns [true] iff K is cliquish in G *)
val vm_is_kvm : GGLA_HFT.Type.hg -> int list -> component list -> bool

(* [is_pmc hg idents = bool]
    returns true iff [idents] (i.e., K) is a pmc in the graph [hg] (i.e., G) *)
val is_pmc : GGLA_HFT.Type.hg -> int list -> bool

val enum_pmc : GGLA_HFT.Type.hg -> bool array list
val enum_pmc_from_seed : GGLA_HFT.Type.hg -> bool array -> bool array list
val enum_pmc_from_seed_with_bound : GGLA_HFT.Type.hg -> bool array -> int -> bool array list

(* [DEBUG] *)
module EnumBoundedPMC :
sig
  val explore3_is_pmc_calls : (GGLA_HFT.Type.hg * bool option array * int) list ref
end

module EnumUtils :
sig
  type leaf = int list
  (* a set of vertices forming a PMC in the vertex-minor graph *)
  type node = int list array option
  (* each index maps a vertex of the current level to a vertex of the previous level *)
  (* if [node = None] then it is the identity array *)
  type tree = (node, leaf) Tree.atree

  type explicit_tree = int list Tree.tree

  module ToS :
  sig
    open STools.ToS

    val leaf : leaf t
    val node : node t
    val tree : tree t
  end

  val explicit_tree : tree -> explicit_tree
end

module EnumPMC2 :
sig
  val implicit_enum_from_seed : GGLA_HFT.Type.hg -> bool array -> EnumUtils.tree

  (* [enum_from_seed t s = (pmc_list, nnode)] where:
   *   - [pmc_list] is the list of PMC extended pmc_list
   *   - [nnode] is the number of intermediary computation leading to this result
   *)
  val enum_from_seed : GGLA_HFT.Type.hg -> bool array -> bool array list * int
end

module EnumPMC3 :
sig
  val implicit_enum_from_seed : ?bound:int option -> GGLA_HFT.Type.hg -> bool array -> EnumUtils.tree

  (* [enum_from_seed t s = (pmc_list, nnode)] where:
   *   - [pmc_list] is the list of PMC extended pmc_list
   *   - [nnode] is the number of intermediary computation leading to this result
   *)
  val enum_from_seed : GGLA_HFT.Type.hg -> bool array -> bool array list * int
  val enum_from_seed_with_bound : GGLA_HFT.Type.hg -> bool array -> int -> bool array list * int
end

module EnumPMC4 :
sig
  val implicit_enum_from_seed : GGLA_HFT.Type.hg -> bool array -> EnumUtils.tree

  (* [enum_from_seed t s = (pmc_list, nnode)] where:
   *   - [pmc_list] is the list of PMC extended pmc_list
   *   - [nnode] is the number of intermediary computation leading to this result
   *)
  val enum_from_seed : GGLA_HFT.Type.hg -> bool array -> bool array list * int
end
