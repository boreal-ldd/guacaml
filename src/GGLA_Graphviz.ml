(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraGenLA : Generic Adjacency List Graph
 *
 * === NOTE ===
 *
 * neighbors : prefer "neighbors" rather than "neighbours" or "neighborhood"
 *)

open Extra
open STools
open BTools
open GraphGenLA

module ToS =
struct
  open GraphGenLA.Type
  open STools.ToS

  let edge_to_string
    ?(directed=true)
    ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
     (src:int) (vlabel:'v) (dst:int) (elabel:'e) : string =
    let join = if directed then " -> " else " -- " in
    "\tN"^(int src)^join^"N"^(int dst)^(ce src vlabel dst elabel)^";\n"

  let vertex_to_string
    ?(directed=true)
    ?(cv=((fun _ _ -> ""):int -> 'v -> string))
    ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
     (v:('v, 'e)vertex) : string =
    let src = v.index in
    "\tN"^(int src)^(cv src v.label)^"\n"^
    (String.concat ""
      (MyList.opmap
        (fun (dst, elabel) ->
          if directed || src < dst
          then Some(edge_to_string ~directed ~ce src v.label dst elabel)
          else None
        ) v.edges)
    )

  let graph_to_string
    ?(directed=true)
    ?(name="")
    ?(cv=((fun _ _ -> ""):int -> 'v -> string))
    ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
     (g:('v, 'e)graph) : string =
    (if directed then "digraph" else "graph")^" "^name^" "^"{\n\t"^
    (SUtils.catmap_array "" (vertex_to_string ~directed ~cv ~ce) g)^"\n}"
end

module ToASTree =
struct
  open GraphGenLA.Type
  open STools.ToS
  open Tree
  open STools.ASTreeUtils

  let edge_to_astree
      ?(directed=true)
      ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
         (src:int) (vlabel:'v) (dst:int) (elabel:'e) : t =
    let join = if directed then " -> " else " -- " in
    ANode ("", [ALeaf "\tN"; ALeaf (int src); ALeaf join; ALeaf "N"; ALeaf (int dst); ALeaf (ce src vlabel dst elabel); ALeaf ";\n"])

  let vertex_to_astree
      ?(directed=true)
      ?(cv=((fun _ _ -> ""):int -> 'v -> string))
      ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
         (v:('v, 'e)vertex) : t =
    let src = v.index in
    let vline = ANode ("", [ALeaf "\tN"; ALeaf (int src); ALeaf (cv src v.label); ALeaf "\n"]) in
    let eline_list =
      MyList.opmap
        (fun (dst, elabel) ->
          if directed || src < dst
          then Some(edge_to_astree ~directed ~ce src v.label dst elabel)
          else None
        ) v.edges
    in
    ANode ("", vline::eline_list)

  let graph_to_astree
      ?(directed=true)
      ?(name="")
      ?(cv=((fun _ _ -> ""):int -> 'v -> string))
      ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
         (g:('v, 'e)graph) : t =
    let head = ANode ("", [ALeaf (if directed then "digraph" else "graph"); ALeaf " "; ALeaf name; ALeaf " {\n\t"]) in
    let astree_core = g
      |> Array.to_list
      ||> vertex_to_astree ~directed ~cv ~ce
    in
    let tail = ALeaf "\n}" in
    ANode ("", [head; ANode ("", astree_core); tail])

  let graph_to_string
      ?(directed=true)
      ?(name="")
      ?(cv=((fun _ _ -> ""):int -> 'v -> string))
      ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
         (g:('v, 'e)graph) : string =
    g
    |> graph_to_astree ~directed ~name ~cv ~ce
    |> to_string

  let graph_to_file
      ?(directed=true)
      ?(name="")
      ?(cv=((fun _ _ -> ""):int -> 'v -> string))
      ?(ce=((fun _ _ _ _ -> ""):int -> 'v -> int -> 'e -> string))
         (target:string) (g:('v, 'e)graph) : unit =
    g
    |> graph_to_astree ~directed ~name ~cv ~ce
    |> to_file target
end

