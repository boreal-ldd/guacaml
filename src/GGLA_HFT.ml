(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA_HFT : GGLA-based implementation of H-FT-Graph
 *
 * === NOTES ===
 *
 * H-Graphs are undirected graphs where true twins (i.e., vertices with
 * identical neighbors) are merged, such transformation is called
 * H-reduction
 *
 * === NOTES ===
 *
 * H-FT-Graph
 * House - Fixed Terminals - Graphs
 * This kind of graph is defined for WAP (Weighted Adjacency Propagation) Problem.
 * House = true-twins are merged
 * Fixed Terminals = some vertices cannot be supressed,
 *   thus cannot merge with regular vertices
 *)

open Extra
open STools
open BTools
module GGLA = GraphGenLA
open GGLA.Type

module Type =
struct
  type vlabel = {
    vlabel_names  : int list;
    vlabel_weight : int;
    vlabel_fixed  : bool;
    (*
      true  => Normal
      false => NoKill
     *)
    vlabel_useful : bool;
    (*
      true  => the vertex is used
      false => the vertex is not used
     *)
  }

  type elabel = unit

  type hv = (vlabel, unit) vertex
  type hg = (vlabel, unit) graph

  type hgl = (vlabel list, elabel list) graph
end
open Type

module ToS =
struct
  open ToS
  open GGLA.ToS

  let vlabel ?(short=true) (vt:vlabel) =
    if short
    then (
      "{vlabel}{ names="^(list int vt.vlabel_names)^
      "; weight="^(int vt.vlabel_weight)^
      "; fixed="^(bool vt.vlabel_fixed)^
      "; useful="^(bool vt.vlabel_useful)^" }"
    )
    else (
      "{ vlabel_names="^(list int vt.vlabel_names)^
      "; vlabel_weight="^(int vt.vlabel_weight)^
      "; vlabel_fixed="^(bool vt.vlabel_fixed)^
      "; vlabel_useful="^(bool vt.vlabel_useful)^" }"
    )

  let elabel ?(short=true) () = unit ()

  let hv ?(short=true) hv = vertex (vlabel ~short) (elabel ~short) hv
  let hg ?(short=true) hg = graph  (vlabel ~short) (elabel ~short) hg

  let pprint_hg ?(short=true) hg =
    pprint_graph (vlabel ~short) (elabel ~short) hg

  let pprint_ophg ?(short=true) hg =
    pprint_opgraph (vlabel ~short) (elabel ~short) hg
end

module ToShiftS =
struct
  open ToShiftS
  open GGLA.ToShiftS

  let vlabel : vlabel t =
    fun vt ->
      ShiftS.inline (
        ShiftS.record [
          ("vlabel_names" , list int vt.vlabel_names);
          ("vlabel_weight", int vt.vlabel_weight);
          ("vlabel_fixed" , bool vt.vlabel_fixed);
          ("vlabel_useful", bool vt.vlabel_useful);
        ]
      )

  let elabel = unit

  let hv ?(inline=false) hv = vertex ~inline vlabel elabel hv
  let hg ?(inline=false) hg = graph  ~inline vlabel elabel hg
end

module ToB =
struct
  open ToB
  open GGLA.ToB

  let vlabel (vt:vlabel) (s:stream) : stream =
    quad (list int) int bool bool (vt.vlabel_names, vt.vlabel_weight, vt.vlabel_fixed, vt.vlabel_useful) s

  let elabel = unit

  let hv (hv:hv) (s:stream) : stream = vertex vlabel elabel hv s
  let hg (hg:hg) (s:stream) : stream = graph  vlabel elabel hg s
end

module OfB =
struct
  open OfB
  open GGLA.OfB

  let vlabel (s:stream) : vlabel * stream =
    let (vlabel_names, vlabel_weight, vlabel_fixed, vlabel_useful), stream = quad (list int) int bool bool s in
    {vlabel_names; vlabel_weight; vlabel_fixed; vlabel_useful}, stream

  let elabel = unit

  let hv (s:stream) : hv * stream = vertex vlabel elabel s
  let hg (s:stream) : hg * stream = graph  vlabel elabel s
end

let hg_to_bin (hg:hg) : barray =
  BTools.ToB.closure ToB.hg hg
let hg_of_bin (ba:barray) : hg =
  BTools.OfB.closure OfB.hg ba

let print_vertex ?(short=true) (v:hv) : unit =
  print_string (ToS.hv ~short v)
let print_graph ?(short=true) (g:hg) : unit =
  ToS.pprint_hg ~short g

let vertex_names  (v:hv) : int list =
  v.label.vlabel_names
let vertices_names (h:hg) (il:int list) : int list =
  il ||> (fun i -> vertex_names h.(i)) |> SetList.union_list

let vertex_weight (v:hv) : int =
  v.label.vlabel_weight
let vertex_fixed  (v:hv) : bool =
  v.label.vlabel_fixed
let vertex_alive  (v:hv) : bool =
  not (vertex_fixed v)
let vertex_useful (v:hv) : bool =
  v.label.vlabel_useful

let vertex_fixed_set (hg:hg) (i:int) (vlabel_fixed:bool) : unit =
  let label = {hg.(i).label with vlabel_fixed} in
  hg.(i) <- {hg.(i) with label}

let vertex_alive_set (hg:hg) (i:int) (alive:bool) : unit =
  vertex_fixed_set hg i (not alive)

let weight_of_set (g:hg) (set:int list) : int =
  MyList.map_sum (fun i -> vertex_weight g.(i)) set

let weight_of_selection (g:hg) (a:bool array) : int =
    weight_of_set g (MyArray.list_of_indexes_true a)

(* [TODO] Check for H-reduction *)
let check ?(hred=false) (g:hg) =
  GGLA.Check.graph g &&
  GGLA.Utils.graph_is_undirected g &&
  GGLA.Utils.graph_is_reflexive ~refl:false g &&
  (if hred
   then GGLA.TrueTwinsFree.compute_naive g vertex_fixed
   else true)

let vlabel_add (v1:vlabel) (v2:vlabel) : vlabel =
  if v1.vlabel_fixed <> v2.vlabel_fixed
  then failwith "[GuaCaml.GGLA_HFT.vlabel_add] [error] fields [vlabel_fixed] are not equal";
  if v1.vlabel_useful <> v1.vlabel_useful
  then print_endline "[GuaCaml.GGLA_HFT.vlabel_add] [warning] fields [vlabel_useful] are not equal";
  {
    vlabel_names = SetList.union v1.vlabel_names v2.vlabel_names;
    vlabel_weight = v1.vlabel_weight + v2.vlabel_weight;
    vlabel_fixed = v1.vlabel_fixed;
    vlabel_useful = v1.vlabel_useful || v1.vlabel_useful;
  }

(* Collapses a list-extend H-Graph to a regular H-Graph *)
let hg_of_hgl (gl:hgl) : hg =
  let map_vlabel_list (vl:vlabel list) : vlabel =
    match vl with
    | [] -> (failwith "[GuaCaml.GGLA_HFT.hg_of_hgl] error")
    | v0 :: vl' ->
      List.fold_left vlabel_add v0 vl'
  in
  let map_elabel_list (el:elabel list) : elabel = ()
  in
  GGLA.Utils.graph_map_both
    map_vlabel_list
    map_elabel_list
    gl
  |> GGLA.Utils.graph_rm_reflexive
  |> Tools.check_print ~debug_only:true (check ~hred:true)
    (fun hg ->
      let open STools.ToS in
      print_endline  "[hg_of_hgl] [error] \"[hg] is not reduced\"";
      print_endline ("[hg_of_hgl] [error] hg:"^(ToS.hg hg));
      print_endline ("[hg_of_hgl] [error] check_graph:"^(bool (GGLA.Check.graph hg)));
      print_endline ("[hg_of_hgl] [error] is_undirected:"^(bool (GGLA.Utils.graph_is_undirected hg)));
      print_endline ("[hg_of_hgl] [error] (is_reflexive ~refl:false):"^(bool (GGLA.Utils.graph_is_reflexive ~refl:false hg)));
      print_endline  "[hg_of_hgl] [error] end";
    )

(* Alternative Versions of [GGLA.TrueTwin.compute_*] *)
let compute_truetwins_naive (hg:hg) =
  GGLA.TrueTwins.compute_naive hg vertex_fixed

let compute_truetwins_naiveD (hg:hg) =
  GGLA.TrueTwins.compute_naiveD hg vertex_fixed

let compute_truetwins_naive_pseudoquad (hg:hg) =
  GGLA.TrueTwins.compute_naive_pseudoquad hg vertex_fixed

let compute_truetwins_pseudolinear (hg:hg) =
  GGLA.TrueTwins.compute_pseudolinear hg vertex_fixed

let normalize (hg:hg) : hg =
  (*
    print_newline();
    print_graph hg;
    print_newline();
   *)
  (* remark: compute_cmp = compute_pseudolinear *)
  let qt = GGLA.TrueTwins.compute_cmp hg vertex_fixed in
  hg_of_hgl qt.GGLA.Component.qgraph
  |> Tools.check_print ~debug_only:true (check ~hred:true)
    (fun hg' ->
      print_string "[GGLA_HFT.normalize] [error] begin"; print_newline();
      print_string "[GGLA_HFT.normalize] [error] (GGLA.TrueTwinsFree.compute_naive hg' vertex_fixed = false)"; print_newline();
      print_string "[GGLA_HFT.normalize] [error] hg:"; print_newline();
      print_graph ~short:false hg; print_newline();
      print_string "[GGLA_HFT.normalize] [error] hg':"; print_newline();
      print_graph ~short:false hg'; print_newline();
      print_string "[GGLA_HFT.normalize] [error] end"; print_newline();
    )

let vertex_contraction (hg:hg) (iv:int) : hg =
  let v = hg.(iv) in
  (* adding a clique on the adjacency of the vertex [v] *)
  (* we may chose to not use the "~reflexive_false:true"
     option of graph_add_clique, as reflexives edges are
     removed again later (and may appear during merging) *)
  let hgK = GGLA.Utils.graph_add_clique ~replace:true ~reflexive_false:true hg v.edges in
  (* vertex [v] is removed *)
  let hg' = GGLA.Utils.graph_rm_vertex v.index hgK in
  (* the result is normalized *)
  (* [TODO] perform local normalization *)
  normalize hg'

(*
  ft : fixed terminals (default = [], no ft-vertex)
  [default_useful = true] all vertices are labelged as useful
  [default_useful = false] only vertices with at least one edge (including reflexive ones) are labelged as useful
 *)
let of_GraphLA ?(ft=([]:int list)) ?(default_useful=false) (g:GraphLA.graph) : hg =
  let len = Array.length g.GraphLA.edges in
  (* fta : fixed terminal array *)
  let fta = MyArray.of_indexes len ft in
  (* fixed.(i) = fta.(i) *)
  let map_vertex (index:int) (el:int list) =
    assert(SetList.sorted_nat el);
    let vlabel_useful = default_useful || el <> [] in
    let edges =
      SetList.minus el [index]
      |> Tools.map (fun y -> (y, ()))
    in
    Some {
      index;
      label = {
        vlabel_names  = [index];
        vlabel_weight = 1;
        vlabel_fixed  = fta.(index);
        vlabel_useful;
      };
      edges;
    }
  in
  let ophg = Array.mapi map_vertex g.GraphLA.edges in
  let dead = MyArray.of_indexes len g.GraphLA.nodes in
  Array.iteri (fun i b -> if not b then ophg.(i) <- None) dead;
  let hg = GGLA.Utils.unop_opgraph ophg in
  if not (check ~hred:false hg)
  then (ToS.pprint_hg hg; (failwith "[GuaCaml.GGLA_HFT] error"));
  normalize hg

let neighbors_weight (g:hg) (iv:int) : int =
  let v = g.(iv) in
  List.fold_left
    (fun card (iu,()) -> card + vertex_weight g.(iu))
    (vertex_weight v)
     v.edges

let total_weight (hg:hg) : int =
  Array.fold_left (fun w hv -> w + vertex_weight hv) 0 hg

let wap_Sc (g:hg) (iv:int) : BNat.nat =
  BNat.pow2 (neighbors_weight g iv)

let call_to_wap_S = ref 0
let reset_call_to_wap_S () : int  =
  let value = !call_to_wap_S in
  call_to_wap_S:=0;
  value

let wap_S (g:hg) (iv:int) : BNat.nat * hg =
  incr call_to_wap_S;
  let sc = wap_Sc g iv in
  let sg = vertex_contraction g iv in
  (sc, sg)

let wap_S' (g:hg) (iv:int) : int * BNat.nat * hg =
  incr call_to_wap_S;
  let k = neighbors_weight g iv in
  let sc = BNat.pow2 k in
  let sg = vertex_contraction g iv in
  (k, sc, sg)

let wap_SS (g:hg) (ivl:int list) : BNat.nat * hg =
  List.fold_left (fun (sc, sg) iv ->
    let sc', sg' = wap_S sg iv in
    (BNat.(sc +/ sc'), sg')
  ) (BNat.zero(), g) ivl

let wap_SS_get_names (g:hg) (ivl:int list) : BNat.nat * hg * int list list =
  let sc, sg, vll =
    List.fold_left (fun (sc, sg, vll) iv ->
      let vl' = vertex_names sg.(iv) in
      let sc', sg' = wap_S sg iv in
      (BNat.(sc +/ sc'), sg', vl' :: vll)
    ) (BNat.zero(), g, []) ivl
  in (sc, sg, List.rev vll)

let graph_set_fixed (vl:int list) ?(fixed=false) (hg:hg) : hg =
  if vl = []
  then hg
  else (
    let hg = Array.copy hg in
    let vlabel_fixed = fixed in
    List.iter (fun index ->
      let label = hg.(index).label in
      hg.(index) <- {hg.(index) with label = {label with vlabel_fixed}}) vl;
    hg
  )

let call_to_wap_R = ref 0
let reset_call_to_wap_R () : int  =
  let value = !call_to_wap_R in
  call_to_wap_R:=0;
  value

let wap_R (hg:hg) (vl:int list) : hg list =
  incr call_to_wap_R;
  let len = Array.length hg in
  assert(List.for_all (fun iv -> 0 <= iv && iv < len) vl);
  (* we may chose to not use the "~reflexive_false:true"
     option of graph_add_clique, as reflexives edges are
     removed again later (and may appear during merging) *)
  let hgK = GGLA.Utils.graph_add_clique ~replace:true ~reflexive_false:true hg (vl ||> (fun iv -> (iv,()))) in
  (* change status to [fixed = true] on [vl] vertices *)
  let hgK = graph_set_fixed vl hgK in
  (* decomposition into connected components *)
  let hgKi, _, _ = GGLA.Connected.compute vertex_fixed hgK in
  hgKi |> Array.to_list ||> normalize

let find_pendant_vertex (hg:hg) : int option =
  let rec fpv_rec hg i =
         if i >= Array.length hg
      then None
    else if List.length hg.(i).edges = 1
      then Some i
      else fpv_rec hg (succ i)
  in fpv_rec hg 0

let color_of_vertex label : string =
  match label.vlabel_fixed, label.vlabel_useful with
  | true, _ -> "black"
  | false, true -> "grey"
  | false, false -> "white"

let to_graphviz_vertex index label : string =
  let open STools.ToS in
  let s_weight = int label.vlabel_weight in
  let s_color  = string (color_of_vertex label) in
  String.concat "" ["[label = "; s_weight; "; color="; s_color; "]"]

let to_graphviz_string ?(astree=true) ?(name="") (hg:hg) : string =
  let cv = to_graphviz_vertex in
  if astree
  then (GGLA_Graphviz.ToASTree.graph_to_string ~directed:false ~name ~cv hg)
  else (GGLA_Graphviz.ToS.graph_to_string ~directed:false ~name ~cv hg)

let to_graphviz_file ?(astree=true) ?(name="") (target:string) (hg:hg) : unit =
  if astree
  then (
    let cv = to_graphviz_vertex in
    GGLA_Graphviz.ToASTree.graph_to_file ~directed:false ~name ~cv target hg
  )
  else (SUtils.output_string_to_file target (to_graphviz_string ~astree ~name hg))

let hft_label_of_ft_label ?(revers:int array option) (index:int) : GGLA_FT.Type.vlabel -> Type.vlabel =
  let open GGLA_FT.Type in
  match revers with
  | None -> (
    fun {useful; fixed} ->
      {
        vlabel_names = [index];
        vlabel_weight = 1;
        vlabel_fixed  = fixed;
        vlabel_useful = useful;
      }
  )
  | Some revers -> (
    fun {useful; fixed} ->
      {
        vlabel_names = [revers.(index)];
        vlabel_weight = 1;
        vlabel_fixed  = fixed;
        vlabel_useful = useful;
      }
  )

let of_FT ?(remove_useless=false) (g:GGLA_FT.Type.hg) : hg =
  if remove_useless
  then (
    let ru = GGLA_FT.remove_useless g in
    let revers = ru.GGLA_FT.ru_revers in
    ru.GGLA_FT.ru_graph
    |> GGLA.Utils.graph_mapi_both (hft_label_of_ft_label ~revers) (fun _ _ () -> ())
    |> normalize
  )
  else (
    g
    |> GGLA.Utils.graph_mapi_both hft_label_of_ft_label (fun _ _ () -> ())
    |> normalize
  )

(* returns an H-FT-Graph matching the KD-graph given as input
 * vertices apearing in the input KD-graph and labelged as fixed are
 * effectively labeled as fixed (i.e. fixed vertices which are not used
 * are not added)
 * ~remove_useless:false (by default) [kd.nodes] is used as graph's domain
 * ~remove_useless:true [support kd] is used as graph's domain
 *)
let of_GraphKD ?(verbose=0) ?(remove_useless=false) (kd:GraphKD.graph) : hg =
  assert(GraphKD.check kd);
  (* renaming vertices to induce a tight namespace *)
  let profile (text:string) =
    if verbose > 0
    then OProfile.(profile default) ("[GuaCaml.GGLA_HFT.of_GraphKD] "^text)
    else (fun f -> f)
  in
  let nodes = if remove_useless then GraphKD.support kd else kd.nodes in
  let n = List.length nodes in
  let h = Hashtbl.create n in
  let r = Hashtbl.create n in
  List.iteri (fun i v ->
    Hashtbl.add h v i;
    Hashtbl.add r i v;
  ) nodes;
  let nodes = MyList.init n (fun i -> i) in
  let fixed : int list = MyList.opmap (Hashtbl.find_opt h) kd.fixed in
  let cliques = kd.cliques ||> (fun k -> k ||> Hashtbl.find h) in
  let kd : GraphKD.graph = {nodes; fixed; cliques} in
  assert(GraphKD.check kd);
  kd
  |> profile "GGLA_FT.of_GraphKD" GGLA_FT.of_GraphKD
  |> profile "of_FT" of_FT
  |> GGLA.Utils.graph_map_vlabel
    (fun label ->
      {label with vlabel_names = Tools.map (Hashtbl.find r) label.vlabel_names}
    )

let autorename_vertex (h:hg) : hg =
  Array.map
    (fun v ->
      let label = {v.label with vlabel_names = [v.index]} in
      {v with label}
    )
    h
