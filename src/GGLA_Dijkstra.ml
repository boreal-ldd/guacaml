(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GGLA_Dijkstra : Basic Implementation of Dijkstra's Algorithm
 *
 * === NOTE ===
 *
 * [TODO] add unit test and functional tests
 *
 *)

module GGLA = GraphGenLA
open GGLA.Type

module Make(W:Set.OrderedType) =
struct
  type w = W.t

  module PQ = PriorityQueue.MakeMin(W)
  (* [dijkstra graph source = (distances, predecessors)]
   * Dijkstra's algorithm for finding the shortest path from a source vertex to
   * all other vertices in a graph.
   *
   * This function returns a list of distances from the source vertex to all other
   * vertices, as well as a list of predecessors for each vertex, which can be used
   * to reconstruct the shortest path.
   *)
  let dijkstra
      (weight_of_edge : int -> 'v -> int -> 'e -> w)
      (* ( zero : unit -> w) *)
      ( (+/) : w -> w -> w )
      (graph: ('v, 'e) graph)
      (sources: ((int * w) list)) : w option array * int option array =
    let n = Array.length graph in
    (* Create an array to mark visited vertices *)
    let visited = Array.make n false in
    (* Create an array to store the distances from the source vertex to all other
       vertices, initialized to infinity (max_int)
    *)
    let distances = Array.make n None in

    (* Create an array to store the predecessor of each vertex in the shortest
     * path from the source vertex.
     *)
    let predecessors = Array.make n None in

    (* Create a priority queue to store the vertices that have not yet been
       processed, with the distance from the source vertex as the priority
    *)
    let queue = PQ.empty() in

    (* add all source vertices in the queue with their corresponding initial
     * weight
     *)
    List.iter (fun (v, w) -> PQ.add queue w v) sources;

    (* While the queue is not empty, process the vertex with the smallest distance
       from source vertices
     *)
    PQ.while_not_empty queue (fun wi i ->
      if visited.(i) = false
      then (
        let vi = graph.(i) in
        visited.(i) <- true;
        (* Iterate over all edges emanating from vertex [i] *)
        List.iter (fun (j, ej) ->
          (* retrieve the specific weight of this edge *)
          let wj = weight_of_edge i vi.label j ej in
          (* Calculate the distance from the source vertex to v through u *)
          let alt = wi +/ wj in
          (* If the distance from the source to [j] through [i] is smaller than
           * the current distance from the source to [j], update the distance
           * and set [i] as the predecessor of [j]
          *)
          match distances.(j) with
          | Some dj when W.compare alt dj < 0 -> (
            distances.(j) <- Some alt;
            predecessors.(j) <- Some i;
            PQ.add queue alt j
          )
          | _ -> ()
        ) vi.edges;
      )
    );

    (* Return the distances and predecessors arrays *)
    (distances, predecessors)
end
