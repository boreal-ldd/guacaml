(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * FlexArray : extendible arrays
 *
 * === AUTHOR ===
 *
 * B. Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes
 *)

(* type of trees and flex-arrays *)

type 'a tree =
    Blank (* height contribution = 0 *)
  | Leaf of 'a array (* height contribution = 1 *)
  | Node of 'a tree array (* height contribution = 1 *)

type 'a t =
  {
            base      : int; (* size of tables *)
            default   : 'a; (* default value of array elements *)
    mutable height    : int; (* max of sum of height contributions along all branches *)
    mutable length    : int64;
    mutable efflength : int64;
    (* minimal value such that the range [0 ... efflength-1] contains all elements *)
    (* alternatively [efflength] := ["index of the last element"+1] *)
    mutable tree      : 'a tree;
  }

(* [make base default] initializes an empty array, with default value [default] and base degree [base] *)

let make base default =
  assert(base >= 0);
  {
    base;
    default;
    height    = 0;
    length    = 0L;
    efflength = 0L;
    tree      = Blank
  }

let get_default (t:'a t) : 'a = t.default

(* Checks internal consistency of 'height' and 'tree' fields *)

let rec assert_tree_height height tree : unit =
  if height < 0
  then failwith "[GuaCaml.FlexArray.check_tre] state inconsistency : negative value of 'height'";
  match tree with
  | Blank -> ()
  | Leaf _ -> (
      if height <> 1
      then failwith "[GuaCaml.FlexArray.check_tree_height] state inconsistency : 'Leaf' element in non-1-height tree"
    )
  | Node ta -> (
      if height <= 1
      then (failwith "[GuaCaml.FlexArray.check_tree_height] state inconsistency : 'Node' element in lower-than-1 tree")
      else (Array.iter (assert_tree_height (height-1)) ta)
    )

let rec assert_tree_base base =
  function
  | Blank -> ()
  | Leaf vec -> (
      if Array.length vec <> base
      then failwith "[GuaCaml.FlexArray.check_tree_base] state inconsistency : 'Leaf''s length is not 'base'"
    )
  | Node ta -> (
      if Array.length ta <> base
      then (failwith "[GuaCaml.FlexArray.check_tree_base] state inconsistency : 'Node''s length is not 'base'")
      else (Array.iter (assert_tree_base base) ta)
    )

let check_tree base height tree : bool =
  try
    assert_tree_base   base   tree;
    assert_tree_height height tree;
    true
  with Failure _ ->
    false

let ipow (x:int64) (k:int) : int64 =
  Tools.quick_pow (Int64.of_int 1) Int64.mul x k

let iipow (x:int) (k:int) : int64 =
  ipow (Int64.of_int x) k

let check (t:'a t) : bool =
  t.height >= 0 &&
  t.base   >  0 &&
  if t.height = 0
  then (
    t.tree = Blank
  )
  else (
    check_tree t.base t.height t.tree &&
    Int64.equal t.length (iipow t.base t.height)
  )

(* Returns the number of allocated elements *)

let rec size_tree base tree : int64 =
  match tree with
  | Blank  -> 0L
  | Leaf _ -> Int64.of_int base
  | Node ta -> (
      Array.fold_left
        (fun k son -> Int64.add k (size_tree base son))
        0L
        ta
    )

let size ({base; tree} : 'a t) : int64 = size_tree base tree

(* Returns the highest index + 1 that can be stored without increasing the height of the tree structure *)

let length ({efflength} : 'a t) : int64 = efflength

(* [split_coord base index length = (index0, index', length')]
 * splits coordinates between the current layer ( [index0] ) and the next ( [index'] and [length'] )
 *)
let split_coord (base:int) (index:int64) (length:int64) : int * int64 * int64 =
  if not (0L <= index && index < length)
  then invalid_arg "[GuaCaml.FlexArray.split_coord] out of bound [index]";
  let length' = Int64.div length (Int64.of_int base) in
  let index' = Int64.rem index length' in
  let index0 = Int64.to_int (Int64.div index length') in
  if index0 < base
  then (index0, index', length')
  else failwith "[GuaCaml.FlexArray.split_coord] internal inconsistency (out of bound coordinate)"

(* [get t i] retrieves the element of [a] at index [i], if allocated. Otherwise, return default value of [a] *)

let rec get_tree base default height tree (index:int64) (length:int64) =
  match tree with
  | Blank    -> default
  | Leaf vec -> (
    if Int64.compare index length < 0 (* index < base *)
    then vec.(Int64.to_int index)
    else failwith "[GuaCaml.FlexArray.get_tree] out of bound access in 'Leaf' element"
  )
  | Node ta -> (
    let index0, index', length' = split_coord base index length in
    get_tree base default (pred height) ta.(index0) index' length'
  )

let get { base; default; height; length; tree} index =
  if Int64.compare index 0L < 0 (* index < 0 *)
  then invalid_arg "[GuaCaml.FlexArray.ge] negative [index] "
  else if Int64.compare index length < 0
  then get_tree base default height tree index length
  else default

(* [single_array base default index value]
 * returns a ['a array] of size [base] with all value set at [default]
 * except at index [index] which value holds [value]
 *)

let single_array (base:int) (default:'a) (index:int) (value:'a) : 'a array =
  let r = Array.make base default in
  r.(index) <- value;
  r

(* [single_tree base default height index value]
 * returns a ['a tree] of height [height] and base [base] containing a single
 * element [value] at index [index]
 *)

let rec single_tree base default height index length value : 'a tree =
  match height with
  | _ when height <= 0 -> invalid_arg "[GuaCaml.FlexArray.single_tree] non-positive [height]"
  | 1 -> Leaf(single_array base default (Int64.to_int index) value)
  | _ -> (
    let index0, index', length' = split_coord base index length in
    let single_son = single_tree base default (pred height) index' length' value in
    Node(single_array base Blank index0 single_son)
  )

(* assumes that [tree <> Blank] *)
let rec set_tree base default height (index:int64) length value : 'a tree -> unit =
  function
  | Blank -> failwith "[GuaCaml.FlexArray.set_tree] 'Blank' tree"
  | Leaf vec -> vec.(Int64.to_int index) <- value
  | Node ta -> (
    let index0, index', length' = split_coord base index length in
    if ta.(index0) = Blank
    then ta.(index0) <- single_tree base default (pred height) index' length' value
    else set_tree base default (pred height) index' length' value ta.(index0)
  )

let grow (t:'a t) : unit =
  t.height <- succ t.height;
  t.tree   <- Node(single_array t.base Blank 0 t.tree);
  ()

let rec set (({base; default; height; length; tree} as t): 'a t) (index:int64) (value:'a) : unit =
  if Int64.compare index length < 0 (* index < length *)
  then (
    set_tree base default height index length value tree;
    t.efflength <- Int64.max t.efflength (Int64.succ index)
  )
  else (grow t; set t index value)

let rec copy_tree =
  function
  | Blank -> Blank
  | Leaf vec -> Leaf(Array.copy vec)
  | Node ta -> Node(Array.map copy_tree ta)

let copy ({base; default; height; length; efflength; tree}:'a t) : 'a t =
  {base; default; height; length; efflength; tree = copy_tree tree}

let to_array a n =
  let b = Array.make n a.default in
  for i = 0 to n-1 do
    b.(i) <- get a (Int64.of_int i)
  done;
  b

let rec ranged_iter (f:'a -> unit) (t:'a t) fst len : 'b =
  if Int64.compare len 0L > 0 (* len > 0 *)
  then ranged_iter f t (Int64.succ fst) (Int64.pred len)

let iter f t =
  ranged_iter f t 0L (length t)

let rec sparse_iter_tree (f:'a -> unit) : 'a tree -> unit =
  function
  | Blank -> ()
  | Leaf vec -> Array.iter f vec
  | Node ta -> Array.iter (sparse_iter_tree f) ta

let sparse_iter (f:'a -> unit) (t:'a t) : unit =
  sparse_iter_tree f t.tree

let rec sparse_iteri_tree base shift0 (p:'a -> bool) (f:int64 -> 'a -> unit) : 'a tree -> unit =
  function
  | Blank -> ()
  | Leaf vec ->
    Array.iteri
      (fun index0 value ->
        if p value
        then
          let index = Int64.add shift0 (Int64.of_int index0) in
          f index value
      )
      vec
  | Node ta ->
    let shift0' = Int64.mul shift0 (Int64.of_int base) in
    Array.iteri (fun index0 t ->
      let index = Int64.add (Int64.of_int index0) shift0' in
      sparse_iteri_tree base index p f t
    )
    ta

let sparse_iteri (p:'a -> bool) (f:int64 -> 'a -> unit) (t:'a t) : unit =
  sparse_iteri_tree t.base 0L p f t.tree

let option_iter (f:'a -> unit) (t:'a option t) : unit =
  iter (function None -> () | Some x -> f x) t

let rec rec_fold_left red init (t:'a t) fst lst : 'b =
  if Int64.compare fst lst < 0 (* fst < lst *)
  then rec_fold_left red (red init (get t fst)) t (Int64.succ fst) lst
  else init

let ranged_fold_left red init (t:'a t) fst len : 'b =
  rec_fold_left red init t fst (Int64.add fst len)

let fold_left red init (t:'a t) =
  rec_fold_left red init t 0L (length t)

let option_fold_left red init t =
  let opred x = function None -> x | Some y -> red x y in
  fold_left opred init t

let rec rec_fold_right red (t:'a t) (init:'b) fst lst : 'b =
  if Int64.compare fst lst < 0 (* fst < lst *)
  then rec_fold_right red t (red (get t lst) init) fst (Int64.pred lst)
  else init

let ranged_fold_right red t init fst len =
  rec_fold_right red t init fst (Int64.add fst len)

let fold_right red t init =
  rec_fold_right red t init 0L (length t)

let option_fold_right red t init =
  let opred opx y = match opx with None -> y | Some x -> red x y in
  fold_right opred t init
