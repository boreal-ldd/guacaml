(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * SparseVector: sparse vector structure based on FlexArray
 *
 * === AUTHOR ===
 *
 * Benoit Caillaud
 *  - <benoit.caillaud@inria.fr>, Inria Rennes
 *
 * === CONTRIBUTOR ===
 *
 * Mathias Malandain
 *  - <mathias.malandain@inria.fr>, Inria Rennes
 *)

(* open PPrint *)

let base = 64

module type MSig =
sig
  type t
  type s
  type sub
  val index : t -> int
  val card : s -> int
  val iter : (t -> unit) -> s -> unit
  val iter_subset : (t -> unit) -> sub -> unit
  val fold : (t -> 'a -> 'a) -> s -> 'a -> 'a
  val to_string : s -> t -> string
end

module type Sig =
sig
  module I : MSig
  type 'a t
  val make : ?base:int -> I.s -> 'a -> 'a t
  val get_set  : 'a t -> I.s
  val get_card : 'a t -> int
  val get : 'a t -> I.t -> 'a
  val set : 'a t -> I.t -> 'a -> unit
  val iter : ('a -> bool) -> (I.t -> 'a -> unit) -> 'a t -> unit
  val fold : ('a -> bool) -> (I.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
end

module Make(MI:MSig) : Sig
  with type I.t   = MI.t
  and  type I.s   = MI.s
  and  type I.sub = MI.sub
 =
struct
  module I = MI

  type 'a t =
    {
      set  : I.s;
      card : int;
      mat  : 'a FlexArray.t;
    }

  let get_set t = t.set
  let get_card t = t.card

  let index ( _ : 'a t) i = Int64.of_int (I.index i)

  let make ?(base=64) set default =
    {
      set;
      card = I.card set;
      mat = FlexArray.make base default;
    }

  let get m i =
    let z = index m i in
    FlexArray.get m.mat z

  let set m i x =
    let z = index m i in
    FlexArray.set m.mat z x

  let iter test foo m =
    I.iter
      (fun i ->
         let z = index m i in
         let x = FlexArray.get m.mat z in
         if test x then foo i x)
      m.set

  let fold test foo m y0 =
    let y = ref y0 in
    iter test (fun i x -> y := foo i x (!y)) m;
    !y
end
