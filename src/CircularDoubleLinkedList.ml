(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * CircularDoubleLinkedList
 *
 * === NOTE ===
 *
 * 1. [Circular Doubly Linked List (Wikipedia)](https://en.wikipedia.org/wiki/Doubly_linked_list#Circular_doubly_linked_lists)
 * 2. partially replicated from [GuaCaml/DoubleLinkedList]
 *
 *)

open Extra
module DLL = DoubleLinkedList

module CDLL =
struct
  include DLL

  let check (first:'a t) : bool =
    let rec check_succ (prev:'a t) (curr:'a t) : bool =
      (curr.prev %== prev) && (
        (curr == first) ||
        (check_succ_opt curr curr.succ)
      )
    and     check_succ_opt curr (curr_succ:'a o) : bool =
      match curr_succ with
      | None -> true
      | Some curr_succ -> check_succ curr curr_succ
    in
    check_succ_opt first first.succ

  let check_opt (t:'a o) : bool =
    Tools.opmap_default check true t

  let check_items_opt (expected:'a list) (o:'a o) : bool =
    internal_check_items_opt ~lst:(Some o) expected o

  let check_items (expected:'a list) (t:'a t) : bool =
    check_items_opt expected (Some t)

  let check_structure_forward_opt ?(verbose=false) (sides:('a o * 'a o) list) (o:'a o) : bool =
    internal_check_structure_forward_opt ~verbose o sides o

  let check_structure_forward ?(verbose=false) (sides:('a o * 'a o) list) (t:'a t) : bool =
    check_structure_forward_opt ~verbose sides (Some t)

  let create item =
    let dll = DLL.create item in
    internal_set_prev_succ dll dll;
    dll

  (* [INCLUDE] internal_set_oprev_osucc *)
  (* [INCLUDE] internal_set_oprev_succ *)
  (* [INCLUDE] internal_set_prev_osucc *)
  (* [INCLUDE] internal_set_prev_succ *)

  (* [INCLUDE] create_before *)
  (* [COPY] create_before_opt *)
  let create_before_opt (o:'a o) (item:'a) : 'a t =
    match o with
    | None -> create item
    | Some t -> create_before t item

  (* [INCLUDE] create_after *)
  (* [COPY] create_after_opt *)
  let create_after_opt (o:'a o) (item:'a) : 'a t =
    match o with
    | None -> create item
    | Some t -> create_after t item

  (* [INCLUDE] remove *)
  (* [INCLUDE] insert_between *)
  (* [INCLUDE] insert_between_opt *)
  (* [INCLUDE] insert_before *)
  (* [INCLUDE] insert_after *)

  (* [REMOVED] remove_fst *)
  (* [REMOVED] remove_lst *)

  let to_seq_opt (t:'a o) : 'a t Seq.t =
    internal_to_seq_opt t t

  (* [COPY] to_seq *)
  let to_seq (t:'a t) : 'a t Seq.t =
    to_seq_opt (Some t)

  (* [COPY] to_item_seq *)
  let to_item_seq (t:'a t) : 'a Seq.t =
    Seq.map (fun t -> t.item) (to_seq t)

  (* [COPY] to_item_seq_opt *)
  let to_item_seq_opt (t:'a o) : 'a Seq.t =
    Seq.map (fun t -> t.item) (to_seq_opt t)

  (* [COPY] create_after_item_seq *)
  let rec create_after_item_seq (t:'a t) (seq:'a Seq.t) : 'a t =
    match seq () with
    | Seq.Nil -> t
    | Seq.Cons (item, seq) ->
      create_after_item_seq (create_after t item) seq

  (* [COPY] create_before_item_seq *)
  let rec create_before_item_seq (t:'a t) (seq:'a Seq.t) : 'a t =
    match seq () with
    | Seq.Nil -> t
    | Seq.Cons (item, seq) ->
      create_before_item_seq (create_after t item) seq

  let of_item_seq (seq:'a Seq.t) : 'a o =
    match seq () with
    | Seq.Nil -> None
    | Seq.Cons (item, seq) -> (
      let fst = create item in
      ignore(create_after_item_seq fst seq);
      (Some fst)
    )

  (* [COPY] to_list *)
  let to_list (t:'a t) : 'a list =
    t |> to_item_seq |> List.of_seq

  (* [COPY] to_list_opt *)
  let to_list_opt (o:'a o) : 'a list =
    Tools.opmap_default to_list [] o

  (* [COPY] of_list *)
  let of_list (l:'a list) : 'a o =
    l |> List.to_seq |> of_item_seq

  (* [INCLUDE] nth_opt *)
  (* [INCLUDE] nth *)

  (* [COPY] iter_opt *)
  let iter_opt (f:'a -> unit) (o:'a o) : unit =
    Seq.iter f (to_item_seq_opt o)

  (* [COPY] iter *)
  let iter (f:'a -> unit) (t:'a t) : unit =
    Seq.iter f (to_item_seq t)
end
open CDLL

(* Circular Double Linked List attached with a Set structure (in this case,
 * an HashSet), allows to make ordered set with fast insertion, deletion, and,
 * access
 *)
module HashSet =
struct
  type 'k ht = {
    access : ('k, 'k t) Hashtbl.t;
    mutable first : 'k o;
  }

  let mem (t:'k ht) (k:'k) : bool =
    Hashtbl.mem t.access k

  let create ?(hsize=100) () : _ ht =
    { access = Hashtbl.create hsize; first = None }

  let iter (f:'k -> unit) (t:'k ht) : unit =
    iter_opt f t.first

  let to_list (t:'k ht) : 'k list =
    to_list_opt t.first

  let nth (t:'k ht) (i:int) : 'k =
    Tools.unop (nth_opt t.first i)

  let length (t:'k ht) : int =
    Hashtbl.length t.access

  let to_string (to_string:'k -> string) (t:'k ht) : string =
    "CDLL.HashSet.of_list "^(STools.ToS.(list to_string) (to_list t))

  let get (t:'k ht) (x:'k) (fail : unit -> 'k t) : 'k t =
    match Hashtbl.find_opt t.access x with
    | None -> fail ()
    | Some _x -> _x

  (* [insert_after t x y] inserts [y] after [x] *)
  let insert_after (t:'k ht) (x:'k) (y:'k) : unit =
    let _x = get t x (fun () -> invalid_arg "[x] is not a key in [t]") in
    if Hashtbl.mem t.access y
    then invalid_arg "[y] is already in [t]";
    let _y = create_after _x y in
    Hashtbl.add t.access y _y;
    ()

  (* [insert_before t x y] inserts [y] before [x] *)
  let insert_before (t:'k ht) (x:'k) (y:'k) : unit =
    if t.first = None then invalid_arg "[t] is empty";
    let _x = get t x (fun () -> invalid_arg "[x] is not a key in [t]") in
    if Hashtbl.mem t.access y
    then invalid_arg "[y] is already in [t]";
    let _y = create_before _x y in
    Hashtbl.add t.access y _y;
    if t.first %== _x then t.first <- Some _y;
    ()

  let append (t:'k ht) (x:'k) : unit =
    if Hashtbl.mem t.access x
    then invalid_arg "[x] is already in [t]";
    let _x = CDLL.create_before_opt t.first x in
    Hashtbl.add t.access x _x;
    if t.first = None then t.first <- Some _x;
    ()

  let of_list ?(hsize=100) (l:'k list) : 'k ht =
    let t = create ~hsize () in
    List.iter (append t) l;
    t

  (* Remove x from the sequence.
   * If [x == first]
   *   If [first.next == first]
   *   then [first := None]
   *   Else [first := first.next]
   *)
  let remove (t:'k ht) (x:'k) : unit =
    let _x = get t x (fun () -> invalid_arg "[x] is not a key in [t]") in
    if t.first %== _x
    then (t.first <- if _x.succ %== _x then None else _x.succ);
    CDLL.remove _x;
    Hashtbl.remove t.access x;
    ()

  (* Find the previous element in the sequence. *)
  let get_prev (t:'k ht) (x:'k) : 'k =
    let _x = get t x (fun () -> invalid_arg "[x] is not a key in [t]") in
    (Tools.unop _x.prev).item

  (* Find the next element in the sequence. *)
  let get_succ (t:'k ht) (x:'k) : 'k =
    let _x = get t x (fun () -> invalid_arg "[x] is not a key in [t]") in
    (Tools.unop _x.succ).item

  let get_first (t:'k ht) : 'k =
    match t.first with
    | None -> invalid_arg "[t] is empty"
    | Some first -> first.item

  let get_first_opt (t:'k ht) : 'k option =
    Tools.opmap CDLL.get_item t.first

  type 'a t = 'a ht
end

(* Circular Double Linked List attached with a Map structure (in this case,
 * an HashMap), allows to make ordered set with fast insertion, deletion, and,
 * access, even on elements which are not easily hashable (e.g. mutable elements).
 *)
module HashKeySet =
struct
  type ('k, 'v) ht = {
    key : 'v -> 'k;
    access : ('k, 'v) Hashtbl.t;
    order : 'k HashSet.ht;
  }

  let mem (t:('k, 'v) ht) (v:'v) : bool =
    Hashtbl.mem t.access (t.key v)

  let create ?(hsize=100) (key:'v -> 'k) : _ ht =
    {
      key;
      access = Hashtbl.create hsize;
      order = HashSet.create ~hsize ()
    }

  let item_of_key (t:('k, 'v) ht) (k:'k) : 'v =
    match Hashtbl.find_opt t.access k with
    | None -> invalid_arg "[x] is not in [t]"
    | Some v -> v

  (* Iterate through the objects in the sequence.
   * May give unpredictable results if sequence changes mid-iteration.
   * Terminate iff the underlying linked-list is indeed a perfect loop
   *)
  let iter (f:'k -> 'v -> unit) (t:('k, 'v) ht) : unit =
    Hashtbl.iter f t.access

  let to_list (t:('k, 'v) ht) : 'v list =
    let kl = HashSet.to_list t.order in
    kl ||> item_of_key t

  let nth (t:('k, 'v) ht) (i:int) : 'v =
    HashSet.nth t.order i |> item_of_key t

  let length (t:('k, 'v) ht) : int =
    Hashtbl.length t.access

  let to_string (to_string:'v -> string) (t:('k, 'v) ht) : string =
    "CDLL.HashKeySet.of_list "^(STools.ToS.list to_string (to_list t))

(*
  let get (t:('k, 'v) ht) (x:'k) (fail : unit -> 'k t) : 'k t =
    match Hashtbl.find_opt t.access x with
    | None -> fail ()
    | Some _x -> _x
 *)

  (* [insert_after t x y] inserts [y] after [x] *)
  let insert_after (t:('k, 'v) ht) (x:'v) (y:'v) : unit =
    let kx = t.key x in
    let ky = t.key y in
    if Hashtbl.mem t.access kx = false
    then invalid_arg "[x] is not an element of [t]";
    if Hashtbl.mem t.access ky
    then invalid_arg "[y] is already an element of [t]";
    HashSet.insert_after t.order kx ky;
    Hashtbl.add t.access ky y;
    ()

  (* [insert_before t x y] inserts [y] before [x] *)
  let insert_before (t:('k, 'v) ht) (x:'v) (y:'v) : unit =
    let kx = t.key x in
    let ky = t.key y in
    if Hashtbl.mem t.access kx = false
    then invalid_arg "[x] is not a valid element in [t]";
    if Hashtbl.mem t.access ky
    then invalid_arg "[y] is already an element of [t]";
    HashSet.insert_before t.order kx ky;
    Hashtbl.add t.access ky y;
    ()

  let append (t:('k, 'v) ht) (x:'v) : unit =
    let kx = t.key x in
    if Hashtbl.mem t.access kx
    then invalid_arg "[x] is already an element of [t]";
    HashSet.append t.order kx;
    Hashtbl.add t.access kx x;
    ()

  let of_list ?(hsize=100) (key : 'v -> 'k) (l:'v list) : ('k, 'v) ht =
    let access = Hashtbl.create hsize in
    let kl = l ||>
      (fun x ->
        let kx = key x in
        if Hashtbl.mem access kx
        then invalid_arg "[kx] appears more than once in the list"
        else Hashtbl.add access kx x;
        kx
      )
    in
    let order = HashSet.of_list ~hsize kl in
    {key; access; order}

  (* Remove x from the sequence.
   * If [x == first]
   *   If [first.next == first]
   *   then [first := None]
   *   Else [first := first.next]
   *)
  let remove (t:('k, 'v) ht) (x:'v) : unit =
    let kx = t.key x in
    if Hashtbl.mem t.access kx = false
    then invalid_arg "[x] is not an element of [t]";
    HashSet.remove t.order kx;
    Hashtbl.remove t.access kx;
    ()

  (* Find the previous element in the sequence. *)
  let get_prev (t:('k, 'v) ht) (x:'v) : 'v =
    let kx = t.key x in
    if Hashtbl.mem t.access kx = false
    then invalid_arg "[x] is not an element of [t]";
    let ky = HashSet.get_prev t.order kx in
    item_of_key t ky

  (* Find the next element in the sequence. *)
  let get_succ (t:('k, 'v) ht) (x:'v) : 'v =
    let kx = t.key x in
    if Hashtbl.mem t.access kx = false
    then invalid_arg "[x] is not an lement of [t]";
    let ky = HashSet.get_succ t.order kx in
    item_of_key t ky

  let get_first (t:('k, 'v) ht) : 'v =
    if Hashtbl.length t.access = 0
    then invalid_arg "[t] is empty";
    HashSet.get_first t.order |> item_of_key t

  let get_first_opt (t:('k, 'v) ht) : 'v option =
    Tools.opmap (item_of_key t) (HashSet.get_first_opt t.order)

  type ('k, 'v) t = ('k, 'v) ht
end
