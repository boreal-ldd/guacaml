(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraGenLA : Generic Adjacency List Graph
 *
 * KIC : K-Initial Components
 *
 * === NOTE ===
 *
 * A K-vertex [k] is an H-vertex such that :
 *   1. N_G(k) is a PMC in G
 *   2. SN_G(k) is not a PMC in S(G, k)
 *)

open Extra
module GGLA = GraphGenLA
open GGLA.Type
open GGLA_HFT
open GGLA_HFT.Type

module Test =
struct
  (* [MOVEME] GGLA_HFT : be carefull *)
  let strict_neighbors (g:hg) (iv:int) : int list =
    let ni = g.(iv).edges ||> fst in
    assert(List.mem iv ni = false);
    ni

  let neighbors (g:hg) (iv:int) : int list =
    SetList.union [iv] (strict_neighbors g iv)

  let strict_and_neighbors (g:hg) (iv:int) : int list * int list =
    let sni = strict_neighbors g iv in
    (sni, SetList.union [iv] sni)

  let connected_components_and_separators_from_list hg nx =
    GGLA.AdvancedConnected.connected_components_and_separators_from_list hg nx

  let test (hg:hg) (x:int) : bool =
    (* neighbors of [x] in [h] *)
    let snx, nx = strict_and_neighbors hg x in
    (* print_endline ("nx:"^(STools.ToS.(list int) nx)); *)
    (* print_endline ("snx:"^(STools.ToS.(list int) snx)); *)
    (snx = []) (* isolated vertex *) ||
    (
      (* cs_list = [(component, separator); ...] *)
      let cs_list = connected_components_and_separators_from_list hg nx in
      (* SN_G(x) has a full component in S(G, x) *)
      List.exists (fun (_, separator) -> snx = separator) cs_list
    )
end

module Find =
struct
  let naive (h:hg) : int option =
    Array.find_map
      (fun v ->
        if Test.test h v.index
        then Some v.index
        else None
      )
      h

  let using_lexbfs (h:hg) : int option =
    if Array.length h = 0
    then None
    else (
      (* [LATER] test with [GGLA_LexBFS.lexbfs2] to compare performances *)
      let order = GGLA_LexBFS.lexbfs1 h in
      assert(List.length order = Array.length h);
      let _, x = MyList.last order in
      (* [TODO] optional disabling for performance *)
      assert(Test.test h x);
      Some x
    )
end

module FindSeq =
struct
  type find = hg -> int option

  let seq_of_find (find:find) (hg:hg) : int list =
    let rec loop (carry:int list) (hg:hg) : int list =
      match find hg with
      | None -> (
        if Array.length hg <> 0
        then (
          print_endline ("[GuaCaml.GGLA_KIC.FindSeq.seq_of_find] hg:"^(GGLA_HFT.ToS.hg ~short:false hg));
          failwith "[GuaCaml.GGLA_KIC.FindSeq.seq_of_find] hg is not empty"
        );
        List.rev carry
      )
      | Some x -> (
        loop (x::carry) (GGLA_HFT.vertex_contraction hg x)
      )
    in
    loop [] hg

  let naive hg = seq_of_find Find.naive hg
  let using_lexbfs hg = seq_of_find Find.naive hg
end

module Enum =
struct
  let naive (h:hg) : int list =
    let stack = ref [] in
    Array.iter
      (fun v ->
        if Test.test h v.index
        then stack_push stack v.index
      )
      h;
    List.rev !stack
end
