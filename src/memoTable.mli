(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * MemoTable : generic memoization facility
 *)

module type Sig =
sig
  type ('a, 'b) t

  val clear : (_, _) t -> unit
  val reset : (_, _) t -> unit

  val default_size : int
  val create : int -> (_, _) t

  val test : ('a, 'b) t -> 'a -> bool
  val push : ('a, 'b) t -> 'a -> 'b -> unit
  val memo : ('a, 'b) t -> 'a -> 'b -> 'b
  val pull : ('a, 'b) t -> 'a -> 'b

  val apply : ('a, 'b) t -> ('a -> 'b) -> 'a -> 'b
  val make : int -> ('a, 'b) t * (('a -> 'b) -> 'a -> 'b)

  val print_stats : (_, _) t -> unit
  val dump_stats : (_, _) t -> Tree.stree
end

module Module : Sig

include Sig
  with type ('a, 'b) t = ('a, 'b) Module.t
