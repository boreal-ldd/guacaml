(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * CoGraph : Implementation of Co-Graphs (aka Parallel/Series graphs)
 *
 * === NOTES ===
 *
 * Co-Graphs are undirected graphs where cograph modules are factorized, i.e.,
 * we iterative factorize (hyper-)vertices which are either true of false twins*, i.e.,
 * *twins : vertices with identical neighorbood
 *   - true-twins : adjacent twins
 *   - false-twins : non-adjacent twins
 *
 * the factorization step is called C-reduction
 *)

open Extra
open STools

module Explicit =
struct
  module Type =
  struct
    type 'a t =
      | Empty
      | Vertex of 'a
      | NonTrivial of bool * 'a Tree.trees
    (* [bool = true ] =>  true-twins => Series
       [bool = false] => false-twins => Parallel
     *)
  end
  open Type

  module Check =
  struct

    (* [FIXME] *)
    (* do not forget double-alternation for singleton *)
    let t : 'a t -> bool =
      function
      | Empty -> true
      | Vertex _ -> true
      | NonTrivial (b, t) ->
        match t with
        | [] | [_] -> false
        | _ -> true (* [FIXME] *)
  end

  module ToS =
  struct
    open ToS

    let t a : 'a Type.t t =
      function
      | Empty -> "Empty"
      | Vertex v ->
        constructor "Vertex" a v
      | NonTrivial (b, t) ->
        constructor "NonTrivial" (bool * TreeUtils.ToS.trees a) (b, t)
  end

  module ToShiftS =
  struct
    open ShiftS
    open ToShiftS

    let t a : 'a Type.t t =
      function
      | Empty -> of_string "Empty"
      | Vertex v ->
        constructor "Vertex" a v
      | NonTrivial (b, t) ->
        constructor "NonTrivial" (bool * TreeUtils.ToShiftS.trees a) (b, t)
        (* [t = Leaf _] is not allowed for normalization reasons *)
  end

  let sort l = List.sort Stdlib.compare l

  let make_empty ?(dual=false) ?(sorted=false) (l:'a list) : 'a t =
    let l = if sorted then l else sort l in
    match l with
    | [] -> Empty
    | [x] -> Vertex x
    | l -> NonTrivial (dual, l ||> (fun v -> Tree.Leaf v))

  (* element -> forest -> forest (with this new element) *)
  let tree_add_element : 'a Tree.tree -> 'a Tree.trees -> 'a Tree.trees =
    fun x t -> sort(x::t)

  let tree_pair : 'a Tree.tree -> 'a Tree.tree -> 'a Tree.trees =
    fun x y -> (
      if x <= y then [x; y] else [y; x]
    )

  (* [FIXME] List.merge_sorted (?) *)
  let tree_append : 'a Tree.trees -> 'a Tree.trees -> 'a Tree.trees =
    fun x y -> sort(x @> y)

  (* let append ?(dual=false) (t1:'a t) (t2:'a t) = *)
  let append ?(verbose=false) ?(dual=false) (t1:int t) (t2:int t) =
    if verbose then (
      let open STools.ToS in
      let open ToS in
      print_endline ("[GuaCaml.CoGraph.Explicit.append] dual:"^(bool dual));
      print_endline ("[GuaCaml.CoGraph.Explicit.append] t1:"^(t int t1));
      print_endline ("[GuaCaml.CoGraph.Explicit.append] t2:"^(t int t2));
    );
    match t1, t2 with
    | Empty, t | t, Empty -> t
    | Vertex v1, Vertex v2 -> (
      NonTrivial(dual, tree_pair (Tree.Leaf v1) (Tree.Leaf v2))
    )
    | Vertex v, NonTrivial (b, f) | NonTrivial (b, f), Vertex v -> (
      if b = dual
      then NonTrivial (dual, tree_add_element (Tree.Leaf v) f)
      else NonTrivial (dual, tree_pair (Tree.Leaf v) (Tree.Node f))
    )
    | NonTrivial(b1, f1), NonTrivial(b2, f2) -> (
      if b1 = b2
      then if b1 = dual
        then NonTrivial(dual, tree_append f1 f2)
        else NonTrivial(dual, tree_pair (Tree.Node f1) (Tree.Node f2))
      else if b1 = dual
        then (* b1 = dual && b2 = not dual *)
          NonTrivial(dual, tree_add_element (Tree.Node f2) f1)
        else (* b1 = not dual && b2 = dual *)
          NonTrivial(dual, tree_add_element (Tree.Node f1) f2)
    )

  let ( // ) t1 t2 = append ~dual:false t1 t2
  let ( ** ) t1 t2 = append ~dual:true  t1 t2

  (* correct even if l = [] or l = [t] *)
  let flatten ?(dual=false) (l:'a t list) : 'a t =
    List.fold_left (append ~dual) Empty l

  let complement ?(cond=true) : 'a t -> 'a t =
    function
    | Empty -> Empty
    | Vertex v -> Vertex v
    | NonTrivial (b, t) -> NonTrivial(b <> cond, t)

  let flatten : 'a t -> 'a list =
    function
    | Empty -> []
    | Vertex v -> [v]
    | NonTrivial (b, t) -> TreeUtils.trees_flatten t

  let support (t:'a t) : 'a list =
    sort(flatten t)
end

(* Symbolic Bag *)
module SymBag =
struct
  (* SymBag allows elements to be present a negative amount of time
   * having negative element prevent effective expansion, however
   * it simplifies algebra *)
  type 'a t = ('a * int) list

  module ToS =
  struct
    open ToS

    let t a = list(a * int)
  end

  module Check =
  struct
    let t (x:'a t) : bool =
      Assoc.sorted x &&
      List.for_all (fun (_, e) -> e <> 0) x
  end

  module ToShiftS =
  struct
    open ToShiftS

    let t a = list(a * int)
  end

  let check (t:'a t) : bool = Assoc.sorted t

  let empty = []

  let single (a:'a) : 'a t = [(a, 1)]

  let pair (x:'a) (y:'a) : 'a t =
         if x = y then [(x, 2)]
    else if x < y then [(x, 1); (y, 1)]
                  else [(y, 1); (x, 1)]

  let ntimes (n:int) (a:'a) : 'a t =
    if n = 0 then [] else [(a, n)]

  let internal_cons ((a, n):('a * int)) (t:'a t) : 'a t =
    if n = 0 then t else ((a, n)::t)

  let union (t1:'a t) (t2:'a t) : 'a t =
    let rec union_rec (carry:'a t) (t1:'a t) (t2:'a t) : 'a t =
      match t1, t2 with
      | [], t | t, [] -> List.rev_append carry t
      | (a1, n1)::tail1, (a2, n2)::tail2 -> (
        if a1 = a2
        then (union_rec (internal_cons (a1, (n1+n2)) carry) tail1 tail2)
        else if a1 < a2
          then (union_rec ((a1, n1)::carry) tail1 t2)
          else (union_rec ((a2, n2)::carry) t1 tail2)
      )
    in union_rec [] t1 t2

  let cons (a:'a) (t:'a t) : 'a t = union (single a) t
end

module SymBagTree =
struct
  module Type =
  struct
    type 'a t =
      | Leaf of 'a
      | Node of 'a f
    and  'a f =
      'a t SymBag.t

    type 'a tree = 'a t
    type 'a forest = 'a f
  end
  open Type

  module Check =
  struct
    let rec t : 'a t -> bool =
      function
      | Leaf _ -> true
      | Node xf -> f xf
    and     f (xf:'a f) : bool =
      List.for_all (fun (xt, _) -> t xt) xf &&
      SymBag.Check.t xf
  end

  module ToS =
  struct
    open ToS

    let rec t a =
      function
      | Leaf x -> constructor "Leaf" a x
      | Node x -> constructor "Node" (f a) x
    and     f a =
      fun x -> SymBag.ToS.t (t a) x

    let tree a = t a
    let forest a = f a
  end

  module ToShiftS =
  struct
    open ToShiftS

    let rec t a : _ Type.t t =
      function
      | Leaf x -> constructor "Leaf" a x
      | Node x -> constructor "Node" (f a) x
    and     f a : _ Type.f t =
      fun x -> SymBag.ToShiftS.t (t a) x

    let tree a = t a
    let forest a = f a
  end

  let tree_of_forest ?(check=true) (f:'a f) : 'a t =
    if check then assert(Assoc.sorted f);
    Node f

  let forest_ntimes (n:int) (a:'a) : 'a f =
    SymBag.ntimes n (Leaf a)

  let forest_cons  (t:'a t) (f:'a f) : 'a f =
    SymBag.cons t f

  let forest_union (f1:'a f) (f2:'a f) : 'a f =
    SymBag.union f1 f2

  let forest_pair (t1:'a t) (t2:'a t) : 'a f =
    SymBag.pair t1 t2
end

module Implicit =
struct
  module SBT = SymBagTree

  module Type =
  struct
    type 'a t =
      | Empty
      | Vertex of 'a
      | NonTrivial of bool * 'a SBT.Type.forest
    (* [bool = true ] =>  true-twins => Series
       [bool = false] => false-twins => Parallel
     *)
  end
  open Type

  module Check =
  struct
    (* [FIXME] [LATER] check:
     *  | NonTrivial (_, []) -> false
     *  | NonTrivial (_, [t]) -> false
     *)
    let t : 'a t -> bool =
      function
      | Empty -> true
      | Vertex _ -> true
      | NonTrivial (b, f) -> SymBagTree.Check.f f
  end

  module ToS =
  struct
    open ToS

    let t a : 'a Type.t t =
      function
      | Empty -> "Empty"
      | Vertex v ->
        constructor "Vertex" a v
      | NonTrivial (b, t) ->
        constructor "NonTrivial" (bool * SBT.ToS.forest a) (b, t)
  end

  module ToShiftS =
  struct
    open ShiftS
    open ToShiftS

    let t a : 'a Type.t t =
      function
      | Empty -> of_string "Empty"
      | Vertex v ->
        constructor "Vertex" a v
      | NonTrivial (b, t) ->
        constructor "NonTrivial" (bool * SBT.ToShiftS.forest a) (b, t)
  end

  let make_empty ?(dual=false) (n:int) (a:'a) : 'a t =
    assert(n >= 0);
    match n with
    | 0 -> Empty
    | 1 -> Vertex a
    | _ -> NonTrivial (dual, SBT.forest_ntimes n a)

  let append ?(dual=false) (t1:'a t) (t2:'a t) =
    match t1, t2 with
    | Empty, t | t, Empty -> t
    | Vertex v1, Vertex v2 -> (
      NonTrivial(dual, SBT.(forest_pair (Leaf v1) (Leaf v2)))
    )
    | Vertex v, NonTrivial (b, f) | NonTrivial (b, f), Vertex v -> (
      if b = dual
      then NonTrivial (dual, SBT.(forest_cons (Leaf v) f))
      else NonTrivial (dual, SBT.(forest_pair (Leaf v) (Node f)))
    )
    | NonTrivial(b1, f1), NonTrivial(b2, f2) -> (
      if b1 = b2
      then if b1 = dual
        then NonTrivial(dual, SBT.forest_union f1 f2)
        else NonTrivial(dual, SBT.forest_pair (Node f1) (Node f2))
      else if b1 = dual
        then (* b1 = dual && b2 = not dual *)
          NonTrivial(dual, SBT.forest_cons (Node f2) f1)
        else (* b1 = not dual && b2 = dual *)
          NonTrivial(dual, SBT.forest_cons (Node f1) f2)
    )

  let ( // ) t1 t2 = append ~dual:false t1 t2
  let ( ** ) t1 t2 = append ~dual:true  t1 t2

  (* correct even if l =� [] or l = [t] *)
  let flatten ?(dual=false) (l:'a t list) : 'a t =
    List.fold_left (append ~dual) Empty l

  let complement ?(cond=true) : 'a t -> 'a t =
    function
    | Empty -> Empty
    | Vertex v -> Vertex v
    | NonTrivial (b, t) -> NonTrivial(b <> cond, t)
end
