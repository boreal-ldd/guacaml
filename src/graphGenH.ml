(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraphGenH [DEPRECATED] : implementation of H-graphs
 *
 * === NOTES ===
 *
 * H-Graphs are undirected graphs where true twins (i.e., vertices with
 * identical neighbors) are merged, such transformation is called
 * H-reduction
 *)

let (++) = SetList.union
let (--) = SetList.minus

type ('t, 'b) node = {
  index : int;
  edges : int list;
(* used to discrimate nodes when merging *)
  label : 't;
(* does not affect merging condition, bags are merged on merging *)
  bag : 'b;
}

type ('t, 'b) graph = ('t, 'b) node option array

module ToS =
struct
  open STools.ToS

  let node (slabel:'t t) (sbag:'b t) (node:('t, 'b) node) : string =
     "{index= "^(int node.index)^
    "; edges= "^(list int node.edges)^
      "; label= "^(slabel node.label)^
      "; bag= "^(sbag node.bag)^"}"

  let graph (slabel:'t t) (sbag:'b t) (graph:('t, 'b) graph) : string =
    array (option(node slabel sbag)) graph

  let print_graph slabel sbag graph : unit =
    MyArray.pprint_array (option(node slabel sbag)) graph

end

let strict_neighbors (graph:('t, 'b)graph) (node:('t, 'b)node) : ('t, 'b) node list =
  Tools.map (fun x ->
    match graph.(x) with
    | None -> (failwith "[GuaCaml.GraphGenH.strict_neighbors] error")
    | Some node -> node) node.edges

let internal_elim_node (graph:('t, 'b) graph) (node:('t, 'b) node) =
  graph.(node.index) <- None;
  List.iter
    (fun (node':('t, 'b) node) ->
      let edges : int list = SetList.minus (node'.edges:int list) [(node.index:int)] in
      graph.(node'.index) <- Some {node' with edges})
    (strict_neighbors graph node)

(* Time Complexity O(n^3) *)
let merging (eq_label:'t->'t->bool) (merge_bag:'b->'b->'b) (graph:('t, 'b) graph) : unit =
  (* check for merging nodes *)
  let n = Array.length graph in
  for x = 0 to n-1 (* O(n) *)
  do
    match graph.(x) with
    | None -> ()
    | Some nodeX -> (
      let bag    = ref nodeX.bag in
      let edges  = ref nodeX.edges in
      for y = x+1 to n-1 (* O(n) *)
      do
        assert(x<y);
        match graph.(y) with
        | None -> ()
        | Some nodeY -> (
          (* O(n) *)
          if !edges ++ [nodeX.index] = nodeY.edges ++ [nodeY.index] &&
            (eq_label nodeX.label nodeY.label)
          then (
            internal_elim_node graph nodeY;
            bag    := merge_bag !bag nodeY.bag;
            edges  := !edges -- [nodeY.index];
          )
        )
      done;
      graph.(x) <- Some {nodeX with edges = !edges; bag = !bag}
    )
  done

(* [connected_components graph] return decomposition of the
     graph [graph] into connected components.

   warning : if [graph] is empty or made of a single component
     then [connex_components graph = [graph]] to avoid copy if not necessary
 *)

let connected_components (graph:('t, 'b) graph) : ('t, 'b) graph list =
  let n = Array.length graph in
  let uf = UnionFind.init n in
  for i = 0 to n-1
  do match graph.(i) with
    | None -> uf.(i) <- -1;
    | Some node -> (
      List.iter (UnionFind.union uf node.index) node.edges
    )
  done;
  let nbcomp = MyArray.counti (=) uf in
  if nbcomp <= 1 then [graph] else (
    let comps = Array.init nbcomp (fun _ -> Array.make n None) in
    let indexes = MyArray.indexesi (=) uf in
    let rev_indexes = Array.make n (-1) in
    Array.iteri (fun i x -> rev_indexes.(x) <- i) indexes;
    for i = 0 to n-1
    do
      if uf.(i) >= 0 then (
        let j = UnionFind.find uf i in
        comps.(rev_indexes.(j)).(i) <- graph.(i)
      )
    done;
    comps |> Array.to_list
  )

let check eq_label graph : bool =
  let state = ref true in
  let add (b:bool) : unit = state := !state && b in
  Array.iteri (fun index -> function
    | None -> ()
    | Some node -> (
      add (index = node.index);
      add (not(List.mem index node.edges));
      List.iter (fun index' -> match graph.(index') with
          | None       -> (add false)
          | Some node' -> (
            add (List.mem index node'.edges);
            let neigh = node.edges ++ [node.index]
            and neigh' = node'.edges ++ [node'.index]
            and labeldiff = not(eq_label node.label node'.label) in
            add (neigh <> neigh' || labeldiff)
          )
        ) node.edges
    )
  ) graph;
  !state

