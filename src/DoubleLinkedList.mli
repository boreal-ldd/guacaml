(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * DoubleLinkedList : Implement standard Doubly-Linked List
 *
 * === NOTE ===
 *
 * [Double Linked List](https://en.wikipedia.org/wiki/Doubly_linked_list)
 *
 *)

(* [WARNING] avoid direct access to the internal structure *)

type 'a t = {
  mutable prev : 'a o;
  mutable succ : 'a o;
  mutable item : 'a
}
and 'a o = 'a t option

(* [check t] returns [true] iff
 * all elements of [t] (going both forward and backward) are structurally
 * consistent, i.e. [t.prev.succ == t] and [t.succ.prev == t]
 * and the list does not contain loops
 * - runtime : O(n)
 * - memory overhead : O(1)
 * - stack overhead : O(1)
 *)
val check : 'a t -> bool
val check_opt : 'a o -> bool
val internal_check_items_opt : ?lst:'a o option -> 'a list -> 'a o -> bool
val internal_check_structure_forward_opt : ?verbose:bool -> 'a o -> ('a o * 'a o) list -> 'a o -> bool
val internal_to_seq_opt : 'a o -> 'a o -> 'a t Seq.t
val check_items_opt : 'a list -> 'a o -> bool
val check_items : 'a list -> 'a t -> bool
val check_structure_forward_opt : ('a o * 'a o) list -> 'a o -> bool
val check_structure_forward : ('a o * 'a o) list -> 'a t -> bool
val get_item : 'a t -> 'a
val set_item : 'a t -> 'a -> unit
val get_succ_opt : 'a t -> 'a o
val get_prev_opt : 'a t -> 'a o

(* [internal_set_prev_succ x y] forces a directed connection between [x] and [y]
 *   x.succ = y
 *   y.prev = x
 *)
val internal_set_oprev_osucc : 'a o -> 'a o -> unit
val internal_set_oprev_succ : 'a o -> 'a t -> unit
val internal_set_prev_osucc : 'a t -> 'a o -> unit
val internal_set_prev_succ : 'a t -> 'a t -> unit

(* [create x] create a single-item list which contains [x] *)
val create : 'a -> 'a t

(* [create_before t x] creates a new list element and insert it between
 *   [t.prev] and [t]
 * before : t.prev -> t -> t.succ
 * after  : t.prev -> [x] -> t -> t.succ
 *)
val create_before : 'a t -> 'a -> 'a t
val create_before_opt : 'a o -> 'a -> 'a t

(* [create_after t x] creates a new list element and insert it between
 * [t] and [t.succ]
 * before : t.prev -> t -> t.succ
 * after  : t.prev -> t -> [x] -> t.succ
 *)
val create_after : 'a t -> 'a -> 'a t
val create_after_opt : 'a o -> 'a -> 'a t

(* [remove x] isolates a list element from its neighbors
 * before : t.prev -> t -> t.succ
 * after :
 *   t.prev -> t.succ (on one side)
 *   None -> [t] -> None
 *)
val remove : 'a t -> unit
val remove_fst : 'a t -> 'a * 'a o
val remove_lst : 'a t -> 'a * 'a o
val to_seq_opt : 'a o -> 'a t Seq.t
val to_seq : 'a t -> 'a t Seq.t
val to_item_seq : 'a t -> 'a Seq.t

(* [create_after_item_seq t seq = t'] creates a list element for each element
 * of the sequence, and insert between [t] and [t.succ].
 * This function returns [t'] returns the last element created by the sequence
 * or [t] iff the sequence is empty
 *)
val create_after_item_seq : 'a t -> 'a Seq.t -> 'a t
val create_before_item_seq : 'a t -> 'a Seq.t -> 'a t
val of_item_seq : 'a Seq.t -> 'a o * 'a o
val to_list : 'a t -> 'a list
val to_list_opt : 'a o -> 'a list
val of_list : 'a list -> 'a o * 'a o

val nth_opt : 'a o -> int -> 'a option
val nth : 'a t -> int -> 'a option
