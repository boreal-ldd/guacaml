(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * MemoBTable : Similar to [MemoTable] but allows for binarization AB
 *   which allows to reduced memory impact.
 *)

module type Sig =
sig
  type ('a, 'aa, 'b, 'bb) t

  val clear : (_, _, _, _) t -> unit
  val reset : (_, _, _, _) t -> unit

  val default_size : int
  (* when converting forward OR backward an element,
   * automatically check that the operation is reversible *)
  val create :
    ?__check_reverse__:bool ->
    ('a, 'aa) O3.o3 ->
    ('b, 'bb) O3.o3 ->
    int ->
      ('a, 'aa, 'b, 'bb) t

  val set_err : ('a, 'aa, 'b, 'bb) t -> (int -> 'a option -> 'aa option -> 'b option -> 'bb option -> unit) -> unit
  val add_err : ('a, 'aa, 'b, 'bb) t -> (int -> 'a option -> 'aa option -> 'b option -> 'bb option -> unit) -> unit

  val test : ('a, 'aa, 'b, 'bb) t -> 'a -> bool
  val push : ('a, 'aa, 'b, 'bb) t -> 'a -> 'b -> unit
  val memo : ('a, 'aa, 'b, 'bb) t -> 'a -> 'b -> 'b
  val pull : ('a, 'aa, 'b, 'bb) t -> 'a -> 'b

  val apply : ('a, 'aa, 'b, 'bb) t -> ('a -> 'b) -> 'a -> 'b
  val make :
    ?__check_reverse__:bool ->
    ('a, 'aa) O3.o3 ->
    ('b, 'bb) O3.o3 ->
    int ->
      ('a, 'aa, 'b, 'bb) t * (('a -> 'b) -> 'a -> 'b)
  val nocheck_make :
    ('a, 'aa) O3.o3 ->
    ('b, 'bb) O3.o3 ->
    int ->
      ('a, 'aa, 'b, 'bb) t * (('a -> 'b) -> 'a -> 'b)

  val print_stats : (_, _, _, _) t -> unit
  val dump_stats : (_, _, _, _) t -> Tree.stree
end

module Module : Sig =
struct

  let default_size = 10000

  type ('a, 'aa, 'b, 'bb) t = {
    o3sA : ('a, 'aa) O3.o3;
    o3sB : ('b, 'bb) O3.o3;
    mutable err : (int -> 'a option -> 'aa option -> 'b option -> 'bb option -> unit);
    mutable each : ('a -> 'aa -> 'b -> 'bb -> unit);
    table : ('aa, 'bb) Hashtbl.t;
    hitCnt : int ref;
    clcCnt : int ref;
    __check_reverse__ : bool;
  }

  let clear (t:('a, 'aa, 'b, 'bb) t) : unit =
    Hashtbl.clear t.table
  let reset (t:('a, 'aa, 'b, 'bb) t) : unit =
    Hashtbl.reset t.table

  let create ?(__check_reverse__=false) o3sA o3sB n = {
    o3sA; o3sB;
    err = (fun x _ _ _ _ ->
      print_newline();
      print_string "[GuaCaml.MemoBTable:create] - apply error: "; print_int x;
      print_newline()
    );
    each = (fun _ _ _ _ -> ());
    table = Hashtbl.create n;
    hitCnt = ref 0;
    clcCnt = ref 0;
    __check_reverse__;
  }

  let set_err mem err = mem.err <- err
  let add_err mem err =
    mem.err <- (fun x1 x2 x3 x4 x5 ->
    mem.err x1 x2 x3 x4 x5;
    err x1 x2 x3 x4 x5;
  )

  let test mem a = Hashtbl.mem mem.table (fst mem.o3sA a);;
  let push mem a b =
    if test mem a
    then failwith "[GuaCaml.MemoBTable:push] - already stored"
    else Hashtbl.add mem.table (fst mem.o3sA a) (fst mem.o3sB b)

  let memo mem a b = push mem a b; b;;
  let pull mem a = Hashtbl.find mem.table (fst mem.o3sA a) |> snd mem.o3sB;;

  let nocheck_apply mem fonc a =
    let aa = fst mem.o3sA a in
    try
      let bb = Hashtbl.find mem.table aa in
      let b  = snd mem.o3sB bb in
      incr mem.hitCnt;
      b
    with Not_found ->
    (
      incr mem.clcCnt;
      let b = fonc a in
      let bb = fst mem.o3sB b in
      Hashtbl.add mem.table aa bb;
      b
    )

  let apply mem fonc a =
    let err x = mem.err x (Some a) in
    let aa = try fst mem.o3sA a with exn -> (err 0 None None None; raise exn) in
    let err x = err x (Some aa) in
    let a' = try snd mem.o3sA aa with exn -> (err 1 None None; raise exn) in
    if not(a = a') then (err 2 None None; (failwith "[GuaCaml.MemoBTable] error"));
    try
      let bb = Hashtbl.find mem.table aa in
      let err x opb = err x opb (Some bb) in
      incr mem.hitCnt;
      let b = try snd mem.o3sB bb with exn -> (err 3 None; raise exn) in
      let err x = err x (Some b) in
      let bb' = try fst mem.o3sB b with exn -> (err 4; raise exn) in
      if not(bb = bb') then (err 5; (failwith "[GuaCaml.MemoBTable] error"));
      b
    with Not_found ->
    (
      incr mem.clcCnt;
      let b = try fonc a with exn -> (err 6 None None; raise exn) in
      let err x = err x (Some b) in
      let bb = try fst mem.o3sB b with exn -> (err 7 None; raise exn) in
      mem.each a aa b bb;
      let err x = err x (Some bb) in
      let b' = try snd mem.o3sB bb with exn -> (err 8; raise exn) in
      if not (b = b') then (err 9; (failwith "[GuaCaml.MemoBTable] error"));
      Hashtbl.add mem.table aa bb;
      b
    )

  let print_stats mem =
    print_string   "MemoTable's length:\t";
    print_int (Hashtbl.length (mem.table));
    print_string  "\nMemoTable's HitCnt:\t";
    print_int (!(mem.hitCnt));
    print_string  ".\nMemoTable's ClcCnt:\t";
    print_int (!(mem.clcCnt));
    print_string  ".\n"

  let dump_stats mem =
    let open STools in
    Tree.(Node [
      Node [Leaf "length:"; ToSTree.int (Hashtbl.length (mem.table))];
      Node [Leaf "hit count:"; ToSTree.int (!(mem.hitCnt))];
      Node [Leaf "clc count:"; ToSTree.int (!(mem.clcCnt))]
    ])

  let make ?(__check_reverse__=false) o3sA o3sB n =
    let mem = create ~__check_reverse__ o3sA o3sB n in
    ( mem, (if mem.__check_reverse__ then apply else nocheck_apply) mem )

  let nocheck_make o3sA o3sB n =
    let mem = create o3sA o3sB n in
    ( mem, nocheck_apply mem )
end
include Module
