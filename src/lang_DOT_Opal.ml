(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_DOT_Opal : Basic Parser for the DOT language from Graphviz
 *
 * === TODO ===
 *
 * parsing HTML identifier
 *)

open Extra
module Types = Lang_C_Types
open Types
(* module ToPrettyS = Lang_C_ToPrettyS *)
module ToS = Lang_C_ToS
open Opal

(* Section. Atomic Lexer *)

(* we split the [char] stream into [token] stream with the following rules :
 * - Word    => contiguous sequence of alpha-numerical characters + '_' are assembled
 *   + Keyword (* [TODO] *)
 *   + Ident   (* [TODO] *)
 * - Comment => both comments '/* ... */' and '// ... \n' are assembled
 * - String  => we assemble <string> atoms
 * - Symbol  => we assemble symbolic tokens
 * - White   => white characters are isolated
 * - Other   => every other character is left as is
 *)

module AtomicLexer =
struct
  (* Symbols *)

  let list_of_symbols = [
    "#";
    ":"; ";";",";
    "["; "]";
    "{"; "}";
    "=";
    "->";
    "--";
  ]

  (* [TODO] generate automatically *)
  let symbol : (char, string) t =
    let* c = any in
    match c with
    | '#' -> return "#"
    | ':' -> return ":"
    | ';' -> return ";"
    | ',' -> return ","
    | '[' -> return "["
    | ']' -> return "]"
    | '{' -> return "{"
    | '}' -> return "}"
    | '=' -> return "="
    | '-' -> (
      (exactly '>' >> return "->") <|>
      (exactly '-' >> return "--")
    )
    | _ -> mzero

  (* Comments *)
  let comment_EndOfLine : (char, string) t =
    let* _ = exactly '/' in
    let* _ = exactly '/' in
    must
      (fun () -> failwith "reached end of file without ending comment '//'")
      (
        let* cl = (until_stop any (exactly '\n' >> return ['n'] <|> eof [])) => (fun (a, b) -> a @> b) in
        return (implode ('/'::'/'::cl))
      )

  let comment_StarBracket : (char, string) t =
    let* _ = exactly '/' in
    let* _ = exactly '*' in
    must
      (fun () -> failwith "started comment with '/*', reached end of file without closure")
      (
        let* cl = ((until_stop any (exactly '*' $:: (exactly '/' >> return ['/']))) => (fun (a, b) -> a @> b)) in
        return (implode ('/'::'*'::cl))
      )

  let comment : (char, string) t =
    comment_EndOfLine <|> comment_StarBracket

  (* Keywords *)
  (* [TODO?] use an hashtable instead ? *)
  let list_of_keywords = [
    "node";
    "edge";
    "graph";
    "digraph";
    "subgraph";
    "strict";
  ]

  let keywords = one_of list_of_keywords

  (* Atomic Values *)

  type value =
    | String of string
    | Char   of char
    | Int    of int

  let value : (char, value) t =
    ( CharStream.string => (fun s -> String s)) <|>
    ( CharStream.char   => (fun c -> Char   c)) <|>
    ( CharStream.int    => (fun i -> Int    i))

  let is_keyword : string -> bool =
    fun s -> List.mem s list_of_keywords

  (* [is_ident : string -> bool] returns [true] iff
   * the input string starts with an 'alpha_' character followed by 'alpha_num_' character
   *)
  let is_ident : string -> bool =
    CharStream.(recognize_string (alpha_ >> skip_many alpha_num_ >> eof()))

  (* Atomic Lexer *)

  type token =
    | Comment of string (* comments *)
    | Symbol  of string (* registered <symbol>s *)
    | Value   of value  (* special values : integers, characters, strings, ... *)
    | Keyword of string (* registered <keyword>s *)
    | Ident   of string (* identifier *)
    | Word    of string (* any other words *)
    | White   of char   (* white character *)
    | Other   of char   (* any other character *)

  module ToS =
  struct
    open STools
    open ToS

    let value =
      function
      | String s -> "String "^(string s)
      | Char   c -> "Char "^(char c)
      | Int    i -> "Int "^(int i)

    let token =
      function
      | Comment c -> "Comment "^(string c)
      | Symbol  s -> "Symbol "^(string s)
      | Value   v -> "Value "^(value v)
      | Keyword k -> "Keyword "^(string k)
      | Ident   i -> "Ident "^(string i)
      | Word    w -> "Word "^(string w)
      | White   w -> "White "^(char w)
      | Other   o -> "Other "^(char o)
  end

  let lexer : (char, token) t =
    (comment => (fun s -> Comment s)) <|>
    (symbol  => (fun s -> Symbol  s)) <|>
    (value   => (fun v -> Value   v)) <|>
    (CharStream.word_ =>
      (fun w ->
             if is_keyword w
        then Keyword w
        else if is_ident w
        then Ident   w
        else (
          print_endline ("unexpected word:"^(STools.ToS.string w));
          Word    w
        )
      )
    ) <|>
    (CharStream.white => (fun c -> White c)) <|>
    (any     => (fun c -> print_endline ("unexpected character: "^(STools.ToS.char c)); Other c))
end

module ParaLex =
struct
  module Model : OpalParaLex.MSig
  with type token   = AtomicLexer.token
  and  type bracket = string
  =
  struct
    type token   = AtomicLexer.token
    type bracket = string

    open AtomicLexer
    let ignore =
      function
      | Comment _
      | White _ -> true
      | _ -> false

    let bracket =
      function
      (* | Symbol "(" -> Some(function Symbol ")" -> Some "( )" | _ -> None) *)
      | Symbol "{" -> Some(function Symbol "}" -> Some "{ }" | _ -> None)
      | Symbol "[" -> Some(function Symbol "]" -> Some "[ ]" | _ -> None)
      | _ -> None

    let failure =
      function
      (* | Symbol ")" -> Some "closed bracket ')' with prior opening with '('" *)
      | Symbol "}" -> Some "closed bracket '}' with prior opening with '{'"
      | Symbol "]" -> Some "closed bracket ')' with prior opening with '['"
      | _ -> None
  end
  include Model

  module ToPrettyS =
  struct
    open STools.ToS
    open AtomicLexer.ToS

    let token_tree = TreeUtils.ToPrettyS.atree string token
  end

  module Module = OpalParaLex.Make(Model)
  include Module
end

module Parser =
struct
  open AtomicLexer
  open ParaLex
  open OpalATree

  type 'r t = (string, token, 'r) OpalATree.t

  let comment : string t =
    let* leaf = any_leaf in
    match leaf with
    | Comment c -> return c
    | _ -> mzero

  let exactly_symbol (s:string) : token t =
    exactly_leaf (Symbol s)

  let symbol : string t =
    let* leaf = any_leaf in
    match leaf with
    | Symbol s -> return s
    | _ -> mzero

  let value : value t =
    let* leaf = any_leaf in
    match leaf with
    | Value v -> return v
    | _ -> mzero

  let string : string t =
    let* v = value in
    match v with
    | String s -> return s
    | _ -> mzero

  let exactly_keyword (k:string) : token t =
    exactly_leaf (Keyword k)

  let keyword : string t =
    let* leaf = any_leaf in
    match leaf with
    | Keyword s -> return s
    | _ -> mzero

  let ident : string t =
    let* leaf = any_leaf in
    match leaf with
    | Ident i -> return i
    | _ -> mzero

  let tuple_coma_square_bracket (elem:'r t) : 'r list t =
    sub_tuple "[ ]" (exactly_symbol ",") elem

  let tuple_coma_wavy ?(empty=true) (elem:'r t) : 'r list t =
    sub_tuple ~empty "{ }" (exactly_symbol ",") elem

  let tuple_wavy ?(empty=true) (elem:'t t) : 'r list t =
    sub "{ }" (if empty then (until elem) else (until1 elem))

  let tuple_coma (elem:'r t) : 'r list t =
    elem $:: (until (exactly_symbol "," >> elem))

  (* Section. Non-Recurisve Types *)

  open Lang_DOT_Types

  let id : id t =
    string <|>
    ident

  (* compass_point ::= "n" | "ne" | "e" | "se" | "s" | "sw" | "w" | "nw" | "c" | "_" *)
  (* case independent *)
  let compass_point : compass_point t =
    let* cp = string in
    match String.lowercase_ascii cp with
    | "n"  -> return CP_N
    | "ne" -> return CP_NE
    | "e"  -> return CP_E
    | "se" -> return CP_SE
    | "s"  -> return CP_S
    | "sw" -> return CP_SW
    | "w"  -> return CP_W
    | "nw" -> return CP_NW
    | "c"  -> return CP_C
    | "_"  -> return CP_Any (* '_' *)
    | _ -> mzero

  (* port ::= { ':' ID }? { ':' compass_point }? *)

  let port : port t =
    (option ( exactly_symbol ":" >> id )) $* (option ( exactly_symbol ":" >> compass_point ))

  (* node_id ::= ID port *)

  let node_id : node_id t = id $* port

  (* edgeop ::= "->" | "--" *)
  let edgeop : edgeop t =
    let* cp = symbol in
    match cp with
    | "->" -> return EO_Toward
    | "--" -> return EO_Undirected
    | _ -> mzero

  (* "subgraph" { ID }? *)
  let subgraph_left : subgraph_left t =
    exactly_keyword "subgraph" >> (option id)

  let colon : colon t =
    let* s = symbol in
    match s with
    | ";" -> return SemiColon
    | "," -> return Coma
    | _ -> mzero

  let attr_left : attr_left t =
    let* kw = keyword in
    match kw with
    | "graph" -> return AL_Graph
    | "node"  -> return AL_Node
    | "edge"  -> return AL_Edge
    | _ -> mzero

  (* attr_set ::= ID '=' ID { colon }? *)
  let attr_set : attr_set t =
    let* id1 = id in
    let* _ = exactly_symbol "=" in
    let* id2 = id in
    let* _ = option colon in
    return (id1, id2)

  (* attr_list ::= '[' { attr_set }* ']' *)
  let attr_list : attr_list t =
    sub "[ ]" (until attr_set)

  (* attr_statement ::= attr_left { attr_list }+ *)
  let attr_statement : attr_statement t =
    attr_left $* (until1 attr_list)

  (* node_statement ::= node_id { attr_list }* *)
  let node_statement : node_statement t =
    node_id $* (until attr_list)

  (* edge_right ::= edgeop node_id_or_subgraph *)
  let rec edge_right : edge_right t =
    (fun x -> (edgeop $* node_id_or_subgraph) x)

  (* edge_statement ::= node_id_or_subgraph { edge_right }+ { attr_list }* *)
  and     edge_statement =
    (fun x -> (trio node_id_or_subgraph (until1 edge_right) (until attr_list)) x)

  (* node_id_or_subgraph ::= node_id | subgraph *)
  and     node_id_or_subgraph : node_id_or_subgraph t =
    (fun x ->
      ((node_id => (fun x -> NIOS_Node x)) <|>
      (subgraph => (fun x -> NIOS_SubGraph x))) x
    )

  (* subgraph ::= { subgraph_left }? '{' statement_list '}' *)
  and     subgraph : subgraph t =
    (fun x -> ((option subgraph_left) $* (sub "{ }" (until statement))) x)

  (*
  statement ::=
  | node_statement
  | edge_statement
  | attr_statement
  | ID '=' ID
  | subgraph
   *)
  and     statement : statement t =
    (fun x ->
      ((edge_statement => (fun x -> Statement_Edge x)) <|>
      (node_statement => (fun x -> Statement_Node x)) <|>
      (attr_statement => (fun x -> Statement_Attr x)) <|>
      ((id $* (exactly_symbol "=" >> id)) => (fun (x, y) -> Statement_Equal (x, y))) <|>
      (subgraph => (fun x -> Statement_SubGraph x))) x
    )

  (* statement_list ::= '{' { statement { ';' }? }* '}' *)
  and     statement_list : statement_list t =
    (fun x -> (sub "{ }" (until (statement << (option(exactly_symbol ";"))))) x)

  (* graph ::= { strict }? { graph | digraph } { ID }? statement_list *)
  let graph : graph t =
    let* graph_strict = (exactly_keyword "strict" >> return true <|> return false) in
    let* graph_directed = (
      (exactly_keyword "digraph" >> return true) <|>
      (exactly_keyword "graph" >> return false)
    )
    in
    let* graph_id = option id in
    let* graph_statement = statement_list in
    return {graph_strict; graph_directed; graph_id; graph_statement}

end

let lex_file (target:string) : AtomicLexer.token list =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let lexed_list = LazyStream.to_list lexed_stream in
  lexed_list

let paralex_file (target:string) : ParaLex.token_tree list =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  paralexed_list

let parse_file (target:string) : Lang_DOT_Types.graph =
  let channel = open_in target in
  (* let tee c = print_string (STools.ToS.char c); flush stdout in *)
  let stream  = LazyStream.of_channel channel in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let program = Parser.graph paralexed_list |> Tools.unop |> fst in
  program

