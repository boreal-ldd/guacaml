(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Opal : Monadic Parser Combinators for OCaml
 *
 * === DISCLAIMER ===
 *
 * This file is more or less directly imported from the
 * [opal](https://github.com/pyrocat101/opal) project on GitHub.
 * With time both versions may diverge, in particular :
 *   - factorisation with other part of GuaCaml
 *   - extended syntax and utilities
 *
 *)

module Stream = Iter.Stream

(* Section. Lazy Stream *)
module LazyStream :
sig
  (* Polymorphic lazy stream type. *)
  type 't t = Cons of 't * 't t Lazy.t | Nil

  (* Build a lazy stream from stream. *)
  val of_stream : 't Stream.t -> 't t

  (* Build a lazy stream from a function `f`. The elements in the stream is populated by calling `f ()`. *)
  val of_function : (unit -> 't option) -> 't t

  (* Build a char lazy stream from string. *)
  val of_string : string -> char t

  (* Build a lazy stream from a list. *)
  val of_list : 'a list -> 'a t

  (* Build a char lazy stream from a input channel. *)
  val of_channel : ?tee:(char -> unit) -> in_channel -> char t

  (* Evaluates the stream, retrieves it's first element if any, do one computation step *)
  val eval1 : 'a t -> ('a * 'a t) option

  (* Retrives the first element of the lazy stream, no computation step *)
  val first : 'a t -> 'a option

  (* Evaluates a lazy stream into an explicit list *)
  val to_list : 'a t -> 'a list
end

(* an alias for LazyStream.t *)
type 't stream = 't LazyStream.t

(* Section. String Utilities *)

(* Implode character list into a string. Useful when used with `many`. *)
val implode : char list -> string

(* Explode a string into a character list. *)
val explode : string -> char list

(* Infix operator for left-to-right function composition. `(f % g % h) x` is equivalent to `h (g (f x))`. *)
val ( % ) : ('a -> 'b) -> ('b -> 'c) -> 'a -> 'c

(* Section. Primitives *)

(* A parser is a function that accepts an input stream and returns either `None` on
 * failure, or `Some (result, input')`, where `result` is user-defined value and
 * `input'` is unconsumed input stream after parsing.
 *)
type 'token input = 'token LazyStream.t

type ('token, 'result) monad (* = ('result * 'token input) option *)

type ('token, 'result) parser = 'token input -> ('token, 'result) monad

type ('t, 'r) t = ('t, 'r) parser

(* [parse parser stream]
 * parses [stream] with [parser], and returns [Some result] if succeed, or [None] on failure.
 *)
val parse : ('t, 'a) t -> 't stream -> 'a option

(* [parse_from_file parser file_name]
 * parses [file_name] with [parser], and returns [Some result] if succeed, or [None] on failure.
 *)
val parse_from_file : (char, 'a) t -> string -> 'a option

(* Accepts a value and an input, and returns a monad. *)
val return : 'a -> ('t, 'a) t

(* `x >>= f` returns a new parser that if parser `x` succeeds, applies function `f`
 * on monad produced by `x`, and produces a new monad (a.k.a. `bind`). *)
val ( >>= )  : ('t, 'a) t -> ('a -> ('t, 'b) t) -> ('t, 'b) t

(* This operator is the same as `>>=` but using the `let` notation.
   It is usefull to avoid ugly sequences of bindings. For exemple, `p >>= fun x -> f x` can
   be rewritten `let* x = p in f x`. Combined with the `return` function, we can define complex parsers :

```ocaml
let tuple_parser =
  let* x = digit in
  let* _ = exactly ',' in
  let* y = digit in
  return (x, y)
```
 *)
val ( let* ) : ('t, 'a) t -> ('a -> ('t, 'b) t) -> ('t, 'b) t

(* Choice combinator. The parser `p <|> q` first applies `p`. If it succeeds, the
   value of `p` is returned. If `p` fails, parser `q` is tried.
 *)
val ( <|> ) : ('t, 'a) t -> ('t, 'a) t -> ('t, 'a) t

(* [scan parser stream = stream'] where :
 *   - the input [stream] is of the form [stream = [< x1; x2; x3; ...>] ], and,
 *   - the returned [stream'] is of the form [stream' = [<y1; y2; y3; ...>] ] where :
 *     + y1 = parser [< x_1; x_2; x_3; ... >]
 *     + y2 = parser [< x_2; x_3; ... >]
 *     + ...
 *     + yk = parser [< x_k; x_{k+1}; ... >]
 *)
(* [TODO] find use case *)
val scan : ('t, 't) t -> 't stream -> 't stream

(* A parser that always fails. *)
val mzero : ('t, _) t

(* A parser that parses nothing *)
val mtrue : ('t, unit) t

(* The parser succeeds for any token in the input. Consumes a token and returns it. *)
val any : ('t, 't) t

(* The parser `satisfy test` succeeds for any token for which the supplied function
 * `test` returns `true`. Returns the token that is actually parsed.
 * (NB: if the [test] returns [false] the parser fails (with mzero))
 *)
val satisfy : ('t -> bool) -> ('t, 't) t

(* The parser `eof x` succeeds if the input is exhausted. Returns value `x`. *)
val eof : 'a -> ('t, 'a) t

(* Section. Derived Combinators *)

(* Map combinator. `x => f` parses `x`. If it succeeds, returns the value of `x` applied with `f`. *)
val ( => ) : ('t, 'a) t -> ('a -> 'b) -> ('t, 'b) t

(* Ignore-right combinator. `x >> y` parses `x` and then `y`. Returns the value returned by `x`. *)
val ( << ) : ('t, 'a) t -> ('t, 'b) t -> ('t, 'a) t

(* Ignore-left combinator. `x >> y` parses `x` and then `y`. Returns the value returned by `y`. *)
val ( >> ) : ('t, 'a) t -> ('t, 'b) t -> ('t, 'b) t

(* Cons combinator. `head $:: tail` parses `head` and then `tail`.
 * Returns [head' :: tail'] where
 *   - [head'] is the value returned by [head]
 *   - [tail'] is the value returned by [tail]

```ocaml
let ident = letter $:: many alpha_num
```
 *)
val ( $:: ) : ('t, 'a) t -> ('t, 'a list) t -> ('t, 'a list) t

(* [TODO] *)
val ( $* ) : ('t, 'a) t -> ('t, 'b) t -> ('t, 'a * 'b) t

(* `choice ps` tries to apply the parsers in the list `ps` in order, until one of
 * them succeeds. Returns the value of the succeeding parser. *)
val choice : ('t, 'a) t list -> ('t, 'a) t

(* `ntimes n` parses `n` occurrences of `p`. If `n` is smaller or equal to zero, the
 * parser equals to `return []`. Returns a list of `n` values returned by `p`. *)
val ntimes : int -> ('t, 'a) t -> ('t, 'a list) t

(* `between open close p` parses `open`, followed by `p` and `close`.
 * Returns the value returned by `p`.

```ocaml
let braces = between (exactly '{') (exactly '}')
```
 *)
val between : ('t, 'a) t -> ('t, 'b) t -> ('t, 'c) t -> ('t, 'c) t

(* `option default p` tries to apply parser `p`. If `p` fails, it returns the
 * value `default`, otherwise the value returned by `p`.

```ocaml
let priority = option_default 0 (digit => STools.SUtils.int_of_char)
```
 *)
val option_default : 'a -> ('t, 'a) t -> ('t, 'a) t

(* `optional p` tries to apply parser `p`. It will parse `p` or nothing. It only
   fails if `p` fails. Discard the result of `p`.
 *)
val optional   : ('t, 'a) t -> ('t, unit) t

(* try to parse [a] if succeeds return [Some a'] where [a'] is the result retuned by [a],
 * and [None] otheriwse *)
val option : ('t, 'a) t -> ('t, 'a option) t

(* `skip_many p` applies `p` *zero or more* times, skipping its result.

```ocaml
let spaces = skip_many space
```
 *)
val skip_many  : ('t, 'a) t -> ('t, unit) t

(* `skip_many1 p` applies `p` *one or more* times, skipping its result. *)
val skip_many1 : ('t, 'a) t -> ('t, unit) t

(* `many p` applies the parser `p` *zero or more* times. Returns a list of returned values of `p`. *)
val many  : ('t, 'a) t -> ('t, 'a list) t

(* `many1 p` applies the parser `p` *one or more* times. Returns a list of returned
 * values of `p`. *)
val many1 : ('t, 'a) t -> ('t, 'a list) t

(* `sep_by p sep` parses *zero or more* occurrences of `p`, separated by `sep`.
   Returns a list of values returned by `p`.

```ocaml
let comma_sep p = sep_by p (token ",")
```
 *)
val sep_by  : ('t, 'a) t -> ('t, 'b) t -> ('t, 'a list) t

(* `sep_by1 p sep` parses *one or more* occurrences of `p`, separated by `sep`.
   Returns a list of values returned by `p`. *)
val sep_by1 : ('t, 'a) t -> ('t, 'b) t -> ('t, 'a list) t

(* `end_by p sep` parses *zero or more* ocurrences of `p`, separated and ended by
   `sep`. Returns a list of values returned by `p`.

```ocaml
let statements = end_by statement (token ";")
```
  *)
val end_by  : ('t, 'a) t -> ('t, 'b) t -> ('t, 'a list) t

(* `end_by1 p sep` parses *one or more* ocurrences of `p`, separated and ended by
   `sep`. Returns a list of values returned by `p`. *)
val end_by1 : ('t, 'a) t -> ('t, 'b) t -> ('t, 'a list) t

(* `chainl p op default` parses *zero or more* occurrences of `p`, separated by
   `op`. Returns a value obtained by a *left* associative application of all
   functions by `op` to the values returned by `p`. If there are zero occurences
   of `p`, the value `default` is returned.
 *)
val chainl  : ('t, 'a) t -> ('t, 'a -> 'a -> 'a) t -> 'a -> ('t, 'a) t

(* `chainl1 p op` parses *one or more* occurrences of `p`, separate by `op`.
   Returns a value obtained by a *left* associative application of all functions
   returned by `op` to the values returned by `p`. This parser can be used to
   eliminate left recursion which typically occurs in expression grammars. See
    the arithmetic caculator example above.
 *)
val chainl1 : ('t, 'a) t -> ('t, 'a -> 'a -> 'a) t -> ('t, 'a) t

(* `chainr p op default` parses *zero or more* occurrences of `p`, separated by
     `op`. Returns a value obtained by *right* associative application of all
    functions returned by `op` to the values returned by `p`. If there are no
    occurrences of `p`, the value `x` is returned.
 *)
val chainr  : ('t, 'a) t -> ('t, 'a -> 'a -> 'a) t -> 'a -> ('t, 'a) t

(* `chainr1 p op` parses *one or more* occurrences of `p`, separated by `op`.
   Returns a value obtained by *right* associative application of all functions
   returned by `op` to the values returned by `p`.
 *)
val chainr1 : ('t, 'a) t -> ('t, 'a -> 'a -> 'a) t -> ('t, 'a) t

(* [TODO]
 * semantically equivalent to [many] but tail-recursive hence faster
 *)
val until : ('t, 'a) t -> ('t, 'a list) t

(* [TODO]
 * semantically equivalent to [many1] but tail-recursive hence faster
 *)
val until1 : ('t, 'a) t -> ('t, 'a list) t

(* [TODO]
 * tail-recursive
 *)
val until_stop : ('t, 'a) t -> ('t, 'b) t -> ('t, 'a list * 'b) t

(* [TODO]
 * tail-recursive
 *)
val until_skip_stop : ('t, 'a) t -> ('t, _)t -> ('t, 'b) t -> ('t, 'a list * 'b) t

(* [TODO]
 * tail-recursive
 *)
val skip_until : ('t, 'a) t -> ('t, 'a) t

(* Section. Utils *)

(* [must fail p] returns a parser which acts like [p]
 * except that if [p] fails, [fail] is called
 *)
val must : ?pre:(unit -> unit) -> (unit -> unit) -> ('t, 'r) t -> ('t, 'r) t

(* [otherwise_failwith msg p] is similar to [must] but raise (Failure msg) *)
val otherwise_failwith : string -> ('t, 'r) t -> ('t, 'r) t

(* [TODO] *)
val post_process :
  ?parsed:('r -> unit) ->
  ?failed:(unit -> unit) ->
    ('t, 'r)t -> ('t, 'r)t

(* [tuple fst sep lst elem] recognizes streams of the form
 * [fst] [elem] [sep] [elem] [sep] ... [sep] [elem] [lst] and returns the list of recognized [elem]
 *)
val tuple : ('t, _)t -> ('t, _)t -> ('t, _)t ->  ('t, 'a)t -> ('t, 'a list) t

(* [TODO] *)
(* usefull to use a first parser as lexer *)
val stream_of_parser : ('a, 'b) t -> 'a stream -> 'b stream

(* Section. Singletons *)

(* `exactly x` parses a single token `x`. Returns the parsed token (i.e. `x`).
 * if the parsed token is not `x` then [exactly x] fails (see mzero)

```ocaml
let semi_colon = exactly ';'
```
 *)
val exactly : 't -> ('t, 't) t

(* [one_of xs] succeeds if the current token is in the supplied list of tokens
 *   `xs`. Returns the parsed token.

```ocaml
let vowel = one_of ['a'; 'e'; 'i'; 'o'; 'u']
```
 *)
val one_of : 't list -> ('t, 't) t

(* As the dual of `one_of`, `none_of xs` succeeds if the current token *not* in
   the supplied list of tokens `xs`. Returns the parsed token.

```ocaml
let consonant = none_of ['a'; 'e'; 'i'; 'o'; 'u']
```
 *)
val none_of : 't list -> ('t, 't) t

(* [range low high] succeeds if the current token is in the range between `low`
 *  and `high` (inclusive). Returns the parsed token.
 *)
val range : 't -> 't -> ('t, 't) t

module CharStream :
sig
(** Section. Char Parsers **)

  (* Parses a space character (`'\s\t\r'`). Returns the parsed character.
   *)
  val space     : (char, char) t

  (* Skip *zero or more* spaces characters. *)
  val spaces    : (char, unit) t

  (* Parses a white space character (`'\s\t\r\n'`). Returns the parsed character.
   *)
  val white     : (char, char) t

  (* Skip *zero or more* white spaces characters. *)
  val whites    : (char, unit) t

  (* Parses a newline character (`'\n'`). Returns a newline character. *)
  val newline   : (char, char) t

  (* Parses a tab character (`'\t'`). Returns a tab character. *)
  val tab       : (char, char) t

  (* Parses an upper case letter (a character between 'A' and 'Z'). Returns the parsed character. *)
  val upper     : (char, char) t

  (* Parses a lower case letter (a character between 'a' and 'z'). Returns the parsed character. *)
  val lower     : (char, char) t

  (* Parses a digit. Returns the parsed character. *)
  val digit     : (char, char) t

  (* Parses a letter. Returns the parsed character. *)
  val letter    : (char, char) t

  (* Parses a letter or digit. Returns the parsed character. *)
  val alpha_num : (char, char) t

  (* Parses an alpha_num of '_' the underscore. Returns the parsed character. *)
  val alpha_num_ : (char, char) t

  (* Parses a letter or '_' the underscore. Return the parsed character *)
  val alpha_ : (char, char) t

  (* Parses a hexadecimal digit (a digit or a letter between 'a' and 'f' or 'A' and 'F').
   * Returns the parsed character. *)
  val hex_digit : (char, char) t

  (* Parses an octal digit (a character between '0' and '7'). Returns the parsed character. *)
  val oct_digit : (char, char) t

  (* Parses a continuous string of alpha-numerical characters, e.g. "abc", "123", "2x", "x2"
   * To parse "2x2" as [(2, "x2")] use the following command : [(option_default 1 int) $* word]
   *)
  val word : (char, string) t

  (* Similar to [word].
   * Parses a continuous string of alpha-numerical + '_' (underscore) characters, e.g. "abc", "_123", "2_x", "_x2"
   *)
  val word_ : (char, string) t

  (* Similar to [word].
   * Parses either an alpha-numerical character OR '_' (underscore) OR '\'' (apostroph)
   *)
  val ocaml_ident_char : (char, char) t

  (* Similar to [word].
   * Parses a continuous string of alpha-numerical + '_' (underscore) + '\'' (apostroph) characters, e.g. "'abc", "_123'", "2'_x", "_x2'"
   *)
  val ocaml_ident : (char, string) t

(* Section. Basic Types *)
  (* [TODO]
   *)
  val nat : (char, int) t

  (* [TODO]
   *)
  val int : (char, int) t

  (* [TODO]
   * [FIXME]
   *)
  val string : (char, string) t

  (* [TODO]
   * [FIXME]
   *)
  val char : (char, char) t

  (* [TODO] *)
  val gen_tuple_coma_bracket : ?left:char -> ?sep:char -> ?right:char -> (char, 'a)t -> (char, 'a list)t

  (* [TODO] *)
  val tuple_coma_bracket : (char, 'a) t -> (char, 'a list) t

(* Section. Lexer *)

  (* lexes [word]s, any non-word material character is turned into a single character string *)
  val lexer : (char, string) t

  (* lexes [word_]s, any non-word material character is turned into a single character string *)
  val lexer_ : (char, string) t

  (* just like [lexer_] but spaces (i.e. " \t\r") are mapped to " " single character string *)
  (* we split the 'char' stream into 'string' stream with the following rules :
   * - contiguous sequence of alpha-numerical characters + '_' are assembled
   * - space-ish [' '; '\t', '\r'] characters are converted to ' '
   * - every other character is left as is
   *)
  val lexer_spaces_ : (char, string) t

(* Section. Lex Helper *)

  (* `lexeme p` first applies `skip_many white` and then parser `p`.
   * Returns the value returned by `p`.
   *)
  val lexeme : (char, 't) t -> (char, 't) t

  (* [token s] skips leading white spaces and parses a sequence of characters given
   * by string `s`. Returns the parsed character sequence as a list.

  ```ocaml
  div_or_mod = token "div" <|> token "mod"
  ```
   *)
  val token : string -> (char, string) t

  (* [TODO] *)
  (* returns [true] iff [EOF], [false] otherwise *)
  val skip_until_newline : (char, bool) t

  (* [TODO] *)
  val tee : (char -> unit) -> char stream -> char stream

  (* [TODO] *)
  val ctee : ?cond:bool -> (char -> unit) -> char stream -> char stream

  (* [TODO] *)
  val parse_from_string : (char, 'r) t -> string -> 'r option

  (* [parse_string_as_char_stream parser string]
   * parses [string] with [parser], and returns [Some result] if succeed, or [None] on failure.
   *)
  val parse_string_as_char_stream : (char, 'a) t -> (string, 'a) t

  (* [recognize_string parser string] returns [true] iff
   * [parser] can parse [string]
   * if the word as to be exactly recognize (and not just a prefix) end the parser with [parser << eof()]
   *)
  val recognize_string : (char, 'a)t -> string -> bool
end

module StringStream :
sig
  (* [TODO] *)
  val gen_tuple_coma_bracket :
    ?left:string -> ?sep:string -> ?right:string -> (string, 'a)t -> (string, 'a list)t

  (* [TODO] *)
  val tuple_coma_bracket : (string, 'a) t -> (string, 'a list) t
end
