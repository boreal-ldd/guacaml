(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_OCaml_types_ToS : the let of OCaml's types => export to string
 *
 *)

open Lang_OCaml_type_Types
open STools
module ThisToS = Lang_OCaml_type_ToS
open ShiftS
open ToShiftS

(* comments are a hierarchical structure :
 *   every sub-comment starting with '(*' and ending with '*)'
 *)
let comment : comment t = of_ToS ThisToS.comment

let upper_identifier : upper_identifier t = of_ToS ThisToS.upper_identifier

let lower_identifier  : lower_identifier t = of_ToS ThisToS.lower_identifier

let abstract_identifier  : abstract_identifier t = of_ToS ThisToS.abstract_identifier

let module_name  : module_name t = of_ToS ThisToS.module_name

let module_path  : module_path t = of_ToS ThisToS.module_path

let type_name_def  : type_name_def t = of_ToS ThisToS.type_name_def

let type_name_ref  : type_name_ref t = of_ToS ThisToS.type_name_ref

let constr_name_def  : constr_name_def t = of_ToS ThisToS.constr_name_def

let field_name_def  : field_name_def t = of_ToS ThisToS.field_name_def

let rec atomic_type  : atomic_type t =
  (function
    | AT_identifier x -> constructor "AT_identifier" (type_name_ref) x
    | AT_abstract x -> constructor "AT_abstract" (abstract_identifier) x
    | AT_multi (x1, x2) -> constructor "AT_multi " (pair (list product_type) type_name_ref) (x1, x2)
    | AT_subtype x -> constructor "AT_subtype" (product_type) x
  )

and     single_instanciation_type  : single_instanciation_type t =
  (fun x -> pair atomic_type (list type_name_ref) x)

and     product_type  : product_type t =
  (fun x -> (list single_instanciation_type) x)

let record_type  : record_type t =
  (list (pair field_name_def product_type))

let product_or_record_type  : product_or_record_type t =
  (function
    | PORT_product x -> constructor "PORT_product" (product_type) x
    | PORT_record x -> constructor "PORT_record" (record_type) x
  )

let sum_type  : sum_type t =
  (list (pair constr_name_def ((option product_or_record_type))))

let named_type_definition  : named_type_definition t =
  (function
    | NTR_Alias x -> constructor "NTR_Alias" (product_or_record_type) x
    | NTR_Sum x -> constructor "NTR_Sum" (sum_type) x
  )

let named_type  : named_type t =
  (fun { nt_params; nt_name; nt_def } -> ShiftS.record [ ( "nt_params", (list abstract_identifier) nt_params ); ( "nt_name", type_name_def nt_name ); ( "nt_def", named_type_definition nt_def ) ] )

let named_type_rec  : named_type_rec t =
  (list named_type)

let system_type  : system_type t =
  (list named_type_rec)
