(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Guacaml_meta : software for automatic code generation
 *
 * === NOTE ===
 *
 * This software aims at simplifying the automatic generation of
 * simple yet lengthy pieces of code, mostly factories such as :
 *   - ToS : dummy ocaml-type to string conversion [TODO]
 *   - ToPrettyS : pretty-printer for ocaml-type to string conversion [LATER]
 *   - ToB : dummy ocaml-type to binary conversion [LATER]
 *   - OfB : dummy binary to ocaml-type conversion [LATER]
 *
 * The main interest of generating source-code instead of doing
 * straight-forward code generation is that it allows to make arbitrarily
 * complex modification of the source code afterward
 *
 * Furthermore, by using automatic code generation, we may hope at simplifying
 * the unification process of accross the GuaCaml library
 *
 *)

let cmd_parse (input_file:string) (output_file:string) : unit =
  let ocaml = Lang_OCaml_type_Opal.parse_file input_file in
  let string = Lang_OCaml_type_ToPrettyS.system_type ocaml in
  STools.SUtils.output_string_to_file output_file string;
  ()

let cmd_dump_ToShiftS (input_file:string) (output_file:string) : unit =
  let ocaml = Lang_OCaml_type_Opal.parse_file input_file in
  let string = Lang_OCaml_type_ToShiftS.system_type ocaml (Some 0) in
  STools.SUtils.output_string_to_file output_file string;
  ()

let cmd_gen_ToS (input_file:string) (output_file:string) : unit =
  let ocaml = Lang_OCaml_type_Opal.parse_file input_file in
  let string = Guacaml_meta_ocaml_gen_ToS.system_type ocaml in
  STools.SUtils.output_string_to_file output_file string;
  ()

let cmd_gen_ToShiftS (input_file:string) (output_file:string) : unit =
  let ocaml = Lang_OCaml_type_Opal.parse_file input_file in
  let string = Guacaml_meta_ocaml_gen_ToShiftS.system_type ocaml in
  STools.SUtils.output_string_to_file output_file string;
  ()

let _ =
  if Array.length Sys.argv <= 3
  then (failwith "[Guacaml_meta] not enough arguments : --parse | --dump-ToShiftS | --gen-ToS | --gen-ToShiftS")
  else (
    let input_file = Sys.argv.(1) in
    let mode = Sys.argv.(2) in
    let output_file = Sys.argv.(3) in
    match mode with
    | "--parse" -> cmd_parse input_file output_file
    | "--dump-ToShiftS" -> cmd_dump_ToShiftS input_file output_file
    | "--gen-ToS" -> cmd_gen_ToS input_file output_file
    | "--gen-ToShiftS" -> cmd_gen_ToShiftS input_file output_file
    | _ -> failwith ("[Guacaml_meta] "^(STools.ToS.string mode)^" is not a valid mode")
  )
