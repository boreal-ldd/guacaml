open Extra
open PartitionRefinementEppstein
open OUnit

module SOrd =
struct
  type t = string
  let compare = Stdlib.compare
end

module SPR = Make(SOrd)

let test_SPR_create () =
  (* Create a new partition *)
  let p = SPR.make ["a"; "b"; "c"; "d"; "e"; "f"] in
  (* Check the number of sets in the partition *)
  error_assert
    "SPR.length p = 1"
    (SPR.length p = 1);
  (* Check the list representation of the partition *)
  let seta = SPR.set_of_elt p "a" in
  assert (SPR.SET.to_list seta = ["a"; "b"; "c"; "d"; "e"; "f"]);
  assert (SPR.set_of_elt p "b" = seta);
  assert (SPR.set_of_elt p "c" = seta);
  assert (SPR.set_of_elt p "d" = seta);
  assert (SPR.set_of_elt p "e" = seta);
  assert (SPR.set_of_elt p "f" = seta);
  ()

let _ = test_SPR_create ()

let _ = push_test "test_SPR_create" test_SPR_create

let test_SPR_add () =
  (* Create a new partition *)
  let p = SPR.make ["a"; "b"; "c"; "d"; "e"; "f"] in
  (* Add some elements to the partition *)
  SPR.add p "g" (SPR.set_of_elt p "b");
  SPR.add p "h" (SPR.set_of_elt p "c");
  SPR.add p "i" (SPR.set_of_elt p "e");
  (* Check the number of sets in the partition *)
  assert (SPR.length p = 1);
  let seta = SPR.set_of_elt p "a" in
  assert (SPR.SET.to_list seta = ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"])

let _ = push_test "test_SPR_add" test_SPR_add

let test_PR_empty_set () =
  let s = SPR.SET.empty () in
  assert (SPR.SET.is_empty s);
  SPR.SET.iter (fun x -> assert false) s

let _ = push_test "test_PR_empty_set" test_PR_empty_set

let test_PR_set_of_list () =
  let s = SPR.SET.of_list ["a"; "b"; "c"] in
  assert (not (SPR.SET.is_empty s));
  assert(SPR.SET.to_list s = ["a"; "b"; "c"]);
  ()

let _ = push_test "test_PR_set_of_list" test_PR_set_of_list

let test_PR_add () =
  (* Create a new partition with elements "a", "b", "c", and "d" *)
  let pr = SPR.make ["a"; "b"; "c"; "d"] in
  (* Check that the partition has the expected sets and elements *)
  assert (SPR.to_list_list pr = [["a"; "b"; "c"; "d"]]);
  (* Add element "e" to the set containing "a" *)
  SPR.add pr "e" (SPR.set_of_elt pr "a");
  (* Check that the partition has the expected sets and elements *)
  assert (SPR.to_list_list pr = [["a"; "b"; "c"; "d"; "e"]])

let _ = push_test "test_PR_add" test_PR_add

let test_PR_remove () =
  (* Create a new partition with elements "a", "b", "c", and "d" *)
  let pr = SPR.make ["a"; "b"; "c"; "d"] in
  (* Check that the partition has the expected sets and elements *)
  assert (SPR.to_list_list pr = [["a"; "b"; "c"; "d"]]);
  (* Add element "e" to the set containing "a" *)
  SPR.remove pr "c";
  (* Check that the partition has the expected sets and elements *)
  assert (SPR.to_list_list pr = [["a"; "b"; "d"]])

let _ = push_test "test_PR_remove" test_PR_remove

let test_PR_refine_000 () =
  (* Create a new partition with the elements "a", "b", "c", and "d" *)
  let pr = SPR.make ["a"; "b"; "c"; "d"] in
  (* Refine the partition using the elements "b" and "c" *)
  let refined_sets = SPR.refine pr ["b"; "c"] in
  (* Check that the partition has been refined into two sets:
     one containing "b" and "c" and the other containing "a" and "d" *)
  assert (refined_sets = [(SPR.set_of_elt pr "b", SPR.set_of_elt pr "a")]);
  (* Check that the elements in the sets are correct *)
  assert (SPR.SET.to_list (SPR.set_of_elt pr "b") = ["b"; "c"]);
  assert (SPR.SET.to_list (SPR.set_of_elt pr "a") = ["a"; "d"])

let _ = push_test "test_PR_refine_000" test_PR_refine_000

let test_PR_refine_001 () =
  (* Create a new partition with the elements ["a"; "b"; "c"; "d"] *)
  let pr = SPR.make ["a"; "b"; "c"; "d"] in
  (* Check that the partition has one set with 4 elements *)
  assert (SPR.length pr = 1);
  assert (SPR.SET.to_list (SPR.set_of_elt pr "a") = ["a"; "b"; "c"; "d"]);
  (* Refine the partition according to the element "b" *)
  let refined_sets = SPR.refine pr ["b"] in
  (* Check that the partition now has 2 sets with 2 elements each *)
  assert (SPR.length pr = 2);
  (* Check that the refined sets are correct *)
  assert (refined_sets = [(SPR.set_of_elt pr "b", SPR.set_of_elt pr "a")]);
  assert (SPR.SET.to_list (SPR.set_of_elt pr "a") = ["a"; "c"; "d"]);
  assert (SPR.SET.to_list (SPR.set_of_elt pr "b") = ["b"])

let _ = push_test "test_PR_refine_001" test_PR_refine_001

let test_PR_refine_002 () =
  let pr = SPR.make ["0"; "1"] in
  assert(SPR.to_list_list pr = [["0"; "1"]]);
  SPR.remove pr "0";
  assert(SPR.to_list_list pr = [["1"]]);
  let out = SPR.refine pr ["1"] in
  assert(out = []);
  ()

let _ = push_test "test_PR_refine_002" test_PR_refine_002

let test_PR_refine_003 () =
  let pr = SPR.make ["0"; "1"; "2"; "3"; "4"] in
  SPR.remove pr "0";
  assert(SPR.to_list_list ~sort:true pr = [["1"; "2"; "3"; "4"]]);
  let out1 = SPR.refine pr ["1"] in
  assert(SPR.to_list_list ~sort:true pr = [["1"]; ["2"; "3"; "4"]]);
  let res1 = out1 ||> (fun (x, y) -> (SPR.SET.to_list x, SPR.SET.to_list y)) in
  assert(res1 = [(["1"], ["2"; "3"; "4"])]);
  SPR.remove pr "2";
  assert(SPR.to_list_list ~sort:true pr = [["1"]; ["3"; "4"]]);
  let out2 = SPR.refine pr ["1"; "3"] in
  assert(SPR.to_list_list ~sort:true pr = [["1"]; ["3"]; ["4"]]);
  let res2 = out2 ||> (fun (x, y) -> (SPR.SET.to_list x, SPR.SET.to_list y)) in
  assert(res2 = [(["3"], ["4"])]);
  SPR.remove pr "4";
  print_endline (STools.ToS.(list (list string)) (SPR.to_list_list pr));
  assert(SPR.to_list_list ~sort:true pr = [["1"]; ["3"]]);
  SPR.remove pr "1";
  assert(SPR.to_list_list ~sort:true pr = [["3"]]);
  SPR.remove pr "3";
  assert(SPR.to_list_list ~sort:true pr = []);
  ()

let _ = push_test "test_PR_refine_003" test_PR_refine_003

let _ = run_tests ()
