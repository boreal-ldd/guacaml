(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_OCaml_types_ToS : the let of OCaml's types => export to string
 *
 *)

open Lang_OCaml_type_Types
open STools
open ToS

(* comments are a hierarchical structure :
 *   every sub-comment starting with '(*' and ending with '*)'
 *)
let comment = TreeUtils.ToS.tree char

(* [upper_identifier] identifiers starting with an upper case letter *)
let upper_identifier = string

(* [lower_identifier] identifiers starting with a lower case letter (or '_') *)
let lower_identifier = string

(* [abstract_identifier] identifiers starting with the '\'' character *)
let abstract_identifier = string

let module_name = upper_identifier

(* { <module_name> '.' }* *)
let module_path = list module_name

(* identifier used for type definition *)
let type_name_def = lower_identifier

(* identifier used for type reference *)
(* <type-name-ref> ::= <module-path> <type-name-def> *)
let type_name_ref = module_path * type_name_def

(* identifier used for constructor definition *)
let constr_name_def = upper_identifier
(* identifier used for constructor reference *)

(* type identifier regular identifier with lower case first letter or '_' *)
let field_name_def = lower_identifier

(* <atomic-type> ::=
    | <type-identifier>
    | { tuple '(' ',' ')' <product-type> } <type-identifier
    | '(' <product-type> ')'
 *)
let rec atomic_type =
  function
  | AT_identifier  x     ->
    constructor "AT_identifier" type_name_ref                          x
  | AT_abstract    x     ->
    constructor "AT_abstract"   abstract_identifier                    x
  | AT_multi      (x, y) ->
    constructor "AT_multi"      ((list product_type) * type_name_ref) (x, y)
  | AT_subtype     x     ->
    constructor "AT_subtype"    product_type                           x

(* <single-intanciation-type> ::= <atomic-type> { <type-identifier> }* *)
and single_instanciation_type x =  (atomic_type * (list type_name_ref)) x

(* tuple '*' <single-instanctiation-type> *)
and product_type x = list single_instanciation_type x

(* <record-type> ::=
    tuple+ '{' ';' '}' { <name-identifier> ':' <product-type> }
      (* there is an optional ';' at the end *)
 *)
let record_type x = list (field_name_def * product_type) x

let product_or_record_type =
  function
  | PORT_product x -> constructor "PORT_product" product_type x
  | PORT_record  x -> constructor "PORT_record"  record_type  x

let sum_type = list (constr_name_def * (option product_or_record_type))

(* <named-type-right> ::= <product-or-record-type> | <sum-type> *)
let named_type_definition : named_type_definition t =
  function
  | NTR_Alias   x ->
    constructor "NTR_Alias"   product_or_record_type x
  | NTR_Sum     x ->
    constructor "NTR_Sum"     sum_type               x

(* single type declaration
 * NB: 'type' or 'and' has already been parsed
 *)
let named_type : named_type t =
  fun {nt_params; nt_name; nt_def} ->
  SUtils.record [
    ("nt_params", list abstract_identifier nt_params);
    ("nt_name  ", type_name_def            nt_name  );
    ("nt_def   ", named_type_definition    nt_def   );
  ]

(* list of mutually-recursive types (SCC in the dependency graph) *)
let named_type_rec : named_type_rec t = list named_type

(* list of list of mutually-recursive types *)
let system_type : system_type t = list named_type_rec
