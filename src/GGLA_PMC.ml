(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * GraGenLA : Generic Adjacency List Graph
 *
 * PMC : potential maximal clique
 *
 * === CONTRIBUTORS ===
 *
 * Joan Thibault
 *   - joan.thibault@irisa.fr
 * Maxime Bridoux
 *   - maxime.bridoux@ens-rennes.fr
 *
 * === NOTE ===
 *
 * being a PMC (Potential Maximal Clique) is a graph property
 * (stable by H-reduction), we shall be able to generize to
 * GGLA.
 *
 *)

open Extra
module GGLA = GraphGenLA
open GGLA.Type
open GGLA_HFT
open GGLA_HFT.Type

module IsPMC =
struct
  type component = GGLA.AdvancedConnected.component
  (* [component : (component, separator)] *)

  (* [connected_components_and_separators hg idents = [(component, separator)]]
   *  (the notion of non-suppressible node is ignore for computation)
   *  [TODO] [??] deal with ft-vertices
   *      (* [??] G = (V u L, E, w) an H-FT-Graph
   *      we say that K \subset V is an MKVM of G iff
   *      K u L is an MKVM of G' = (V u L, E u L^2, w) an H-Graph *)
   *  (we assume [SetList.sorted idents])
   *  returns a list of pairs, one pair per connected component of G\K :
   *  1. component : list of vertices C
   *  2. excluded : set S of vertices of K which are adjacent to C
   *)
  let connected_components_and_separators =
    GGLA.AdvancedConnected.connected_components_and_separators_from_list
  let connected_components_and_separators_from_array =
    GGLA.AdvancedConnected.connected_components_and_separators_from_array

  (* 1. G\K has no full component associated to K *)
  let vm_is_ks (idents:int list) (components:GGLA.AdvancedConnected.component list) : bool =
    List.for_all (fun (_, neighbors) -> neighbors <> idents) components

  (* 2. K is cliquish *)
  let vm_is_kvm (hg:hg) (idents:int list) (components:GGLA.AdvancedConnected.component list) : bool =
    let g' : hg = List.fold_left
      (fun g' (_, neighbors) ->
        GGLA.Utils.graph_add_clique
          ~inplace:true
          ~replace:true
          ~reflexive_false:true
           g'
        (neighbors ||> (fun j -> (j, ()))))
      (Array.copy hg)
      components
    in
    GGLA.Utils.graph_is_clique g' idents

  let is_kvm (hg:hg) (idents:int list) : bool =
    let components = connected_components_and_separators hg idents in
    (* K is cliquish *)
    (vm_is_kvm hg idents components)

  (* match result with
   * | None => hg(selected) is a kvm but not ks => keep going
   * | Some true => hg(selected) is kvm and ks, hence PMC => stop and record
   * | Some false => hg(selected) is not a kvm => stop and skip
   *)
  let internal_pmc (hg:hg) (idents:int list) : bool option * component list =
    let components = connected_components_and_separators hg idents in
    let result =
      (* 2. K is cliquish *)
      if vm_is_kvm hg idents components
      (* 1. G\K has no full component associated to K *)
      then if vm_is_ks idents components
        then Some true
        else None
      else Some false
    in
    (result, components)

  (* [is_pmc hg idents = bool]
      returns true iff idents(=K) is a pmc in the graph hg(=G) *)
  let is_pmc (hg:hg) (idents:int list) : bool =
    let bop, _ = internal_pmc hg idents in
    bop = Some true

  (* match result with
   * | None => hg(selected) is a kvm but not ks => keep going
   * | Some true => hg(selected) is kvm and ks, hence PMC => stop and record
   * | Some false => hg(selected) is not a kvm => stop and skip
   *)
  let ternary_pmc (hg:hg) (idents:int list) : bool option =
    internal_pmc hg idents |> fst
end

type component = IsPMC.component

let connected_components_and_separators =
  IsPMC.connected_components_and_separators

(* [vm_is_ks cif_list] returns [true] iff there is not full component *)
let vm_is_ks = IsPMC.vm_is_ks

(* [vm_is_kvm G K cif_list] returns [true] iff K is cliquish in G *)
let vm_is_kvm = IsPMC.vm_is_kvm

(* [is_pmc hg idents = bool]
    returns true iff [idents] (i.e., K) is a pmc in the graph [hg] (i.e., G) *)
let is_pmc = IsPMC.is_pmc

module EnumPMC =
struct
  let prefix = "GuaCaml.GGLA_PMC.EnumPMC"

  (* [is_pmc hg selected = (ternary, component_list)] where :
   * \forall i, match selected.(i) with
   * | None => unspecified
   * | Some true => selected
   * | Some false => not selected
   *
   * match ternary with
   * | None => [selected] is cliquish in [hg] but not KS => keep going
   * | Some true => [selected] is a PMC of [hg] => stop and record
   * | Some false => [selected] is not cliquish in [hg] => stop and skip
   *)
  let is_pmc (hg:hg) (selected:bool option array) : bool option * IsPMC.component list =
    let idents = MyArray.list_of_indexes ((=)(Some true)) selected in
    IsPMC.internal_pmc hg idents

  let explore1 (hg:hg) (selected:bool option array) : bool array Tree.tree =
    let count = ref 0 in
    let rec explore (hg:hg) (selected:bool option array) : bool array Tree.tree =
      incr count;
      match is_pmc hg selected |> fst with
      | Some true -> Tree.Leaf (Array.map ((=)(Some true)) selected)
      | Some false -> Tree.Node []
      | None -> (
        let selected = Array.copy selected in
        (* [TODO] filter out non-full components *)
        let idents = MyArray.list_of_indexes ((=)None) selected in
        Tree.Node (
          idents
          ||> (fun i ->
            let selected' = Array.copy selected in
            selected.(i) <- Some false;
            selected'.(i) <- Some true;
            explore hg selected'
          )
        )
      )
    in
    let result = explore hg selected in
    print_endline("[GGLA_PMC.EnumPMC.explore1] count:"^(string_of_int !count));
    result

  let explore2 (hg:hg) (selected:bool option array) : bool array Tree.tree =
    let count = ref 0 in
    let rec explore (hg:hg) (selected:bool option array) : bool array Tree.tree =
      incr count;
      let hg_is_pmc, components = is_pmc hg selected in
      match hg_is_pmc with
      | Some true -> Tree.Leaf (Array.map ((=)(Some true)) selected)
      | Some false -> Tree.Node []
      | None -> (
        let selected = Array.copy selected in
        let selected_list = MyArray.list_of_indexes ((=)(Some true)) selected in
        (* filter out non-full components *)
        List.iter (fun (component, neighbors) ->
          if neighbors = selected_list
          then List.iter (fun i -> selected.(i) <- Some false) component
        ) components;
        let idents = MyArray.list_of_indexes ((=)None) selected in
        Tree.Node (
          idents
          ||> (fun i ->
            let selected' = Array.copy selected in
            selected.(i) <- Some false;
            selected'.(i) <- Some true;
            explore hg selected'
          )
        )
      )
    in
    let result = explore hg selected in
    print_endline("[GGLA_PMC.EnumPMC.explore2] count:"^(string_of_int !count));
    result

  let explore3 (hg:hg) (selected:bool option array) : bool array Tree.tree =
    let count = ref 0 in
    let rec explore (hg:hg) (selected:bool option array) : bool array Tree.tree =
      incr count;
      let hg_is_pmc, components = is_pmc hg selected in
      match hg_is_pmc with
      | Some true -> Tree.Leaf (Array.map ((=)(Some true)) selected)
      | Some false -> Tree.Node []
      | None -> (
        let selected = Array.copy selected in
        (* [NOTE] reduce branching factor *)
        let idents = MyArray.list_of_indexes ((=)(Some true)) selected in
        let neighbors : int list = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg idents in
        let candidates =
          (* if neighbors are connected in hg *)
          if GGLA.Connected.is_connected hg neighbors
          then (List.filter (fun i -> selected.(i) = None) neighbors)
          else (MyArray.list_of_indexes ((=)None) selected)
        in
        Tree.Node (
          candidates
          ||> (fun i ->
            let selected' = Array.copy selected in
            selected.(i) <- Some false;
            selected'.(i) <- Some true;
            explore hg selected'
          )
        )
      )
    in
    let result = explore hg selected in
    print_endline("[GGLA_PMC.EnumPMC.explore3] count:"^(string_of_int !count));
    result

  let explore = explore3

  let enum_pmc_from_seed (hg:hg) (seed:bool array) : bool array list =
    seed
    |> Array.map (function true -> Some true | false -> None)
    |> explore hg
    |> TreeUtils.tree_flatten

  let enum_pmc (hg:hg) : bool array list =
    Array.make (Array.length hg) None
    |> explore hg
    |> TreeUtils.tree_flatten
end

let enum_pmc_from_seed = EnumPMC.enum_pmc_from_seed
let enum_pmc = EnumPMC.enum_pmc

module EnumBoundedPMC =
struct
  (* match selected.(i) with
   * | None => unspecified
   * | Some true => selected
   * | Some false => not selected
   *)

  (* match result with
   * | None => hg(selected) is a kvm but not ks => keep going
   * | Some true => hg(selected) is kvm and ks, hence PMC => stop and record
   * | Some false => hg(selected) is not a kvm => stop and skip
   *)
  let is_pmc (hg:hg) (selected:bool option array) : bool option * IsPMC.component list =
    let idents = MyArray.list_of_indexes ((=)(Some true)) selected in
    let components = IsPMC.connected_components_and_separators hg idents in
    let result =
      if IsPMC.vm_is_kvm hg idents components
      then if IsPMC.vm_is_ks idents components
        then Some true
        else None
      else Some false
    in
    (result, components)

  let explore1 (hg:hg) (selected:bool option array) (bound:int) : bool array Tree.tree =
    let count = ref 0 in
    let rec explore (hg:hg) (selected:bool option array) (bound:int) : bool array Tree.tree =
      incr count;
      if bound < 0
      then (Tree.Node [])
      else (
        match is_pmc hg selected |> fst with
        | Some true -> Tree.Leaf (Array.map ((=)(Some true)) selected)
        | Some false -> Tree.Node []
        | None -> (
          if bound = 0
          then (Tree.Node [])
          else (
            let selected = Array.copy selected in
            (* [TODO] filter out non-full components *)
            let idents = MyArray.list_of_indexes ((=)None) selected in
            Tree.Node (
              idents
              ||> (fun i ->
                let selected' = Array.copy selected in
                selected.(i) <- Some false;
                selected'.(i) <- Some true;
                explore hg selected' (bound - (vertex_weight hg.(i)))
              )
            )
          )
        )
      )
    in
    let result = explore hg selected bound in
    print_endline("[GGLA_PMC.EnumBoundedPMC.explore1] count:"^(string_of_int !count));
    result

  let explore2 (hg:hg) (selected:bool option array) (bound:int) : bool array Tree.tree =
    let count = ref 0 in
    let rec explore (hg:hg) (selected:bool option array) (bound:int) : bool array Tree.tree =
      incr count;
      if bound < 0
      then (Tree.Node [])
      else (
        let hg_is_pmc, components = is_pmc hg selected in
        match hg_is_pmc with
        | Some true -> Tree.Leaf (Array.map ((=)(Some true)) selected)
        | Some false -> Tree.Node []
        | None -> (
          if bound = 0
          then (Tree.Node [])
          else (
            let selected = Array.copy selected in
            let selected_list = MyArray.list_of_indexes ((=)(Some true)) selected in
            (* filter out non-full components *)
            List.iter (fun (component, separator) ->
              if separator = selected_list
              then List.iter (fun i -> selected.(i) <- Some false) component
            ) components;
            let idents = MyArray.list_of_indexes ((=)None) selected in
            Tree.Node (
              idents
              ||> (fun i ->
                let selected' = Array.copy selected in
                selected.(i) <- Some false;
                selected'.(i) <- Some true;
                explore hg selected' (bound - (vertex_weight hg.(i)))
              )
            )
          )
        )
      )
    in
    let result = explore hg selected bound in
    print_endline("[GGLA_PMC.EnumPMC.explore2] count:"^(string_of_int !count));
    result

  (* [DEBUG] *)
  let explore3_is_pmc_calls : (hg * bool option array * int) list ref = ref []

  let explore3 (hg:hg) (selected:bool option array) (bound:int) : bool array Tree.tree =
    let count = ref 0 in
    let rec explore (hg:hg) (selected:bool option array) (bound:int) : bool array Tree.tree =
      incr count;
      if bound < 0
      then (Tree.Node [])
      else (
        stack_push explore3_is_pmc_calls (hg, selected, bound);
        let hg_is_pmc, components = is_pmc hg selected in
        match hg_is_pmc with
        | Some true -> Tree.Leaf (Array.map ((=)(Some true)) selected)
        | Some false -> Tree.Node []
        | None -> (
          if bound = 0
          then (Tree.Node [])
          else (
            let selected = Array.copy selected in
            (* [NOTE] reduce branching factor *)
            let idents = MyArray.list_of_indexes ((=)(Some true)) selected in
            let neighbors : int list = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg idents in
            let candidates =
              (* if neighbors are connected in hg *)
              if GGLA.Connected.is_connected hg neighbors
              then (List.filter (fun i -> selected.(i) = None) neighbors)
              else (MyArray.list_of_indexes ((=)None) selected)
            in
            Tree.Node (
              candidates
              ||> (fun i ->
                let selected' = Array.copy selected in
                selected.(i) <- Some false;
                selected'.(i) <- Some true;
                explore hg selected' (bound - (vertex_weight hg.(i)))
              )
            )
          )
        )
      )
    in
    let result = explore hg selected bound in
    print_endline("[GGLA_PMC.EnumBoundedPMC.explore3] count:"^(string_of_int !count));
    result

  let explore = explore3

  (* only return PMC with [size <= bound] *)
  let enum_pmc_from_seed_with_bound (hg:hg) (seed:bool array) (bound:int) : bool array list =
    let selected = Array.map (function true -> Some true | false -> None) seed in
    let bound = bound - weight_of_selection hg seed in
    explore hg selected bound
    |> TreeUtils.tree_flatten
end

let enum_pmc_from_seed_with_bound = EnumBoundedPMC.enum_pmc_from_seed_with_bound

(* Section ?. Extend-Or-Contract Strategy *)

module EnumUtils =
struct
  let prefix = "GuaCaml.GGLA_PMC.EnumUtils"

  type leaf = int list
  (* a set of vertices forming a PMC in the vertex-minor graph *)
  type node = int list array option
  (* each index maps a vertex of the current level to a vertex of the previous level *)
  (* if [node = None] then it is the identity array *)
  type tree = (node, leaf) Tree.atree

  module ToS =
  struct
    open STools
    open ToS

    let leaf = list int
    let node = option(array(list int))

    let tree = TreeUtils.ToS.atree node leaf
  end

  let vertex_neighbors (hg:hg) (x:int) : int list =
    SetList.union [x] (hg.(x).edges ||> fst)

  let vertex_strict_neighbors (hg:hg) (x:int) : int list =
    (hg.(x).edges ||> fst)

  (* [MOVEME] GGLA_HFT *)
  (* [x] is a set of indices (not names) *)
  let extract_minor (hg:hg) (x:int list) : hg =
    assert(GGLA.Check.graph hg);
    assert(x <> []);
    let n = Array.length hg in
    let components = IsPMC.connected_components_and_separators hg x in
    let mask = Array.make n true in
    let g' = Array.copy hg in
    List.iter (fun (component, separator) ->
      (* we add a clique on [separator] in [g'] *)
      ignore(GGLA.Utils.graph_add_clique
        ~inplace:true
        ~replace:true
        ~reflexive_false:true
         g'
        (separator ||> (fun j -> (j, ()))));
      List.iter (fun j -> mask.(j) <- false) component
    ) components;
    assert(mask = MyArray.of_indexes n x);
    g'
    |> GGLA.Utils.subgraph mask
    |> (fun (hg, _, _) -> hg)
    |> GGLA_HFT.normalize

  let rec tree_is_empty = function
    | Tree.ALeaf _ -> false
    | Tree.ANode (_, tl) -> List.for_all tree_is_empty tl

  let tree_is_empty t =
    let nleaf, _ = TreeUtils.count_atree t in
    nleaf = 0

  let print_internal (root:string) (file_name:string) (hg:hg) (x:bool array) : unit =
    print_endline ("["^root^"] g:"^(GGLA_HFT.ToS.hg ~short:false hg));
    let g' = Array.copy hg in
    Array.iteri (GGLA_HFT.vertex_fixed_set g') x;
    GGLA_HFT.to_graphviz_file ~name:"GGLA_PMC" file_name g';
    print_endline ("["^root^"] x:"^(STools.ToS.(array bool) x));
    ()

  type searcher = hg -> bool array -> tree

  (* [g] is the current graph
   *   - [g] is NOT auto-renamed
   * [x] is the seed that needs to be extended
   *   - [Array.length g <= Array.length x]
   *   - [x] denotes a set of vertices (not indices)
   * => normalizes [x] so that the problem is properly expressed
   *    in term of the H-vertices and not the underlying vertices
   *)
  let normalize_autorename (next:searcher) (g:hg) (x:bool array) : tree =
    assert(Array.length g <= Array.length x);
    if (MyArray.for_alli (fun i v -> vertex_names v = [i]) g) &&
       (Array.length g = Array.length x)
    then next g x
    else (
      let rename = Array.map vertex_names g in
      let x' = Array.map (fun vl -> List.exists (fun i -> x.(i)) vl) rename in
      assert(Array.length x' = Array.length g);
      let g' = GGLA_HFT.autorename_vertex g in
      assert(Array.length g' = Array.length x');
      Tree.ANode (Some rename, [next g' x'])
    )

  (* Section ?. Domination Related methods *)

  (* x <= y (i.e., [y] dominates [x] *)
  let vertex_dominates (hg:hg) (x:int) (y:int) : bool =
    SetList.subset_of (vertex_neighbors hg x) (vertex_neighbors hg y)

  let vertex_dominators_of (hg:hg) (x:int) : int list =
    let sngx = vertex_strict_neighbors hg x in
    (* find every [y] which dominates [x] *)
    List.filter (fun y -> vertex_dominates hg x y) sngx

  let vertex_dominateds_by (hg:hg) (x:int) : int list =
    let sngx = vertex_strict_neighbors hg x in
    (* find every [y] which is dominated by [x] *)
    List.filter (fun y -> vertex_dominates hg y x) sngx

  let vertex_find_dominator_of (hg:hg) (x:int) : int option =
    let sngx = vertex_strict_neighbors hg x in
    List.find_opt (fun y -> vertex_dominates hg x y) sngx

  let vertex_find_dominated_by (hg:hg) (x:int) : int option =
    let sngx = vertex_strict_neighbors hg x in
    List.find_opt (fun y -> vertex_dominates hg y x) sngx

  let vertex_top_dominators_of (hg:hg) (x:int) : int list =
    vertex_dominators_of hg x
    |> List.filter (fun y -> vertex_find_dominator_of hg y = None)

  let vertex_top_dominateds_by (hg:hg) (x:int) : int list =
    vertex_dominateds_by hg x
    |> List.filter (fun y -> vertex_find_dominated_by hg y = None)

  let vertex_find_top_dominator_of (hg:hg) (x:int) : int option =
    vertex_dominators_of hg x
    |> List.find_opt (fun y -> vertex_find_dominator_of hg y = None)

  let vertex_find_top_dominated_by (hg:hg) (x:int) : int option =
    vertex_dominateds_by hg x
    |> List.find_opt (fun y -> vertex_find_dominated_by hg y = None)

  (* Section ?. Vertex-Dominator-Extension Theorem (VDET) *)

  (* Case 1 :
   * List each vertex $y$ which dominates a vertex $x$ within the seed $X$
   * Then, apply VDET with $y$ and $X$
   *)
  let extend_with_seed_vertex_dominator
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    let xl = MyArray.list_of_indexes_true x in
    (* find the set of vertices [y] which dominates (at least) one vertex
     * within the seed *)
    let doms = xl ||> vertex_dominators_of hg |> SetList.union_list in
    let doms = SetList.minus doms xl in
    if doms = []
    then next_None hg x
    else next_Some hg (MyArray.copy_set_list x doms true)

  (* Section ?. Expansion Methods *)

  (* assumes that [y] is either not a dominator of [x] n [hg] or that [y] is a K-vertex of [hg] *)
  let basic_expansion next ?(skip_left=false) ?(skip_right=false) (y:int) (hg:hg) (x:bool array) =
    let right () =
      assert(x.(y) = false);
      let span = y :: vertex_dominators_of hg y in
      next hg (MyArray.copy_set_list x span true)
    in
    let left () =
      next (GGLA_HFT.vertex_contraction hg y) x
    in
    [
      (if skip_left then None else Some(left()));
      (if skip_right then None else Some(right()));
    ]
    |> MyList.unop
    |> (function
      | [t] -> t
      |  f  -> Tree.ANode(None, f)
    )

  (* Theorem : Constraint-Free H-vertex Disjunction
   * Let
   * [g] be an H-Graph,
   * [x] be a set of vertices,
   * and [y] (with [x.(y) = false]) be an H-vertex of [g]
   *
   * $$EP(G, X) =
   *   \biguplus_{\{C\in\mathcal{C}\mid X\subseteq C\}} EP(G[C], X)
   *   \biguplus EP(G, X\uplus\{y\})\enspace.$$
   * where :
   *   $\mathcal{C}$ is the decomposition of $S(G, y)$ into
   *   connected components with respect to $N_G[y]$.
   *
   * **NOTE**
   *   The implementation of Solomon's Theorem is spliited accross three
   *   functions :
   *   [solomon_left_masks] : computes the set of sub-components of the
   *     decomposition into connected components of $G[V\setminis N_G(y)]$
   *     and filter-out these which do not contain the seed $X$
   *)
  let solomon_left_masks (hg:hg) (x:bool array) (y:int) : bool array list =
    let n = Array.length hg in
    assert(n = Array.length x);
    (* We compute $\mathcal{C}$ the connected component decomposition
     * of $S(G, y)$ with respect to N_G[y] *)
    let sngy = vertex_neighbors hg y in
    let maskY = MyArray.of_indexes n sngy in
    let xl = MyArray.list_of_indexes_true x in
    IsPMC.connected_components_and_separators_from_array hg maskY
    |> MyList.opmap (fun (component, separator) ->
      let mask = SetList.union component separator in
      (* filter-out components $C \in\mathcal{C}$ such that
       * $X \not\subseteq C$
       *)
      if SetList.subset_of xl mask
      then (Some(MyArray.of_indexes n mask))
      else  None
    )

  (* see Solomon's Theorem above *)
  let solomon_left_minors (hg:hg) (x:bool array) (y:int) : hg list =
    let n = Array.length hg in
    assert(n = Array.length x);
    (* We compute $\mathcal{C}$ the connected component decomposition
     * of $S(G, y)$ with respect to N_G[y].
     * Components which do not contain the seed have already been
     * filtered-out.
     *)
    let components = solomon_left_masks hg x y in
    (* if components = [] we return [] to avoid duplicating the graph *)
    if components = [] then [] else (
      let sngy = vertex_neighbors hg y in
      let hg' = Array.copy hg in
      ignore(GGLA.Utils.graph_add_clique
        ~inplace:true
        ~replace:true
        ~reflexive_false:true
         hg'
         (sngy ||> (fun i -> (i, ()))));
      (* We compute the matching minor for each subgraph *)
      components ||> (fun mask ->
        let subgraph, _, _ = GGLA.Utils.subgraph mask hg' in
        GGLA_HFT.normalize subgraph
      )
    )

  (* see Solomon's Theorem above *)
  let solomon_expansion
      (next:searcher)
      ?(skip_left=false)
      ?(skip_right=false)
      (y:int)
      (hg:hg) (x:bool array) : tree =
    let n = Array.length hg in
    assert(n = Array.length x);
    let right () =
      assert(x.(y) = false);
      let span = y :: vertex_dominators_of hg y in
      next hg (MyArray.copy_set_list x span true)
    in
    let left () =
      solomon_left_minors hg x y
      ||> (fun hgc -> next hgc x)
    in
    let nexts =
      (if skip_right then [] else [right()]) @>
      (if skip_left  then [] else  left()  )
    in
    match nexts with
    | [] -> Tree.ANode(None, [])
    | [t] -> t
    |  f  -> Tree.ANode(None, f)

  let extend_with_solomon_left_side_free
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    let xl = MyArray.list_of_indexes_true x in
    (* [sngx] : vertices adjacent to the seed $X$ *)
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    let right_siders = sngx |> List.filter (fun y -> solomon_left_masks hg x y = []) in
    if right_siders = []
    then next_None hg x
    else next_Some hg (MyArray.copy_set_list x right_siders true)

  let normalize_separative_clique next_None next_Some hg x =
    match GGLA_SeparativeClique.compute hg with
    | None -> next_None hg x
    | Some td -> (
      let inK = MyArray.list_of_indexes_true x in
      td.TreeDecomposition.components
      |> Array.to_list
      |> List.filter (SetList.subset_of inK)
      ||> extract_minor hg
      |> (fun subs -> Tree.ANode(None, subs ||> (fun g -> next_Some g x)))
    )

  type explicit_tree = int list Tree.tree

  let rec internal_explicit_tree (a:int Tree.tree array) : tree -> explicit_tree =
    function
    | Tree.ALeaf il -> (
      il
      ||> Array.get a
      |> TreeUtils.trees_flatten
      |> SetList.sort
      |> (fun set -> Tree.Leaf set)
    )
    | Tree.ANode (rename, trees) -> (
      let a =
        match rename with
        | None -> a
        | Some rename ->
          Array.map (fun il -> Tree.Node (il ||> Array.get a)) rename
      in
      Tree.Node (trees ||> (internal_explicit_tree a))
    )

  let rec explicit_tree : tree -> explicit_tree =
    function
    | Tree.ALeaf il -> Tree.Leaf il
    | Tree.ANode (None, trees) -> Tree.Node (trees ||> explicit_tree)
    | Tree.ANode (Some rename, trees) -> (
      let a : int Tree.tree array =
        Array.map (fun il -> Tree.Node (il ||> (fun i -> Tree.Leaf i))) rename
      in
      Tree.Node (trees ||> internal_explicit_tree a)
    )

  let seed_dominator (hg:hg) (x:bool array) (y:int) : bool =
    let xl = MyArray.list_of_indexes_true x in
    SetList.subset_of xl (vertex_strict_neighbors hg y)

  let seed_weak_dominator (hg:hg) (x:bool array) (y:int) : bool =
    let xl = MyArray.list_of_indexes_true x in
    let setA = SetList.inter xl (vertex_strict_neighbors hg y) in
    let setB = SetList.minus xl setA in
    (* we check that [setA] -- [setB] is a bipartite subgraph of [hg] *)
    (GGLA.Utils.graph_is_biclique hg setA setB) &&
    (* we check that [setB]^2 is a clique of [hg] *)
    (GGLA.Utils.graph_is_clique hg setB)

  (* Section ?. ? *)

  let is_seed_dominator_free (hg:hg) (x:bool array) : bool =
    assert(Array.length hg = Array.length x);
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    List.for_all (fun y -> seed_dominator hg x y = false) sngx

  let is_seed_adjacent_vertex_dominator_free (hg:hg) (x:bool array) : bool =
    assert(Array.length hg = Array.length x);
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    List.for_all (fun y -> vertex_find_dominated_by hg y = None) sngx

  let are_seed_neighbors_connected (hg:hg) (x:bool array) : bool =
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    GGLA.Connected.is_connected hg sngx

  let are_seed_neighbors_kvertex_free (hg:hg) (x:bool array) : bool =
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    List.for_all (fun y -> GGLA_KIC.Test.test hg y = false) sngx

  let are_seed_neighbors_kvertex_only (hg:hg) (x:bool array) : bool =
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    List.for_all (fun y -> GGLA_KIC.Test.test hg y = true) sngx

  let is_cliquish_extension ?(with_dominators=false) hg x y =
    let xl = MyArray.list_of_indexes_true x in
    let span =
      if with_dominators
      then SetList.union [y] (vertex_dominators_of hg y)
      else [y]
    in
    IsPMC.is_kvm hg (SetList.union span xl)

  let is_extended_pmc_extension hg x y : bool =
    let xl = MyArray.list_of_indexes_true x in
    let span = SetList.union [y] (vertex_dominators_of hg y) in
    let comp : int list = SetList.union span xl in
    IsPMC.is_pmc hg comp

  let are_seed_adjacent_elementary_cliquish_extension_only
     ?(with_dominators=false)
      (hg:hg) (x:bool array) : bool =
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    List.for_all (is_cliquish_extension ~with_dominators hg x) sngx

  let is_elementary_cliquish_extension_only
     ?(with_dominators=false)
      (hg:hg) (x:bool array) : bool =
    Array.for_all (fun vy -> is_cliquish_extension ~with_dominators hg x vy.index) hg

  let do_vertex_dominates_seed_adjacents (hg:hg) (x:bool array) (y:int) : bool =
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    SetList.subset_of sngx (vertex_neighbors hg y)

  let find_vertex_which_dominates_seed_adjacents (hg:hg) (x:bool array) : int option =
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    List.find_opt (fun y -> SetList.subset_of sngx (vertex_neighbors hg y)) sngx

  let filter_post_adjacent_kvertex_expansion (next:searcher) hg x : tree =
    assert(Array.length hg = Array.length x);
    assert(are_seed_neighbors_kvertex_free hg x);
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    if List.length xl + List.length sngx = Array.length hg
    then Tree.ANode(None, [])
    else next hg x

end

module type EnumModel =
sig
  open EnumUtils

  val prefix : string
  val expansion : searcher -> ?skip_left:bool -> ?skip_right:bool -> int -> searcher
end

module EnumMeta(M:EnumModel) =
struct
  open EnumUtils

  (* Section ?. Vertex-Dominator-Extension Theorem (VDET) *)

  (* see Case 1 in EnumUtils *)
  (* Case 2 :
   * Find any vertex $y$ which dominates a vertex $z$ adjacent to the seed.
   * Then, apply Salomon's Theorem on $z$.
   * Then, on the right-branch, $z$ belongs to the seed.
   * Then, apply VDET with $y$ and $X\uplus\{z\}$
   *)
  let expand_with_adjacent_vertex_dominator
      (next_None:searcher)
      (next_Some:searcher)
      hg x =
    assert(Array.length hg = Array.length x);
    let xl = MyArray.list_of_indexes_true x in
    (* [sngx] : vertices adjacent to the seed $X$ *)
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    match
      List.find_map (fun z ->
        (* [y] dominates [z] *)
        let yl = vertex_dominators_of hg z in
        (* we remove z-dominator vertices which are already in the seed *)
        let yl = SetList.minus yl xl in
        if yl = [] then None else Some(z, yl)
      ) sngx
    with
    | None -> next_None hg x
    | Some(z, yl) -> (
      assert(x.(z) = false);
      (* every [y] dominates [z] *)
      assert(List.for_all (fun y -> x.(y) = false && vertex_dominates hg z y) yl);
      M.expansion next_Some z hg x
    )

  (* [g] is the current graph
   *   - [g] is auto-renamed
   * [x] is the seed that needs to be extended
   *   - [x] denotes a set of indices
   * => normalizes [g] so that [g] is elementary extendible w.r.t [x]
   *   - that is, after normalization, the only vertices that
   *     cannot extend the only vertex
   *)
  let simplify_non_cliquish_extension
      ?(with_dominators=false)
      (test:hg -> bool array -> int -> bool)
      (next_None:searcher)
      (next_Some:searcher)
      hg x =
    assert(Array.length hg = Array.length x);
    let non_cliquish_extension y =
      (x.(y) = false) &&
      (test hg x y) &&
      (is_cliquish_extension ~with_dominators hg x y = false)
    in
    match Array.find_opt (fun v -> non_cliquish_extension v.index) hg with
    | None   -> next_None hg x
    | Some y -> M.expansion next_Some ~skip_right:true y.index hg x

  let fixpoint_simplify_non_cliquish_extension
      ?(with_dominators=false)
      (test:hg -> bool array -> int -> bool)
      (next_None:searcher)
      (next_Some:searcher)
      hg x =
    let rec fixpoint_nonKVM hg x =
      simplify_non_cliquish_extension
        ~with_dominators
        test
        next_Some
        (normalize_autorename fixpoint_nonKVM)
        hg x
    in
    (* we unroll the first loop to make [next_None] explicit *)
    simplify_non_cliquish_extension
      ~with_dominators
      test
      next_None
      (normalize_autorename fixpoint_nonKVM)
      hg x

  let simplify_adjacent_non_cliquish_extension
      ?(with_dominators=false)
      (test:hg -> bool array -> int -> bool)
      (next_None:searcher)
      (next_Some:searcher)
      hg x =
    assert(Array.length hg = Array.length x);
    let xl = MyArray.list_of_indexes_true x in
    let non_cliquish_extension y =
      (test hg x y) &&
      (is_cliquish_extension ~with_dominators hg x y = false)
    in
    let sn = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    match List.find_opt non_cliquish_extension sn with
    | None -> next_None hg x
    | Some y -> M.expansion next_Some ~skip_right:true y hg x

  let simplify_extended_pmc_extension
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    let test y =
      (x.(y) = false) &&
      (is_extended_pmc_extension hg x y)
    in
    match Array.find_opt (fun vy -> test vy.index) hg with
    | None -> next_None hg x
    | Some vy -> M.expansion next_Some vy.index hg x

  (* [g] is the current graph
   *   - [g] is auto-renamed
   * [x] is the seed that needs to be extended
   *   - [x] denotes a set of indices
   * => normalizes [g] so that [g\x] is a full component of [g]
   *)
  let normalize_full_components next_None next_Some (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    let idents = MyArray.list_of_indexes_true x in
    let components = IsPMC.connected_components_and_separators_from_array hg x in
    let k = List.length components in
    if k = 0
    then (
      if Array.for_all (fun b -> b) x = false
      then (
        print_internal (M.prefix^".normalize_full_components") "ggla-pmc-normalize2-problem.log.dot" hg x;
        failwith ("["^M.prefix^".normalize_full_components] #components = 0, yet, [x <> [|true; ...|]]")
      );
      if GGLA.Utils.graph_is_clique hg idents
      then Tree.ALeaf idents
      else Tree.ANode(None, [])
    )
    else if k = 1
    then (
      if IsPMC.vm_is_kvm hg idents components
      then if IsPMC.vm_is_ks idents components
        then Tree.ALeaf idents
        else next_None hg x
      else Tree.ANode(None, [])
    )
    else (
      if IsPMC.vm_is_kvm hg idents components
      then if IsPMC.vm_is_ks idents components
        then Tree.ALeaf idents
        else (
          components
          |> MyList.opmap (fun (component, neighbors) ->
            if neighbors = idents
            then (Some(extract_minor hg (SetList.union component neighbors)))
            else None
          )
          |> (fun subG -> Tree.ANode (None, subG ||> (fun g -> next_Some g x)))
        )
      else Tree.ANode(None, [])
    )

  let test_conjecture name (test:hg -> bool array -> bool) (next:searcher) hg x : tree =
    let prediction = test hg x in
    let tree = next hg x in
    let nleaf, nnode = TreeUtils.count_atree tree in
    let status = if (nleaf <> 0) = prediction then "VALID" else "FALSE" in
    print_endline (Printf.sprintf "[%s]  TEST Conjecture nleaf:%d [%s] nnode:%d" name nleaf status nnode);
    let file_name = Printf.sprintf "%s-%d-%s-%d.log.dot" name nleaf status nnode in
    print_internal name file_name hg x;
    tree

  let unsat_conjecture name (next:searcher) hg x : tree =
    let tree = next hg x in
    let nleaf, nnode = TreeUtils.count_atree tree in
    let status = if nleaf = 0 then "VALID" else "FALSE" in
    print_endline (Printf.sprintf "[%s] UNSAT Conjecture nleaf:%d [%s] nnode:%d" name nleaf status nnode);
    let file_name = Printf.sprintf "%s-%d-%s-%d.log.dot" name nleaf status nnode in
    print_internal name file_name hg x;
    tree

  let sat_conjecture name (next:searcher) hg x : tree =
    let tree = next hg x in
    let nleaf, nnode = TreeUtils.count_atree tree in
    let status = if nleaf <> 0 then "VALID" else "FALSE" in
    print_endline (Printf.sprintf "[%s]   SAT Conjecture nleaf:%d [%s] nnode:%d" name nleaf status nnode);
    let file_name = Printf.sprintf "%s-%d-%s-%d.log.dot" name nleaf status nnode in
    print_internal name file_name hg x;
    tree

  let extend_with_seed_adjacent_dominator
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    match find_vertex_which_dominates_seed_adjacents hg x with
    | None -> next_None hg x
    | Some y -> M.expansion next_Some y hg x

  let is_seed_adjacent_dominator_free (hg:hg) (x:bool array) : bool =
    find_vertex_which_dominates_seed_adjacents hg x = None

  (* [g] is the current graph
   *   - [g] is auto-renamed
   * [x] is the seed that needs to be extended
   *   - [x] denotes a set of indices
   *)
  let adjacent_kvertex_expansion
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    (* [V1] we simply use that [y] a K-vertex, EP(G, X) = EP(S(G, y), X) + EP(G, X + y) *)
    (* 1. we compute the strict neighbors of [x] in [g] *)
    let xl = MyArray.list_of_indexes_true x in
    let sn = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    (* 2. we retrieve the first K-vertex among them *)
    match List.find_opt (GGLA_KIC.Test.test hg) sn with
    | None -> next_None hg x
    | Some y -> M.expansion next_Some y hg x

  (* [g] is the current graph
   *   - [g] is auto-renamed
   * [x] is the seed that needs to be extended
   *   - [x] denotes a set of indices
   * **NOTE** cannot be used with basic-expansion scheme
   *          requires Salomon's Expansion or higher
   *)
  let adjacent_non_kvertex_expansion
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    (* [V1] we simply use that [y] a K-vertex, EP(G, X) = EP(S(G, y), X) + EP(G, X + y) *)
    (* 1. we compute the strict neighbors of [x] in [g] *)
    let xl = MyArray.list_of_indexes_true x in
    let sn = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    (* 2. we retrieve the first K-vertex among them *)
    match List.find_opt (fun y -> GGLA_KIC.Test.test hg y = false) sn with
    | None -> next_None hg x
    | Some y -> M.expansion next_Some y hg x

  (* [g] is the current graph
   *   - [g] is auto-renamed
   * [x] is the seed that needs to be extended
   *   - [x] denotes a set of indices
   *)
  let adjacent_non_dominator_expansion
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    (* [V1] we simply use that [y] a K-vertex, EP(G, X) = EP(S(G, y), X) + EP(G, X + y) *)
    let xl = MyArray.list_of_indexes_true x in
    (* 1. we compute the strict neighbors of [x] in [g] *)
    let sn = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    (* 2. we retrieve the first seed-adjacent H-vertex which does not dominate [x] *)
    match List.find_opt (fun y -> seed_dominator hg x y = false) sn with
    | None -> next_None hg x
    | Some y -> M.expansion next_Some y hg x

  (* [g] is the current graph
   *   - [g] is auto-renamed
   * [x] is the seed that needs to be extended
   *   - [x] denotes a set of indices
   *)
  let seed_dominator_expansion
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    (* [V1] we simply use that [y] a K-vertex, EP(G, X) = EP(S(G, y), X) + EP(G, X + y) *)
    let xl = MyArray.list_of_indexes_true x in
    (* 1. we compute the strict neighbors of [x] in [g] *)
    let sn = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    (* 2. we retrieve the first seed-adjacent H-vertex which does not dominate [x] *)
    match List.find_opt (fun y -> seed_dominator hg x y) sn with
    | None -> next_None hg x
    | Some y -> M.expansion next_Some y hg x

  (* [g] is the current graph
   *   - [g] is auto-renamed
   * [x] is the seed that needs to be extended
   *   - [x] denotes a set of indices
   *)
  let seed_weak_dominator_expansion
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    (* [V1] we simply use that [y] a K-vertex, EP(G, X) = EP(S(G, y), X) + EP(G, X + y) *)
    let xl = MyArray.list_of_indexes_true x in
    (* 1. we compute the strict neighbors of [x] in [g] *)
    let sn = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    (* 2. we retrieve the first seed-adjacent H-vertex which does not dominate [x] *)
    match List.find_opt (fun y -> seed_weak_dominator hg x y) sn with
    | None -> next_None hg x
    | Some y -> M.expansion next_Some y hg x

  let adjacent_expansion
      (next_None:searcher)
      (next_Some:searcher)
      (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    (* [V1] we simply use that [y] a K-vertex, EP(G, X) = EP(S(G, y), X) + EP(G, X + y) *)
    let xl = MyArray.list_of_indexes_true x in
    (* 1. we compute the strict neighbors of [x] in [g] *)
    let sn = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    (* 2. we retrieve the first seed-adjacent H-vertex which does not dominate [x] *)
    match sn with
    | [] -> next_None hg x
    | y :: _ -> M.expansion next_Some y hg x

end

module EnumPMC2 =
struct
  open EnumUtils
  let prefix = "GuaCaml.GGLA_PMC.EnumPMC2"

  module MetaModel =
  struct
    let prefix = prefix
    let expansion = EnumUtils.basic_expansion
  end
  module Meta = EnumMeta(MetaModel)
  open Meta

  (* 1. H-vertex auto-normalization
   * 2. decomposition into connected full components (if yes, GOTO 1)
   * 3. simplification of non-cliquish-extensions (if yes, GOTO 1)
   *     using this order : {adjacent, non-adjacent} x {non-dominator, K-vertex} non-cliquish-extensions
   * => at this point the only remaining non-cliquish-extensions are both dominator of $X$ and K-vertices
   * 4. disjunction on an arbitrary K-vertex using basic expansion lemma (if yes, GOTO 1)
   * 5. apply the decomposition theorem by separative cliques (if yes, GOTO 1)
   * => **CONJECTURE 2**, if one reaches this point, the sub-instance is unsatisfiable
   * 6. apply basic H-disjunction on an arbitrary seed-adjacent non-dominator H-vertex (if yes, GOTO 1)
   * => **FAILURE**
   * => **CONJECTURE 1**, does not fail (i.e., does not reach this point)
   *)

  (* apply auto-rename *)
  let rec ep_step1 (hg:hg) (x:bool array) : tree =
    normalize_autorename ep_step2 hg x
  (* decomposition into connected full-components *)
  and     ep_step2 (hg:hg) (x:bool array) : tree =
    normalize_full_components ep_step3a ep_step1 hg x
  (* simplify non-cliquish-extensions which are not dominators of [x] *)
  and     ep_step3a (hg:hg) (x:bool array) : tree =
    (* [REMOVE] let xl = MyArray.list_of_indexes_true x in *)
    Meta.simplify_adjacent_non_cliquish_extension
      (fun hg _ y -> seed_dominator hg x y = false)
        ep_step3b
        ep_step1
        hg x
  (* simplify non-cliquish-extensions which are K-vertex and adjacent to [x] *)
  and     ep_step3b (hg:hg) (x:bool array) : tree =
    Meta.simplify_adjacent_non_cliquish_extension
      (fun hg _ y -> GGLA_KIC.Test.test hg y)
        ep_step3c
        ep_step1
        hg x
  (* simplify non-cliquish-extensions which are K-vertex and non-adjacent to [x] *)
  and    ep_step3c (hg:hg) (x:bool array) : tree =
    let xl = MyArray.list_of_indexes_true x in
    let ngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some true) hg xl in
    let in_ngx = MyArray.of_indexes (Array.length hg) ngx in
    Meta.simplify_non_cliquish_extension
      (fun hg _ y -> in_ngx.(y) = false && GGLA_KIC.Test.test hg y)
      ep_step4
      ep_step1
      hg x
  and    ep_step3d (hg:hg) (x:bool array) : tree =
    simplify_extended_pmc_extension ep_step4 ep_step1 hg x
  (* disjunction on arbitrary seed-adjacent K-vertex using basic expansion lemma *)
  and    ep_step4 (hg:hg) (x:bool array) : tree =
    Meta.adjacent_kvertex_expansion ep_step4' ep_step1 hg x
  and    ep_step4' (hg:hg) (x:bool array) : tree =
    filter_post_adjacent_kvertex_expansion ep_step5 hg x
  (* apply the decomposition theorem by separative clique *)
  and    ep_step5 (hg:hg) (x:bool array) : tree =
    (* normalize_separative_clique (unsat_conjecture (prefix^"#Conjecture2") ep_step6) ep_step1 hg x *)
    normalize_separative_clique ep_step6 ep_step1 hg x
  (* apply basic H-disjunction on an arbitrary seed-adjacent non-dominator H-vertex *)
  and    ep_step6 (hg:hg) (x:bool array) : tree =
    Meta.adjacent_non_dominator_expansion (fun _ _ -> assert false) ep_step1 hg x

  let implicit_enum_from_seed (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
(*  print_internal gprefix^".implicit_enum_from_seed" "ggla-pmc-implicit-enum-from-seed.log.dot" g x;*)
    let hg = GGLA_HFT.autorename_vertex hg in
    ep_step1 hg x
    (* fixpoint_simplify_non_cliquish_extension ep_step1 ep_step1 hg x *)
    (* normalize_separative_clique extension1_problem normalize1_problem hg x *)
    (* normalize3_problem hg x *)

  let enum_from_seed (hg:hg) (seed:bool array) : bool array list * int =
    let len = Array.length hg in
    assert(len = Array.length seed);
    let itree = implicit_enum_from_seed hg seed in (* we craft the implicit tree first *)
    let etree = explicit_tree itree  in (* we translate it into an explicit tree *)
    let _, nnode = TreeUtils.count_tree etree in
    (TreeUtils.tree_flatten etree ||> (MyArray.of_indexes len), nnode)
end

module EnumPMC3 =
struct
  open EnumUtils
  let prefix = "GuaCaml.GGLA_PMC.EnumPMC3"

  module MetaModel =
  struct
    let prefix = prefix
    let expansion = EnumUtils.solomon_expansion
  end
  module Meta = EnumMeta(MetaModel)
  open Meta

  (* 1. H-vertex auto-normalization
   * 2. decomposition into connected full components (if yes, GOTO 1)
   * 3. disjunction on H-dominator (using Solomon's Theorem) (if yes, GOTO 1)
   * 4. simplification of non-cliquish-extensions (using Solomon's Theorem) (if yes, GOTO 1)
   *     - start by the seed's neighbors
   * 5. disjunction on seed-adjacent K-vertices (using Solomon's Theorem) (if yes, GOTO 1)
   * => **CONJECTURE 2**, if one reaches this point, the sub-instance is unsatisfiable
   * 6. apply the decomposition theorem by separative cliques (if yes, GOTO 1)
   * => **CONJECTURE 1**, if one reaches this point, the instance is unsatisfiable
   * 7. disjunction on seed-adjacent H-vertices (using Solomon's Theorem), GOTO 1
   *)

  let seed_weight (hg:hg) (x:bool array) : int =
    MyArray.sum (fun v -> if x.(v.index) then vertex_weight v else 0) hg

  (* only explore solution which cost is no higher that [bound]
   * i.e. (weight(pmc) <= bound)
   * **NOTE** explore may return solutions which have a cost higher
   *          that [bound], please perform a post-treatment to remove
   *          them if they are critical to your analysis
   *)
  let explore (bound:int option) (hg:hg) (x:bool array) : tree =
    let bound : int =
      match bound with
      | Some bound -> bound
      | None -> MyArray.sum vertex_weight hg
    in
    (* 1. apply auto-rename *)
    let rec ep_step1 (hg:hg) (x:bool array) : tree =
      normalize_autorename ep_step2a hg x
    (* 2.a decomposition into connected full-components *)
    and     ep_step2a (hg:hg) (x:bool array) : tree =
      (* assert(GGLA.Connected.is_connected hg (MyArray.list_of_indexes_true x)); *)
      if seed_weight hg x <= bound
      then (
        normalize_full_components
          (fun hg x ->
            (* [x] is not a PMC, hence at least 1 vertex of weight 1 shall
             * be added to extend it into one.
             *)
            if seed_weight hg x < bound
            then ep_step2b hg x
            else Tree.ANode(None, [])
          )
          ep_step1
          hg x
      )
      else Tree.ANode(None, [])
    (* 2.b we extend the seed with every vertices which dominates it *)
    and     ep_step2b (hg:hg) (x:bool array) : tree =
      extend_with_seed_vertex_dominator ep_step2c ep_step2a hg x
    and     ep_step2c (hg:hg) (x:bool array) : tree =
      extend_with_solomon_left_side_free ep_step3a ep_step2a hg x
    (* 3. disjunction on arbitrary seed-dominator *)
    and     ep_step3a (hg:hg) (x:bool array) : tree =
      seed_dominator_expansion ep_step3b ep_step1 hg x
    and     ep_step3b (hg:hg) (x:bool array) : tree =
      seed_weak_dominator_expansion ep_step3c ep_step1 hg x
    and     ep_step3c (hg:hg) (x:bool array) : tree =
      extend_with_seed_adjacent_dominator ep_step4a ep_step1 hg x
    (* 4. simplification of non-cliquish-extensions *)
    and     ep_step4a (hg:hg) (x:bool array) : tree =
      (* assert(is_seed_adjacent_dominator_free hg x); *)
      Meta.simplify_adjacent_non_cliquish_extension
        (fun _ _ _ -> true)
          ep_step4b
          ep_step1
          hg x
    and    ep_step4b (hg:hg) (x:bool array) : tree =
      assert(Array.length hg = Array.length x);
      (* assert(are_seed_adjacent_elementary_cliquish_extension_only hg x); *)
      let xl = MyArray.list_of_indexes_true x in
      let ngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some true) hg xl in
      let in_ngx = MyArray.of_indexes (Array.length hg) ngx in
      Meta.simplify_non_cliquish_extension
        (fun _ _ y -> in_ngx.(y) = false)
        ep_step4c
        ep_step1
        hg x
    and    ep_step4c (hg:hg) (x:bool array) : tree =
      simplify_extended_pmc_extension ep_step5a ep_step1 hg x
    (* 5. elimination of non-K-vertices *)
    (* 5.a disjunction on (any) (non-seed) dominated seed-adjacent H-vertex *)
    and    ep_step5a (hg:hg) (x:bool array) : tree =
      (* assert(is_elementary_cliquish_extension_only hg x); *)
      expand_with_adjacent_vertex_dominator ep_step5c ep_step1 hg x
    (* 5.c disjunction on (any) seed-ajacent non-K-vertex *)
    and    ep_step5c (hg:hg) (x:bool array) : tree =
      assert(Array.length hg = Array.length x);
      (* assert(is_seed_adjacent_vertex_dominator_free hg x); *)
      Meta.adjacent_non_kvertex_expansion
        (* (sat_conjecture (prefix^"#Conjecture4") ep_step6a) *)
        ep_step6a
        ep_step1
        hg x
    (* 6. case disjunction on K-vertices *)
    and    ep_step6a (hg:hg) (x:bool array) : tree =
      assert(Array.length hg = Array.length x);
      (* assert(are_seed_neighbors_kvertex_only hg x); *)
      Meta.adjacent_kvertex_expansion (fun _ _ -> assert false) ep_step1 hg x
  (*
    and    ep_step6' (hg:hg) (x:bool array) : tree =
      assert(are_seed_neighbors_kvertex_free hg x);
      filter_post_adjacent_kvertex_expansion (unsat_conjecture (prefix^"#Conjecture2") ep_step7) hg x
    (* 6. apply the decomposition theorem by separative clique *)
    and    ep_step6b (hg:hg) (x:bool array) : tree =
      normalize_separative_clique (fun _ _ -> assert false) ep_step1 hg x
    (* 7. disjunction on arbitrary seed-adjacent H-vertex *)
    and    ep_step7 (hg:hg) (x:bool array) : tree =
      Meta.adjacent_expansion (fun _ _ -> assert false) ep_step1 hg x
   *)
  in
  ep_step1 hg x

  let implicit_enum_from_seed ?(bound=None) (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
(*  print_internal prefix^".implicit_enum_from_seed" "ggla-pmc-implicit-enum-from-seed.log.dot" g x;*)
    let hg = GGLA_HFT.autorename_vertex hg in
    (* ep_step1 hg x *)
    normalize_separative_clique (explore bound) (explore bound) hg x
    (* fixpoint_simplify_non_cliquish_extension ep_step1 ep_step1 hg x *)
    (* normalize_separative_clique extension1_problem normalize1_problem hg x *)
    (* normalize3_problem hg x *)

  let enum_from_seed (hg:hg) (seed:bool array) : bool array list * int =
    let len = Array.length hg in
    assert(len = Array.length seed);
    let itree = implicit_enum_from_seed hg seed in (* we craft the implicit tree first *)
    let etree = explicit_tree itree  in (* we translate it into an explicit tree *)
    let _, nnode = TreeUtils.count_tree etree in
    (TreeUtils.tree_flatten etree ||> (MyArray.of_indexes len), nnode)

  let enum_from_seed_with_bound
      (hg:hg)
      (seed:bool array)
      (bound:int) : bool array list * int =
    let len = Array.length hg in
    assert(len = Array.length seed);
    let itree = implicit_enum_from_seed hg seed in (* we craft the implicit tree first *)
    let etree = explicit_tree itree  in (* we translate it into an explicit tree *)
    let _, nnode = TreeUtils.count_tree etree in
    let pmc_list =
      etree
      |> TreeUtils.tree_flatten
      ||> MyArray.of_indexes len
      |> List.filter (fun x -> seed_weight hg x <= bound)
    in
    (pmc_list, nnode)
end

(* The [IsPMCE] module aims at deciding in poly-time iff a given instance of the
 * PMC-Extension Problem has (at least) a solution.
 *
 * The strategy is based on the naive construction of a K-sequence.
 * If the construction properly terminates, the instance is satisfiable (we just
 * created a witness), otherwise (we conjecture that) the instance unsatisfiable.
 *)
module IsPMCE =
struct
  open EnumUtils
  let prefix = "GuaCaml.GGLA_PMC.IsPMCE"

  (* We assume that [hg] is connected *)
  let rec gep_step1 (hg:hg) (x:bool array) : tree =
    normalize_autorename gep_step2a hg x
  and      gep_step2a (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    match IsPMC.ternary_pmc hg (MyArray.list_of_indexes_true x) with
    | None -> gep_step3a hg x
    | Some true -> Tree.ALeaf(MyArray.list_of_indexes_true x)
    | Some false -> Tree.ANode(None, [])
  (* 2.b we extend the seed with every vertices which dominates it *)
  and     gep_step2b (hg:hg) (x:bool array) : tree =
    extend_with_seed_vertex_dominator gep_step2c gep_step2a hg x
  and     gep_step2c (hg:hg) (x:bool array) : tree =
    extend_with_solomon_left_side_free gep_step3a gep_step2a hg x
  (* try to conclude with a single-step PMC extension of the current seed *)
  and     gep_step3a (hg:hg) (x:bool array) : tree =
    let test y =
      x.(y) = false &&
      is_extended_pmc_extension hg x y
    in
    match Array.find_opt (fun v -> test v.index) hg with
    | None -> gep_step3b hg x
    | Some vy -> (
      let y = vy.index in
      let x' = MyArray.copy_set_list x (y::(vertex_dominators_of hg y)) true in
      let pmc = MyArray.list_of_indexes_true x' in
      assert(IsPMC.is_pmc hg pmc);
      Tree.ALeaf pmc
    )
  (* early eliminate non-cliquish extension which also are K-vertices *)
  and     gep_step3b (hg:hg) (x:bool array) : tree =
    let test y =
      x.(y) = false &&
      is_cliquish_extension ~with_dominators:true hg x y = false
    in
    match Array.find_opt (fun v -> test v.index) hg with
    | None -> gep_step3c hg x
    | Some y -> (
      solomon_expansion gep_step1 ~skip_right:true y.index hg x
    )
  (* try to conclude using a internal-seed-dominator, which neighborhood is a PMC *)
  and     gep_step3c (hg:hg) (x:bool array) : tree =
    let xl = MyArray.list_of_indexes_true x in
    let test y =
      let ngy = vertex_neighbors hg y in
      (SetList.subset_of xl ngy) && (is_pmc hg ngy)
    in
    match List.find_opt test xl with
    | None -> gep_step3d hg x
    | Some y -> Tree.ALeaf(vertex_neighbors hg y)
  (* detect seed-dominators *)
  and     gep_step3d (hg:hg) (x:bool array) : tree =
    let xl = MyArray.list_of_indexes_true x in
    let sngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some false) hg xl in
    match List.find_opt (fun y -> seed_dominator hg x y) sngx with
    | None -> gep_step4 hg x
    | Some y -> (
      solomon_expansion gep_step1 ~skip_right:true y hg x
    )
  (* default *)
  and     gep_step4 (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    let test y =
      x.(y) = false &&
      GGLA_KIC.Test.test hg y
    in
    match Array.find_opt (fun v -> test v.index) hg with
    | None -> Tree.ANode(None, [])
    | Some y -> gep_step1 (GGLA_HFT.vertex_contraction hg y.index) x

  (* Either return
   * - [Some k] where [k] is a PMC extending [x] if one is found
   * - [None] otherwise
   *)
  let greedy (hg:hg) (x:bool array) : bool array option =
    let n = Array.length hg in
    assert(Array.length x = n);
    let indexes = List.init n (fun i -> i) in
    assert(GGLA.Connected.is_connected hg indexes);
    let itree = gep_step2a hg x in
    let etree = explicit_tree itree in
    match TreeUtils.tree_flatten etree with
    | [] -> None
    | kl :: _ -> Some(MyArray.of_indexes n kl)
end

module EnumPMC4 =
struct
  open EnumUtils
  let prefix = "GuaCaml.GGLA_PMC.EnumPMC4"

  module MetaModel =
  struct
    let prefix = prefix
    let expansion = EnumUtils.solomon_expansion
  end
  module Meta = EnumMeta(MetaModel)
  open Meta

  (* 1. H-vertex auto-normalization
   * 2. decomposition into connected full components (if yes, GOTO 1)
   * 3. disjunction on H-dominator (using Solomon's Theorem) (if yes, GOTO 1)
   * 4. simplification of non-cliquish-extensions (using Solomon's Theorem) (if yes, GOTO 1)
   *     - start by the seed's neighbors
   * 5. disjunction on seed-adjacent K-vertices (using Solomon's Theorem) (if yes, GOTO 1)
   * => **CONJECTURE 2**, if one reaches this point, the sub-instance is unsatisfiable
   * 6. apply the decomposition theorem by separative cliques (if yes, GOTO 1)
   * => **CONJECTURE 1**, if one reaches this point, the instance is unsatisfiable
   * 7. disjunction on seed-adjacent H-vertices (using Solomon's Theorem), GOTO 1
   *)

  (* 1. apply auto-rename *)
  let rec ep_step1 (hg:hg) (x:bool array) : tree =
    normalize_autorename
      (test_conjecture
        "ConjectureG0"
        (fun hg x -> IsPMCE.greedy hg x <> None)
        ep_step2a)
      hg x
  (* 2.a decomposition into connected full-components *)
  and     ep_step2a (hg:hg) (x:bool array) : tree =
    (* assert(GGLA.Connected.is_connected hg (MyArray.list_of_indexes_true x)); *)
    normalize_full_components ep_step2b ep_step1 hg x
  (* 2.b we extend the seed with every vertices which dominates it *)
  and     ep_step2b (hg:hg) (x:bool array) : tree =
    extend_with_seed_vertex_dominator ep_step2c ep_step2a hg x
  and     ep_step2c (hg:hg) (x:bool array) : tree =
    extend_with_solomon_left_side_free ep_step3a ep_step2a hg x
  (* 3. disjunction on arbitrary seed-dominator *)
  and     ep_step3a (hg:hg) (x:bool array) : tree =
    seed_dominator_expansion ep_step3b ep_step1 hg x
  and     ep_step3b (hg:hg) (x:bool array) : tree =
    seed_weak_dominator_expansion ep_step3c ep_step1 hg x
  and     ep_step3c (hg:hg) (x:bool array) : tree =
    extend_with_seed_adjacent_dominator ep_step4a ep_step1 hg x
  (* 4. simplification of non-cliquish-extensions *)
  and     ep_step4a (hg:hg) (x:bool array) : tree =
    (* assert(is_seed_adjacent_dominator_free hg x); *)
    Meta.simplify_adjacent_non_cliquish_extension
      (fun _ _ _ -> true)
        ep_step4b
        ep_step1
        hg x
  and    ep_step4b (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    (* assert(are_seed_adjacent_elementary_cliquish_extension_only hg x); *)
    let xl = MyArray.list_of_indexes_true x in
    let ngx = GGLA.Utils.vertex_list_sons ~reflexive:(Some true) hg xl in
    let in_ngx = MyArray.of_indexes (Array.length hg) ngx in
    Meta.simplify_non_cliquish_extension
      (fun _ _ y -> in_ngx.(y) = false)
      ep_step4c
      ep_step1
      hg x
  and    ep_step4c (hg:hg) (x:bool array) : tree =
    simplify_extended_pmc_extension ep_step5a ep_step1 hg x
  (* 5. elimination of non-K-vertices *)
  (* 5.a disjunction on (any) (non-seed) dominated seed-adjacent H-vertex *)
  and    ep_step5a (hg:hg) (x:bool array) : tree =
    (* assert(is_elementary_cliquish_extension_only hg x); *)
    expand_with_adjacent_vertex_dominator ep_step5c ep_step1 hg x
  (* 5.c disjunction on (any) seed-ajacent non-K-vertex *)
  and    ep_step5c (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    assert(is_seed_adjacent_vertex_dominator_free hg x);
    Meta.adjacent_non_kvertex_expansion
      (sat_conjecture (prefix^"#Conjecture4") ep_step6a)
      ep_step1
      hg x
  (* 6. case disjunction on K-vertices *)
  and    ep_step6a (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
    assert(are_seed_neighbors_kvertex_only hg x);
    Meta.adjacent_kvertex_expansion (fun _ _ -> assert false) ep_step1 hg x
(*
  and    ep_step6' (hg:hg) (x:bool array) : tree =
    assert(are_seed_neighbors_kvertex_free hg x);
    filter_post_adjacent_kvertex_expansion (unsat_conjecture (prefix^"#Conjecture2") ep_step7) hg x
  (* 6. apply the decomposition theorem by separative clique *)
  and    ep_step6b (hg:hg) (x:bool array) : tree =
    normalize_separative_clique (fun _ _ -> assert false) ep_step1 hg x
  (* 7. disjunction on arbitrary seed-adjacent H-vertex *)
  and    ep_step7 (hg:hg) (x:bool array) : tree =
    Meta.adjacent_expansion (fun _ _ -> assert false) ep_step1 hg x
 *)

  let implicit_enum_from_seed (hg:hg) (x:bool array) : tree =
    assert(Array.length hg = Array.length x);
(*  print_internal prefix^".implicit_enum_from_seed" "ggla-pmc-implicit-enum-from-seed.log.dot" g x;*)
    let hg = GGLA_HFT.autorename_vertex hg in
    (* ep_step1 hg x *)
    normalize_separative_clique ep_step1 ep_step1 hg x
    (* fixpoint_simplify_non_cliquish_extension ep_step1 ep_step1 hg x *)
    (* normalize_separative_clique extension1_problem normalize1_problem hg x *)
    (* normalize3_problem hg x *)

  let enum_from_seed (hg:hg) (seed:bool array) : bool array list * int =
    let len = Array.length hg in
    assert(len = Array.length seed);
    let itree = implicit_enum_from_seed hg seed in (* we craft the implicit tree first *)
    let etree = explicit_tree itree  in (* we translate it into an explicit tree *)
    let _, nnode = TreeUtils.count_tree etree in
    (TreeUtils.tree_flatten etree ||> (MyArray.of_indexes len), nnode)
end
