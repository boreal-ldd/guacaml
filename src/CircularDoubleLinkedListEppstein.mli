module WithKey :
sig
  type ('k, 'v) t
  val create : ?hsize:int -> ('v -> 'k) -> ('k, 'v) t
  val _items_get : ('k, 'v) t -> 'k -> 'v
  val iter : ('v -> unit) -> ('k, 'v) t -> unit
  val to_list : ('k, 'v) t -> 'v list
  val nth : ('k, 'v) t -> int -> 'v
  val length : ('k, 'v) t -> int
  val to_string : ('v -> string) -> ('k, 'v) t -> string
  val key : ('k, 'v) t -> 'v -> 'k
  val insert_after_internal : ('k, 'v) t -> 'k -> 'k -> unit
  val insert_before_internal : ('k, 'v) t -> 'k -> 'k -> unit
  val append_internal : ('k, 'v) t -> 'k -> unit
  val append : ('k, 'v) t -> 'v -> unit
  val of_list : ?hsize:int -> ('v -> 'k) -> 'v list -> ('k, 'v) t
  val remove_internal : ('k, 'v) t -> 'k -> unit
  val remove : ('k, 'v) t -> 'v -> unit
  val insert_after : ('k, 'v) t -> 'v -> 'v -> unit
  val insert_before : ('k, 'v) t -> 'v -> 'v -> unit
  val get_prev : ('k, 'v) t -> 'v -> 'v
  val get_succ : ('k, 'v) t -> 'v -> 'v
  val get_first : ('k, 'v) t -> 'v
  val get_first_opt : ('k, 'v) t -> 'v option
  val pull_first_opt : ('k, 'v) t -> 'v option
end
module NoKey : sig end
