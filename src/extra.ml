(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Extra : Simple Extension to OCaml's Standard Short Notations
 *)

let flip f x y = f y x
let ( <| ) x y = x y
let ( >> ) f g x = g ( f x )
let ( << ) g f x = g ( f x )
let ( ||> ) l f = List.rev_map f l |> List.rev
let ( <|| ) f l = List.rev_map f l |> List.rev
let ( |+ ) l f x = List.fold_left f x l
let ( ||>> ) f g x = List.rev_map g (f x) |> List.rev
let ( |>| ) x f = f x; x

let ( @> ) l1 l2 = List.rev_append (List.rev l1) l2

let  (+=) x y = x:=!x+y
let  (-=) x y = x:=!x-y
let (+.=) x y = x:=!x+.y
let (-.=) x y = x:=!x-.y
let (!++) x = let x' = !x in x := succ x'; x'
let (!--) x = let x' = !x in x := pred x'; x'
(* [LATER] *)
let array_push a i x = a.(i) <- x :: a.(i)
let stack_push l x = l := x :: !l
let stack_pull (l:'a list ref) : 'a =
  match !l with
  | [] -> (failwith "[GuaCaml.Extra] error")
  | hd :: tl  -> (l := tl; hd)
(* [LATER]
let stack_push_list (stack: 'a list ref) (liste: 'a list) =
  List.iter (fun v -> stack_push stack(Hashtbl.find vars v)) liste
 *)

let ( %==% ) opx opy =
  match opx, opy with
  | None, None -> true
  | Some x, Some y -> x == y
  | _, _ -> false

let ( %!=% ) opx opy = not ( opx %==% opy )

let ( %== ) opx y =
  match opx with
  | None -> false
  | Some x -> x == y

let ( %!= ) opx y = not ( opx %== y )

let ( ==% ) x opy =
  match opy with
  | None -> false
  | Some y -> x == y

let ( !=% ) x opy = not ( x ==% opy )
