(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_PACE2017_Opal : Bare Syntax Parser for PACE 2017 Challenge (written in Opal)
 *
 *)

open Lang_PACE2017_Core
open Types
open Utils
open Opal
open CharStream

let edge : (char, Types.edge) Opal.t =
  spaces >> nat $* (spaces >> nat << skip_until_newline)

let comment : (char, string) Opal.t =
  exactly 'c' >> (until_stop any (exactly '\n') => (fun (cl, _) -> implode cl))

let primitive : (char, string * int * int) Opal.t =
  let* _ = exactly 'p' in
  let* mode = spaces >> word in
  let* nv = spaces >> nat in
  let* nc = spaces >> nat in
  let* _ = skip_until_newline in
  return (mode, nv, nc)

let pace2017_line : (char, pace2017_line) Opal.t =
  (comment => (fun s -> Comment s)) <|>
  (primitive => (fun (mode, nv, ne) -> Primitive(mode, nv, ne))) <|>
  (edge => (fun e -> Edge e))

let pace2017 : (char, pace2017) Opal.t =
  until pace2017_line

let of_file ?(sort=false) (target:string) : pace2017 =
  match parse_from_file pace2017 target with
  | Some some -> if sort then List.sort Stdlib.compare some else some
  | None -> failwith "[GuaCaml.Lang_PACE2017_Opal.from_file] uncaught parsing error"

let to_file (target:string) (p:pace2017) : unit =
  STools.SUtils.output_string_to_file target (Lang_PACE2017_Core.ToPrettyS.pace2017 p)
