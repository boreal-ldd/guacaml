(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * DoubleLinkedList : Implement standard double-linked lists
 *
 * === NOTE ===
 *
 * [Double Linked List](wikipedia) [FIXME]
 *
 *)

open Extra

type 'a t = {
  mutable prev : 'a o;
  mutable succ : 'a o;
  mutable item : 'a;
}
and 'a o = 'a t option

let check (first:'a t) : bool =
  let rec check_succ (prev:'a t) (curr:'a t) : bool =
    (curr.prev %== prev) &&
    (first != curr) &&
    (check_succ_opt curr curr.succ)
  and     check_succ_opt curr (curr_succ:'a o) : bool =
    match curr_succ with
    | None -> true
    | Some curr_succ -> check_succ curr curr_succ
  in
  let rec check_prev (succ:'a t) (curr:'a t) : bool =
    (curr.succ %== succ) &&
    (first != curr) &&
    (check_prev_opt curr curr.prev)
  and     check_prev_opt curr (curr_prev:'a o) : bool =
    match curr_prev with
    | None -> true
    | Some curr_prev -> check_prev curr curr_prev
  in
  (check_prev_opt first first.prev) &&
  (check_succ_opt first first.succ)

let check_opt (t:'a o) : bool =
  Tools.opmap_default check true t

let rec internal_check_items_opt ?(lst=None) (expected:'a list) (o:'a o) : bool =
  match expected with
  | [] -> (match lst with None -> true | Some o' -> o %==% o')
  | h :: t -> (
    match o with
    | None -> false
    | Some curr ->
      (h = curr.item) &&
      (internal_check_items_opt ~lst t curr.succ)
  )

let check_items_opt (expected:'a list) (o:'a o) : bool =
  internal_check_items_opt ~lst:(Some None) expected o

let check_items (expected:'a list) (t:'a t) : bool =
  check_items_opt expected (Some t)

let print_if_false ?(verbose=false) text cond : bool =
  if verbose && (cond = false) then print_endline text;
  cond

let rec internal_check_structure_forward_opt
    ?(verbose=false)
    (stop: 'a o)
    (sides:('a o * 'a o) list)
    (o:'a o) : bool =
  match sides with
  | [] -> print_if_false ~verbose "false" false
  | (prev', succ') :: sides -> (
    match o with
    | None -> false
    | Some curr -> (
      (print_if_false ~verbose "curr.prev %==% prev'" (curr.prev %==% prev')) &&
      (print_if_false ~verbose "curr.succ %==% succ'" (curr.succ %==% succ')) &&
      (
        if curr.succ %==% stop
        then print_if_false ~verbose "sides = []" (sides = [])
        else (internal_check_structure_forward_opt ~verbose stop sides curr.succ)
      )
    )
  )

let check_structure_forward_opt (sides:('a o * 'a o) list) (o:'a o) : bool =
  internal_check_structure_forward_opt None sides o

let check_structure_forward (sides:('a o * 'a o) list) (t:'a t) : bool =
  check_structure_forward_opt sides (Some t)

let get_item (t:'a t) = t.item
let set_item (t:'a t) (a:'a) = t.item <- a

let create item = { prev = None; succ = None; item }

let internal_set_oprev_osucc (prev:'a o) (succ:'a o) : unit =
  Tools.opiter (fun prev -> prev.succ <- succ) prev;
  Tools.opiter (fun succ -> succ.prev <- prev) succ;
  ()

let internal_set_oprev_succ x y =
  internal_set_oprev_osucc x (Some y)

let internal_set_prev_osucc x y =
  internal_set_oprev_osucc (Some x) y

let internal_set_prev_succ x y =
  internal_set_oprev_osucc (Some x) (Some y)

(* [create_before node item = node_prev]
 * insert the element [item] in the double-linked sequence before
 * the element [node].
 * the element [node.prev] (if any before the call) is used as
 *   [node_prev.prev] (after the call)
 * the element [node.succ] (if any before the call) is left unchanged
 *)
let create_before node item =
  let new_node = create item in (* node.prev -> new_node -> node *)
  let node_prev = node.prev in
  internal_set_oprev_succ node_prev new_node;
  internal_set_prev_succ new_node node;
  new_node

let create_before_opt (o:'a o) (item:'a) : 'a t =
  match o with
  | None -> create item
  | Some t -> create_before t item

(* similar to [create_before] but in the other direction *)
let create_after node item =
  let new_node = create item in (* node -> new_node -> node.succ *)
  let node_succ = node.succ in
  internal_set_prev_succ node new_node;
  internal_set_prev_osucc new_node node_succ;
  new_node

let create_after_opt (o:'a o) (item:'a) : 'a t =
  match o with
  | None -> create item
  | Some t -> create_after t item

let remove node =
  (* node.prev.succ <- node.succ *)
  (* node.succ.prev <- node.prev *)
  internal_set_oprev_osucc node.prev node.succ;
  node.prev <- None;
  node.succ <- None

let insert_between (prev:'a o) (item:'a t) (succ:'a o) : unit =
  internal_set_oprev_succ prev item;
  internal_set_prev_osucc item succ;
  ()

let insert_between_opt (prev:'a o) (item:'a o) (succ:'a o) : unit =
  match item with
  | None -> internal_set_oprev_osucc prev succ
  | Some item -> insert_between prev item succ

let insert_before (root:'a o) (item:'a t) : unit =
  remove item;
  match root with
  | None -> ()
  | Some curr -> insert_between curr.prev item root

let insert_after (root:'a o) (item:'a t) : unit =
  remove item;
  match root with
  | None -> ()
  | Some curr -> insert_between root item curr.succ

let get_succ_opt (t:'a t) : 'a o =
  t.succ

let get_prev_opt (t:'a t) : 'a o =
  t.prev

(* similar to [remove t] but can only be applied if
 * [t.prev = None]
 *)
let remove_fst (({prev; succ; item} as t):'a t) : 'a * 'a o =
  assert(prev = None);
  t.succ <- None;
  Tools.opiter (fun t' -> t'.prev <- None) succ;
  (item, succ)

(* similar to [remove t] but can only be applied if
 * [t.succ = None]
 *)
let remove_lst (({prev; succ; item} as t):'a t) : 'a * 'a o =
  assert(succ = None);
  t.prev <- None;
  Tools.opiter (fun t' -> t'.succ <- None) prev;
  (item, prev)

let rec internal_to_seq_opt (curr:'a o) (stop:'a o) : 'a t Seq.t =
  fun () ->
    match curr with
    | None -> Seq.Nil
    | Some curr ->
      let seq : 'a t Seq.t =
        if curr.succ %==% stop
        then Seq.empty
        else internal_to_seq_opt curr.succ stop
      in
      Seq.Cons (curr, seq)

let to_seq_opt (t:'a o) : 'a t Seq.t =
  internal_to_seq_opt t None

let to_seq (t:'a t) : 'a t Seq.t =
  to_seq_opt (Some t)

let to_item_seq (t:'a t) : 'a Seq.t =
  Seq.map (fun t -> t.item) (to_seq t)

let to_item_seq_opt (o:'a o) : 'a Seq.t =
  Seq.map (fun t -> t.item) (to_seq_opt o)

let rec create_after_item_seq (t:'a t) (seq:'a Seq.t) : 'a t =
  match seq () with
  | Seq.Nil -> t
  | Seq.Cons (item, seq) ->
    create_after_item_seq (create_after t item) seq

let rec create_before_item_seq (t:'a t) (seq:'a Seq.t) : 'a t =
  match seq () with
  | Seq.Nil -> t
  | Seq.Cons (item, seq) ->
    create_before_item_seq (create_after t item) seq

let of_item_seq (seq:'a Seq.t) : 'a o * 'a o =
  match seq () with
  | Seq.Nil -> (None, None)
  | Seq.Cons (item, seq) -> (
    let fst = create item in
    let lst = create_after_item_seq fst seq in
    (Some fst, Some lst)
  )

let to_list (t:'a t) : 'a list =
  t |> to_item_seq |> List.of_seq

let to_list_opt (o:'a o) : 'a list =
  Tools.opmap_default to_list [] o

let of_list (l:'a list) : 'a o * 'a o =
  l |> List.to_seq |> of_item_seq

let rec nth_opt (o:'a o) (i:int) : 'a option =
  match o with
  | None -> None
  | Some curr ->
         if i = 0
    then Some curr.item
    else if i > 0
    then nth_opt curr.succ (pred i)
    else nth_opt curr.prev (succ i)

let nth (t:'a t) (i:int) : 'a option =
  nth_opt (Some t) i

let iter_opt (f:'a -> unit) (o:'a o) : unit =
  Seq.iter f (to_item_seq_opt o)

let iter (f:'a -> unit) (t:'a t) : unit =
  Seq.iter f (to_item_seq t)
