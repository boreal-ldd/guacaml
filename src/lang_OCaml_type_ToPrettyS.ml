(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_OCaml_types_ToS : the let of OCaml's types => export to string
 *
 *)

open Lang_OCaml_type_Types
open STools
open ToS

(* comments are a hierarchical structure :
 *   every sub-comment starting with '(*' and ending with '*)'
 *)
let comment = TreeUtils.ToS.tree char

(* [upper_identifier] identifiers starting with an upper case letter *)
let upper_identifier x = x

(* [lower_identifier] identifiers starting with a lower case letter (or '_') *)
let lower_identifier x = x

(* [abstract_identifier] identifiers starting with the '\'' character *)
let abstract_identifier x = x

let module_name = upper_identifier

(* { <module_name> '.' }* *)
let module_path p =
  SUtils.catmap "" (fun x -> (upper_identifier x)^".") p

(* identifier used for type definition *)
let type_name_def = lower_identifier

(* identifier used for type reference *)
(* <type-name-ref> ::= <module-path> <type-name-def> *)
let type_name_ref (p, d) =
  (module_path p)^(type_name_def d)

(* identifier used for constructor definition *)
let constr_name_def = upper_identifier
(* identifier used for constructor reference *)

(* type identifier regular identifier with lower case first letter or '_' *)
let field_name_def = lower_identifier

(* <atomic-type> ::=
    | <type-identifier>
    | { tuple '(' ',' ')' <product-type> } <type-identifier
    | '(' <product-type> ')'
 *)
let rec atomic_type : atomic_type t =
  function
  | AT_identifier  x     -> type_name_ref x
  | AT_abstract    x     -> abstract_identifier x
  | AT_multi      (x, y) ->
    ((SUtils.tuple (Tools.map product_type x))^" "^(type_name_ref y))
  | AT_subtype     x     -> "( "^(product_type x)^" )"

(* <single-intanciation-type> ::= <atomic-type> { <type-identifier> }* *)
and single_instanciation_type (x, l) : string =
  String.concat " " ((atomic_type x)::(Tools.map type_name_ref l))

(* tuple '*' <single-instanctiation-type> *)
and product_type x =
  SUtils.catmap " * " single_instanciation_type x

(* <record-type> ::=
    tuple+ '{' ';' '}' { <name-identifier> ':' <product-type> }
      (* there is an optional ';' at the end *)
 *)
let record_type x =
  "{ "^
  (SUtils.catmap "" (fun (n, t) -> "\n\t"^(field_name_def n)^" : "^(product_type t)^";") x)^
  "\n}"

let product_or_record_type =
  function
  | PORT_product x -> product_type x
  | PORT_record  x -> record_type  x

let sum_type x =
  SUtils.catmap
    ""
    (fun (n, opt) ->
      "\n\t| "^(constr_name_def n)^(match opt with Some t -> " of "^(product_or_record_type t) | None -> "")
    )
    x

(* <named-type-definition> ::= <product-or-record-type> | <sum-type> *)
let named_type_definition : named_type_definition t =
  function
  | NTR_Alias   x -> product_or_record_type x
  | NTR_Sum     x -> sum_type               x

(* single type declaration
 * NB: 'type' or 'and' has already been parsed
 *)
let named_type : named_type t =
  fun {nt_params; nt_name; nt_def} ->
  (if nt_params = []
    then ""
    else ((SUtils.tuple (Tools.map abstract_identifier nt_params))^" ")
  )^
  (type_name_def nt_name)^" = "^
  (named_type_definition nt_def)

(* list of mutually-recursive types (SCC in the dependency graph) *)
let named_type_rec : named_type_rec t =
  fun ntr -> "type "^(SUtils.catmap "\n\nand  " named_type ntr)

(* list of list of mutually-recursive types *)
let system_type : system_type t = SUtils.catmap "\n\n\n" named_type_rec
