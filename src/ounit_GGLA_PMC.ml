open Extra
module GGLA = GraphGenLA
open GGLA.Type
open GGLA_HFT
open GGLA_HFT.Type

module Examples =
struct
  let make_label i = {
    vlabel_names = [i];
    vlabel_weight = 1;
    vlabel_fixed = false;
    vlabel_useful = true;
  }

  let edges_of_list il = il ||> (fun i -> (i, ()))

  let make_vertex index edges =
    {index; label = make_label index; edges = edges_of_list edges}

  let error_2023_01_07_graph = [|
    make_vertex  0 [1; 2; 3; 4; 5; 6; 7; 8; 12; 14; 16; 17; 19];
    make_vertex  1 [0; 2; 3; 4; 7; 9; 11; 12; 15; 17; 18];
    make_vertex  2 [0; 1; 5; 7; 9; 10; 13; 16; 18];
    make_vertex  3 [0; 1; 4; 5; 10; 11; 12; 13; 14; 16; 19];
    make_vertex  4 [0; 1; 3; 5; 6; 7; 12; 13; 14; 17; 18; 19];
    make_vertex  5 [0; 2; 3; 4; 6; 7; 8; 10; 13; 14; 15; 17; 19];
    make_vertex  6 [0; 4; 5; 7; 8; 9; 10; 11; 13; 15];
    make_vertex  7 [0; 1; 2; 4; 5; 6; 8; 10; 14; 16; 17];
    make_vertex  8 [0; 5; 6; 7; 9; 10; 12; 16; 18; 19];
    make_vertex  9 [1; 2; 6; 8; 11; 13; 14; 18];
    make_vertex 10 [2; 3; 5; 6; 7; 8; 11; 12; 13; 16; 18];
    make_vertex 11 [1; 3; 6; 9; 10; 14; 17];
    make_vertex 12 [0; 1; 3; 4; 8; 10; 13];
    make_vertex 13 [2; 3; 4; 5; 6; 9; 10; 12; 15; 16; 17];
    make_vertex 14 [0; 3; 4; 5; 7; 9; 11; 15; 16; 17; 19];
    make_vertex 15 [1; 5; 6; 13; 14; 17];
    make_vertex 16 [0; 2; 3; 7; 8; 10; 13; 14; 17];
    make_vertex 17 [0; 1; 4; 5; 7; 11; 13; 14; 15; 16; 18; 19];
    make_vertex 18 [1; 2; 4; 8; 9; 10; 17; 19];
    make_vertex 19 [0; 3; 4; 5; 8; 14; 17; 18]
  |]

  let error_2023_01_07_seed =
    [|true; false; false; true; true; true; false; false; false; false; false; false; false; false; true; false; false; false; false; true|]

  let error_2023_01_08_graph : hg =
  [|
    make_vertex  0 [2; 4; 5; 6; 7; 9; 11; 12; 13; 14; 16; 17; 21; 22; 23; 24; 25; 26; 27];
    make_vertex  1 [2; 3; 4; 5; 6; 7; 8; 9; 11; 12; 14; 16; 17; 18; 19; 22; 23; 24; 25; 26];
    make_vertex  2 [0; 1; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 19; 22; 23; 24; 25; 26];
    make_vertex  3 [1; 4; 5; 9; 14; 16; 18; 19; 22; 23; 25; 27];
    make_vertex  4 [0; 1; 2; 3; 5; 6; 7; 9; 12; 13; 14; 16; 18; 20; 21; 22; 23; 25; 27];
    make_vertex  5 [0; 1; 2; 3; 4; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 18; 19; 20; 21; 22; 23; 24; 25; 26; 27];
    make_vertex  6 [0; 1; 2; 4; 8; 11; 12; 14; 15; 16; 17; 19; 20; 21; 25];
    make_vertex  7 [0; 1; 2; 4; 5; 8; 9; 11; 12; 14; 16; 19; 20; 22; 23; 24; 25; 26];
    make_vertex  8 [1; 2; 5; 6; 7; 9; 11; 12; 13; 14; 15; 19; 20; 21; 22; 23; 24; 25; 26; 27];
    make_vertex  9 [0; 1; 2; 3; 4; 5; 7; 8; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 21; 22; 23; 24; 25; 26; 27];
    make_vertex 10 [2; 5; 9; 11; 14; 16; 17; 18; 20; 21; 24; 25];
    make_vertex 11 [0; 1; 2; 5; 6; 7; 8; 9; 10; 12; 14; 15; 16; 18; 19; 21; 22; 23; 24; 25; 26];
    make_vertex 12 [0; 1; 2; 4; 5; 6; 7; 8; 9; 11; 14; 16; 17; 18; 19; 22; 23; 24; 25; 26];
    make_vertex 13 [0; 2; 4; 5; 8; 9; 15; 16; 17; 19; 20; 21; 22; 23; 24; 25; 27];
    make_vertex 14 [0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 15; 19; 21; 22; 23; 24; 25; 26];
    make_vertex 15 [5; 6; 8; 9; 11; 13; 14; 16; 19; 20; 21; 25; 26; 27];
    make_vertex 16 [0; 1; 3; 4; 5; 6; 7; 9; 10; 11; 12; 13; 15; 21; 23; 24; 25; 26; 27];
    make_vertex 17 [0; 1; 6; 9; 10; 12; 13; 18; 19; 20; 25];
    make_vertex 18 [1; 3; 4; 5; 9; 10; 11; 12; 17; 19; 20; 24; 25; 26; 27];
    make_vertex 19 [1; 2; 3; 5; 6; 7; 8; 9; 11; 12; 13; 14; 15; 17; 18; 20; 21; 22; 23; 24; 25; 26; 27];
    make_vertex 20 [4; 5; 6; 7; 8; 9; 10; 13; 15; 17; 18; 19; 21; 22; 24; 25; 26];
    make_vertex 21 [0; 4; 5; 6; 8; 9; 10; 11; 13; 14; 15; 16; 19; 20; 22; 23; 25; 27];
    make_vertex 22 [0; 1; 2; 3; 4; 5; 7; 8; 9; 11; 12; 13; 14; 19; 20; 21; 23; 24; 25; 26; 27];
    make_vertex 23 [0; 1; 2; 3; 4; 5; 7; 8; 9; 11; 12; 13; 14; 16; 19; 21; 22; 24; 25; 26; 27];
    make_vertex 24 [0; 1; 2; 5; 7; 8; 9; 10; 11; 12; 13; 14; 16; 18; 19; 20; 22; 23; 25; 26];
    make_vertex 25 [0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 21; 22; 23; 24; 26; 27];
    make_vertex 26 [0; 1; 2; 5; 7; 8; 9; 11; 12; 14; 15; 16; 18; 19; 20; 22; 23; 24; 25; 27];
    make_vertex 27 [0; 3; 4; 5; 8; 9; 13; 15; 16; 18; 19; 21; 22; 23; 25; 26]
  |]

  let error_2023_01_08_seed =
    [|true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true; true|]

  let error_2023_01_09_graph =
    [|
      make_vertex  0 [1; 2; 4; 5; 7; 9; 12; 15; 16; 17];
      make_vertex  1 [0; 4; 5; 6; 7; 8; 9; 12; 15; 16; 17];
      make_vertex  2 [0; 6; 8; 11; 12; 13; 14];
      make_vertex  3 [10; 16];
      make_vertex  4 [0; 1; 5; 6; 7; 8; 9; 12; 13; 15; 16; 17];
      make_vertex  5 [0; 1; 4; 6; 7; 9; 12; 15; 16; 17];
      make_vertex  6 [1; 2; 4; 5; 8; 9; 11; 12; 13; 14];
      make_vertex  7 [0; 1; 4; 5; 9; 13; 15; 16; 17];
      make_vertex  8 [1; 2; 4; 6; 11; 12; 13; 14; 15; 16; 18];
      make_vertex  9 [0; 1; 4; 5; 6; 7; 15; 16; 17];
      make_vertex 10 [3; 14; 17];
      make_vertex 11 [2; 6; 8; 12; 13; 14; 15];
      make_vertex 12 [0; 1; 2; 4; 5; 6; 8; 11; 13; 14; 16];
      make_vertex 13 [2; 4; 6; 7; 8; 11; 12; 14; 16; 18];
      make_vertex 14 [2; 6; 8; 10; 11; 12; 13; 15; 17];
      make_vertex 15 [0; 1; 4; 5; 7; 8; 9; 11; 14; 16; 17; 18];
      make_vertex 16 [0; 1; 3; 4; 5; 7; 8; 9; 12; 13; 15; 17; 18];
      make_vertex 17 [0; 1; 4; 5; 7; 9; 10; 14; 15; 16];
      make_vertex 18 [8; 13; 15; 16]
    |]

  let error_2023_01_09_seed = [|true; true; true; false; true; true; true; true; true; true; false; true; true; true; false; true; false; false; true|]

  let error_2023_01_10_graph =
    [|
      make_vertex  0 [7; 10; 23];
      make_vertex  1 [5; 13; 14; 15; 19; 23];
      make_vertex  2 [6; 13; 15; 22; 27; 28];
      make_vertex  3 [4; 17; 18; 21];
      make_vertex  4 [3; 8; 12];
      make_vertex  5 [1; 12; 14; 17; 26];
      make_vertex  6 [2; 11; 14; 26; 28];
      make_vertex  7 [0; 14; 22; 27];
      make_vertex  8 [4; 29];
      make_vertex  9 [17];
      make_vertex 10 [0; 12];
      make_vertex 11 [6; 25; 26; 29];
      make_vertex 12 [4; 5; 10];
      make_vertex 13 [1; 2; 15; 26];
      make_vertex 14 [1; 5; 6; 7; 26];
      make_vertex 15 [1; 2; 13; 17];
      make_vertex 16 [28];
      make_vertex 17 [3; 5; 9; 15; 20; 23; 26];
      make_vertex 18 [3];
      make_vertex 19 [1];
      make_vertex 20 [17];
      make_vertex 21 [3];
      make_vertex 22 [2; 7; 24];
      make_vertex 23 [0; 1; 17];
      make_vertex 24 [22];
      make_vertex 25 [11];
      make_vertex 26 [5; 6; 11; 13; 14; 17; 28];
      make_vertex 27 [2; 7];
      make_vertex 28 [2; 6; 16; 26];
      make_vertex 29 [8; 11];
    |]

  let error_2023_01_10_seed = STools.SUtils.bool_array_of_string "010001000000001000000000000000"

  let error_2023_01_10_ex001_graph =
    [|
      {index=  0; label= { vlabel_names=[ 0]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(2, ()); (6, ()); (8, ()); (24, ()); (25, ())]};
      {index=  1; label= { vlabel_names=[ 1]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(22, ()); (24, ())]};
      {index=  2; label= { vlabel_names=[ 2]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(0, ()); (7, ()); (15, ()); (21, ()); (22, ()); (24, ())]};
      {index=  3; label= { vlabel_names=[ 3]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(10, ()); (27, ())]};
      {index=  4; label= { vlabel_names=[ 4]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(12, ()); (27, ())]};
      {index=  5; label= { vlabel_names=[ 5]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(8, ()); (10, ()); (21, ()); (24, ())]};
      {index=  6; label= { vlabel_names=[ 6]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(0, ()); (27, ())]};
      {index=  7; label= { vlabel_names=[ 7]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(2, ()); (8, ()); (24, ())]};
      {index=  8; label= { vlabel_names=[ 8]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(0, ()); (5, ()); (7, ())]};
      {index=  9; label= { vlabel_names=[9; 18]; vlabel_weight=2; vlabel_fixed=false; vlabel_useful=true }; edges= []};
      {index= 10; label= { vlabel_names=[10]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(3, ()); (5, ()); (14, ()); (22, ())]};
      {index= 11; label= { vlabel_names=[11]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(21, ()); (26, ())]};
      {index= 12; label= { vlabel_names=[12]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(4, ()); (17, ()); (23, ()); (27, ())]};
      {index= 13; label= { vlabel_names=[13]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(22, ()); (25, ())]};
      {index= 14; label= { vlabel_names=[14]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(10, ()); (23, ()); (27, ())]};
      {index= 15; label= { vlabel_names=[15]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(2, ()); (25, ())]};
      {index= 16; label= { vlabel_names=[16]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(18, ())]};
      {index= 17; label= { vlabel_names=[17]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(12, ()); (19, ())]};
      {index= 18; label= { vlabel_names=[19]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(16, ()); (20, ()); (23, ()); (24, ())]};
      {index= 19; label= { vlabel_names=[20]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(17, ()); (24, ()); (26, ())]};
      {index= 20; label= { vlabel_names=[21]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(18, ()); (27, ())]};
      {index= 21; label= { vlabel_names=[22]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(2, ()); (5, ()); (11, ()); (24, ())]};
      {index= 22; label= { vlabel_names=[23]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(1, ()); (2, ()); (10, ()); (13, ())]};
      {index= 23; label= { vlabel_names=[24]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(12, ()); (14, ()); (18, ())]};
      {index= 24; label= { vlabel_names=[25]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(0, ()); (1, ()); (2, ()); (5, ()); (7, ()); (18, ()); (19, ()); (21, ())]};
      {index= 25; label= { vlabel_names=[26]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(0, ()); (13, ()); (15, ()); (27, ())]};
      {index= 26; label= { vlabel_names=[27]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(11, ()); (19, ())]};
      {index= 27; label= { vlabel_names=[28]; vlabel_weight=1; vlabel_fixed=false; vlabel_useful=true }; edges= [(3, ()); (4, ()); (6, ()); (12, ()); (14, ()); (20, ()); (25, ())]};
    |]

  let error_2023_01_10_ex001_seed = STools.SUtils.bool_array_of_string "1010000000000000000000001000"

  let error_2023_01_10_ex002_graph =
    [|
      make_vertex  0 [1; 2; 4; 5; 7; 9; 12; 14];
      make_vertex  1 [0; 2; 4; 5; 8; 9; 11; 13; 14];
      make_vertex  2 [0; 1; 5; 11];
      make_vertex  3 [5; 6];
      make_vertex  4 [0; 1; 7; 8; 9; 11; 13; 14];
      make_vertex  5 [0; 1; 2; 3; 7; 8; 10; 11; 12; 13];
      make_vertex  6 [3; 10; 13];
      make_vertex  7 [0; 4; 5; 8; 12];
      make_vertex  8 [1; 4; 5; 7; 9; 10; 11; 13; 14];
      make_vertex  9 [0; 1; 4; 8; 11; 13; 14];
      make_vertex 10 [5; 6; 8; 14];
      make_vertex 11 [1; 2; 4; 5; 8; 9; 12; 13; 14];
      make_vertex 12 [0; 5; 7; 11];
      make_vertex 13 [1; 4; 5; 6; 8; 9; 11; 14];
      make_vertex 14 [0; 1; 4; 8; 9; 10; 11; 13]
    |]

  let error_2023_01_10_ex002_seed = [|true; true; true; false; true; false; false; true; false; true; false; false; true; false; false|]

  let error_2023_01_11_ex000_graph =
    [|
      make_vertex  0 [1; 2; 6; 7; 8; 12; 13; 15; 16; 18; 19];
      make_vertex  1 [0; 3; 4; 6; 7; 8; 10; 12; 13; 15; 16; 18; 19];
      make_vertex  2 [0; 4; 6; 7; 8; 10; 13; 15; 18];
      make_vertex  3 [1; 8; 10; 12; 13; 14; 16];
      make_vertex  4 [1; 2; 5; 9; 19];
      make_vertex  5 [4; 6; 7; 8; 17];
      make_vertex  6 [0; 1; 2; 5; 7; 8; 12; 16; 18; 19];
      make_vertex  7 [0; 1; 2; 5; 6; 8; 12; 13; 15; 18; 19];
      make_vertex  8 [0; 1; 2; 3; 5; 6; 7; 10; 11; 12; 14; 18; 19];
      make_vertex  9 [4; 17; 19];
      make_vertex 10 [1; 2; 3; 8; 11; 13; 14; 15; 18; 19];
      make_vertex 11 [8; 10; 12; 14; 15; 16; 18; 19];
      make_vertex 12 [0; 1; 3; 6; 7; 8; 11; 15; 16; 18; 19];
      make_vertex 13 [0; 1; 2; 3; 7; 10; 14; 15; 16; 18];
      make_vertex 14 [3; 8; 10; 11; 13; 16; 18; 19];
      make_vertex 15 [0; 1; 2; 7; 10; 11; 12; 13; 16; 18];
      make_vertex 16 [0; 1; 3; 6; 11; 12; 13; 14; 15];
      make_vertex 17 [5; 9];
      make_vertex 18 [0; 1; 2; 6; 7; 8; 10; 11; 12; 13; 14; 15; 19];
      make_vertex 19 [0; 1; 4; 6; 7; 8; 9; 10; 11; 12; 14; 18]
    |]

  let error_2023_01_11_ex000_seed = [|true; false; true; true; false; false; false; false; false; false; true; true; true; true; true; true; true; false; false; false|]
end

let instance_examples : (string * hg * bool array) list = Examples.[
  ("error_2023_01_07_graph", error_2023_01_07_graph, error_2023_01_07_seed);
  ("error_2023_01_08_graph", error_2023_01_08_graph, error_2023_01_08_seed);
  ("error_2023_01_09_graph", error_2023_01_09_graph, error_2023_01_09_seed);
  ("error_2023_01_10_graph", error_2023_01_10_graph, error_2023_01_10_seed);
  ("error_2023_01_10_ex001_graph", error_2023_01_10_ex001_graph, error_2023_01_10_ex001_seed);
  ("error_2023_01_10_ex002_graph", error_2023_01_10_ex002_graph, error_2023_01_10_ex002_seed);
  ("error_2023_01_11_ex000_graph", error_2023_01_11_ex000_graph, error_2023_01_11_ex000_seed);
]

open GGLA_PMC

open OUnit

type enum_from_seed = GGLA_HFT.Type.hg -> bool array -> bool array list * int

let solver_list : (string * enum_from_seed) list = [
  ("EnumPMC2", EnumPMC2.enum_from_seed);
  ("EnumPMC3", EnumPMC3.enum_from_seed);
  ("EnumPMC4", EnumPMC4.enum_from_seed);
]

let _ =
  List.iter (fun ((instance_name, instance_graph, instance_seed):string * hg * bool array) ->
    Printf.printf "[%s]\n" instance_name;
    List.iter (fun (solver_name, solver_function) ->
      Printf.printf "\n\t[%s:%s]\n" instance_name solver_name;
      let pmc_list, nnode = solver_function instance_graph instance_seed in
      Printf.printf
        "[ounit_GGLA_PMC.%s:%s] pmc_list:%s\n"
        instance_name solver_name
        (STools.ToS.list STools.SUtils.string_of_bool_array pmc_list);
      Printf.printf
        "[ounit_GGLA_PMC.%s:%s] nnode:%d"
        instance_name solver_name
        nnode;
    ) solver_list
  ) instance_examples
