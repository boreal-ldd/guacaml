(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * ALInt : Arbitrary Length Integers
 *
 * === NOTES ===
 *
 * This module is implemented with bool array on purpose. In
 * order to be used as a simpler to check reference for other
 * modules. In particular it is used as ground-truth by
 * Snowflake parametric equivalent of this module
 *)

let prefix = "GuaCaml.ALInt"

module Unsigned =
struct
  let prefix = prefix^".Unsigned"

  let fulladd2 (a:bool) (b:bool) : bool * bool =
    let car = a && b in (* carry = a /\ b *)
    let sum = a <> b in (* sum = a (+) b *)
    (sum, car)

  let fulladd3 (a:bool) (b:bool) (c:bool) : bool * bool =
    let car = (a || (b && c)) && (b || c) in
    let sum = a <> b <> c in
    (sum, car)

  type t = bool array

  let normalized ?(default=false) (x:t) : bool =
    (MyArray.last x <> Some default)

  let normalize ?(default=false) (x:t) : t =
    match MyArray.find_last ((=)(not default)) x with
    | None -> [||]
    | Some i -> Array.sub x 0 (i+1)

  let opnorm ?(norm=true) (x:t) : t =
    if norm then normalize x else x

  let get ?(default=false) (x:t) (k:int) : bool =
    assert(0 <= k);
    if k < Array.length x
    then x.(k)
    else default

  let map ?(norm=true) (f:bool -> bool) (x:t) : t =
    opnorm ~norm (Array.map f x)

  let of_bool (b:bool) : t = [|b|]
  let of_nat (n:int) : t =
    assert(n >= 0);
    Tools.bin_of_int n
  (* [FIXME] not platform independent
   * platform : 64-bit
   * raises Invalid_arg if the [x] has more than 62 bits
   *)
  let to_nat (x:t) : int =
    if Array.length x <= 62
    then Tools.int_of_bin (Array.to_list x)
    else invalid_arg "[GuaCaml.ALInt.Unsigned.to_nat] native integer overflow"

  let one () = [|true|]

  (* internal *)
  let rec add_rec (dx:bool) (x:t) (dy:bool) (y:t) (s:t) (len:int) (ci:bool) (i:int) : bool =
    if i < len
    then (
      let xi = get ~default:dx x i in
      let yi = get ~default:dy y i in
      let si, c_si = fulladd3 xi yi ci in
      s.(i) <- si;
      add_rec dx x dy y s len c_si (succ i)
    )
    else ci

  let add ?(norm=true) ?(carry=false) ?(dx=false) (x:t) ?(dy=false) (y:t) : t =
    let lx = Array.length x in
    let ly = Array.length y in
         if lx = 0 then opnorm ~norm y
    else if ly = 0 then opnorm ~norm x
    else (
      let len = max lx ly in
      let s = Array.make (succ len) false in
      let c = add_rec dx x dy y s len carry 0 in
      s.(len) <- c;
      opnorm ~norm s
    )

  let ( +/ ) x y = add x y

  (* [LATER] merge both shifts *)

  (* decreasing shift *)
  let shift_right_nat' ?(norm=true) (shift:int) (x:t) : t =
    assert(shift >= 0);
    let len = Array.length x in
    if shift = 0 || len = 0 then x else (
      if shift < len
      then opnorm ~norm (Array.sub x shift (len-shift))
      else [||]
    )

  let shift_right_nat shift x = shift_right_nat' x shift

  (* increasing shift *)
  let shift_left_nat' ?(norm=true) (shift:int) (x:t) : t =
    assert(shift >= 0);
    let len = Array.length x in
    if shift = 0 || len = 0 then x else (
      let sx : t = Array.make (shift+len) false in
      Array.blit x 0 sx shift len;
      sx
    )

  let shift_left_nat shift x = shift_left_nat' x shift

  let shift_left' ?(norm=true) (shift:int) (x:t) : t =
         if shift = 0 then opnorm ~norm x
    else if shift < 0 then shift_right_nat' ~norm (-shift) x
    else                   shift_left_nat'  ~norm   shift  x

  let shift_right' ?(norm=true) (shift:int) (x:t) : t =
    shift_left' ~norm (-shift) x

  let shift_left x k = shift_left' k x
  let shift_right x k = shift_right' k x

  let ( <! ) (x:bool) (y:bool) : bool = (not x) && y

  let rec lX_rec (c0:bool) dx x dy y i maxi : bool =
    if i < maxi
    then (
      let xi = get ~default:dx x i in
      let yi = get ~default:dy y i in
      let ci =
        if xi = yi
        then c0
        else xi <! yi
      in
      lX_rec ci dx x dy y (succ i) maxi
    )
    else c0

  let lX (c0:bool) ?(dx=false) (x:t) ?(dy=false) (y:t) : bool =
    let lx = Array.length x in
    let ly = Array.length y in
    let len = max lx ly in
    lX_rec c0 dx x dy y 0 len

  let lt = lX false

  let le = lX true

  let gX default intX intY = lX default intY intX

  let gt = gX false

  let ge = gX true

  let (  </ ) x y = lt x y
  let ( <=/ ) x y = le x y
  let (  >/ ) x y = gt x y
  let ( >=/ ) x y = ge x y

end

module U = Unsigned (* [ALIAS] *)

(* Representation using Two's Complement *)
module Signed =
struct
  let prefix = prefix ^ ".Signed"

  let signed_fulladd3 (c:bool) (s0:bool) (s1:bool) : bool * bool =
    let carry = c <> s0 <> s1 in
    let sign  = (not c) <> (((not c) <> s0) && ((not c) <> s1)) in
    (carry, sign)

  type t = {
    pos : U.t;
    ult : bool;
  }

  let length (x:t) = Array.length x.pos

  let normalized (x:t) : bool =
    U.normalized ~default:x.ult x.pos

  let normalize (x:t) : t =
    {x with pos = U.normalize ~default:x.ult x.pos}

  let opnorm ?(norm=true) (x:t) : t =
    if norm then normalize x else x

  let get (x:t) (k:int) : bool =
    U.get ~default:x.ult x.pos k

  let of_unsigned (pos:U.t) : t =
    {pos; ult = false}

  let of_bool (b:bool) : t = of_unsigned (U.of_bool b)

  let of_nat (n:int) : t =
    assert(n >= 0);
    of_unsigned (U.of_nat n)

  let to_nat (x:t) : int =
    if x.ult then invalid_arg "[GuaCaml.ALInt.Signed.to_nat] [x] is negative, cannot be casted to 'nat'";
    U.to_nat x.pos

  let zero () : t = {pos = [||]; ult = false}
  let one () : t = {pos = [|true|]; ult = false}
  let negative_one () : t = {pos = [||]; ult = true}

  let add ?(norm=true) ?(carry=false) (x:t) (y:t) =
    let lx = length x in
    let ly = length y in
    let len = max lx ly in
    let pos = Array.make (succ len) false in
    let c = U.add_rec x.ult x.pos y.ult y.pos pos len carry 0 in
    let c', ult = signed_fulladd3 c x.ult y.ult in
    pos.(len) <- c';
    opnorm ~norm {pos; ult}

  let ( +/ ) x y = add x y

  let negb ?(norm=true) (x:t) : t =
    let negb x = not x in
    {
      pos = Array.map negb x.pos;
      ult = not x.ult
    }

  let negate ?(norm=true) (x:t) : t =
    let nx = negb ~norm x in
    (* add ~norm nx (one()) *)
    add ~norm ~carry:true nx (zero())

  let ( ~-/ ) t = negate t

  let minus ?(norm=true) (x:t) (y:t) : t =
    let ny = negb ~norm y in
    add ~norm ~carry:true x ny

  let ( -/ ) x y = minus x y

  let of_int' ?(norm=true) (z:int) : t =
    if z >= 0
    then of_nat z
    else negate ~norm (of_nat (-z))

  let of_int (z:int) : t = of_int' z

  (* [FIXME] not platform independent
   * platform : 64-bit
   * raises Invalid_arg if the [x] has more than 62 bits
   *)
  let to_int' ?(norm=true) (x:t) : int =
    if length x <= 62
    then if x.ult
      then (-(to_nat(negate ~norm x)))
      else (  to_nat              x  )
    else invalid_arg "[GuaCaml.ALInt.Signed.to_int] native integer overflow"

  let to_int x = to_int' x
end

module S = Signed (* [ALIAS] *)
