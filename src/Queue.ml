(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Queue : Queue #DataStructure (a.k.a., FIFO list, First In, First Out)
 *
 * [WARNING] : this file is not complete yet, do not use !
 *)

module DLL = DoubleLinkedList

type 'a t = {
  mutable fst : 'a DLL.o;
  mutable lst : 'a DLL.o;
  mutable len : int;
}

let get_fst_opt (t:'a t) : 'a DLL.o = t.fst
let get_fst (t:'a t) : 'a DLL.t = t.fst |> Tools.unop
let get_fst_item (t:'a t) : 'a =
  DLL.get_item (get_fst t)
let set_fst_item (t:'a t) (a:'a) : unit =
  DLL.set_item (get_fst t) a

let get_lst_opt (t:'a t) : 'a DLL.o = t.lst
let get_lst (t:'a t) : 'a DLL.t = t.lst |> Tools.unop
let get_lst_item (t:'a t) : 'a =
  DLL.get_item (get_lst t)
let set_lst_item (t:'a t) (a:'a) : unit =
  DLL.set_item (get_lst t) a

let push (t:'a t) (a:'a) : unit =
  t.lst <- Some(DLL.create_after_opt t.lst a);
  if t.fst = None then t.fst <- t.lst;
  t.len <- succ t.len

let pull (t:'a t) : 'a =
  match t.fst with
  | None -> invalid_arg "[GuaCaml/Queue.pull] empty queue"
  | Some fst -> (
    let item, fst = DLL.remove_fst fst in
    t.fst <- fst;
    if fst = None then t.lst <- None;
    t.len <- pred t.len;
    item
  )
