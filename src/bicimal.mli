(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Bicimal : encoding binary number as the canonical sum of their power of two
 * (represented as sorted list of integers)
 *)

type t = int list
(* positive bicimal *)
(* sorted by increasing order *)
(* [[t]] = sum_{x\in t} 2^x *)

module ToS :
sig
  val t : t STools.ToS.t
end

module ToShiftS :
sig
  val t : t STools.ToShiftS.t
end

val add_single : ?strict:bool -> int -> t -> t
val add : ?strict:bool -> t -> t -> t
(*  [normalize il = t]
    where [il] is an unsorted list of signed integers
 *)
val normalize : ?sorted:bool -> int list -> t
