(*
 * (c) B. Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes, 2019-2022.
 *
 * Part of Isamdae: a structural analysis method for multimode DAE system, inspired by J. Pryce's
 * Sigma method.
 *
 *)

(**
   Sparse vector structure based on FlexArray
*)

(** TODO An accurate description of the following module *)
module type MSig =
sig
  type t
  type s
  type sub
  val index : t -> int
  val card : s -> int
  val iter : (t -> unit) -> s -> unit
  val iter_subset : (t -> unit) -> sub -> unit
  val fold : (t -> 'a -> 'a) -> s -> 'a -> 'a
  val to_string : s -> t -> string
end

(** TODO An accurate description of the following module *)
module type Sig =
sig
  module I : MSig
  type 'a t
  val make : ?base:int -> I.s -> 'a -> 'a t

  val get_set  : 'a t -> I.s
  val get_card : 'a t -> int

  val get : 'a t -> I.t -> 'a
  val set : 'a t -> I.t -> 'a -> unit
  val iter : ('a -> bool) -> (I.t -> 'a -> unit) -> 'a t -> unit
  val fold : ('a -> bool) -> (I.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
end

module Make(MI:MSig) : Sig
  with type I.t   = MI.t
  and  type I.s   = MI.s
  and  type I.sub = MI.sub
