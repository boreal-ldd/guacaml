(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_DIMACS_Opal : Bare Syntax DIMACS Parser (written in Opal)
 *
 *)

open Lang_GraphKD_Core
open Types
open Utils
open Opal
open CharStream

let clique : (char, int list) Opal.t =
  until1 (spaces >> nat) << skip_until_newline

let fixed : (char, int list) Opal.t =
  exactly 'f' >> until (spaces >> nat) << skip_until_newline

let comment : (char, string) Opal.t =
  exactly 'c' >> (until_stop any (exactly '\n') => (fun (cl, _) -> implode cl))

let primitive : (char, string * int * int) Opal.t =
  let* _ = exactly 'p' in
  let* mode = spaces >> word in
  let* nv = spaces >> nat in
  let* nc = spaces >> nat in
  let* _ = skip_until_newline in
  return (mode, nv, nc)

let graphkd_line : (char, graphkd_line) Opal.t =
  (comment => (fun s -> Comment s)) <|>
  (primitive => (fun (mode, nv, nc) -> Primitive(mode, nv, nc))) <|>
  (fixed => (fun f -> Fixed f)) <|>
  (clique => (fun c -> Clique c))

let graphkd : (char, graphkd) Opal.t =
  until graphkd_line

let from_file (target:string) : graphkd =
  match parse_from_file graphkd target with
  | Some some -> some
  | None -> failwith "[GuaCaml.Lang_GraphKD_Opal.from_file] uncaught parsing error"
