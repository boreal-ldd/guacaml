open IterExtra

let iter_2_bool = Iter.gen_bool $* Iter.gen_bool
let iter_3_bool = (iter_2_bool $* Iter.gen_bool) $$ (fun ((a, b), c) -> (a, b, c))

let string_of_bool b = string_of_int(Bool.to_int b)

let string_of_2_bool (a, b) =
  (string_of_bool a)^(string_of_bool b)

let string_of_3_bool (a, b, c) =
  (string_of_bool a)^(string_of_bool b)^(string_of_bool c)

let gen_k_bits (k:int) (mini:int) (maxi:int) : int Iter.iter =
  Iter.half_square_strict (Iter.range mini maxi) k
  $$ (fun il -> List.fold_left (fun c i -> c lor (1 lsl i)) 0 il)

let gen_2_bits ?(mini=0) (maxi:int) : int Iter.iter =
  gen_k_bits 2 mini maxi

let gen_3_bits ?(mini=0) (maxi:int) : int Iter.iter =
  gen_k_bits 3 mini maxi

let gen_signed_k_bits (k:int) (mini:int) (maxi:int) : int Iter.iter =
  (gen_k_bits k mini maxi $* Iter.gen_bool) $$ (fun (n, b) -> if b then -n else n)

let gen_signed_2_bits ?(mini=0) (maxi:int) : int Iter.iter =
  gen_signed_k_bits 2 mini maxi

let gen_signed_3_bits ?(mini=0) (maxi:int) : int Iter.iter =
  gen_signed_k_bits 3 mini maxi
