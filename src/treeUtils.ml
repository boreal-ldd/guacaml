(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * TreeUtils : tools for generic n-ary trees
 *)

open STools
open Tree

module ToS =
struct
  open ToS

  let tree (fa:'a t) (t:'a tree) : string =
    let rec tree_rec = function
      | Leaf leaf -> constructor "Leaf" fa leaf
      | Node node -> constructor "Node" (list tree_rec) node
    in
    tree_rec t

  let trees fa = list(tree fa)

  let atree (fa:'a t) (fb:'b t) (t:('a, 'b) atree) : string =
    let rec aux = function
      | ALeaf  b     -> constructor "ALeaf" fb b
      | ANode (a, l) -> constructor "ANode" (fa * (list aux)) (a, l)
    in aux t

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl

  let gnext (fa:'a t) (fb:'b t) : ('a, 'b) gnext -> string =
    function
    | GLink a -> constructor "GLink" fa a
    | GLeaf b -> constructor "GLeaf" fb b
end

module ToShiftS =
struct
  open ToShiftS

  let tree (fa:'a t) (t:'a tree) : ShiftS.t =
    let rec tree_rec = function
      | Leaf leaf -> constructor "Leaf" fa leaf
      | Node node -> constructor "Node" (list tree_rec) node
    in
    tree_rec t

  let trees fa = list(tree fa)

  let atree (fa:'a t) (fb:'b t) (t:('a, 'b) atree) : ShiftS.t =
    let rec aux = function
      | ALeaf  b     -> constructor "ALeaf" fb b
      | ANode (a, l) -> constructor "ANode" (fa * (list aux)) (a, l)
    in aux t

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl

  let gnext (fa:'a t) (fb:'b t) : ('a, 'b) gnext -> ShiftS.t =
    function
    | GLink a -> constructor "GLink" fa a
    | GLeaf b -> constructor "GLeaf" fb b
end

module ToPrettyS =
struct
  let allign (n:int) (s:string) : string = (String.make (2*n) ' ')^s
  open STools.ToS

  let rec atree ?(depth=0) (fa:'a t) (fb:'b t) (t:('a, 'b) atree) : string =
    let rec atree_rec (depth:int) = function
      | ALeaf  b     -> allign depth ("("^(fb b)^")")
      | ANode (a, l) -> (
        (allign depth ("{"^(fa a)^"}[\n"))^
        (STools.SUtils.catmap "\n" (atree_rec (succ depth)) l)^"\n"^
        (allign depth "]")
      )
    in atree_rec depth t
end

module ToSTree =
struct
  open STools.ToSTree
  let atree (fa:'a t) (fb:'b t) (t:('a, 'b) atree)  =
    let rec aux : ('a, 'b) atree t = function
    | ALeaf  b ->
      Node [Leaf "ALeaf"; fb b]
    | ANode (a, l) ->
      Node [Leaf "ANode"; (fa * (list aux)) (a, l)]
    in aux t

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl
end

module OfSTree =
struct
  open STools.OfSTree
  let atree (fa:'a t) (fb:'b t) stree =
    let rec aux = function
    | Node[Leaf "ALeaf"; data] ->
      let b = fb data in
      ALeaf b
    | Node[Leaf "ANode"; data] ->
      let a, l = (fa * (list aux)) data in
      ANode (a, l)
    | _ -> (failwith "[GuaCaml.TreeUtils] error")
    in aux stree

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl
end

module ToBStream =
struct
  open BTools.ToBStream

  let gnext (a'lk:'lk t) (a'lf:'lf t)
    (cha:Channel.t) (next:('lk, 'lf) gnext) : unit =
    match next with
    | GLink b ->
    (
      bool cha true;
      a'lk cha b;
    )
    | GLeaf a ->
    (
      bool cha false;
      a'lf cha a;
    )
end

module OfBStream =
struct
  open BTools.OfBStream

  let gnext (a'lk:'lk t) (a'lf:'lf t)
    (cha:Channel.t) : ('lk, 'lf) gnext =
    match bool cha with
    | true  -> GLink (a'lk cha)
    | false -> GLeaf (a'lf cha)
end

module ToB =
struct
  open BTools.ToB

  let gnext (a'link:'lk t) (a'leaf:'lf t) (g:('lk, 'lf)gnext) (s:stream) =
    match g with
    | GLink x -> false :: (a'link x s)
    | GLeaf x -> true  :: (a'leaf x s)
end

module OfB =
struct
  open BTools.OfB

  let gnext a'link a'leaf : ( _, _ ) gnext t =
    function
    | false::s -> (
      let a, s = a'link s in
      (GLink a, s)
    )
    | true ::s -> (
      let a, s = a'leaf s in
      (GLeaf a, s)
    )
    | [] -> failwith "[GuaCaml.TreeUtils.OfB.gnext] binary parsing error : empty stream"
end

module IoB =
struct
open BTools.IoB

  let gnext a b = (ToB.gnext (fst a) (fst b), OfB.gnext (snd a) (snd b))
end

let rec aatree_normalized = function
  | ALeaf _ -> true
  | ANode (_, []) -> false
  | ANode (_, tl) -> aatrees_normalized tl
and    aatrees_normalized (tl:_ aatrees) =
  List.for_all aatree_normalized tl

let rec aatree_normalize = function
  | ALeaf x
  | ANode (x, []) -> ALeaf x
  | ANode (x, tl) -> ANode (x, aatrees_normalize tl)
and    aatrees_normalize (tl: _ aatrees) =
  Tools.map aatree_normalize tl

(* @santiago.bautista *)
let aatree_of_ialist_insert ((i0, e0): int * 'a) (l: (int*('a aatree)) list) : (int*('a aatree)) list =
  let heads, tails =
    MyList.find_onehash_prefix ~p:(fun (i, _) -> i > i0) fst l
  in
  (i0, ANode(e0, Tools.map snd heads))::tails

(* @santiago.bautista *)
let aatree_of_ialist (l:(int * 'a) list) : 'a aatrees =
  let rec aux (left: (int * 'a) list) (l: (int*('a aatree)) list) : _ aatrees =
    match left with
    | [] ->
    ( (* finalizer *)
      assert(MyList.onehash fst l);
      Tools.map snd l
    )
    | line::left -> aux left (aatree_of_ialist_insert line l)
  in
  aatrees_normalize (aux (List.rev l) [])

let unGLeaf = function
  | GLeaf lf -> lf
  | GLink _  -> (failwith "[GuaCaml.TreeUtils] error")
let unGLink = function
  | GLeaf _  -> (failwith "[GuaCaml.TreeUtils] error")
  | GLink lk -> lk

let ungnext = function GLeaf x | GLink x -> x

let filterGLeaf (l:('lk, 'lf) gnext list) : 'lf list =
  let rec filterGLeaf_rec carry = function
    | [] -> List.rev carry
    | (GLeaf a)::t -> filterGLeaf_rec (a::carry) t
    | (GLink _)::t -> filterGLeaf_rec     carry  t
  in filterGLeaf_rec [] l

let filterGLink (l:('lk, 'lf) gnext list) : 'lk list =
  let rec filterGLink_rec carry = function
    | [] -> List.rev carry
    | (GLink b)::t -> filterGLink_rec (b::carry) t
    | (GLeaf _)::t -> filterGLink_rec     carry  t
  in filterGLink_rec [] l

let filterGLeaf_array (a:('a, 'b) gnext array) : 'a list =
  let rec filterGLeafa_rec carry array i =
    if i < 0 then carry else (
      match array.(i) with
      | GLeaf a -> filterGLeafa_rec (a::carry) array (pred i)
      | GLink _ -> filterGLeafa_rec     carry  array (pred i)
    )
  in filterGLeafa_rec [] a (Array.length a -1)

let filterGLink_array (a:('a, 'b) gnext array) : 'b list =
  let rec filterGLinka_rec carry array i =
    if i < 0 then carry else (
      match array.(i) with
      | GLink b -> filterGLinka_rec (b::carry) array (pred i)
      | GLeaf _ -> filterGLinka_rec     carry  array (pred i)
    )
  in filterGLinka_rec [] a (Array.length a -1)

let rec tree_flatten_rec (carry:'a list) (t:'a tree) : 'a list =
  match t with
  | Leaf a -> (a::carry)
  | Node f -> rev_trees_flatten_rec carry (List.rev f)
and     rev_trees_flatten_rec (carry:'a list) (f:'a trees) : 'a list =
  match f with
  | [] -> carry
  | t::f -> rev_trees_flatten_rec (tree_flatten_rec carry t) f

let tree_flatten (t:'a tree) : 'a list =
  tree_flatten_rec [] t

let trees_flatten (f:'a trees) : 'a list =
  rev_trees_flatten_rec [] (List.rev f)

let sum_pair (x0, y0) (x1, y1) = (x0 + x1, y0 + y1)
let sum_pairs l = List.fold_left sum_pair (0, 0) l

(* [count_tree t = (#leaf, #node)] *)
let rec count_tree : 'a tree -> int * int =
  function
  | Leaf _ -> (1, 0)
  | Node trees -> sum_pair (0, 1) (count_trees trees)
and     count_trees : 'a trees -> int * int =
  (fun tl -> List.fold_left (fun c t -> sum_pair c (count_tree t)) (0, 0) tl)

(* [count_atree t = (#leaf, #node)] *)
let rec count_atree : (_, _) atree -> int * int =
  function
  | ALeaf _ -> (1, 0)
  | ANode (_, trees) -> sum_pair (0, 1) (count_atrees trees)
and     count_atrees : (_, _) atrees -> int * int =
  (fun tl -> List.fold_left (fun c t -> sum_pair c (count_atree t)) (0, 0) tl)




