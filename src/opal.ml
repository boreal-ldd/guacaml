(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Opal : Monadic Parser Combinators for OCaml
 *
 * === DISCLAIMER ===
 *
 * This file is more or less directly imported from the
 * [opal](https://github.com/pyrocat101/opal) project on GitHub.
 * With time both versions may diverge, in particular :
 *   - factorisation with other part of GuaCaml
 *   - extended syntax and utilities
 *
 *)

open STools
module Stream = Iter.Stream

(* Section. Lazy Stream *)

module LazyStream = struct
  type 't t = Cons of 't * 't t Lazy.t | Nil

  let of_stream stream =
    let rec next stream =
      (* print_endline "[GuaCaml.Opal.LazyStream.of_stream] tick!"; *)
      try Cons(Stream.next stream, lazy (next stream))
      with End_of_file -> Nil
    in
    next stream

  let of_function f =
    let rec next f =
      (* print_endline "[GuaCaml.Opal.LazyStream.of_function] tick!"; *)
      match f () with
      | Some x -> Cons(x, lazy (next f))
      | None -> Nil
    in
    next f

  let of_channel ?(tee=(fun _ -> ())) channel =
    let f () =
      try (
        let c = input_char channel in
        tee c;
        Some c
      )
      with End_of_file -> None
    in of_function f

  let rec of_list : 'a list -> 'a t =
    function
    | [] -> Nil
    | h::t -> Cons(h, lazy(of_list t))

  let rec of_string : string -> 'a t =
    let rec of_string_rec s i =
      (* print_endline ("[GuaCaml.Opal.LazyStream.of_string] length:"^(STools.ToS.int(String.length s))); *)
      (* print_endline ("[GuaCaml.Opal.LazyStream.of_string] i:"^(STools.ToS.int i)); *)
      if i < String.length s
      then (
        (* print_endline ("[GuaCaml.Opal.LazyStream.of_string] s.[i]:"^(STools.ToS.char s.[i])); *)
        Cons(s.[i], lazy(of_string_rec s (succ i)))
      )
      else Nil
    in fun s -> of_string_rec s 0

  (* let of_string str = str |> Stream.of_string |> of_stream *)
  (* let of_string str = str |> STools.SUtils.explode |> of_list *)
  (* let of_channel ?(tee=(fun _ -> ())) ic = ic |> Stream.of_channel_as_char ~tee |> of_stream *)

  let eval1 : 'a t -> ('a * 'a t) option =
    function
    | Cons(token, input') -> Some(token, Lazy.force input')
    | Nil -> None

  let first : 'a t -> 'a option =
    function
    | Cons(token, _) -> Some token
    | Nil -> None

  (* [TODO] function description *)
  (* [MOVEME] *)
  let gen_iter_to_rev_list (next:'s -> ('a * 's) option) : 's -> 'a list =
    let rec list_rec carry next stream =
      match next stream with
      | Some(head, stream) -> list_rec (head::carry) next stream
      | None               -> carry
    in
    list_rec [] next

  (* Evaluates a lazy stream into an explicit list *)
  let to_list (stream:'a t) : 'a list =
    List.rev(gen_iter_to_rev_list eval1 stream)
end

type 't stream = 't LazyStream.t

(* Section. String Utilities *)

let implode = SUtils.implode

let explode = SUtils.explode

(* ( % ) = Extra.( >> ) *)
let (%) f g = fun x -> g (f x)

(* Section. Primitives *)

type 't input = 't LazyStream.t
type ('t, 'b) monad = ('b * 't input) option
type ('t, 'b) parser = 't input -> ('t, 'b) monad
type ('t, 'b) t = ('t, 'b) parser

let parse (parser:('t, 'a) t) (input:'t stream) : 'a option =
  match parser input with
  | Some(res, _) -> Some res
  | None -> None

let parse_from_file (parser:(char, 'a) t) (file:string) : 'a option =
  let channel = open_in file in
  let res = parse parser (LazyStream.of_channel channel) in
  close_in channel;
  res

let return x input = Some(x, input)

let (>>=) (x:('t, 'a)t) (f:'a -> ('t, 'b) t) : ('t, 'b) t =
  fun input ->
  match x input with
  | Some(result', input') -> f result' input'
  | None -> None

let (let*) = (>>=)

let (<|>) x y =
  fun input ->
  match x input with
  | Some _ as ret -> ret
  | None -> y input

let rec scan x input =
  match x input with
  | Some(result', input') -> LazyStream.Cons(result', lazy (scan x input'))
  | None -> LazyStream.Nil

let mzero _ = None

(* A parser that parses nothing *)
let mtrue : ('t, unit) t =
  (fun input -> Some((), input))

let any = LazyStream.eval1

let satisfy test =
  let* res = any in
  if test res
  then return res
  else mzero

let eof (x:'a) : ('t, 'a) t = function
  | LazyStream.Nil -> Some(x, LazyStream.Nil)
  | _ -> None

(* Section. Derived Combinators *)

let (=>) x f = x >>= fun r -> return (f r)
let (>>) x y = x >>= fun _ -> y
let (<<) x y = x >>= fun r -> y >>= fun _ -> return r
let ($::) head tail =
  let* h = head in
  let* t = tail in
  return (h::t)

let ( $* ) (pa:('t, 'a)t) (pb:('t, 'b)t) : ('t, 'a * 'b)t =
  let* a = pa in
  let* b = pb in
  return (a, b)

let rec choice = function
  | [] -> mzero
  | h :: t -> h <|> choice t

let rec ntimes n x =
  if n > 0
  then x $:: ntimes (pred n) x
  else return []

let between op ed x = op >> x << ed

let option_default default x = x <|> return default
let optional x = option_default () (x >> return ())
let option x = (x => (fun x -> Some x)) <|> return None

let rec skip_many x = option_default () (x >>= fun _ -> skip_many x)
let skip_many1 x = x >> skip_many x

let rec many x = option_default [] (x >>= fun r -> many x >>= fun rs -> return (r :: rs))
let many1 x = x $:: many x

let sep_by1 x sep = x $:: many (sep >> x)
let sep_by x sep = sep_by1 x sep <|> return []

let end_by1 x sep = sep_by1 x sep << sep
let end_by x sep = end_by1 x sep <|> return []

let chainl1 x op =
  let rec loop a =
    (op >>= fun f -> x >>= fun b -> loop (f a b)) <|> return a
  in
  x >>= loop
let chainl x op default = chainl1 x op <|> return default

let rec chainr1 x op =
  x >>= fun a -> (op >>= fun f -> chainr1 x op => f a) <|> return a
let chainr x op default = chainr1 x op <|> return default

(* looks like many *)
let until (elem:('t, 'a) t) : ('t, 'a list) t =
  let rec until_rec carry stream =
    match elem stream with
    | Some(head, stream) -> until_rec (head::carry) stream
    | None -> return (List.rev carry) stream
  in
  until_rec []

let until1 e = e $:: until e

let until_stop (elem:('t, 'a) t) (stop:('t, 'b) t) : ('t, ('a list) * 'b) t =
  let rec until_rec carry =
    (let* b = stop in return (List.rev carry, b)) <|>
    (let* a = elem in until_rec (a::carry))
  in
  until_rec []

let until_skip_stop (elem:('t, 'a) t) (skip:('t, _) t) (stop:('t, 'b) t) : ('t, ('a list) * 'b) t =
  let rec until_rec carry =
    (skip >> until_rec carry) <|>
    (let* b = stop in return (List.rev carry, b)) <|>
    (let* a = elem in until_rec (a::carry))
  in
  until_rec []

let rec skip_until (stop:('t, 'a)t) : ('t, 'a) t =
  fun s -> (stop <|> (any >> skip_until stop)) s

(* Section. Utils *)

let must ?(pre=(fun () -> ())) (fail:unit -> unit) (p:('a, 'b)t) : ('a, 'b) t =
  (fun stream -> (
    pre();
    match p stream with
    | Some some -> Some some
    | None -> fail(); None
  ))

let otherwise_failwith (msg:string) (p:('a, 'b)t) : ('a, 'b) t =
  must (fun() -> failwith msg) p

let post_process
    ?(parsed=(fun _ -> ()))
    ?(failed=(fun () -> ())) (p:('a, 'b)t) : ('a, 'b) t =
  (fun stream -> (
    match p stream with
    | Some some -> parsed (fst some); Some some
    | None -> failed (); None
  ))

let tuple (fst:('t, _)t) (sep:('t, _)t) (lst:('t, _)t) (elem:('t, 'a)t) : ('t, 'a list) t =
  let rec tuple_rec (carry:'a list) : ('t, 'a list) t =
    (* print_endline "[GuaCaml.Opal.tuple_rec] tick!"; *)
    (lst => (fun _ -> List.rev carry)) <|>
    (
      let* _ = sep in
      let* head = elem in
      tuple_rec (head::carry)
    )
  in
  fst >> (
    (lst >> return []) <|>
    (let* head = elem in tuple_rec [head])
  )

let rec stream_of_parser (p:('a, 'b)t) (s:'a stream) : 'b stream =
  (* print_endline "[GuaCaml.Opal.stream_of_parser] tick!"; *)
  match p s with
  | Some (b, s) -> Cons(b, lazy(stream_of_parser p s))
  | None        -> Nil

(* Section. Singletons *)

let exactly x = satisfy ((=) x)
let one_of  l = satisfy (fun x -> List.mem x l)
let none_of l = satisfy (fun x -> not (List.mem x l))
let range l r = satisfy (fun x -> l <= x && x <= r)

(* Section. Parser Of Character Stream *)

module CharStream =
struct
  (* Section. Char Parsers *)
  let space     = one_of [' '; '\t'; '\r']
  let spaces    = skip_many space
  let white     = one_of [' '; '\t'; '\r'; '\n']
  let whites    = skip_many white
  let newline   = exactly '\n'
  let tab       = exactly '\t'
  let upper     = range 'A' 'Z'
  let lower     = range 'a' 'z'
  let digit     = range '0' '9'
  let letter    = lower  <|> upper
  let alpha_num = letter <|> digit
  let hex_digit = range 'a' 'f' <|> range 'A' 'F' <|> digit
  let oct_digit = range '0' '7'

  let word = until1 alpha_num => implode

  let alpha_num_ = alpha_num <|> exactly '_'

  let word_ = until1 alpha_num_ => implode

  let alpha_ = letter <|> exactly '_'

  let ocaml_ident_char = alpha_num_ <|> exactly '\''

  let ocaml_ident = until1 ocaml_ident_char => implode

  (* Section. Lexer *)
  let lexer : (char, string) t = word <|> (any => Tools.string_of_char)
  let lexer_ : (char, string) t = word_ <|> (any => Tools.string_of_char)

  let lexer_spaces_ : (char, string) t =
    (space >> return " ") <|>
    lexer_

  (* Section. Basic Types *)
  (* [FIXME] deal with the 'too many digit' case *)
  let nat : (char, int) t =
    many1 digit =>
      (implode %
        (fun s ->
          try int_of_string s
          with Failure _ ->
            failwith ("[GuaCaml.Opal.nat] s:"^(STools.ToS.string s))
        )
      )

  (* [TODO] *)
  let int : (char, int) t =
    let* sign : char option = option (exactly '-') in
    let* value : int = nat in
    let r : int = if Tools.isSome sign then -value else value in
    return r

  (* [TODO] *)
  (* [FIXME] *)
  let string : (char, string) t =
    let* _ = exactly '"' in
    let* (cl, _) : char list * char = until_stop any (exactly '"') in
    return (implode cl)

  (* [TODO] *)
  (* [FIXME] *)
  let char : (char, char) t =
    between (exactly '\'') (exactly '\'') any

  let gen_tuple_coma_bracket ?(left='(') ?(sep=',') ?(right=')') (p:(char, 'a)t) : (char, 'a list) t =
    tuple (exactly left) (exactly sep) (exactly right) p

  let tuple_coma_bracket (p:(char, 'a)t) : (char, 'a list) t =
    gen_tuple_coma_bracket ~left:'(' ~sep:',' ~right:')' p

  (* Section. Lex Helper *)

  let lexeme x = whites >> x

  let token s =
    let rec loop s i =
      if i >= String.length s
      then return s
      else exactly s.[i] >> loop s (i + 1)
    in
    lexeme (loop s 0)

  let skip_until_newline : (char, bool) t =
    skip_until ((exactly '\n' >> return false) <|> (eof true))

  let tee (output:char -> unit) (s:char stream) : char stream =
    stream_of_parser (any => (fun c -> output c; c)) s

  let ctee ?(cond=true) output stream =
    if cond then tee output stream else stream

  (* [BUGGY] [TODO : fix recusion strategy
  let parser_composition (lexer:('a, 'b)t) (parser:('b, 'c)t) : ('a, 'c)t =
    (fun (astream:'a stream) ->
      let bstream : 'b stream = stream_of_parser lexer astream in
      parser bstream)
  end
   *)

  let parse_from_string (parser:(char, 'a) t) (s:string) : 'a option =
    parse parser (LazyStream.of_string s)

  let parse_string_as_char_stream (p:(char, 'a)t) : (string, 'a)t =
    let* word = any in
    match parse_from_string p word with
    | Some a -> return a
    | None   -> mzero

  let recognize_string (p:(char, 'a)t) (s:string) : bool =
    s |> parse_from_string p |> Tools.isSome
end

module StringStream =
struct
  let gen_tuple_coma_bracket ?(left="(") ?(sep=",") ?(right=")") (p:(string, 'a)t) : (string, 'a list) t =
    tuple (exactly left) (exactly sep) (exactly right) p

  let tuple_coma_bracket (p:(string, 'a)t) : (string, 'a list) t =
    gen_tuple_coma_bracket ~left:"(" ~sep:"," ~right:")" p

end
