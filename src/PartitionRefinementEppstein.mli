module Make (Ord : Set.OrderedType) :
sig
  module SET :
  sig
    type elt = Ord.t
    type t
    val empty : unit -> t
    val of_list : elt list -> t
    val id : t -> int
    val add : t -> elt -> unit
    val remove : t -> elt -> unit
    val iter : (elt -> unit) -> t -> unit
    val minus : t -> t -> unit
    val get_any : t -> elt
    val is_empty : t -> bool
    val to_seq : t -> elt Seq.t
    val to_list : t -> elt list
  end
  type elt = SET.elt
  type set = SET.t
  type t
  (* [make lst] the make function returns a fresh partition composed
   *   of a single set containing all elements in the list
   *)
  val make : elt list -> t
  val set_of_elt : t -> elt -> set
  val iter : (set -> unit) -> t -> unit
  val to_list : t -> set list
  val to_list_list : ?sort:bool -> t -> elt list list
  val length : t -> int
  val add : t -> elt -> set -> unit
  val remove : t -> elt -> unit

  (* Refine each set A in the partition to the two sets :
   * (A \inter S), (A \minus S).
   * Return a list of pairs (A \inter S, A \minus S) for each changed set.
   * Within each pair, A \inter S will be a newly created set,
   * while (A \minus S) will be a modified version of an existing set in
   * the partition.
   *)
  val refine : t -> elt list -> (set * set) list
end

module type MSig =
sig
  include Set.OrderedType

  val to_string : t -> string
end

module MakeTrace(Ord:MSig) :
sig
  module SET :
  sig
    type elt = Ord.t
    type t
    val empty : unit -> t
    val of_list : elt list -> t
    val id : t -> int
    val add : t -> elt -> unit
    val remove : t -> elt -> unit
    val iter : (elt -> unit) -> t -> unit
    val minus : t -> t -> unit
    val get_any : t -> elt
    val is_empty : t -> bool
    val to_seq : t -> elt Seq.t
    val to_list : t -> elt list
  end
  type elt = SET.elt
  type set = SET.t
  type t
  (* [make lst] the make function returns a fresh partition composed
   *   of a single set containing all elements in the list
   *)
  val make : elt list -> t
  val set_of_elt : t -> elt -> set
  val iter : (set -> unit) -> t -> unit
  val to_list : t -> set list
  val to_list_list : ?sort:bool -> t -> elt list list
  val length : t -> int
  val add : t -> elt -> set -> unit
  val remove : t -> elt -> unit

  (* Refine each set A in the partition to the two sets :
   * (A \inter S), (A \minus S).
   * Return a list of pairs (A \inter S, A \minus S) for each changed set.
   * Within each pair, A \inter S will be a newly created set,
   * while (A \minus S) will be a modified version of an existing set in
   * the partition.
   *)
  val refine : t -> elt list -> (set * set) list
end
