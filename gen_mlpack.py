import glob

def compute(path):
    assert(path!='')
    liste = path.split('/')
    name = liste[-1]
    name = name.replace('.ml', '')
    name = list(name)
    name[0] = name[0].upper()
    liste[-1] = ''.join(name)
    module = '/'.join(liste)
    return module

if __name__ == '__main__' :
    import sys
    target = sys.argv[1]
    channel = open(target, 'w')
    channel.write('\n'.join(compute(path) for path in glob.glob("src/*.ml"))+'\n')
    channel.close()

    

