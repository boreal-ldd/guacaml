# Name
FR : guacaml
EN : guacaml

# Long Name
FR : Boîte à Outils pour OCaml
EN : Generic Unspecific Algorithmic in OCaml

# Accroche
FR : Extension de la bibliothèque standard d'OCaml regroupant des fonctionnalités nativement implémentées dans d'autre langages.
EN : A simpl extension to the core standard library of OCaml gathering essential methods that are natively implemented in other languages.

# Description fonctionnelle

## FR
GuaCaml est une bibliothèque étendant la librairie standard par de multiples fonctionnalités pour la majeure partie disponible nativement dans d'autres langages.
Elle inclus notamment un ensemble de modules permettant l'export d'un type arbitraire vers des types spécifiques utilisés pour la communication, l'affichage ou le stockage.
Les différents modules sont en pure OCaml et ne nécessite donc pas la connaissance d'un langage tiers tel que C, facilitant grandement leur intégration et leur réutilisabilité.
L'implémentation de GuaCaml vise à trouver un compromis entre le temps de dévelopement, la généricité et les performances en temps et en mémoire. 

## EN
GuaCaml is a library extending the standard library of OCaml by various methods that are natively implemented in other languages.
In particular, it includes several facilities allowing for the conversion of arbitrary types into specific types used when exchanging, displaying of storing information.
All modules are implemented using pure OCaml, thus do not require knowledge of third party programming languages such as C.
The implementation aims at finding a compromise between the development effort, the genericity and performances, either computation time or memory consumption. 

# Mots-clés
