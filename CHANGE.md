# release-0.06

## 2023/01/15 (40HP) : Improved EnumPMC using Solomon's Theorem
- **DONE** improved EnumPMC2 using continuation and step-by-step description
- **TODO** turn the code into data-structure (like a list of some well-chosen type)
- **NEW** added EnumPMC3, similar to EnumPMC2 but based on [Solomon's Theorem](https://codimd.math.cnrs.fr/C5mysNg8R8q5JrMW7A0X-A#Theorem--Constraint-Free-H-vertex-Disjunction) which allows to contract any non-seed vertex, while keeping (most) clique separators at bay.
- **WIP** started IsPMCE, which aims at checking in poly-time if an instance (graph, seed) is PMC-extensible
- **WIP** started integration in an extra module EnumPMC4
- **NOTE** EnumPMC4 is not yet safe for use, please prefer EnumPMC3 for performances and reasonnable confidence.
- **NEW** added `GGLA.Utils.graph_is_biclique` to test if the graph induced by two given sets is a biclique
- **NEW** added `copy_set`, `copy_set_list`, `copy_multiset` to MyArray
- **DONE** refactored `ounit_GGLA_PMC` to simplify the representation of graphs, and turn unit-tests into cartesian products
- **NEW** added `[STools.Utils.input_string_of_file (file_name:string) = (content:string)]`

## 2023/01/08 (22HP) : `GGLA_HFT`-related improvements, added KIC, SeparativeClique, EnumPMC2
- **NEW** added serialization model-functor for DBBC
- **FIX** fixed serialization of `GGLA_HFT`
- **NEW** added `GGLA_HFT.autorename`
- **NEW** added `GGLA_KIC`
    + about 10% improvement for WAP-solver
    + allows implementing a naive version of SeparativeClique
- **DONE** moved `IsPMC.connected_components_exclusion` to `GGLA.AdvancedConnected.connected_components_and_separators`
- **NEW** `EnumPMC2` based on K-vertex extension or contraction instead of H-vertex extension or locking
    + about 100x faster on used to be couple minutes examples
    + adding separative-clique to the inner normalization process hinder performances (about 10x) hence disabled
- **NEW** small set of example graphs, used for unit tests
- **NEW** `|>|` operator in `Extra`
- **NEW** new methods in `MyArray` : `rev_iter(i)`, `rev_map(i)`, `opmap(i)`, `list_of_indexes_false`
- **FIX** added unit tests for `SeparativeClique`, `UnionFind`
- **NEW** added leaf/node counting functions for `Tree.tree` and `Tree.atree`
- **FIX** fixed `UnionFind`

## 2022/12/26 (45HP) : adding LexBFS
- **NEW** `LexBFS` (as `GGLA_LexBFS`), `PartitionRefinement`, `DoubleLinkedList`, `CircularDoubleLinkedList` : implementation, unit tests, debuged
- **NEW** `HashSet` module to `CircularDoubleLinkedList` to implement ordered `HashSet` with element insertion in O(1)
- **NEW** `HashKeySet` module, similar to `HashSet` but allows to hash an indentifier instead of the element itself
- **NEW** a `MakeTrace` to `PartitionRefinement` to trace calls
- **NEW** DOT language (i.e. Graphviz)
- **NEW** Dijktra's algorithm for shortest Path
- **NEW** ounit_ from src/ to ounits/

# release-0.05

### 2022/06/14 (10HP)
- **NEW** graphviz export from `GGLA_FT`
- **NEW** export method to `Lang_PACE2017` from `GGLA_FT`
- **NEW** `GGLA_LexBFS` (draft)
- **NEW** `Assoc.sort`

### 2022/06/06 (12 HP)
- **NEW** `CoGraph` module containing
  - `SymBag` Symbolic Bag (similar to sets except that the same object can be present several times) _[MOVEME]_
  - `SymBagTree` Symbolic Bag Tree a tree of Bag containing Bag containing ... _[MOVEME]_
  - `Explicit` every vertex are represented individually
  - `Implicit` (used with unit descriptor) indistinguishable vertices are merged
- **NEW** `GGLA_CFT` module Co-Graph Module Factorization In Graph
  - _Pretty_ export to Graphviz
- **NEW** `MyList.mapi` and `MyList.rev_mapi`
- **NEW** `Tree.ToShiftS`
- **NEW** `Tree.flatten` single-pass tail-recursive
- **NEW** `Bicimal.ToS` and `Bicimal.ToShiftS`

## 2022/06/05 (3DP) : Standardizing GGLA's modules GraphHFT and `Lang_PACE_2017_GGLA`
- **FIX** renamed `GraphHFT` into `GGLA_HFT`
- **FIX** extracted `GGLA_FT` from `Lang_PACE_2017_GGLA`
- **FIX** renamed `alive` field into `fixed` in `GGLA_HFT`
- **FIX** added field `useful` field in `GGLA_HFT`
- **NEW** added conversion from `GGLA_FT` to `GGLA_HFT`
- **NEW** added conversion from `GraphKD` to `GGLA_HFT`
- **NEW** added `GraphKD.support`
- **FIX** replaced `Stdlib.(@)` (not tail-recursive) by `Extra.(@>)` (tail-recursive but two passes on the first argument)
  + raised major performance issue within some tests with DAGaml
- **NEW** added simple text-based interface to `GraphKD` and extended it with parameter variables
- **FIX** fixed `BranchAndBound` file
- **FIX** fixed several methods to convert `GGLA` to `Graphviz` 

## 2022/06/03 (11HP) : Interfacing with PACE 2017 language
- **NEW** parsing PACE 2017 input
- **NEW** pretty-printing PACE 2017 input
- **NEW** parsing PACE 2017 output
- **NEW** pretty-printing PACE 2017 output 

## 2022/06/01 (11DP) : Meta Generation of OCaml Code for GuaCaml, parser combinator and C parser
- **NEW** extended STools with ShiftS and ToShiftS
- **NEW** Opal-based parser combinator
  - some lexers
  - generic parser of parenthesis (or any matching pairs of tokens)
  - derived tree-parser
- **NEW** simple (not complete) OCaml's type parser
  - meta-generation of OCaml code
    - ToS 
    - ToShiftS
- **NEW** simple (almost complete) C parser
  - does not deal with integer extension (e.g. `32ul`)
  - float parsing not checked
  - deals with place-specific pragma of the form `_Pragma( "some string" )`
- **NEW** basic cnf DIMACS parser

## 2022/05/11 : Added `Assoc.sort`
- **NEW** `Assoc.sort`
- **FIX** fixed title levels in 'release-0.04' CHANGE.md

# release-0.04

## 2022/04/26 : Added Raw Annotation String Tree representation and Linear-Time conversion to String
- **NEW** ASTree-based modules => ASTreeUtils

## 2022/04/26 : Added `filling_partial_order` to `Assoc`
- **NEW** added `filling_partial_order` function to `Assoc` file / module

## 2022/04/26 : Added sparse tabular representation (@author Benoit Caillaud)
- **NEW** added files / modules `FlexArray`, `SparseVector` and `SparseMatrix`
- **NEW** added files : AUTHORS and CONTRIBUTORS
- **TODO** add unit test for each modules
- **TODO** add documentation for each modules

## 2022/04/19 : Added Graphviz output to GGLA representation
- **NEW** `GGLA_Graphviz` file / module
- **FIX** fixed deprecated `Stream` module from standard library by integrating one into GuaCaml

## 2022/03/15 : Added ALInt (Arbitrary Length Integer)
- **NEW** added ALInt module for unsigned and signed arbitrary length integer
- **NEW** added `ounit_ALInt` including relevant unit tests
- **FIX** fixed display of Bicimal's OUnit

## 2022/03/15 : Factored `Bool.to_int` and renamed `pop` into `cnt` in BChar and BArray
- replaced `if ?X then 1 else 0` by `Bool.to_int ?X`
- renamed `pop` by `cnt` in `BTools_BChar` and `BTools_BArray`
- added an exhaustive unit test for `BTools_BChar.proto_cnt` in `ounit_BTools_BChar`

## 2021/11/20 : Added Naive MinMax Algorithm (@author Santiago Bautista)
- **NEW** added Naive MinMax algorithm (file `minmax.ml`)

# release-0.03

## 2021/10/05 : Added Signed Int to ToB and OfB (in BTools)

## 2021/09/28 : Boolean RLE
- **NEW** BRLE module (Boolean Run Length Encoding)
- **NEW** added marginal ounit testing

## 2021/04/02 : Single-Hash (hash)Table

## 2021/03/12 : DBBC-related improvements
- **NEW** added auto-profiling functor
- **FIX** fixed Sum snode in DBBC

## 2021/03/09 : wap-related improvements
- **FIX** fixed some bugs in DBBC
- **FIX** set name convention related to 'neighborhood' vs 'neighbors' to 'neighbors'
- **NEW** added `is_clique` to GGLA

## 2021/03/02 : added wap-specific methods and functions
- **NEW** Branch And Bound
- **FIX** DBBC
- **NEW** GGLA clique detection module
- **NEW** GGLA domination
- **NEW** GGLA utils
- **NEW** GGLA vertex separators
- **WIP** GGLA (various work in progress)
- **DEL** Graph Iso Adj
- **DEL** Graph WLA

## 2021/02/27 : implementing RLE

## 2020/11/02 : fixed CHANGE with respect to release-0.02

# release-0.02

## 2020/11/02 : reformatting assertion as named failwith

## 2020/11/01 : added `Tools.opopmax`

## 2020/10/31 : added `OUnit.{print{,_endline}}`

## 2020/10/31 : added a `__check_reverse__` mechanism in `MemoBTable`

## 2020/10/24 : added `Tools.opiter` to `tools.ml`

## 2020/10/13 : added `print_error_endline` to OUnit, imported back some OUnit tests (+ `ounits_bin.sh` and `make ounits`)

## 2020/10/15 : minor improvement in MemoTable.apply

## 2020/10/14 : added Tools.map2, linked MyList.{map, map2} to Tools.{map, map2}

## 2020/10/11 : fixed Assoc.restr

## 2020/10/11 : fixed `wap_rbtf.BPP` (s/supp3/supp2) + removed unnecessary tests

## 2020/10/10 : added Assoc module : manipulation of sorted association list

## 2020/10/09 : added `roman_to_string` in STools.SUtils

## 2020/10/07 : added `apply_haltf3` to `wap_rbtf`

## 2020/10/07 : added filter{A,B} (resp. {GLeaf,GLink}) to AB (resp. TreeUtils)

## 2020/10/06 : fixed error in `wap_exchange_utils.update_tree`, issue with parameter variables

## 2020/10/05 : fixed error in `GGLA.Utils.internal_vertex_edges_rename` (v.index renaming)

## 2020/10/05 : fixed error in SetList.minus

## 2020/10/05 : added ounit\_setlist

## 2020/10/05 : added rev\_flatten to MyList.mli (already in .ml)

## 2020/10/05 : added inclusion test in wap\_exchange\_utils.{check\_input,assert\_input}

## 2020/10/05 : added WAP exchange interface [REMOVED]
-	`wap_exchange.ml`
-	`wap_exchange_utils.ml`
-	`wap_greedy.ml`
-	`wap_rbtf.ml`
-	`wap_rbtf.mli`

## 2020/10/05 : added generic graph modules

# release-0.01
FIXME : many things
