(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_OCaml_type_Opal_OUnit : Bare Syntax C Parser (written in Opal)
 *
 * === NOTES ===
 *
 * operator priority is not respected, see : https://fr.wikibooks.org/wiki/Programmation_C-C%2B%2B/Priorit%C3%A9s_des_op%C3%A9rateurs
 *
 *)

module Types = Lang_OCaml_type_Types
module ToS = Lang_OCaml_type_ToS
(* module ToPrettyS = Lang_OCaml_type_ToPrettyS *)
open Types
open Opal
open Lang_OCaml_type_Opal

(* Seciton. Parser *)

(* <keyword> *)
let _ =
  print_newline();
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input = "type a = b" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.exactly_keyword "type" paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] (exactly_keyword \"type\"):"^(AtomicLexer.ToS.token output));
  ()

(* <type-name-def> *)
let _ =
  print_newline();
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input = " a = b" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.type_name_def paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] type_name_ref:"^(ToS.type_name_def output));
  ()

(* <type-name-ref> *)
let _ =
  print_newline();
  print_endline"<type-name-ref>";
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input = " a = b" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.type_name_ref paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] type_name_ref:"^(ToS.type_name_ref output));
  ()

(* <single-intanciation-type> *)
let _ =
  print_newline();
  print_endline"<single-intanciation-type>";
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input = "b" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.single_instanciation_type paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] type_name_ref:"^(ToS.single_instanciation_type output));
  ()

(* <product-type> *)
let _ =
  print_newline();
  print_endline"<product-type>";
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input = "b" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.product_type paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] type_name_ref:"^(ToS.product_type output));
  ()

(* <named-type-definition> *)
let _ =
  print_newline();
  print_endline"<named-type-definition>";
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input = "b" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.named_type_definition paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] type_name_ref:"^(ToS.named_type_definition output));
  ()

(* <named-type-rec> *)

let _ =
  print_newline();
  print_endline"<named-type-rec>";
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input = "type a = b" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.named_type_rec paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] named_type:"^(ToS.named_type_rec output));
  ()

let _ =
  print_newline();
  print_endline"<named-type-rec>";
  print_endline "[GuaCaml.Lang_OCaml_type_Opal._] {step:0}";
  let string_input =
"(* FOR-LOOP *)
type ('e, 'b) t_for_loop = {
  tfl_fst : 'e;
  tfl_mid : 'e;
  tfl_lst : 'e;
  tfl_body : 'b;
}" in
  let stream = LazyStream.of_string string_input in
  let lexed_stream = stream_of_parser AtomicLexer.lexer stream in
  let paralexed_stream = stream_of_parser ParaLex_NoCom.lexer lexed_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = Parser.named_type_rec paralexed_list |> Tools.unop |> fst in
  print_endline("[GuaCaml.Lang_OCaml_type_Opal._] named_type:"^(ToS.named_type_rec output));
  ()
