(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_DIMACS_Opal_OUnit : Bare Syntax DIMACS Parser (written in Opal)
 *
 *)

open Lang_DIMACS_Core
open Types
open Utils
open Opal
open CharStream
open Lang_DIMACS_Opal

let _ =
  print_endline "[GuaCaml.Lang_DIMACS_Opal_OUnit._] {step:0}";
  let file_name = Sys.argv.(1) in
  print_endline "[GuaCaml.Lang_DIMACS_Opal_OUnit._] {step:1}";
  let d : dimacs = from_file file_name in
  print_endline "[GuaCaml.Lang_DIMACS_Opal_OUnit._] {step:2}";
  print_endline (ToS.dimacs d);
  print_endline "[GuaCaml.Lang_DIMACS_Opal_OUnit._] {step:3}";
  print_endline (ToPrettyS.dimacs d);
  print_endline "[GuaCaml.Lang_DIMACS_Opal_OUnit._] {step:4}";
  exit 0
