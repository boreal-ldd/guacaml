(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *)

open OUnit
open ALInt
open IterExtra
open IterTools

module Unsigned =
struct
  open Unsigned

  let gen_fulladd2 i1 i2 () =
    let sum_ref = (Bool.to_int i1) + (Bool.to_int i2) in
    let (s, c) = fulladd2 i1 i2 in
    let sum = (Bool.to_int s) + 2*(Bool.to_int c) in
    error_assert "sum = sum_ref" (sum = sum_ref);
    ()

  let gen_fulladd3 i1 i2 i3 () =
    let sum_ref = (Bool.to_int i1) + (Bool.to_int i2) + (Bool.to_int i3) in
    let (s, c) = fulladd3 i1 i2 i3 in
    let sum = (Bool.to_int s) + 2*(Bool.to_int c) in
    error_assert "sum = sum_ref" (sum = sum_ref);
    ()

  let _ =
    print_endline "generating 'Unsigned.test_fulladd2' ...";
    Iter.iter (fun (i1, i2) ->
      let name : string = "Unsigned.test_fulladd2["^(string_of_2_bool (i1, i2))^"]" in
      push_test name (gen_fulladd2 i1 i2)
    ) iter_2_bool

  let _ =
    print_endline "generating 'Unsigned.test_fulladd3' ...";
    Iter.iter (fun (i1, i2, i3) ->
      let name : string = "Unsigned.test_fulladd3["^(string_of_3_bool (i1, i2, i3))^"]" in
      push_test name (gen_fulladd3 i1 i2 i3)
    ) iter_3_bool

  let gen_of_nat_to_nat (n:int) () =
    let n' = to_nat (of_nat n) in
    error_assert "n' = n" (n' = n);
    ()

  let _ =
    print_endline "generating 'Unsigned.test_of_nat_to_nat' ...";
    Iter.iter (fun i ->
      let name = "Unsigned.test_of_nat_to_nat["^(OUnit.ToS.int i)^"]" in
      push_test name (gen_of_nat_to_nat i)
    ) (Iter.range 0 (1 lsl 10))

  let gen_get (n:int) (k:int) : unit -> unit =
    assert(0 <= n);
    assert(0 <= k);
    assert(k < 63);
    (fun () -> (
      let g_ref = 0 <> (n land (1 lsl k)) in
      let t = of_nat n in
      let g = get ~default:false t k in
      error_assert "g = g_ref" (g = g_ref);
      ()
    ))

  let _ =
    print_endline "generating 'Unsigned.test_get' ...";
    Iter.iter (fun (n, k) ->
      let name : string = "Unsigned.test_get["^(OUnit.ToS.(pair int int) (n, k))^"]" in
      push_test name (gen_get n k)
    ) ((Iter.range 0 (1 lsl 8)) $* (Iter.range 0 10))

  let gen_plus n1 n2 : unit -> unit =
    assert(0 <= n1);
    assert(0 <= n2);
    assert(0 <= n1 + n2);
    (fun () -> (
      let t1 = of_nat n1 in
      let t2 = of_nat n2 in
      let t12 = t1 +/ t2 in
      let n12 = to_nat t12 in
      error_assert "n12 = n1 + n2" (n12 = n1 + n2);
      ()
    ))

  let _ =
    print_endline "generating 'Unsigned.test_plus_00' ...";
    Iter.iter (fun (n1, n2) ->
      let name : string = "Unsigned.test_plus_00["^(OUnit.ToS.(pair int int) (n1, n2))^"]" in
      push_test name (gen_plus n1 n2)
    ) ((Iter.range 0 (1 lsl 5)) $* (Iter.range 0 (1 lsl 5)))

  let _ =
    print_endline "generating 'Unsigned.test_plus_01' ...";
    Iter.iter (fun (n1, n2) ->
      let name : string = "Unsigned.test_plus_01["^(OUnit.ToS.(pair int int) (n1, n2))^"]" in
      push_test name (gen_plus n1 n2)
    ) ((gen_2_bits 61) $* (gen_2_bits 61))

  let _ =
    print_endline "generating 'Unsigned.test_plus_02' ...";
    Iter.iter (fun (n1, n2) ->
      let name : string = "Unsigned.test_plus_02["^(OUnit.ToS.(pair int int) (n1, n2))^"]" in
      push_test name (gen_plus n1 n2)
    ) ((gen_3_bits 32) $* (gen_3_bits 32))
end

module Signed =
struct
  open Signed

  let gen_signed_fulladd3 c1 s1 s2 () =
    let sum_ref = (Bool.to_int c1) - (Bool.to_int s1) - (Bool.to_int s2) in
    let (c, s) = signed_fulladd3 c1 s1 s2 in
    let sum = (Bool.to_int c) - 2*(Bool.to_int s) in
    error_assert "sum = sum_ref" (sum = sum_ref);
    ()

  let _ =
    print_endline "generating 'Signed.test_signed_fulladd3' ...";
    Iter.iter (fun (i1, i2, i3) ->
      let name : string = "Signed.test_signed_fulladd3["^(string_of_3_bool (i1, i2, i3))^"]" in
      push_test name (gen_signed_fulladd3 i1 i2 i3)
    ) iter_3_bool

  let gen_of_nat_to_nat (n:int) () =
    let n' = to_nat (of_nat n) in
    error_assert "n' = n" (n' = n);
    ()

  let _ =
    print_endline "generating 'Signed.test_of_nat_to_nat' ...";
    Iter.iter (fun i ->
      let name = "Signed.test_of_nat_to_nat["^(OUnit.ToS.int i)^"]" in
      push_test name (gen_of_nat_to_nat i)
    ) (Iter.range 0 (1 lsl 10))

  let gen_of_int_to_int (n:int) () =
    let n' = to_int (of_int n) in
    error_assert "n' = n" (n' = n);
    ()

  let _ =
    print_endline "generating 'Signed.test_of_int_to_int' ...";
    Iter.iter (fun i ->
      let name = "Signed.test_of_int_to_int["^(OUnit.ToS.int i)^"]" in
      push_test name (gen_of_int_to_int i)
    ) (Iter.range (-(1 lsl 9)) (1 lsl 9))

  let gen_get (n:int) (k:int) : unit -> unit =
    assert(0 <= k);
    assert(k < 63);
    (fun () -> (
      let g_ref = 0 <> (n land (1 lsl k)) in
      let t = of_int n in
      let g = get t k in
      error_assert "g = g_ref" (g = g_ref);
      ()
    ))

  let _ =
    print_endline "generating 'Signed.test_get' ...";
    Iter.iter (fun (n, k) ->
      let name : string = "Signed.test_get["^(OUnit.ToS.(pair int int) (n, k))^"]" in
      push_test name (gen_get n k)
    ) ((Iter.range (-(1 lsl 7)) (1 lsl 7)) $* (Iter.range 0 10))

  let gen_plus n1 n2 : unit -> unit =
    (fun () -> (
      let t1 = of_int n1 in
      let t2 = of_int n2 in
      let t12 = t1 +/ t2 in
      let n12 = to_int t12 in
      error_assert "n12 = n1 + n2" (n12 = n1 + n2);
      ()
    ))

  let _ =
    print_endline "generating 'Signed.test_plus_00' ...";
    Iter.iter (fun (n1, n2) ->
      let name : string = "Signed.test_plus_00["^(OUnit.ToS.(pair int int) (n1, n2))^"]" in
      push_test name (gen_plus n1 n2)
    ) ((Iter.range (-(1 lsl 4)) (1 lsl 4)) $* (Iter.range (-(1 lsl 4)) (1 lsl 4)))

  let _ =
    print_endline "generating 'Signed.test_plus_01' ...";
    Iter.iter (fun (n1, n2) ->
      let name : string = "Signed.test_plus_01["^(OUnit.ToS.(pair int int) (n1, n2))^"]" in
      push_test name (gen_plus n1 n2)
    ) ((gen_signed_2_bits 61) $* (gen_signed_2_bits 61))

  let _ =
    print_endline "generating 'Signed.test_plus_02' ...";
    Iter.iter (fun (n1, n2) ->
      let name : string = "Signed.test_plus_02["^(OUnit.ToS.(pair int int) (n1, n2))^"]" in
      push_test name (gen_plus n1 n2)
    ) ((gen_signed_3_bits 16) $* (gen_signed_3_bits 16))

end

let _ = run_tests()
