(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Opal : Monadic Parser Combinators for OCaml
 *
 * === NOTE ===
 *
 * The following tests are not part of the original Opal implementation.
 *
 *)

open Opal

let _ =
  print_newline();
  print_endline "[GuaCaml.Lang_C_Opal._] {step:0}";
  let liste_input = ["a"; "b"; "c"; "d"] in
  let stream = LazyStream.of_list liste_input in
  let liste_output = LazyStream.to_list stream in
  assert(liste_input = liste_output);
  print_endline "[GuaCaml.Lang_C_Opal._] {step:1}";
  print_endline "!test1";
  ()

let _ =
  print_newline();
  print_endline "[GuaCaml.Lang_C_Opal._] {step:0}";
  let string_input = "abcd" in
  let stream = LazyStream.of_string string_input in
  let liste_output = LazyStream.to_list stream in
  assert(string_input = (liste_output |> implode));
  print_endline "[GuaCaml.Lang_C_Opal._] {step:1}";
  print_endline "!test2";
  ()


let _ =
  print_newline();
  print_endline "[GuaCaml.Lang_C_Opal._] {step:0}";
  let string_input = " bob a manger du pain" in
  let stream = LazyStream.of_string string_input in
  let word_stream = stream_of_parser CharStream.lexer stream in
  let liste_output = LazyStream.to_list word_stream in
  assert(liste_output = [" "; "bob"; " "; "a"; " "; "manger"; " "; "du"; " "; "pain"]);
  assert(string_input = String.concat "" liste_output);
  print_endline "[GuaCaml.Lang_C_Opal._] {step:1}";
  print_endline "!test3";
  ()

let _ =
  print_newline();
  print_endline "[GuaCaml.Lang_C_Opal._] {step:0}";
  let string_input = "(a,b,cool)" in
  let stream = LazyStream.of_string string_input in
  let word_stream = stream_of_parser CharStream.lexer stream in
  let parsed_stream = stream_of_parser (StringStream.tuple_coma_bracket any) word_stream in
  let liste_output = LazyStream.to_list parsed_stream in
  let liste_output_ref = [["a"; "b"; "cool"]] in
  if not (liste_output = liste_output_ref)
  then (
    print_endline "[GuaCaml.Lang_C_Opal_OUnit.test10] failure";
    print_endline ("[GuaCaml.Lang_C_Opal_OUnit.test10] liste_output_ref:"^(STools.ToS.(list(list string)) liste_output_ref));
    print_endline ("[GuaCaml.Lang_C_Opal_OUnit.test10] liste_output    :"^(STools.ToS.(list(list string)) liste_output));

    failwith "[GuaCaml.Lang_C_Opal_OUnit.test10]"
  );
  print_endline "[GuaCaml.Lang_C_Opal._] {step:1}";
  print_endline "!test10";
  ()

let _ =
  print_newline();
  print_endline "[GuaCaml.Lang_C_Opal._] {step:0}";
  let string_input = "11" in
  let stream = LazyStream.of_string string_input in
  let parsed_stream = stream_of_parser CharStream.int stream in
  let liste_output = LazyStream.to_list parsed_stream in
  let liste_output_ref = [11] in
  if not (liste_output = liste_output_ref)
  then (
    print_endline "[GuaCaml.Lang_C_Opal_OUnit.test11] failure";
    print_endline ("[GuaCaml.Lang_C_Opal_OUnit.test11] liste_output_ref:"^(STools.ToS.(list int) liste_output_ref));
    print_endline ("[GuaCaml.Lang_C_Opal_OUnit.test11] liste_output    :"^(STools.ToS.(list int) liste_output));

    failwith "[GuaCaml.Lang_C_Opal_OUnit.test11]"
  );
  print_endline "[GuaCaml.Lang_C_Opal._] {step:1}";
  print_endline "!test11";
  ()
