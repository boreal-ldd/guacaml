(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_GraphKD_Opal_OUnit : Bare Syntax DIMACS Parser (written in Opal)
 *
 *)

open Lang_GraphKD_Core
open Types
open Utils
open Opal
open CharStream
open Lang_GraphKD_Opal

let _ =
  print_endline "[GuaCaml.Lang_GraphKD_Opal_OUnit._] {step:0}";
  let file_name = Sys.argv.(1) in
  print_endline "[GuaCaml.Lang_GraphKD_Opal_OUnit._] {step:1}";
  let d : graphkd = from_file file_name in
  print_endline "[GuaCaml.Lang_GraphKD_Opal_OUnit._] {step:2}";
  print_endline (ToS.graphkd d);
  print_endline "[GuaCaml.Lang_GraphKD_Opal_OUnit._] {step:3}";
  print_endline (ToPrettyS.graphkd d);
  print_endline "[GuaCaml.Lang_GraphKD_Opal_OUnit._] {step:4}";
  exit 0

let test0 =
"p kd 10 2
1 2 3 4"

let test1 =
"p kd 10 2
f 1 9
1 2 3 4
4 5 6 7"

let test2 =
"p kd 10 1
2 4 6 8 10"
