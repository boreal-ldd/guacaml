(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_GraphKD_Utils_OUnit : Bare Syntax DIMACS Parser (written in Opal)
 *
 *)

open Lang_GraphKD_Core
open Types
open Utils
open Opal
open CharStream
open Lang_GraphKD_Opal
open Lang_GraphKD_Utils

let cmd_KD (args:string list) : unit =
  let print_endline text =
    print_endline ("[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_KD] "^text)
  in
  match args with
  | [] -> failwith "[.cmd_KD] no input file name"
  | input_file_name :: args -> (
    print_endline "[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_KD] {step:1}";
    let (mode, g) : string * GraphKD.graph = of_file input_file_name in
    print_endline "[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_KD] {step:2}";
    print_endline (GraphKD.ToS.graph g);
    print_endline "[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_KD] {step:3}";
    print_endline (GraphKD.ToShiftS.graph g (Some 0));
    print_endline "[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_KD] {step:4}";
    match args with
    | [] -> (
      print_endline "[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_KD] no output file provided";
    )
    | output_file_name :: args -> (
      to_file output_file_name ~mode g;
      print_endline "[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_KD] {step:5}";
    )
  )

let cmd_FT (args:string list) : unit =
  let print_endline text =
    print_endline ("[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_FT] "^text)
  in
  match args with
  | [] -> failwith "[.cmd_FT] no input file name"
  | input_file_name :: args -> (
    print_endline " {step:1}";
    let (mode, g) : string * GraphKD.graph = of_file input_file_name in
    print_endline " {step:2}";
    print_endline (GraphKD.ToS.graph g);
    print_endline " {step:3}";
    print_endline (GraphKD.ToShiftS.graph g (Some 0));
    print_endline " {step:4}";
    let ft : GGLA_FT.Type.hg = GGLA_FT.of_GraphKD g in
    print_endline " {step:5}";
    print_endline (GGLA_FT.ToS.hg ft);
    print_endline " {step:6}";
    print_endline (GGLA_FT.ToShiftS.hg ~inline:true ft (Some 0));
    print_endline " {step:7}";
  )

let cmd_HFT (args:string list) : unit =
  let print_endline text =
    print_endline ("[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_HFT] "^text)
  in
  match args with
  | [] -> failwith "[.cmd_HFT] no input file name"
  | input_file_name :: args -> (
    print_endline " {step:1}";
    let (mode, kd) : string * GraphKD.graph = of_file input_file_name in
    print_endline " {step:2}";
    print_endline (GraphKD.ToS.graph kd);
    print_endline " {step:3}";
    print_endline (GraphKD.ToShiftS.graph kd (Some 0));
    print_endline " {step:4}";
    let hft = GGLA_HFT.of_GraphKD ~remove_useless:true kd in
    print_endline " {step:5}";
    print_endline (GGLA_HFT.ToS.hg hft);
    print_endline " {step:6}";
    print_endline (GGLA_HFT.ToShiftS.hg ~inline:true hft (Some 0));
    print_endline " {step:7}";
    match args with
    | [] -> (
      print_endline " no output file provided";
    )
    | output_file_name :: args -> (
      GGLA_HFT.to_graphviz_file ~name:"HFT" output_file_name hft;
      print_endline " {step:8}";
    )
  )

let cmd_CFT (args:string list) : unit =
  let print_endline text =
    print_endline ("[GuaCaml.Lang_GraphKD_Utils_OUnit.cmd_CFT] "^text)
  in
  match args with
  | [] -> failwith "[.cmd_FT] no input file name"
  | input_file_name :: args -> (
    print_endline " {step:1}";
    let (mode, kd) : string * GraphKD.graph = of_file input_file_name in
    print_endline " {step:2}";
    print_endline (GraphKD.ToS.graph kd);
    print_endline " {step:3}";
    print_endline (GraphKD.ToShiftS.graph kd (Some 0));
    print_endline " {step:4}";
    let hft = GGLA_HFT.of_GraphKD ~remove_useless:true kd in
    print_endline " {step:5}";
    print_endline (GGLA_HFT.ToS.hg hft);
    print_endline " {step:6}";
    print_endline (GGLA_HFT.ToShiftS.hg ~inline:true hft (Some 0));
    print_endline " {step:7}";
    let cft = GGLA_CFT.of_HFT hft in
    print_endline " {step:8}";
    print_endline (GGLA_CFT.ToS.hg cft);
    print_endline " {step:9}";
    print_endline (GGLA_CFT.ToShiftS.hg ~inline:true cft (Some 0));
    print_endline " {step:A}";
    match args with
    | [] -> (
      print_endline " no output file provided";
    )
    | output_file_name :: args -> (
      GGLA_CFT.to_graphviz_file ~name:"CFT" output_file_name cft;
      print_endline " {step:B}";
    )
  )

let _ =
  let args : string list = Array.to_list Sys.argv |> List.tl in
  match args with
  | [] -> failwith "no provided mode"
  | mode::args -> (
    match mode with
    | "KD" -> cmd_KD args
    | "FT" -> cmd_FT args
    | "HFT" -> cmd_HFT args
    | "CFT" -> cmd_CFT args
    | _ -> failwith "not a valid mode"
  )
