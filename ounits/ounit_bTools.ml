(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *)

open OUnit
open BTools

let test_sum_list_00 () =
  let len = 3 in
  let il = [1; 2] in
  let s = ToB.sum_list ~min:1 len il [] in
  let il', s' = OfB.sum_list ~min:1 len s in
  error_assert "il' = il" (il' = il);
  error_assert "s' = []" (s' = []);
  ()

let _ = push_test
  "test_sum_list_00"
   test_sum_list_00

let test_sum_list_01 () =
  let len = 3 in
  let il = [3] in
  let s = ToB.sum_list ~min:1 len il [] in
  let il', s' = OfB.sum_list ~min:1 len s in
  error_assert "il' = il" (il' = il);
  error_assert "s' = []" (s' = []);
  ()

let _ = push_test
  "test_sum_list_01"
   test_sum_list_01

let test_sum_list_02 () =
  let len = 6 in
  let il = [2; 1; 2; 1] in
  let s = ToB.sum_list ~min:1 len il [] in
  let il', s' = OfB.sum_list ~min:1 len s in
  error_assert "il' = il" (il' = il);
  error_assert "s' = []" (s' = []);
  ()

let _ = push_test
  "test_sum_list_02"
   test_sum_list_02

let _ = run_tests()
