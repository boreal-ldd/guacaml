open OUnit
open BTools_BChar

let gen_proto_cnt (c:char) () =
  let cnt_ref = MyList.count_true (to_bool_list c) in
  let cnt = proto_cnt (Char.code c) in
  error_assert "cnt = cnt_ref" (cnt = cnt_ref);
  ()

let _ = Iter.iter (fun c ->
    let name = "test_proto_cnt["^(OUnit.ToS.int(Char.code c))^"]" in
    push_test name (gen_proto_cnt c)
  ) Iter.gen_char

let _ = run_tests()
