(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_PACE2017_Opal_OUnit : Bare Syntax Parser for PACE 2017 Challenge (written in Opal)
 *
 *)

open Lang_PACE2017_Core
open Types
open Utils
open Opal
open CharStream
open Lang_PACE2017_Opal

let _ =
  print_endline "[GuaCaml.Lang_PACE2017_Opal_OUnit._] {step:0}";
  let file_name = Sys.argv.(1) in
  print_endline "[GuaCaml.Lang_PACE2017_Opal_OUnit._] {step:1}";
  let d : pace2017 = of_file ~sort:true file_name in
  print_endline "[GuaCaml.Lang_PACE2017_Opal_OUnit._] {step:2}";
  print_endline (ToS.pace2017 d);
  print_endline "[GuaCaml.Lang_PACE2017_Opal_OUnit._] {step:3}";
  print_endline (ToPrettyS.pace2017 d);
  print_endline "[GuaCaml.Lang_PACE2017_Opal_OUnit._] {step:4}";
  exit 0
