
(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *)

open OUnit
open FlexArray
open IterExtra
open IterTools

let self_test p q u n m b =
  let g = ref u in
  let next () = g := (p * (!g)) mod q in
  let a = Array.make q 0
  and a' = make b 0 in
  let check () =
    for i=0 to q-1 do
      if a.(i) <> get a' (Int64.of_int i) then failwith "Flexarray.self_test"
    done in
  check ();
  for i=1 to m do
    for i=1 to n do
      a.(!g) <- i;
      set a' (Int64.of_int (!g)) i;
      next ()
    done;
    check ()
  done
