(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_DOT_Opal : Basic Parser for the DOT language from Graphviz
 *
 * === TODO ===
 *
 * parsing HTML identifier
 *)

module Types = Lang_DOT_Types
module ToS = Lang_DOT_ToS
(* module ToPrettyS = Lang_DOT_ToPrettyS *)
open Types
open Opal
open Lang_DOT_Opal

(* Section. Parser *)
let _ = print_endline "# Section. Parser"

(* [MOVEME] *)
let gen_parser_printer
    (function_name:string)
    (comment:string)
    (input_string:string)
    (parser:'a Parser.t)
    (result_to_string:'a -> string) : unit =
  print_newline();
  let print_endline : string -> unit =
    let header = "["^function_name^"] "^comment^" : " in
    fun text -> print_endline(header^text)
  in
  print_endline ("input_string:"^(STools.ToS.string input_string));
  let char_stream = LazyStream.of_string input_string in
  let token_stream = stream_of_parser AtomicLexer.lexer char_stream in
  let paralexed_stream = stream_of_parser ParaLex.lexer token_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = parser paralexed_list |> Tools.opmap fst in
  print_endline "printing result";
  print_endline ("output:"^(STools.ToS.option result_to_string output));
  ()

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.graph"
    "{test:0}"
    "a"
  Parser.id
  Lang_DOT_ToS.id

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.graph"
    "{test:0}"
    "\"a\""
  Parser.id
  Lang_DOT_ToS.id

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.graph"
    "{test:0}"
    "digraph \"G\" { }"
  Parser.graph
  Lang_DOT_ToS.graph

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.statement"
    "{test:0}"
    "node [\"shape\"=\"circle\"]"
  Parser.statement
  Lang_DOT_ToS.statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.statement"
    "{test:0}"
    "\"a\" [\"label\"=\"a\"]"
  Parser.statement
  Lang_DOT_ToS.statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.statement"
    "{test:0}"
    "\"a\" -> \"b\" [\"label\"=\"edge\"]"
  Parser.statement
  Lang_DOT_ToS.statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.statement_list"
    "{test:0}"
    "{
  node [\"shape\"=\"circle\"];
  \"a\" [\"label\"=\"a\"];
  \"b\" [\"label\"=\"b\"];
  \"c\" [\"label\"=\"c\"];
  \"a\" -> \"b\" ;
  \"b\" -> \"c\";
  \"b\" -> \"b\";
}"
  Parser.statement_list
  Lang_DOT_ToS.statement_list

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.graph"
    "{test:0}"
    "digraph \"G\" {
  node [\"shape\"=\"circle\"];
  \"a\" [\"label\"=\"a\"];
  \"b\" [\"label\"=\"b\"];
  \"c\" [\"label\"=\"c\"];
  \"a\" -> \"b\" ;
  \"b\" -> \"c\";
  \"b\" -> \"b\";
}"
  Parser.graph
  Lang_DOT_ToS.graph

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.graph"
    "{test:0}"
    "digraph \"G\" {
  node [\"shape\"=\"circle\"]
  \"a\" [\"label\"=\"a\"]
  \"b\" [\"label\"=\"b\"]
  \"c\" [\"label\"=\"c\"]
  \"a\" -> \"b\"
  \"b\" -> \"c\"
  \"b\" -> \"b\"
}"
  Parser.graph
  Lang_DOT_ToS.graph

let _ =
  gen_parser_printer
    "GuaCaml.Lang_DOT_Opal.graph"
    "{test:0}"
    "digraph G {
  node [shape=circle]
  a [label=\"a\"]
  b [label=\"b\"]
  c [label=\"c\"]
  a -> b
  b -> c
  b -> b
}"
  Parser.graph
  Lang_DOT_ToS.graph
