(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2022 Joan Thibault (joan.thibault@irisa.fr)
 *
 * GuaCaml : Generic Unspecific Algorithmic in OCaml
 *
 * Lang_C_Opal_OUnit : Bare Syntax C Parser (written in Opal)
 *
 * === NOTES ===
 *
 * operator priority is not respected, see : https://fr.wikibooks.org/wiki/Programmation_C-C%2B%2B/Priorit%C3%A9s_des_op%C3%A9rateurs
 *
 *)

module Types = Lang_C_Types
module ToS = Lang_C_ToS
(* module ToPrettyS = Lang_C_ToPrettyS *)
open Types
open Opal
open Lang_C_Opal

(* Section. Parser *)
let _ = print_endline "# Section. Parser"

(* [MOVEME] *)
let gen_parser_printer
    (function_name:string)
    (comment:string)
    (input_string:string)
    (parser:'a Parser.t)
    (result_to_string:'a -> string) : unit =
  print_newline();
  let print_endline : string -> unit =
    let header = "["^function_name^"] "^comment^" : " in
    fun text -> print_endline(header^text)
  in
  print_endline ("input_string:"^(STools.ToS.string input_string));
  let char_stream = LazyStream.of_string input_string in
  let token_stream = stream_of_parser AtomicLexer.lexer char_stream in
  let paralexed_stream = stream_of_parser ParaLex.lexer token_stream in
  let paralexed_list = LazyStream.to_list paralexed_stream in
  let output = parser paralexed_list |> Tools.opmap fst in
  print_endline "printing result";
  print_endline ("output:"^(STools.ToS.option result_to_string output));
  ()

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ x = y; }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ int i; }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ x = y; int i; }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.expression"
    "{test:0}"
    "x = 0"
    Parser.expression
    Lang_C_ToS.expression

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.expression"
    "{test:1}"
    "x < 0"
    Parser.expression
    Lang_C_ToS.expression

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.expression"
    "{test:2}"
    "x++"
    Parser.expression
    Lang_C_ToS.expression

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.expression"
    "{test:2}"
    "x = 0; x < 10; x++"
    Parser.expression
    Lang_C_ToS.expression

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.for_statement"
    "{test:0}"
    "for(x = 0; x < 10; x++) { x = y;}"
    Parser.for_statement
    Lang_C_ToS.for_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ for(x = 0; x < 10; x++) { x = y;} }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.pointer_option"
    "{test:0}"
    "statemate_FH_DU( void );"
    Parser.pointer_option
    Lang_C_ToS.pointer_option

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.direct_declarator_left"
    "{test:0}"
    "statemate_FH_DU( void );"
    Parser.direct_declarator_left
    Lang_C_ToS.direct_declarator_left

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.type_specifier"
    "{test:0}"
    "void"
    Parser.type_specifier
    Lang_C_ToS.type_specifier

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.declaration_specifier"
    "{test:0}"
    "void"
    Parser.declaration_specifier
    Lang_C_ToS.declaration_specifier

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.declarator"
    "{test:0}"
    ""
    Parser.declarator
    Lang_C_ToS.declarator

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.declaration_specifier"
    "{test:0}"
    ""
    Parser.declaration_specifier
    Lang_C_ToS.declaration_specifier

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.pointer_option"
    "{test:0}"
    ""
    Parser.pointer_option
    Lang_C_ToS.pointer_option

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.abstract_declarator"
    "{test:0}"
    ""
    Parser.abstract_declarator
    Lang_C_ToS.abstract_declarator

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.parameter_declaration_right"
    "{test:0}"
    ""
    Parser.parameter_declaration_right
    Lang_C_ToS.parameter_declaration_right

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.parameter_declaration"
    "{test:0}"
    "void"
    Parser.parameter_declaration
    Lang_C_ToS.parameter_declaration

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.parameter_list"
    "{test:0}"
    "void"
    Parser.parameter_list
    Lang_C_ToS.parameter_list

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.parameter_type_list"
    "{test:0}"
    "void"
    Parser.parameter_type_list
    Lang_C_ToS.parameter_type_list

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.direct_declarator_right"
    "{test:0}"
    "( void );"
    Parser.direct_declarator_right
    Lang_C_ToS.direct_declarator_right

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.direct_declarator"
    "{test:0}"
    "statemate_FH_DU( void );"
    Parser.direct_declarator
    Lang_C_ToS.direct_declarator

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.declarator"
    "{test:0}"
    "statemate_FH_DU( void );"
    Parser.declarator
    Lang_C_ToS.declarator

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.translation_unit"
    "{test:0}"
    "void statemate_FH_DU( void );"
    Parser.translation_unit
    Lang_C_ToS.translation_unit

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.declaration_specifier"
    "{test:0}"
    "unsigned long var;"
    Parser.declaration_specifier
    Lang_C_ToS.declaration_specifier

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.declaration"
    "{test:0}"
    "unsigned long var;"
    Parser.declaration
    Lang_C_ToS.declaration

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ x = 0; { x = 0; } { { y = 0;} } }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ if(x){ x = 0;} }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ if(x) x = 0; }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  STools.ShiftS.tab_elem := "  ";
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:0}"
    "{ if ( ! f() ) x = 0; }"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.expression"
    "{test:0}"
    "!foo( val0, val1 )"
    Parser.expression
    Lang_C_ToS.expression

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.statement"
    "{test:0}"
    "if ( foo( val0, val1 ) ) x = 0;"
    Parser.statement
    Lang_C_ToS.statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.function_definition"
    "{test:1}"
    "int statemate_return( void);"
    Parser.function_definition
    Lang_C_ToS.function_definition

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:1}"
"{
  unsigned long int checksum = 0;
  int index;
  _Pragma( \"loopbound min 64 max 64\" )
  for ( index = 63 ; index >= 0 ; index-- )
    checksum += ( unsigned long ) ( statemate_bitlist[ index ] << index );
  return ( checksum != 32 );
}"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.translation_unit"
    "{test:1}"
"int statemate_return()
{
  unsigned long int checksum = 0;
  int index;
  _Pragma( \"loopbound min 64 max 64\" )
  for ( index = 63 ; index >= 0 ; index-- )
    checksum += ( unsigned long ) ( statemate_bitlist[ index ] << index );
  return ( checksum != 32 );
}"
    Parser.translation_unit
    Lang_C_ToS.translation_unit

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.translation_unit"
    "{test:2}"
"void _Pragma ( \"entrypoint\" ) statemate_main( void )
{
  statemate_FH_DU();
}"
    Parser.translation_unit
    Lang_C_ToS.translation_unit

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.statement"
    "{test:3}"
    "foo();"
    Parser.statement
    Lang_C_ToS.statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.statement"
    "{test:3}"
    "return foo();"
    Parser.statement
    Lang_C_ToS.statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.compound_statement"
    "{test:3}"
"{
  statemate_init();
  statemate_main();

  return statemate_return();
}"
    Parser.compound_statement
    Lang_C_ToS.compound_statement

let _ =
  gen_parser_printer
    "GuaCaml.Lang_C_Opal.translation_unit"
    "{test:3}"
"int main ( void )
{
  statemate_init();
  statemate_main();

  return statemate_return();
}"
    Parser.translation_unit
    Lang_C_ToS.translation_unit

let _ =
  if Array.length Sys.argv > 1
  then (
    print_endline "[GuaCaml.Lang_C_Opal._] {step:0}";
    let file_name = Sys.argv.(1) in
    print_endline "[GuaCaml.Lang_C_Opal._] {step:1}";
    (* List.iter (fun t -> print_endline (AtomicLexer.ToS.token t)) (lex_file file_name); *)
    print_endline "[GuaCaml.Lang_C_Opal._] {step:2}";
    List.iter (fun t -> print_endline (ParaLex.ToPrettyS.token_tree t)) (paralex_file file_name);
    print_endline "[GuaCaml.Lang_C_Opal._] {step:3}";
    print_newline();
    let string : string = Lang_C_ToPrettyS.translation_unit (parse_file file_name) (Some 0) in
    (* let string : string = Lang_C_ToShiftS.translation_unit (parse_file file_name) (Some 0) in *)
    if Array.length Sys.argv > 2
    then (
      let output_file = Sys.argv.(2) in
      STools.SUtils.output_string_to_file output_file string;
    )
    else (
      print_endline string
    );
    print_endline "[GuaCaml.Lang_C_Opal._] {step:4}";
    ()
  )
